<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arFilter = array(
    'ACTIVE'=>'Y',
    'IBLOCK_ID'=>$arIblockAccord['otzyiv']
);

if(!empty($_POST['clinic'])){
    $arFilter['PROPERTY_F_BIND'][] = (int)$_POST['clinic'];
}

if(!empty($_POST['spec'])){
    $arFilter['PROPERTY_F_BIND'][] = (int)$_POST['spec'];
}

$arNavParams = array(
    'nPageSize' => 5
);

$arNavParams['iNumPage'] = $_POST['page'] ? $_POST['page'] : 1;

$listElement = array();
$res = CIBlockElement::GetList(array('SORT'=>'ASC', 'created_date'=>'DESC'), $arFilter, false, $arNavParams, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT', 'PROPERTY_DATE_OF_RECALL'));
while($ar_res = $res->GetNext()){
    $listElement[] = $ar_res;
}

$content = '';

ob_start();
foreach ($listElement as $oneElem) {?>
    <div class="review-items">
        <div class="review-head">
            <span class="username"><?=$oneElem['NAME']?></span>
            <span class="data fr">
                <?
                if( date('d.m.Y', strtotime($oneElem['PROPERTY_DATE_OF_RECALL_VALUE'])) != '01.01.1970'){
                    echo date('d.m.Y', strtotime($oneElem['PROPERTY_DATE_OF_RECALL_VALUE']));
                }else{
                    echo date('d.m.Y', strtotime($oneElem['DATE_CREATE']));
                }
                ?>
            </span>
        </div>
        <div class="def-toggle">
            <p>
                <?=$oneElem['DETAIL_TEXT']?>
            </p>
        </div>
    </div>
<?}?>
<?
$res->nPageWindow = 3;
$NAV_STRING = $res->GetPageNavStringEx($navComponentObject, '', 'pagenavicust2', '');
if($NAV_STRING){
    echo $NAV_STRING;
}
$content.= ob_get_contents();
ob_end_clean();
echo json_encode(array('content'=>$content));?>