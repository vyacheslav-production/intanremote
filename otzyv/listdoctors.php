<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arFilter = array(
    'ACTIVE'=>'Y',
    'IBLOCK_ID'=>$arIblockAccord['blogers']
);

if(!empty($_POST['clinic'])){
    $arFilter['PROPERTY_F_CENTRE'] = (int)$_POST['clinic'];
}else{
    $arFilter['PROPERTY_F_BINDCITY'] = getCodeCity();
}

$listElement = array();
$res = CIBlockElement::GetList(array('SORT'=>'ASC', 'created_date'=>'DESC'), $arFilter, false, false, array('IBLOCK_ID', 'ID', '*'));
while($ar_res = $res->GetNext()){
    $listElement[] = $ar_res;
}

$doctors[] = ['id'=>0, 'text'=>'Врач не выбран'];

foreach ($listElement as $oneElem) {
    $doctors[] = [
        'id'=>$oneElem['ID'],
        'text'=>$oneElem['NAME']
    ];
}
echo json_encode(array('doctors'=>$doctors));?>