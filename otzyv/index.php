<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Отзывы об Интане");
$APPLICATION->SetPageProperty("description", "Интан - отзывы о сети стоматологических клиник");
$APPLICATION->SetTitle("Отзывы наших пациентов");
CModule::IncludeModule("iblock");

$nPage = $_REQUEST['PAGEN_1'] ? (int)$_REQUEST['PAGEN_1'] : 1;
?>

    <script>
        var reviewElem;
        var formView;
        $(function(){
            reviewElem = new ReviewList(<?=$nPage?>);
            reviewElem.init();
            formView = new FormView();
            formView.init();
        });
    </script>

    <section>

        <div class="wrap">
            <h1 class="center">Отзывы наших пациентов</h1>
            <div class="w65 review">

                <p>
                    Уважаемые посетители, в данном разделе вы можете оставить отзывы, предложения, жалобы, благодарности как в целом по работе центра, так и по работе персонала клиники. Будем рады увидеть отзывы об имплантации зубов, лечении кариеса и прочих заболеваний, которые проводились в нашем центре. Все ваши сообщения обязательно будут рассмотрены руководством центра и учтены в дальнейшей организации работы.
                </p>

                <div class="filter-docs">
                    <form action="<?=SITE_DIR?>otzyv/listreview.php" class="reqlist" method="POST" onsubmit="return false;">
                        <select name="" id="centers">
                            <option value="0">Все центры</option>
                            <?$res = CIBlockElement::GetList(array('SORT'=>'ASC'), array('IBLOCK_ID'=>$arIblockAccord['dental_centers'], 'ACTIVE'=>'Y'));
                            while($ar_res = $res->GetNext()){?>
                                <option <?=$_GET['clinic'] == $ar_res['ID'] ? 'selected' : ''?> value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                            <?}?>
                        </select>
                        <select name="" id="specs">
                            <option value="0">Все специализации</option>
                            <?$res = CIBlockElement::GetList(array('SORT'=>'ASC'), array('IBLOCK_ID'=>$arIblockAccord['listmedservices'], 'ACTIVE'=>'Y'));
                            while($ar_res = $res->GetNext()){?>
                                <option <?=$_GET['specs'] == $ar_res['ID'] ? 'selected' : ''?> value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                            <?}?>
                        </select>
                        <input type="reset" value="Сбросить фильтр">
                    </form>
                </div>

                <div class="listrev">
                </div>

            </div>

            <div class="w35">
                <div class="enroll-2-doctor right">
                    <div class="h2">Оставить отзыв</div>
                    <form class="sb_form l_feadback" method="post">
                        <input name="form_subm" value="<?=$arIblockAccord['otzyiv']?>" type="hidden" />
                        <input name="name" required type="text" placeholder="Имя">
                        <input name="otzyv_email" required  type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="email">
                        <select name="F_BIND[]" id="form_dent">
                            <option value="0">Центр не выбран</option>
                            <?$arFilter = ['IBLOCK_ID'=>$arIblockAccord['dental_centers'], 'ACTIVE'=>'Y'];
                            $arFilter = getFilArrayByCity($arFilter);
                            $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter);
                            while($ar_res = $res->GetNext()){?>
                                <option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                            <?}?>
                        </select>
                        <select name="F_BIND[]" id="form_doc">
                            <option value="">Врач не выбран</option>
                            <?$res = CIBlockElement::GetList(array('SORT'=>'ASC'), array('IBLOCK_ID'=>$arIblockAccord['blogers'], 'ACTIVE'=>'Y'));
                            while($ar_res = $res->GetNext()){?>
                                <option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                            <?}?>
                        </select>
                        <textarea name="DETAIL_TEXT" placeholder="Отзыв"></textarea>
                        <input type="submit" value="Отправить">
                    </form>
                </div>
            </div>

        </div>
    </section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
