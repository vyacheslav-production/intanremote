<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

if(isset($_POST['review'])){
    var_dump($_POST);
    if(is_array($_POST['F_BIND'])){
        $newFBind = array_diff($_POST['F_BIND'], array(''));
        $_POST['F_BIND'] = array_pop($newFBind);
    }

    var_dump($_POST);
    $el = new CIBlockElement();
    $data = array(
        'IBLOCK_ID' => 10,
        'NAME' => htmlspecialchars($_POST['name']),
        'PROPERTY_VALUES'=>array()
    );
    $black = array('iblock', 'review');
    $main = array('NAME', 'DETAIL_TEXT');
    foreach($_POST as $key => $value){
        if(in_array(mb_strtoupper($key), $main)) {
            $data[mb_strtoupper($key)] = $value;
            continue;
        }elseif(!in_array(mb_strtolower($key), $black)) {
            $data['PROPERTY_VALUES'][$key] = $value;
        }
    }


    $elemID = $el->Add($data);
    if(!empty($el->LAST_ERROR)) {
        $arResult['error'] = $el->LAST_ERROR;
    }else{

        $sendResult = CEvent::Send('REVIEW_FORM', SITE_ID, array(
                'AUTHOR'     => $_POST['name'],
                'AUTHOR_EMAIL'   => $_POST['otzyv_email'],
                'MESSAGE' => $_POST['otzyv_otzyv'],
            )
        );
    }
}

?>