<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Услуги");
header('HTTP/1.1 404 Not Found');
header('Status: 404 Not Found');
?>

    <section>
        <div class="wrap">
            <h1 data-error-code="404">Ошибка <span class="counter"></span></h1>
            <p>	<br>
                Извините! Страница, которую Вы ищете, не может быть найдена. <br>
                Возможно, запрашиваемая Вами страница была перенесена или удалена.
            </p>
            <a class="orange-link" href="<?=SITE_DIR?>">Вернуться на главную страницу</a>
        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>