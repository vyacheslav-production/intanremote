<!doctype html>
<html>
<head>
    <title>Intan</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <link  type="text/css" rel="stylesheet" href="/css/stylesheets/screen.css"  charset="utf-8">
    <link  type="text/css" rel="stylesheet" cssWatch="no"  href="/css/fonts/stylesheet.css">
    <link  type="text/css" rel="stylesheet" cssWatch="no"  href="/js/select2-4.0.0-beta.3/dist/css/select2.css"  charset="utf-8">
    <link  type="text/css" rel="stylesheet" cssWatch="no"  href="/js/swiper-slider/idangerous.swiper.css">
    <link  type="text/css" rel="stylesheet" cssWatch="no"  href="/js/swiper-scrollbar/lib/idangerous.swiper.scrollbar.css">
    <link  type="text/css" rel="stylesheet" cssWatch="no"  href="/js/jscrollpane/style/jquery.jscrollpane.css">
    <script type="text/javascript" src="/js/jquery-ui-1.10.4.custom/js/jquery-1.10.2.js"> </script>
    <script src="/js/fancybox-2.1.5/source/jquery.fancybox.js?v=2.1.5" type="text/javascript"  charset="utf-8"> </script>
    <script src="/js/jscrollpane/script/jquery.mousewheel.js" type="text/javascript"  charset="utf-8"> </script>
    <link   rel="stylesheet" type="text/css"  cssWatch="no"  href="/js/fancybox-2.1.5/source/jquery.fancybox.css">
    <script src="/js/fancybox-2.1.5/source/helpers/jquery.fancybox-media.js" type="text/javascript"  charset="utf-8"> </script>
    <script src="/js/swiper-slider/idangerous.swiper.js" type="text/javascript"  charset="utf-8"> </script>
    <script src="/js/swiper-scrollbar/lib/idangerous.swiper.scrollbar.js" type="text/javascript"  charset="utf-8"> </script>
    <script src="/js/jscrollpane/script/jquery.jscrollpane.js" type="text/javascript"  charset="utf-8"> </script>
    <script src="/js/jquery.mask.js" type="text/javascript"  charset="utf-8"> </script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="/js/js.js">	</script>
    <script type="text/javascript" src="/js/select2-4.0.0-beta.3/dist/js/select2.js">	</script>
    <!--[if lt IE 9]>
    <script  type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<style>

    body{
        font-family: "times" !important;
    }

    .header span{
        font-size: 23px;
    }
    table{
        width: 100%;
    }
    .header{
        text-align: right;
        margin-bottom: 30px;
        padding-top: 30px;
        border-bottom: 1px solid #bebebe;
        line-height: 80px;
    }
    #wrapper{
        max-width: 1000px;
        margin: auto;
    }
    table{
        font-size: 20px;
    }
    table p span{
        position: relative;
        background: white;
        padding-left: 24px;
        z-index: 33;
        padding-right: 10px;
        max-width: 80%;
        display: inline-block;
    }
    table p span.p{
        position: absolute;
        right: 0px;
        width: 100%;
        padding-right: 0;
        padding-left: 0px;
        display: block;
        max-width: 100%;
        height: 22px;
        text-align: right;
        z-index: 11;
        background: transparent;
        border-bottom: 1px solid #ebebeb;
    }
    b{
        position: relative;
        background: white;
        padding-left: 10px;
        z-index: 33;
    }
    table p{
        margin-top: 10px;
        margin-bottom: 10px;
        position: relative;
        font-size: 20px;
        line-height: 24px;
        font-weight: bold;
    }
    h4{
        line-height: 50px;
        font-size: 20px;
    }
    body{
        background:  #ffffff;
        padding-bottom: 30px;
    }
    .hr{
        height: 20px;
    }
    .total span{
        padding-left: 0;
        position:relative;
        top: 7px;
    }
    .total .p{
        padding-left: 0;
        position:absolute;
        top: auto;
    }
    .total p{
        font-weight: normal

    }
    .total b{
        font-size: 30px;
    }
    table .total p span.p {
        height: 27px;
    }
    .inf:before{
        content: "";
        margin-left: -33px;
        display: inline-block;
        width: 20px;
        margin-right: -4px;
    }
    .inf{
        margin-top: 24px;
        padding-top: 24px;
        color: #7b7b7b;
        font-size: 16px;
        padding-right: 36%;
        line-height: 22px;
        border-top: 1px solid #bebebe;
        padding-left: 40px;
    }
    h5{
        font-family: pf_din_text_comp_proregular;
        font-size:30px;
        padding-top: 25px;
        margin-bottom: 23px;
    }
</style>
<div id="wrapper">
    <div class="header">
        <img  style="float:left" src="/images/logo.png" alt="">
        <span>+7 (812) 777-02-02</span>
    </div>

    <div class="calc">
        <h5>Результат рссчета стоимости лечения *</h5>
        <table>
            <tr>
                <td>
                    <h4>Вид услуги</h4>
                    <p><span><?=implode(', ', $jscalc_typeServ)?></span> </p>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="hr"></div>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Количество зубов</h4>
                    <p><span><?=$jscalc_numTeeth?></span>  </p>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="hr"></div>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Параметры услуги</h4>
                    <?foreach($jscalc_paramServ as $oneParamServ):?>
                        <p class="price"><span class="p"><b><?=$oneParamServ[1]?> P</b></span> <span><?=$oneParamServ[0]?></span></p>
                    <?endforeach;?>

                </td>
            </tr>
            <tr>
                <td>
                    <div class="hr"></div>
                </td>
            </tr>
            <!--tr>
                <td>
                    <h4>Дополнитеьные параметры</h4>
                    <p class="price"><span class="p"><b>222 P</b></span> <span>Имплантация, Имплмплантация, Имплантация пантация под ключ </span></p>
                    <p class="price"><span class="p"><b>222 P</b></span> <span>Имплантация, Имплантация под ключ </span></p>
                    <p class="price"><span class="p"><b>222 P</b></span> <span>Имплантация, Имплантация пмплантация, Имплантация пмплантация, Имплантация пмплантация, Имплантация пмплантация, Имплантация пмплантация, Имплантация под ключ </span></p>
                </td>
            </tr-->
            <tr>
                <td>
                    <div class="hr"></div>
                </td>
            </tr>
            <tr class="total">
                <td>

                    <p class="price"><span class="p"><b><?=$jscalc_sumTotal?> P</b></span> <span>Общая стоимость</span></p>
                </td>
            </tr>
        </table>

        <p class="inf">
            * Указанные на сайте цены не являются публичной офертой.
            Определить точную стоимость лечения возможно только на приеме у врача.
            Цены на услуги могут отличаться от цен, указанных на сайте.
        </p>
    </div>
</div>
</body>
</body>
</html>