<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Калькулятор");

require_once($_SERVER['DOCUMENT_ROOT'].'/kalkulyator/print.php');
?>
    <section>
        <form action="">
            <div class="wrap">
                <h1>Цены на стоматологические услуги</h1>
                <a href="/stati/" class="back-to"><i></i><span>к списку статей</span></a>
            </div>
            <?
            $listSales = array();
            $res = CIBlockElement::GetList(
                array('RAND'=>'ASC'),
                array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listsale'], '!PROPERTY_F_SERVICE' => false
                )
            );
            while($ar_res = $res->GetNext()){
                $listSales[] = $ar_res;
            }
            if(!empty($listSales)){?>
                <div class="wrap">
                    <h2>Акции по услугам</h2>
                </div>
                <div class="b-slider m">
                    <div class="controls">
                        <span  class="prev"></span>
                        <span class="next"></span>
                    </div>
                    <div class="swiper-container" >
                        <div class="swiper-wrapper">
                            <?foreach($listSales as $oneSale){?>
                                <?$img = CFile::ResizeImageGet($oneSale["PREVIEW_PICTURE"], array('width'=>427, 'height'=>240), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
                                <a href="<?=$oneSale['DETAIL_PAGE_URL']?>" class="img swiper-slide" style="background-image: url(<?=$img['src']?>)"></a>
                            <?}?>
                        </div>
                    </div>
                    <div class="swiper-scrollbar"></div>
                </div>
            <?}?>
            <div class="wrap">
                <style>
                    .wrap{
                        white-space: nowrap
                    }
                    .wrap>*{
                        white-space: normal;
                    }
                </style>
                <div class="w65  ib">

                    <div class="text-item">
                        <p>
                            Калькулятор расчета стоимости поможет Вам узнать самостоятельно без осмотра врача ориентировачную стоимость лечения в вашем случае.
                            <br>
                            <br>
                            При расчете учитывается большинство факторов из-за чего получается достаточно высокая точность рассчётов (до какой степени это возможно без осмотра врачом).
                        </p>
                    </div>
                    <div id="new_calc">
                        <div class="nc_top">

                            <h5>Выберите услугу </h5>


                            <div class="calc_col1">
                                <div class="sel_wrap">
                                    <select id="ysluga" data-placeholder="Выберите категорию" >
                                        <option value="rst">Выберите категорию</option>
                                        <option id="lechenie1">Лечение</option>
                                        <option id="hiryrgiya1">Хирургия</option>
                                        <option id="implantasiya1">Имплантация</option>
                                        <option id="protezirovanie1">Протезирование</option>
                                        <option id="ortodontiya1">Ортодонтия</option>
                                        <option id="detskaya1">Детская стоматология</option>
                                    </select>
                                </div>
                                <div class="level2wrap">
                                    <div class="level2" id="lechenie">
                                        <div class="sel_wrap">
                                            <select data-placeholder="Выберите вид услуги" >
                                                <option id="">Выберите вид услуги</option>
                                                <option id="lechenie_kariesa2">Лечение кариеса</option>
                                                <option id="lechenie_pylpita2">Лечение пульпита</option>
                                                <!-- <option id="vosstanovlenie_zyba" value="5500">Восстановление зуба</option> -->
                                                <option id="restavrasiya_zyba2">Реставрация зуба</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="level2" id="hiryrgiya">
                                        <div class="sel_wrap">
                                            <select  data-placeholder="Выберите вид услуги" >
                                                <option id="">Выберите вид услуги</option>
                                                <option id="ydalenie2">Удаление</option>
                                                <option id="rezektsiya2">Резекция</option>
                                                <option id="sinys_lifting2">Синус лифтинг</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="level2" id="implantasiya">
                                        <div class="sel_wrap">
                                            <select   data-placeholder="Выберите вид услуги">
                                                <option id="">Выберите вид услуги</option>
                                                <option id="implantat_i_ystanovka">Имплантат и установка</option>
                                                <!-- <option id="implantatsiya_pod_kly4">Имплантация под ключ</option> -->
                                            </select>
                                        </div>
                                    </div>
                                    <div class="level2" id="protezirovanie">
                                        <div class="sel_wrap">
                                            <select id="tip_proteza"    data-placeholder="Выберите тип протеза">
                                                <option id="">Выберите тип протеза</option>
                                                <option id="semniy_protez">Съемный протез</option>
                                                <option id="nesemniy_protez">Несъемный протез</option>
                                            </select></div>
                                    </div>
                                    <div class="level2" id="ortodontiya">
                                        <div class="sel_wrap">
                                            <select   data-placeholder="Выберите вид услуги">
                                                <option id="">Выберите вид услуги</option>
                                                <option value="25490">Брекет-система металлическая (за одну челюсть)</option>
                                                <option value="40790">Брекет-система сапфировая (за одну челюсть)</option>
                                                <option value="185700">Брекет-система лингвальная</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="level2" id="detskaya">
                                        <div class="sel_wrap">
                                            <select   data-placeholder="Выберите вид услуги">
                                                <option id="">Выберите вид услуги</option>
                                                <option id="det_lechenie2">Лечение</option>
                                                <option id="det_ydal_zyba2">Удаление зуба</option>
                                                <option value="280" id="sereb_zuba">Серебрение зуба</option>
                                                <option id="det_germetiz2">Герметизация фиссур</option>
                                                <option  value="630">Удаление молочного зуба</option>
                                                <option id='hh' value="3120">Оперция уздечки губы или языка</option>
                                                <option id='hh' value="600">Иссечение капюшона</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="level3wrap selectr">
                                    <div id="semniy_protez3" class="level3">
                                        <div class="sel_wrap">
                                            <select   data-placeholder="Выберите вид лечения">
                                                <option id="">Выберите вид лечения</option>
                                                <option id="chastichniy">Частичный</option>
                                                <option id="polniy">Полный</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div id="det_lechenie" class="level3">
                                        <div class="sel_wrap">
                                            <select  data-placeholder="Выберите вид лечения" >
                                                <option id="">Выберите вид лечения</option>
                                                <option id="det_lechenie_kariesa">Кариеса</option>
                                                <option id="det_lechenie_pulpita">Пульпита</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="calc_tooth_summ">
                                    <h5>Количество зубов</h5>
                                    <select id="kol_zyb"   data-placeholder="1">
                                        <option value=""></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                        <option value="32">32</option>
                                    </select>
                                </div>
                                <div class="level3wrap">
                                    <div class="level3" id="lechenie_kariesa">
                                        <h5>Выберите параметры услуги</h5>
                                        <div class="calc_impl_list">
                                            <!-- <p><b class="right">от 1900</b><label><input type="radio" value="1900" name="impl_i_yst" />Кариес поверхностный</label></p> -->
                                            <p><b class="right">от 2000</b><label><input type="radio" value="2000" name="impl_i_yst" />Молочных зубов</label></p>
                                            <!-- <p><b class="right">от 2500</b><label><input type="radio" value="2500" name="impl_i_yst" />Кариес средний</label></p> -->
                                            <!-- <p><b class="right">от 3500</b><label><input type="radio" value="3500" name="impl_i_yst" />Кариес глубокий</label></p> -->
                                        </div>
                                    </div>
                                    <div class="level3" id="lechenie_pylpita">
                                        <h5>Выберите параметры услуги</h5>
                                        <div class="calc_impl_list"> <p>
                                                <b class="right">от 4500</b><label><input type="radio" value="4500" name="impl_i_yst" />До 2 каналов</label></p>
                                            <p><b class="right">от 6500</b><label><input type="radio" value="6500" name="impl_i_yst" />До 3 каналов</label></p>
                                            <p><b class="right">от 6500</b><label><input type="radio" value="6500" name="impl_i_yst" />Более 3 каналов</label></p>
                                        </div>
                                    </div>
                                    <div class="level3" id="restavrasiya_zyba">
                                        <h5>Выберите параметры услуги</h5>
                                        <div class="calc_impl_list">
                                            <p><b class="right">от 3500</b><label><input type="radio" value="3500" name="impl_i_yst" />Реставрация средняя</label></p>
                                            <!-- <p><b class="right">от 4000</b><label><input type="radio" value="4000" name="impl_i_yst" />Реставрация глубокая</label></p>
                                            <p><b class="right">от 2500</b><label><input type="radio" value="2500" name="impl_i_yst" />Скол пломбы</label></p>
                                            <p><b class="right">от 5000</b><label><input type="radio" value="5000" name="impl_i_yst" />Виниры из композитного материала</label></p> -->
                                        </div>
                                    </div>
                                    <div class="level3" id="ydalenie">
                                        <h5>Выберите параметры услуги</h5>
                                        <div class="calc_impl_list">
                                            <p><b class="right">от 1210</b><label><input type="radio" value="1210" name="impl_i_yst" />удаление зуба обычное</label></p>
                                            <p><b class="right">от 2500</b><label><input type="radio" value="2500" name="impl_i_yst" />удаление зуба сложное</label></p>
                                            <!-- <p><b class="right">от 4000</b><label><input type="radio" value="4000" name="impl_i_yst" />удаление восьмерки</label></p> -->
                                            <p><b class="right">от 800</b><label><input type="radio" value="800" name="impl_i_yst" />III степень подвижности</label></p>
                                            <p><b class="right">от 4000</b><label><input type="radio" value="4000" name="impl_i_yst" />удаление ретенированного зуба</label></p>
                                            <!-- <p><b class="right">от 2000</b><label><input type="radio" value="2000" name="impl_i_yst" />удаление имплантата (винтового)</label></p> -->
                                            <!-- <p><b class="right">от 3000</b><label><input type="radio" value="3000" name="impl_i_yst" />удаление имплантата (пластиночного)</label></p> -->
                                        </div>
                                    </div>
                                    <div class="level3" id="rezektsiya">
                                        <h5>Выберите параметры услуги</h5>
                                        <div class="calc_impl_list">
                                            <p><b class="right">от 4000</b><label><input type="radio" value="4000" name="impl_i_yst" />резекция верхушки корня</label></p>
                                        </div>
                                    </div>
                                    <div class="level3" id="sinys_lifting">
                                        <h5>Выберите параметры услуги</h5>
                                        <div class="calc_impl_list">
                                            <p><b class="right">от 8725</b><label><input type="radio" value="8725" name="impl_i_yst" />синус-лифтинг закрытый (без материалов)</label></p>
                                            <p><b class="right">от 21190</b><label><input type="radio" value="21190" name="impl_i_yst" />синус-лифтинг открытый (без материалов)</label></p>
                                        </div>
                                    </div>
                                    <div id="implantatsiya_pod_kly43" class="level3">
                                        <p class="forma_zapisi_h" style="margin:0;">Выберите стоматологические материалы</p>
                                        <div class="calc_impl_list">
                                            <!--  <p><b class="right">от 12500</b><label><input class="t-sgs" type="radio" value="12500" name="impl_i_yst" />"Имплантат конический MSI производитель Mirell(Израиль-Голландия) за единицу</label></p>

                                             <p><b class="right">от 12500</b><label><input class="t-sgs" type="radio" value="12500" name="impl_i_yst" />"Имплантат цилиндрический  MСI производитель Mirell(Израиль-Голландия) за единицу</label></p> -->

                                            <!--        <p><b class="right">от 15900</b><label><input class="t-sgs" type="radio" value="15900" name="impl_i_yst" />«Винтовой имплантат P1 Premium с покрытием BONITex®» производитель SGS Systems (Швейцария)</label></p>

                                                   <p><b class="right">от 15900</b><label><input class="t-sgs" type="radio" value="15900" name="impl_i_yst" />«Конический имплантат P7 Premium с покрытием BONITex®» производитель SGS Systems (Швейцария)</label></p> -->

                                            <p><b class="right">от 33000</b><label><input class="t-xive" type="radio" value="33000" name="impl_i_yst" />«XiVE» производитель Friadent Dentsply (Германия)</label></p>

                                            <p><b class="right">от 31000</b><label><input class="t-nobel_r" type="radio" value="31000" name="impl_i_yst" />«Nobel Replace Select» производитель Nobel Biocare (Швеция)</label></p>

                                            <p><b class="right">от 31000</b><label><input class="t-nobel_a" type="radio" value="31000" name="impl_i_yst" />«Nobel Groovy» производитель Nobel Biocare (Швеция)</label></p>

                                            <p><b class="right">от 35000</b><label><input class="t-nobel_a" type="radio" value="35000" name="impl_i_yst" />«Nobel Active» производитель Nobel Biocare (Швеция)</label></p>



                                            <p><b class="right">от 11900</b><label><input class="t-sgs" type="radio" value="11900" name="impl_i_yst" />«Swell» производитель ADIN (Израиль)</label></p>

                                            <p><b class="right">от 11900</b><label><input class="t-sgs" type="radio" value="11900" name="impl_i_yst" />«Touareg» производитель ADIN (Израиль)</label></p>
                                        </div>
                                        <!--  <div class="calc_former">
                                             <p><b class="right">от 1560</b><label><input type="checkbox" value="1560" />Формирователь десны</label></p>
                                         </div> -->
                                        <!--   <div class="calc_abat">
                                              <div class="float_sel_wrap" id="abat">
                                                  <select style="height:21px;width:167px;">
                                                      <option id="abat_titan">Абатмент из титана</option>
                                                      <option id="abat_zirkon">Абатмент из циркония</option>
                                                  </select>
                                              </div>
                                              <div class="float_sel_opt" id="abat_titan_r">
                                                  <p class="abt_sgs"><b class="right">от 4280</b><label><input type="radio" name="abat_titan" value="4280" />Абатмент из титана «SGS»</label></p>
                                                  <p class="abt_xive"><b class="right">от 9100</b><label><input type="radio" name="abat_titan" value="9100" />Абатмент из титана «Xive»</label></p>
                                                  <p class="abt_nobel_r"><b class="right">от 9500</b><label><input type="radio" name="abat_titan" value="9500" />Абатмент из титана «Nobel Replace»</label></p>
                                                  <p class="abt_nobel_a"><b class="right">от 10900</b><label><input type="radio" name="abat_titan" value="10900" />Абатмент из титана «Nobel Active»</label></p>
                                              </div>
                                              <div class="float_sel_opt" id="abat_zirkon_r">
                                                  <p class="abt_sgs"><b class="right">от 6700</b><label><input type="radio" name="abat_zirkon" value="6700" />Абатмент из циркония «SGS»</label></p>
                                                  <p class="abt_xive"><b class="right">от 9100</b><label><input type="radio" name="abat_zirkon" value="9100" />Абатмент из циркония «Xive»</label></p>
                                                  <p class="abt_nobel_r"><b class="right">от 9500</b><label><input type="radio" name="abat_zirkon" value="9500" />Абатмент из циркония «Nobel Replace»</label></p>
                                                  <p class="abt_nobel_a"><b class="right">от 10900</b><label><input type="radio" name="abat_zirkon" value="10900" />Абатмент из циркония «Nobel Active»</label></p>
                                              </div>
                                              <div class="clear"></div>
                                          </div> -->
                                        <!--  <div class="calc_koron_ch">
                                             <div class="float_sel_wrap">
                                                 <label><input type="checkbox" id="koronka" value="calc_koronka" />Коронка</label>
                                             </div>
                                             <div class="float_sel_opt" id="abat_titan_r1">
                                                 <p><b class="right">от 6300</b><label><input type="radio" name="calc_koronka" value="6300" />Коронка на основе каркаса из металла</label></p>
                                                 <p><b class="right">от 8570</b><label><input type="radio" name="calc_koronka" value="8570" />Коронка на основе каркаса из металла по CAD/CAM технологии</label></p>
                                             </div>
                                             <div class="float_sel_opt" id="abat_zirkon_r1">
                                                 <p><b class="right">от 15000</b><label><input type="radio" name="calc_koronka" value="15000" />Коронка на основе каркаса из диоксида циркония</label></p>
                                                 <p><b class="right">от 17000</b><label><input type="radio" name="calc_koronka" value="17000" />Коронка на основе каркаса из оксида алюминия</label></p>
                                             </div>
                                             <div class="clear"></div>
                                         </div> -->
                                    </div>




                                    <div id="nesemniy_protez3" class="level3">
                                        <h5>Выберите параметры услуги</h5>

                                        <div class="calc_impl_list">
                                            <p><b class="right">от 7650</b><label><input type="radio" value="7650" name="impl_i_yst" />Коронка на основе каркаса из металла</label></p>
                                            <p><b class="right">от 40500</b><label><input type="radio" value="40500" name="impl_i_yst" />Коронка на основе каркаса из драгоценного металла</label></p>
                                            <p><b class="right">от 18340</b><label><input type="radio" value="18340" name="impl_i_yst" />Коронка на основе каркаса из диоксида циркония</label></p>
                                            <!-- <p><b class="right">от 19500</b><label><input type="radio" value="19500" name="impl_i_yst" />Коронка на основе каркаса из оксида алюминия</label></p> -->
                                        </div>
                                    </div>
                                    <div class="level3" id="breket_metal">
                                        <h5>Выберите параметры услуги</h5>

                                        <div class="calc_impl_list">
                                            <p><b class="right">от 19550</b><label><input type="radio" value="19550" name="breket_metal" />Dentaurum (Германия)</label></p>
                                            <p><b class="right">от 22000</b><label><input type="radio" value="22000" name="breket_metal" />Ormco (США)</label></p>
                                            <p><b class="right">от 24000</b><label><input type="radio" value="24000" name="breket_metal" />Victory ЗМ Unitек (США)</label></p>
                                            <div style="margin: 15px 0 0 15px">
                                                <p class="left" style="margin:0 40px 0 0;"><label><input class="oneortwo1" type="radio" value="1" name="oneortwo" />Одна челюсть</label></p>
                                                <p class="left"><label><input class="oneortwo2" type="radio" value="2" name="oneortwo" />Две челюсти</label></p>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="level3" id="breket_keram">
                                        <h5>Выберите параметры услуги</h5>

                                        <div class="calc_impl_list">
                                            <p><b class="right">от 33000</b><label><input type="radio" value="33000" name="breket_keram" />Dentaurum (Германия)</label></p>
                                            <p><b class="right">от 37000</b><label><input type="radio" value="37000" name="breket_keram" />Ormco (США)</label></p>
                                            <p><b class="right">от 36000</b><label><input type="radio" value="36000" name="breket_keram" />Damon Clear (США)</label></p>
                                            <p><b class="right">от 39000</b><label><input type="radio" value="39000" name="breket_keram" />Clarity ЗМ Unitек (США)</label></p>
                                            <div style="margin: 15px 0 0 15px">
                                                <p class="left" style="margin:0 40px 0 0;"><label><input class="oneortwo1" type="radio" value="1" name="oneortwo" />Одна челюсть</label></p>
                                                <p class="left"><label><input class="oneortwo2" type="radio" value="2" name="oneortwo" />Две челюсти</label></p>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="det_ydal_zyba" class="level3">
                                        <h5>Выберите параметры услуги</h5>

                                        <div class="calc_impl_list">
                                            <p><b class="right">от 800</b><label><input type="radio" value="800" name="impl_i_yst" />Простое</label></p>
                                            <p><b class="right">от 1500</b><label><input type="radio" value="1500" name="impl_i_yst" />Сложное</label></p>
                                        </div>
                                    </div>
                                    <div id="det_germetiz" class="level3">
                                        <h5>Выберите параметры услуги</h5>

                                        <div class="calc_impl_list">
                                            <p><b class="right">от 720</b><label><input type="radio" value="720" name="impl_i_yst" />молочного зуба</label></p>
                                            <p><b class="right">от 980</b><label><input type="radio" value="980" name="impl_i_yst" />постоянного зуба</label></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="level4wrap">
                                    <div id="chastichniy4" class="level4">
                                        <h5>Выберите параметры услуги</h5>

                                        <div class="calc_impl_list">
                                            <p><b class="right">от 3740</b><label><input type="radio" value="3740" name="chast_protez" />Акриловый</label></p>
                                            <p><b class="right">от 23110</b><label><input type="radio" value="23110" name="chast_protez" />термопластичный Valplast</label></p>
                                            <p><b class="right">от 33990</b><label><input type="radio" value="33990" name="chast_protez" />гипоаллергенные безакриловый (акри-фри)</label></p>
                                            <p><b class="right">от 28325</b><label><input type="radio" value="28325" name="chast_protez" />бюгельный</label></p>
                                        </div>
                                    </div>
                                    <div id="polniy4" class="level4">
                                        <h5>Выберите параметры услуги</h5>

                                        <div class="calc_impl_list">
                                            <p><b class="right">от 16105</b><label><input type="radio" value="16105" name="pol_protez" />акриловый</label></p>
                                            <p><b class="right">от 36250</b><label><input type="radio" value="36250" name="pol_protez" />гипоаллергенные безакриловый (акри-фри)</label></p>
                                        </div>
                                    </div>
                                    <div id="det_lechenie_kariesa4" class="level4">
                                        <h5>Выберите параметры услуги</h5>
                                        <div class="calc_impl_list">
                                            <p><b class="right">от 2000</b><label><input type="radio" value="2000" name="impl_i_yst" />молочных зубов</label></p>
                                            <!-- <p><b class="right">от 3000</b><label><input type="radio" value="3000" name="impl_i_yst" />глубокий</label></p> -->
                                        </div>
                                    </div>
                                    <div id="det_lechenie_pulpita4" class="level4">
                                        <h5>Выберите параметры услуги</h5>

                                        <div class="calc_impl_list">
                                            <p><b class="right">от 4000</b><label><input type="radio" value="4000" name="impl_i_yst" />1-2 канала</label></p>
                                            <p><b class="right">от 5000</b><label><input type="radio" value="5000" name="impl_i_yst" />3 и более</label></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="calc_tot_price">
                            <h5>Общая стоимость  <div id="calc_tot_price_summ">от <span>0</span></div></h5>
                        </div>
                        <div class="text-item">
                            <p>
                                Данный расчёт стоимости лечения не является окончательным. Для точного расчёта необходимо проконсультироваться с лечащим врачом.  При записи на прием результаты рассчета будут высланы администратору выбранной клиники.
                            </p>
                            <div class="slide-toggles">
                                <div class="title active">
                                    <a href="#" class="orange-link">Записаться на прием</a>
                                </div>
                                <article>
                                    <form action="">
                                        <div class="enroll-2-doctor  ">
                                            <input required type="text" placeholder="Имя">
                                            <input required type="text"  placeholder="Телефон" title="Введите телефон вформате +7 (999) 999-99-99" placeholder="+7 (9__)-___-__-__" class="phone">
                                            <select name="" id="" class="time" data-placeholder="Время">
                                                <option value="">Сейчас</option>
                                                <option value="9-12">9-12</option>
                                                <option value="ыфвфывыв">ыфвфывыв</option>
                                            </select>
                                            <select name="" id="" class=""  data-placeholder="Выберите клинику">
                                                <option value="">Выберите кинику</option>
                                                <option value="2">Выберите кинику</option>
                                                <option value="2">Выберите кинику</option>
                                                <option value="3">Выберите кинику</option>
                                            </select>
                                            <input type="submit" value="Отправить">
                                        </div>
                                    </form>
                                </article>
                            </div>
                            <form target="ifrSendForm" id="print-or-pdf" method="post" action="/kalkulyator/">
                                <div class="hidden-input">
                                    <input type="hidden" id="cat-id-1" name="cat[]" value="">
                                    <input type="hidden" id="cat-id-2" name="cat[]" value="">
                                    <input type="hidden" id="cat-id-3" name="cat[]" value="">

                                    <input type="hidden" id="kz" name="kol_zyb" value="">

                                    <input type="hidden" id="usluga_name" name="usluga_name" value="">
                                    <input type="hidden" id="usluga_price" name="usluga_price">

                                    <input type="hidden" id="total_price" name="total_price">

                                    <input type="hidden" name="click_type" id="click_type" value="">
                                </div>
                                <div class="print-block">
                                    <a class="pdf" href="#pdf"><span>Скачать расчет стоимости в PDF</span></a>
                                    <a  class="prnt" href="#prnt"><span>Распечатать</span></a>
                                </div>
                            </form>
                            <iframe src="#" id="ifrSendForm" style="display: none"></iframe>
                            <script>
                                function setHIDDEN(){
                                    if($("select#ysluga :selected").length)
                                        $("#cat-id-1").val($("select#ysluga :selected").text());
                                    else
                                        $("#cat-id-1").val('');

                                    if($(".level2:visible select :selected").length){
                                        $("#cat-id-2").val( $(".level2:visible select :selected").text() );
                                    }else{
                                        $("#cat-id-2").val('');
                                    }

                                    if($(".selectr .level3:visible :selected").length)
                                        $("#cat-id-3").val($(".selectr .level3:visible :selected").text());
                                    else
                                        $("#cat-id-3").val('');


                                    if($(".calc_tooth_summ:visible").length)
                                        $('#kz').val($("select#kol_zyb").val());
                                    else
                                        $('#kz').val('');

                                    if($(".level3wrap:not(.selectr):visible").length){
                                        $("#usluga_name").val($(".level3wrap:not(.selectr) :checked ~ span").text());
                                        $("#usluga_price").val($(".level3wrap:not(.selectr) :checked")[0].value);
                                    }else{
                                        if($(".level4wrap:not(.selectr):visible").length){
                                            $("#usluga_name").val($(".level4wrap :checked ~ span").text());
                                            $("#usluga_price").val($(".level4wrap :checked")[0].value);
                                        }else{
                                            $("#usluga_name").val('');
                                            $("#usluga_price").val('');
                                        }
                                    }

                                    $("#total_price").val($("#calc_tot_price_summ span").text());


                                    return $("#print-or-pdf").serialize();
                                }
                            </script>
                        </div>
                        <a href="#" class="reset active" id="calc_reset"><span>Начать заново</span></a>

                    </div>

                    <script>
                        $(function () {


                            $("body").on('click', '.print-block a', function(e){
                                e.preventDefault();
                                setHIDDEN();
                                $('#click_type').val(this.hash.substr(1));
                                $('#print-or-pdf').submit();
                            })


                            //функция подсчёта суммы
                            var sum = function(){
                                var a = 0;
                                var b = 0;
                                for(var i = 0, j = $(".calc_col1 select:visible option:selected, .detskaya option:selected").length;i < j;i++){
                                    if (($(".calc_col1 option:selected").eq(i).val() != "") && !(isNaN($(".calc_col1 option:selected").eq(i).val())) && ($(".calc_col1 option:selected").eq(i).parent().is(":visible"))){
                                        a += parseInt($(".calc_col1 option:selected").eq(i).val())
                                    }
                                    if (($("#detskaya option:selected").eq(i).val() != "") && !(isNaN($("#detskaya option:selected").eq(i).val())) && ($("#detskaya option:selected").eq(i).parent().is(":visible"))){
                                        a += parseInt($("#detskaya option:selected").eq(i).val())
                                    }
                                }
                                if($(".calc_col1 input:checked:visible").length == 0){
                                    $(".calc_col1 input").attr('checked', false);
                                }

                                for(var i = 0, j = $(".calc_col1 input:visible:checked").length;i < j;i++){
                                    if (($(".calc_col1 input:checked").eq(i).val() != "") && ($(".calc_col1 input:checked").eq(i).val() != "1") && ($(".calc_col1 input:checked").eq(i).val() != "2") && ($(".calc_col1 input:checked").eq(i).prop('disabled', false)) && !(isNaN($(".calc_col1 input:checked").eq(i).val())) && ($(".calc_col1 input:checked").eq(i).is(":visible")) && ($(".calc_col1 input:checked").eq(i).closest(".level3").is(":visible") || $(".calc_col1 input:checked").eq(i).closest(".level4").is(":visible"))){
                                        a += parseInt($(".calc_col1 input:checked").eq(i).val())
                                    }
                                }


                                if(!$('#kol_zyb').is(':hidden')){
                                    a = a*(parseInt($('#kol_zyb option:selected').val()))
                                }else{
                                    if( $(".calc_col1 input:checked:visible").length > 0){
                                        a =  parseInt( $(".calc_col1 input:checked:visible")[0].value ) * parseInt( $('#kol_zyb option:selected').val() )
                                    }

                                }

                                if(isNaN(a ) ){
                                    a =  $(".calc_col1 input:checked:visible")[0].value
                                }

                                if($("#detskaya:visible").length){
                                    a = $('#kol_zyb option:selected').val()*($(".level3wrap:not(.selectr):visible  :checked, .level4wrap:visible  :checked").length>0?$(".level3wrap:not(.selectr):visible  :checked, .level4wrap:visible :checked")[0].value:1)

                                    if($(".level3wrap:not(.selectr):visible  :checked, .level4wrap:not(.selectr):visible  :checked").length == 0){
                                        a = a * $("#detskaya:visible :selected")[0].value
                                    }

                                }

                                if( $("#ortodontiya:visible").length ){
                                    a = $('#kol_zyb option:selected').val()*($("#ortodontiya:visible select").val())
                                }

                                return a;
                            }
                            var setDefault = function(){
                                $('.level2, .level3, .level4, .calc_tooth_summ, #calc_tot_price, #new_calc .text-item, .level3wrap:not(.selectr), .level4wrap').hide()
                                //скрывает самый посл уровань
                                $(".lal5").hide();
                            }


                            var serviceChange = function(){
                                $('.level2, .level3, .level4, .calc_tooth_summ, #calc_tot_price, #new_calc .text-item, .level3wrap:not(.selectr)').hide()

                                switch($('#ysluga option:selected').attr("id")){
                                    case "lechenie1":
                                        $('#lechenie').show();
                                        break;
                                    case "hiryrgiya1":
                                        $('#hiryrgiya').show();
                                        break;
                                    case "implantasiya1":
                                        $('#implantasiya').show();
                                        break;
                                    case "protezirovanie1":
                                        $('#protezirovanie').show();
                                        break;
                                    case "ortodontiya1":
                                        $('#ortodontiya').show();
                                        break;
                                    case "detskaya1":
                                        $('#detskaya').show();
                                        break;
                                }
                            }
                            var fsrsherovka = function(item, farsh){
                                if( farsh ){
                                    $(item).show()
                                }else{
                                    $(".calc_tooth_summ").show();
                                    $(item).hide()
                                }
                            }
                            var level2Change = function(_this,farsh){
                                var ID = $('.level2:visible option:selected').attr('id');
                                // если второй селект есть
                                if(farsh){}else{
                                    $(".calc_tooth_summ, .level3").hide()
                                    $("#kol_zyb").val("")
                                    $("#calc_tot_price, #new_calc .text-item").hide();

                                }

                                if( $('.level2').is(':visible') && ID !="" && ID != undefined) {
                                    // $(".level3wrap").show();

                                    switch($('.level2:visible option:selected').attr('id')){
                                        //лечение
                                        case "lechenie_kariesa2":
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #lechenie_kariesa', farsh)
                                            break;
                                        case "lechenie_pylpita2":
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #lechenie_pylpita', farsh)
                                            break;
                                        case "restavrasiya_zyba2":
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #restavrasiya_zyba', farsh)

                                            break;

                                        case "protezirovanie":
                                            $(".calc_tooth_summ").show();
                                            if(farsh)
                                                $("#calc_tot_price,  #new_calc .text-item").show();
                                            break;
                                        case "vosstanovlenie_zyba":
                                            $(".calc_tooth_summ").show();
                                            if(farsh)
                                                $("#calc_tot_price,  #new_calc .text-item").show();
                                            break;
                                        case "sereb_zuba":
                                            $(".calc_tooth_summ").show();
                                            if(farsh)
                                                $("#calc_tot_price,  #new_calc .text-item").show();
                                            break;

                                        //хирургия
                                        case "ydalenie2":
                                            // $('.level3wrap #ydalenie, .calc_tooth_summ').show()
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #ydalenie', farsh)

                                            break;
                                        case "rezektsiya2":
                                            // $('.level3wrap #rezektsiya, .calc_tooth_summ').show()
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #rezektsiya', farsh)
                                            break;
                                        case "sinys_lifting2":
                                            // $('.level3wrap #sinys_lifting,  .calc_tooth_summ').show()
                                            $(".calc_tooth_summ").show();
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #sinys_lifting', farsh)
                                            break;

                                        //имплантация
                                        case "implantat_i_ystanovka":
                                            // $('.level3wrap #implantatsiya_pod_kly43').show()
                                            $('.calc_former, .calc_abat, .calc_koron_ch').hide()
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #implantatsiya_pod_kly43', farsh)

                                            $('#implantatsiya_pod_kly4 input').prop("checked", false)
                                            break;
                                        case "implantatsiya_pod_kly4":
                                            if(farsh){
                                                $(".level3wrap:not(.selectr)").show();

                                                $('.level3wrap #implantatsiya_pod_kly43').show()
                                                $('.calc_former, .calc_koron_ch').show()
                                            }else{
                                                $(".calc_tooth_summ").show();
                                            }
                                            $("#abat_titan_r p, #abat_zirkon_r p").hide()
                                            $("#abat_titan_r input, #abat_zirkon_r input").prop("checked", false)
                                            switch($("#implantatsiya_pod_kly43 input:visible, #implantatsiya_pod_kly43 select:visible").first().attr('class')){
                                                case "t-sgs":
                                                    $('.calc_abat, .abt_sgs').show()
                                                    break;
                                                case "t-xive":
                                                    $('.calc_abat, .abt_xive').show()
                                                    break;
                                                case "t-nobel_r":
                                                    $('.calc_abat, .abt_nobel_r').show()
                                                    break;
                                                case "t-nobel_a":
                                                    $('.calc_abat, .abt_nobel_a').show()
                                                    break;
                                            }
                                            switch($('.level3:visible #abat option:selected').attr('id')){
                                                case "abat_titan":
                                                    $("#abat_zirkon_r input").prop("checked", false)
                                                    $('#abat_titan_r, #abat_titan_r1').show()
                                                    $('#abat_zirkon_r, #abat_zirkon_r1').hide()
                                                    break;
                                                case "abat_zirkon":
                                                    $("#abat_titan_r input").prop("checked", false)
                                                    $('#abat_zirkon_r, #abat_zirkon_r1').show()
                                                    $('#abat_titan_r, #abat_titan_r1').hide()
                                                    break;
                                            }
                                            switch($('.level3:visible #koronka').is(':checked')){
                                                case true:
                                                    $("#abat_titan_r1 input[name='calc_koronka'], #abat_zirkon_r1 input[name='calc_koronka']").removeAttr("disabled");
                                                    $(".lal5").show()
                                                    break;
                                                case false:
                                                    $("#abat_titan_r1 input[name='calc_koronka'], #abat_zirkon_r1 input[name='calc_koronka']").prop("checked", false).attr("disabled", "disabled");
                                                    $(".lal5").hide()
                                                    break;
                                            }
                                            break;

                                        //протезитрование
                                        case "semniy_protez":
                                            $('.level3wrap #semniy_protez3').show()
                                            if( $('#semniy_protez3 option:selected').attr('id') == ""){
                                                $(".level4wrap, .level4").hide();
                                            }else{
                                                $(".level4wrap").show();
                                                switch($('#semniy_protez3 option:selected').attr('id')){
                                                    case "chastichniy":
                                                        $(".calc_tooth_summ, .level4").hide();

                                                        if(!farsh){
                                                            $('#chastichniy4').show()
                                                        }
                                                        break;
                                                    case "polniy":
                                                        $(".calc_tooth_summ, .level4").hide();
                                                        if(!farsh){
                                                            $('#polniy4').show()
                                                        }
                                                        break;
                                                }
                                            }

                                            break;
                                        case "nesemniy_protez":
                                            $(".calc_tooth_summ").show();
                                            $('.level4,  .level4wrap').hide()
                                            // if(!$("#kol_zyb")[0].disabled){$(".calc_tooth_summ").show().find("select")};
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #nesemniy_protez3', farsh)
                                            break;
                                        //ортодонтия
                                        case "breket_metal1":
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #breket_metal', farsh)
                                            break;
                                        case "breket_keram1":
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #breket_keram', farsh)
                                            break;

                                        //детская стоматология
                                        case "det_lechenie2":

                                            $('#det_lechenie').show()
                                            // $(' #calc_tot_price, #new_calc .text-item').hide()
                                            $(".calc_tooth_summ").hide();
                                            $('.level4wrap, .level4').hide()
                                            switch($('#det_lechenie option:selected').attr('id')){
                                                case "det_lechenie_kariesa":
                                                    $(".calc_tooth_summ").show();
                                                    if( farsh ){
                                                        $('.level4wrap, .level4wrap #det_lechenie_kariesa4').show()
                                                    }
                                                    break;
                                                case "det_lechenie_pulpita":
                                                    $(".calc_tooth_summ").show();
                                                    if( farsh ){
                                                        $('.level4wrap, .level4wrap  #det_lechenie_pulpita4').show()
                                                    }
                                                    break;
                                            }
                                            break;
                                        case "det_ydal_zyba2":
                                            $(".calc_tooth_summ").show();
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #det_ydal_zyba', farsh)
                                            break;
                                        case "det_germetiz2":
                                            $(".calc_tooth_summ").show();
                                            fsrsherovka('.level3wrap:not(.selectr), .level3wrap #det_germetiz', farsh)
                                            break;
                                        default:
                                            $(".calc_tooth_summ").show();
                                            if(farsh){
                                                $("#calc_tot_price,  #new_calc .text-item").show();
                                                $("#calc_tot_price_summ span").eq(0).text(sum());
                                            }
                                            break;
                                    }
                                    if(farsh){
                                        $("#calc_tot_price_summ span").eq(0).text(sum());
                                    }


                                }else{
                                    // если второй пустой или  его просто нет
                                    if( ID ==""|| ID  == undefined ){
                                        console.log("пустой")
                                    }else{
                                        console.log("второго нет")
                                    }
                                    $(".calc_tooth_summ, .level3").hide()
                                    if(ID  == undefined ||  $('.level2:visible option:selected').attr('value') ){
                                        $(".calc_tooth_summ").show();
                                        $("#calc_tot_price,  #new_calc .text-item").show();
                                        $("#calc_tot_price_summ span").eq(0).text(sum());
                                    }
                                }
                            }
                            $('[type="radio"], [type="checkbox"]').each(function(){
                                var self = $(this);
                                var text = $.trim(this.parentNode.innerText);
                                self.after("<ins></ins><span>"+text+"</span>")
                            });
                            setDefault();
                            $('#calc_reset.active').click(function(e){
                                e.preventDefault();
                                $(window).scrollTop($(".nc_top").offset().top);
                                $("select").val("").change();
                                $(".calc_tooth_summ").hide()
                                // window.location.reload()
                            });
                            // $("select").change(function(){
                            // 	console.log($('#detskaya:visible option:selected').attr("id"))
                            // 	if(($('#detskaya:visible option:selected').attr("id") == "hh") ){
                            // 		$(".calc_tooth_summ").hide()
                            // 	}
                            // })
                            $('#ysluga').change(function(){
                                serviceChange();
                            });
                            $("#kol_zyb").change(function(){
                                if( this.value== "" ){
                                    $(".level3wrap:not(.selectr), #calc_tot_price,   #new_calc .text-item,  .level4wrap").hide();
                                }else{
                                    level2Change(this, "farsh");
                                }
                            });
                            $("#implantatsiya_pod_kly43 select, input").change(function(){
                                switch($('.level3:visible #abat option:selected').attr('id')){
                                    case "abat_titan":
                                        $("#abat_zirkon_r input").prop("checked", false)
                                        // $('#abat_titan_r, #abat_titan_r1').show()
                                        // $('#abat_zirkon_r, #abat_zirkon_r1').hide()
                                        $(".lal5").hide()

                                        break;
                                    case "abat_zirkon":
                                        $("#abat_titan_r input").prop("checked", false)
                                        // $('#abat_zirkon_r, #abat_zirkon_r1').show()
                                        // $('#abat_titan_r, #abat_titan_r1').hide()
                                        $(".lal5").hide()
                                        break;
                                }
                                switch($('.level3:visible #koronka').is(':checked')){
                                    case true:
                                        $("#abat_titan_r1 input[name='calc_koronka'], #abat_zirkon_r1 input[name='calc_koronka']").removeAttr("disabled");
                                        $(".lal5").show()
                                        break;
                                    case false:
                                        $("#abat_titan_r1 input[name='calc_koronka'], #abat_zirkon_r1 input[name='calc_koronka']").prop("checked", false).attr("disabled", "disabled");
                                        $(".lal5").hide()
                                        break;
                                }

                                $('.level3:visible .calc_impl_list input').click(function(){
                                    //$("#abat_titan_r p, #abat_zirkon_r p").hide()
                                    $("#abat_titan_r p, #abat_zirkon_r p").hide()
                                    $("#abat_titan_r input, #abat_zirkon_r input").prop("checked", false)
                                    switch($(this).attr('class')){
                                        case "t-sgs":
                                            $('.calc_abat, .abt_sgs').show()
                                            break;
                                        case "t-xive":
                                            $('.calc_abat, .abt_xive').show()
                                            break;
                                        case "t-nobel_r":
                                            $('.calc_abat, .abt_nobel_r').show()
                                            break;
                                        case "t-nobel_a":
                                            $('.calc_abat, .abt_nobel_a').show()
                                            break;
                                    }
                                });
                            })

                            $("body").on("change", ".calc_impl_list input:visible, #implantatsiya_pod_kly43 input:visible, #implantatsiya_pod_kly43 select:visible", function(){

                                $("#calc_tot_price,  #new_calc .text-item").show();
                                $("#calc_tot_price_summ span").eq(0).text(sum());
                            })

                            $('.level3wrap.selectr select, .level2 select').change(function(){

                                $(".calc_tooth_summ select").val("").change();
                                level2Change(this);
                                if(($('#detskaya:visible option:selected').attr("id") == "hh") ){
                                    $(".calc_tooth_summ").hide();
                                    $("#calc_tot_price span").text($('#detskaya:visible option:selected').val())
                                    $("#calc_tot_price,  #new_calc .text-item").show();
                                }
                            });
                        });
                    </script>

                </div>
                <?
                //список услуг
                $listServ = array();
                $res = CIBlockElement::GetList(array(), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listmedservices']), false, false, array('ID', 'CODE', 'NAME', 'DETAIL_PAGE_URL'));
                while($ar_res = $res->GetNext()){
                    $listServ[] = $ar_res;
                }
                ?>
                <div class="w35 ib">
                    <div class="menu-blok notitle">
                        <ul>
                            <?foreach($listServ as $keyServ=>$oneServ){?>
                                <li><a class="<?=$oneServ['CODE']==$_REQUEST['ELEMENT_CODE'] ? 'active' : ''?>" href="/stomatologiya-ceny/<?=$oneServ['CODE']?>/"><span><?=$oneServ['NAME']?></span></a></li>
                            <?}?>
                        </ul>
                    </div>
                    <a  href="/lechenie-v-kredit/" class="right-link credit"><span>Лечение в кредит</span></a>
                </div>
            </div>

    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>