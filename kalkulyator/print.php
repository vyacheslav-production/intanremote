<?php
if(!empty($_POST['click_type'])){

    if($_POST['click_type'] == 'pdf'){
        require_once($_SERVER['DOCUMENT_ROOT']."/dompdf/lib/dompdf_config.inc.php");

        $dompdf = new DOMPDF();

        extract($_POST, EXTR_PREFIX_ALL, "jscalc_");

        ob_start();
        include_once($_SERVER['DOCUMENT_ROOT'].'/kalkulyator/calc-print-page-1.html');
        $html = ob_get_clean();
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');

//$dompdf->set_paper('A4', 'landscape');
        $dompdf->load_html($html);
//$dompdf->load_html_file($_SERVER['DOCUMENT_ROOT'].'/test/calc-print-page.html');
        $dompdf->render();

        $dompdf->stream("hello.pdf");
    }
}
