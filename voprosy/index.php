<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "вопросы о стоматологии, FAQ о стоматологии, стоматология");
$APPLICATION->SetPageProperty("description", "Интан отвечает на вопросы о стоматологии");
$APPLICATION->SetTitle("Часто задаваемые вопросы о стоматологии");

$strSearch = $_GET['q'] ? $_GET['q'] : '';
$nPage = $_REQUEST['PAGEN_1'] ? (int)$_REQUEST['PAGEN_1'] : 1;
?>
    <section>

        <div class="wrap">
            <h1>Вопрос - ответ</h1>
            <div class="w65 faq">

                <div class="filter-docs search">
                    <form action="/voprosy/" class="reqlist" method="GET">
                        <input name="q" type="text" placeholder="Введите часть слова или слово целиком" value="<?=$strSearch ? $strSearch : ''?>">
                        <input type="submit" value="Найти">

                    </form>
                </div>
                <?include_once($_SERVER['DOCUMENT_ROOT'].'/voprosy/listfaq.php')?>

            </div>

            <div class="w35">
                <div class="enroll-2-doctor right">
                    <div class="h2">Задать вопрос</div>
                    <form action="" method="post">
                        <input name="form_subm" value="<?=$arIblockAccord['listfaq']?>" type="hidden" />
                        <input name="F_NAME" required type="text" placeholder="Имя">
                        <input name="F_EMAIL" required title="example@mail.ru" type="text" pattern="[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,4}"  placeholder="Email">
                        <select name="F_CATEGORY" id="" data-placeholder="Тема вопроса">
                            <option value=""></option>
                            <?$res = CIBlockElement::GetList(array('SORT'=>'ASC'), array('IBLOCK_ID'=>$arIblockAccord['listmedservices'], 'ACTIVE'=>'Y'));
                            while($ar_res = $res->GetNext()){?>
                                <option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                            <?}?>
                        </select>
                        <textarea name="name" required placeholder="Вопрос"></textarea>
                        <input type="submit" value="Отправить">
                    </form>
                </div>
            </div>

        </div>
    </section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>