<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arFilter = array(
    'ACTIVE'=>'Y',
    'IBLOCK_ID'=>$arIblockAccord['listfaq'],
    '!DETAIL_TEXT' => false
);

if(!empty($strSearch)){
    $arFilter[] = array(
        'LOGIC' => 'OR',
        array('NAME' => '%'.$strSearch.'%'),
        array('DETAIL_TEXT' => '%'.$strSearch.'%'),
    );
}

if(!empty($_POST['spec'])){
    $arFilter['PROPERTY_F_CATEGORY'] = (int)$_POST['spec'];
}

$arNavParams = array(
    'nPageSize' => 20
);

$arNavParams['iNumPage'] = $nPage ? $nPage : 1;

$listElement = array();
$res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter, false, $arNavParams, array('IBLOCK_ID', 'ID', '*', 'PROPERTY_bloger_position', 'PROPERTY_F_CENTRE'));
while($ar_res = $res->GetNext()){
    $listElement[] = $ar_res;
}

//подсчитаем общее кол-во элементов, удовлетворяющих запросу
$cntElem = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter, array(), false, array('IBLOCK_ID', 'ID', '*', 'PROPERTY_bloger_position', 'PROPERTY_F_CENTRE'));

$strElem = '';
foreach ($listElement as $oneElem) {
        $strElem .= '<div class="faq-item">';
        //$strElem .=    '<a href="#" class="def-item-item">'.$oneElem['NAME'].'</a>';//$oneElem['DETAIL_PAGE_URL']
        $strElem .= '<a href="/voprosy/' . $oneElem['ID'] . '/" class="def-item-item">' . $oneElem['NAME'] . '</a>';//$oneElem['DETAIL_PAGE_URL']
        //$strElem .= '<b>'.$oneElem['NAME'].'</b>';
        $strElem .= '<p><span>';
        if (iconv_strlen($oneElem['DETAIL_TEXT']) > 90) {
            $limit = mb_substr($oneElem['DETAIL_TEXT'], 0, 90);
            $strElem .= mb_substr($limit, 0, mb_strrpos($limit, ' ')) . "...";
        }
        else
            $strElem .= $oneElem['DETAIL_TEXT'];
        //$strElem .=        $oneElem['DETAIL_TEXT'];
        $strElem .= '</span></p></div>';
}


$res->nPageWindow = 3;
$NAV_STRING = $res->GetPageNavStringEx($navComponentObject, '/voprosy/', 'pagenavicust_search', '');
if($NAV_STRING){
    $strElem .= $NAV_STRING;
}
?>
<div class="def-counter">
    Ответов: <span><?=$cntElem?></span>
</div>
<div class="def-toggle">
    <?=$strElem?>
</div>