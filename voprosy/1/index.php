<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Что такое тетрациклиновые зубы?");
?>

<aside class="left_sidebar">
    <?
    $APPLICATION->IncludeComponent("bitrix:menu", "tree1", Array(
        "ROOT_MENU_TYPE" => "top", // Тип меню для первого уровня
        "MENU_CACHE_TYPE" => "N", // Тип кеширования
        "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
        "MAX_LEVEL" => "2", // Уровень вложенности меню
        "CHILD_MENU_TYPE" => "podmenu", // Тип меню для остальных уровней
        "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N", // Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
            ), false
    );
    ?>
    <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "inc_banners",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
false
);?>

</aside>
<div class="content_part">

    <section>
        <h1 class="main_h">Вопрос-ответ</h1>
        <div class="faq_wrap">
            <div class="back_to_list">
                <span class="left_arrow"></span>
                <a href="/voprosy/">Вернуться к списку</a>
            </div>
            <div class="faq_item">
                <div class="faq_q"><h2><span class="faq_quot left">&laquo;</span>Что такое тетрациклиновые зубы?<span class="faq_quot right">&raquo;</span></h2></div>
                <div class="faq_a">
                    <div class="faq_a_ico"></div>
                    <div class="faq_a_text">
                        Зубы, цвет которых изменился в результате приема тетрациклина в период формирования и минерализации тканей зубов. Зубы приобретают желтый, коричневый, либо сероватый цвет.
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="clear"></div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>