<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интан предлагает лечение зубов в кредит");
$APPLICATION->SetTitle("Заявка на лечение в кредит");
?>

    <section>
        <div class="wrap">

            <h1>Лечение в кредит</h1>
            <a href="/<?=getCodeCity()?>/stomatologiya-ceny/" class="back-to"><i></i><span>к списку цен</span></a>

            <div class="w65 text ib">
                  <?
                    $listSales = array();
                    $res = CIBlockElement::GetList(array('RAND'=>'ASC'),
                        array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listsale'])
                    );
                    while($ar_res = $res->GetNext()){
                        $listSales[] = $ar_res;
                    }
                  ?>
                  <?if(!empty($listSales)){?>
                    <div class="wrap">
                        <h1></h1>
                        <h2>Акции</h2>
                    </div>
                    <div class="b-slider mini">
                        <div class="controls">
                            <span  class="prev"></span>
                            <span class="next"></span>
                        </div>
                        <div class="swiper-container" >
                            <div class="swiper-wrapper">
                                <?foreach($listSales as $oneSale){?>
                                    <?$img = CFile::ResizeImageGet($oneSale["PREVIEW_PICTURE"], array('width'=>427, 'height'=>240), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
                                    <a href="<?=$oneSale['DETAIL_PAGE_URL']?>" class="img swiper-slide" style="background-image: url(<?=$img['src']?>)"></a>
                                <?}?>
                            </div>
                        </div>
                        <div class="swiper-scrollbar"></div>
                    </div>
                <?}?>
                <div class="text-item ">
                    <h2>
                        Преимущества
                    </h2>
                    <ul>
                        <li>Переплата от 12% в год</li>
                        <li>Без комиссий за предоставление и пользование кредитом</li>
                        <li>Без справок о доходах и поручителей, при наличии одного документа</li>
                        <li>Оформление заявки на кредит в медицинском Центре </li>
                        <li>Денежные средства поступают на расчетный счет медицинского учреждения</li>
                    </ul>
                    <h2>
                        Условия
                    </h2>
                    <ul>
                        <li>Максимальная сумма кредита 300 тыс. руб.  </li>
                        <li>Срок кредитования до 3-х лет</li>
                    </ul>
                    <h2>
                        Банки-партнеры
                    </h2>
                    <blockquote style="margin-bottom: 55px">
                        <!-- можно юзать спаны, если ссылки не нужны -->
                        <span href="#" style="background-image: url(/images/afb.png);">
                            <h3>Альфа-банк</h3>
                        </span>
                        <!-- можно юзать спаны, если ссылки не нужны -->
                        <span href="#"  style="background-image: url(/images/hcrd.png);">
                            <h3>Home Credit Bank</h3>
                        </span>
                        <!-- можно юзать спаны, если ссылки не нужны -->

                        <!-- можно юзать спаны, если ссылки не нужны -->
                    </blockquote>

                </div>
            </div>
            <div class="w35 ib">
                <div class="enroll-2-doctor consultant ">
                    <div class="h2">Консультация по кредитам</div>
                    <p>
                        Антошко Татьяна Сергеевна <br>
                        +7 (812) 942-66-13<br>
                        (ежедневно: с 11.00 до 20.00)<br>
                    </p>
                    <div class="h2">Заявка на лечение в кредит</div>
                    <form action="" method="post" onsubmit="yaCounter9957124.reachGoal('credit'); ga('send', 'event', 'form', 'form_credit'); kCall2(); return true;">
                        <input name="form_subm" value="<?=$arIblockAccord['credit_calc']?>" type="hidden" />
                        <input name="name" required type="text" placeholder="Имя">
                        <input name="F_PHONE" required type="text"  placeholder="Телефон" title="Введите телефон вформате +7 (999) 999-99-99" placeholder="+7 (9__)-___-__-__" class="phone">
                        <select name="F_TIME" id="" class="time">
                            <option value="">Удобное время для звонка</option>
                            <option value="9-12">9-12</option>
                            <option value="13-16">13-16</option>
                            <option value="16-19">16-19</option>
                        </select>
                        <textarea name="F_COMMENT" placeholder="Комментарий"></textarea>
                        <input type="submit" value="Отправить">
                    </form>
                </div>
            </div>
        </div>
      
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>

