<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetPageProperty("keywords", "стоматология cанкт петербург петербург, стоматология СПб, стоматологические клиники, интан центр имплантация санкт петербург");
$APPLICATION->SetPageProperty("description", "Интан - стоматологии в Петербурге. Стоматология: низкие цены и квалифицированные специалисты. Стоматология - весь спектр услуг: лечение, имплантация и протезирование, детская стоматология.");
$APPLICATION->SetPageProperty("title", "Стоматология Спб: клиника семейной стоматологии Санкт-Петербург. Стоматологическая клиника Интан.");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Стоматологическая клиника Интан в Санкт-Петербурге");
?>


<section>
	<div class="wrap">
		

		<div class="wrap-text">
	

		<!-- заголовки  -->
			<h1>Заголовок H1</h1>
			<h2>Заголовок H2</h2>
			<h3>Заголовок H3</h3>
			<h4>Заголовок H4</h4>
			<h5>Заголовок H5</h5>

			<h1 class="center">Заголовок H1 по центру</h1>
			<h2 class="center">Заголовок H2 по центру</h2>
			<h3 class="center">Заголовок H3 по центру</h3>
			<h4 class="center">Заголовок H4 по центру</h4>
			<h5 class="center">Заголовок H5 по центру</h5>

		
			

			<h1>Заголовок H1</h1>

 			<div class="b-slider mini">
				<h2>Заголовок H2</h2>
				<div class="controls">
					<span  class="prev"></span>
					<span class="next"></span>
				</div>
				<div class="swiper-container" >
					<div class="swiper-wrapper">
						<a href="/aksii/69350/" class="img swiper-slide" style="background-image: url(/upload/iblock/946/9466f407fecaca40fadcc4fb3c9c7aa8.png)"></a>
						<a href="/aksii/69350/" class="img swiper-slide" style="background-image: url(/upload/iblock/946/9466f407fecaca40fadcc4fb3c9c7aa8.png)"></a>
						<a href="/aksii/69350/" class="img swiper-slide" style="background-image: url(/upload/iblock/946/9466f407fecaca40fadcc4fb3c9c7aa8.png)"></a>
					</div>
				</div>
				<div class="swiper-scrollbar"></div>
            </div>



			<h2>Заголовок H2</h2>

			<div class="default-text">
				<img class="big-img" src="https://pp.vk.me/c308528/v308528930/96e6/S-DEOprlFfg.jpg" alt="">
			</div>
			<div class="default-text text-columns">
				<p>
					<b>Lorem ipsum dolor sit amet, <i>consectetur adipisicing elit.</i> Fugiat commodi enim ab reiciendis fugit dolor,</b> asperiores beatae error omnis natus voluptatem eius voluptas,  <i>similique optio esse dolores quas pariatur, consequatur? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error</i> omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat 
				</p>
			</div>
			

			<div class="default-text">
				<img class="big-img" src="https://pp.vk.me/c308528/v308528930/96e6/S-DEOprlFfg.jpg" alt="">
			</div>
			<div class="default-text text-columns">
				<p>
					<!-- Нужен спан. ели вставляется картика -->
					
					<span class="floatimg left">
						<span class='h3'>
							фото 1
						</span>
						<span>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur?
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur?
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur?
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur?
						</span>
					</span>

					<span class="floatimg right">
						<span class='h3'>
							фото 2
						</span>
						<span>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur?
						</span>

					</span>
				</p>
				<div class="clearfix"></div>
			</div>



		



			<!-- Обычный текст в две колонки -->
			<div class="default-text text-columns">
				<p>
					<!-- Нужен спан. ели вставляется картика -->
					
					<span class="floatimg left">
						<img src="/images/video_tmp.png" alt="" >
						<span class='h3'>
							фото 3
						</span>
						<span>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur?
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur?
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur?
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur?
						</span>
					</span>

					<span class="floatimg right">
						<img src="/images/video_tmp.png" alt="" >
						<span class='h3'>
							фото 4
						</span>
						<span>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat commodi enim ab reiciendis fugit dolor, asperiores beatae error omnis natus voluptatem eius voluptas, similique optio esse dolores quas pariatur, consequatur?
						</span>

					</span>
				</p>
				<div class="clearfix"></div>
			</div>




			<!-- слайдер -->
			<div class="slider">

				<h5>Этапы имплантации</h5>

				<div class="mySwiper">
					<div class="mySwiper-container swiper-container" >
						<div class="mySwiper-wrapper swiper-wrapper">
							<div class="mySwiper-slide swiper-slide" >
								<div class="swiper-slide-content fix">
									<div class="swiper-slide-content-pic">
										<span class="swiper-slide-content-pic-in pic-in1"></span>
									</div>
									<div class="swiper-slide-content-info">
										<div class="swiper-slide-content-hd">
											1. Консультация
										</div>
										<div class="swiper-slide-content-desc">
											Консультация имплантолога занимает от 15 минут до 1 часа (в случае, если снимки не были сделаны заранее). На консультации определяется возможность проведения имплантации, а также в зависимости от клинической ситуации выбирается вид имплантата.
										</div>
									</div>
								</div>
							</div>
							<div class="mySwiper-slide swiper-slide" >
								<div class="swiper-slide-content fix">
									<div class="swiper-slide-content-pic">
										<span class="swiper-slide-content-pic-in pic-in2"></span>
									</div>
									<div class="swiper-slide-content-info">
										<div class="swiper-slide-content-hd">
											2. Операция
										</div>
										<div class="swiper-slide-content-desc">
											Сама установка имплантата занимает не более 20 минут, вся процедура вместе с введением анестезии и накладыванием швов занимает около 1 часа. За 1 прием может быть установлено 3-4 имплантата.
										</div>
									</div>
								</div>
							</div>
							<div class="mySwiper-slide swiper-slide" >
								<div class="swiper-slide-content fix">
									<div class="swiper-slide-content-pic">
										<span class="swiper-slide-content-pic-in pic-in3"></span>
									</div>
									<div class="swiper-slide-content-info">
										<div class="swiper-slide-content-hd">
											3. Снятие швов
										</div>
										<div class="swiper-slide-content-desc">
											Через неделю после операции пациент приходит на прием для контрольного осмотра и снятия швов.
										</div>
									</div>
								</div>
							</div>
							<div class="mySwiper-slide swiper-slide">
								<div class="swiper-slide-content fix">
									<div class="swiper-slide-content-pic">
										<span class="swiper-slide-content-pic-in pic-in4"></span>
									</div>
									<div class="swiper-slide-content-info">
										<div class="swiper-slide-content-hd">
											4. Контрольный осмотр через 1 месяц
										</div>
										<div class="swiper-slide-content-desc">
											Далее необходимо время на приживление имплантата. Через месяц пациент приходит для контрольного осмотра, после которого устанавливается формирователь десны.
										</div>
									</div>
								</div>
							</div>
							<div class="mySwiper-slide swiper-slide" >
								<div class="swiper-slide-content fix">
									<div class="swiper-slide-content-pic">
										<span class="swiper-slide-content-pic-in pic-in5"></span>
									</div>
									<div class="swiper-slide-content-info">
										<div class="swiper-slide-content-hd">
											5. Ортопедический этап
										</div>
										<div class="swiper-slide-content-desc">
											Через 2 недели после установки формирователя возможно начало ортопедического этапа: изготавливается слепок для подбора абатмента и изготовления коронки.
										</div>
									</div>
								</div>
							</div>
							<div class="mySwiper-slide swiper-slide" >
								<div class="swiper-slide-content fix">
									<div class="swiper-slide-content-pic">
										<span class="swiper-slide-content-pic-in pic-in6"></span>
									</div>
									<div class="swiper-slide-content-info">
										<div class="swiper-slide-content-hd">
											6. Установка абатмента и коронки
										</div>
										<div class="swiper-slide-content-desc">
											После готовности абатмента он фиксируется на имплантате при помощи винта. Поверх абатмента устанавливается коронка.
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="mySwiper-nav">
						<s href="#" class="mySwiper-arr mySwiper-arr-prev"></s>
						<s href="#" class="mySwiper-arr mySwiper-arr-next"></s>
					</div>
				</div>
				<div class="mySwiper-sub">
					<div class="mySwiper-pagination">
						<div class="mySwiper-pagination-item current">
							Консультация
							<i class="mySwiper-pagination-item-arr">></i>
						</div>
						<div class="mySwiper-pagination-item">
							Операция
							<i class="mySwiper-pagination-item-arr">></i>
							<div class="mySwiper-pagination-item-hint">
								1 неделя
							</div>
						</div>
						<div class="mySwiper-pagination-item">
							<i class="mySwiper-pagination-item-arr">></i>
							Снятие швов
							<div class="mySwiper-pagination-item-hint">
								1 месяц
							</div>
						</div>
						<div class="mySwiper-pagination-item">
							<i class="mySwiper-pagination-item-arr">></i>
							Контрольный 	осмотр
							<div class="mySwiper-pagination-item-hint" style="right: -11px;">
								2-3 дня
							</div>
						</div>
						<div class="mySwiper-pagination-item">
							<i class="mySwiper-pagination-item-arr">></i>
							Ортопедический этап
							<div class="mySwiper-pagination-item-hint">
								3 недели
							</div>
						</div>
						<div class="mySwiper-pagination-item">
							Установка абатмента  и коронки
						</div>
					</div>
				</div>
			</div>


			<!-- слайдер -->
			<div class="slider">

				<h5>Этапы имплантации</h5>

				<div class="mySwiper">
					<div class="mySwiper-container swiper-container" >
						<div class="mySwiper-wrapper swiper-wrapper">
							<div class="mySwiper-slide swiper-slide" >
								<div class="swiper-slide-content fix">
									
									<div class="swiper-slide-content-info  textonly">
										<div class="swiper-slide-content-hd">
											1. Консультация
										</div>
										<div class="swiper-slide-content-desc">
											Консультация имплантолога занимает от 15 минут до 1 часа (в случае, если снимки не были сделаны заранее). На консультации определяется возможность проведения имплантации, а также в зависимости от клинической ситуации выбирается вид имплантата.
										</div>
									</div>
								</div>
							</div>
							<div class="mySwiper-slide swiper-slide" >
								<div class="swiper-slide-content fix">
									
									<div class="swiper-slide-content-info textonly">
										<div class="swiper-slide-content-hd">
											2. Операция
										</div>
										<div class="swiper-slide-content-desc">
											Сама установка имплантата занимает не более 20 минут, вся процедура вместе с введением анестезии и накладыванием швов занимает около 1 часа. За 1 прием может быть установлено 3-4 имплантата.
										</div>
									</div>
								</div>
							</div>
							<div class="mySwiper-slide swiper-slide" >
								<div class="swiper-slide-content fix">
									
									<div class="swiper-slide-content-info textonly">
										<div class="swiper-slide-content-hd">
											3. Снятие швов
										</div>
										<div class="swiper-slide-content-desc">
											Через неделю после операции пациент приходит на прием для контрольного осмотра и снятия швов.
										</div>
									</div>
								</div>
							</div>
							<div class="mySwiper-slide swiper-slide">
								<div class="swiper-slide-content fix">
									
									<div class="swiper-slide-content-info textonly">
										<div class="swiper-slide-content-hd">
											4. Контрольный осмотр через 1 месяц
										</div>
										<div class="swiper-slide-content-desc">
											Далее необходимо время на приживление имплантата. Через месяц пациент приходит для контрольного осмотра, после которого устанавливается формирователь десны.
										</div>
									</div>
								</div>
							</div>
							
							
						</div>
					</div>
					<div class="mySwiper-nav">
						<s href="#" class="mySwiper-arr mySwiper-arr-prev"></s>
						<s href="#" class="mySwiper-arr mySwiper-arr-next"></s>
					</div>
				</div>
				<div class="mySwiper-sub textonly">
					<div class="mySwiper-pagination">
						<div class="mySwiper-pagination-item current">
							слайд 1
							<i class="mySwiper-pagination-item-arr">></i>
						</div>
						<div class="mySwiper-pagination-item">
							слайд 2
							<i class="mySwiper-pagination-item-arr">></i>
						</div>
						<div class="mySwiper-pagination-item">
							<i class="mySwiper-pagination-item-arr">></i>
							слайд 3
						</div>
						<div class="mySwiper-pagination-item">
							слайд 4
						</div>
						
					</div>
				</div>
			</div>



			<!-- блок с цитатой врача -->
			<div class="docinfoblock ">
				<a  href="#" class="docinfoblock-ava" style="background-image: url(http://intan.local/upload/resize_cache/iblock/63c/285_285_1/63c4b2c4a0c9eb24b73f9220671a86c2.png)"></a>
				<div class="docinfoblock-info" >
					<div class="docinfoblock-citate" >
						От 1000 Ваших улыбок, сделанных мною, мир становится добрее!
					</div>
					<div class="docinfoblock-about" >
						<div class="docinfoblock-about-text">
							<div class="bold">Галямова Лилия Рафаильевна</div>
							<div>Главный врач центра на Лиговском пр., дом 125.</div>
							<div>Врач-стоматолог-терапевт-хирург-имплантолог-пародонтолог</div>
						</div>
						<div class="docinfoblock-about-btn">
							<a class="default-btn" onclick="javascript:jQuery('.appointment:visible').click(); return false;" href="#">Записаться на прием</a>
						</div>
					</div>
				</div>
			</div>



			<!-- блок ценником -->
			<div class="priceblock ">
				<div class="priceblock-text">
					<div class="priceblock-table">
						<div class="row">
							<div class="cell priceblock-text-text">
								Операция по установке одноэтапного имплантата (включая стоимость имплантата)
							</div>
							<div class="cell priceblock-text-price">
								от <span>11550</span><i class="rub">p</i>
							</div>
						</div>
					</div>
				</div>
				<div class="priceblock-btn">
					<a class="default-btn"  href="#">Подробнее</a>
				</div>
			</div>


			<!-- Обычный текст в две колонки -->
			<div class="default-text text-columns">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta voluptatum ducimus quia velit quos dolores atque fugiat, sit fugit ex libero praesentium, eum, illo tempore reprehenderit cumque hic! Adipisci, voluptatem.
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus ad cumque cum facere dolorum reprehenderit quibusdam laborum doloribus sequi perspiciatis rem minima quia a quam deserunt tempore totam, id. Id?
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, quae aperiam nulla corporis libero soluta? Impedit sint corrupti modi illum, officia atque reiciendis nesciunt sapiente, autem natus perferendis, aperiam molestias.
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam officiis, iste beatae sint omnis rerum quia. Iure suscipit perferendis ad dolorem, quidem illum eum odio, optio alias aliquam, sed omnis.
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat, laboriosam excepturi dicta eveniet a, fugit error, qui ipsam fuga officia, maiores ex repudiandae. Quasi vitae illum delectus, illo placeat aut!
				</p>
				<div class="clearfix"></div>
			</div>



			<!-- Обычный текст в две колонки  -->
			<div class="default-text text-columns">
				<!-- *(если без картинки то span не нужен внутри P) -->
				<ul>
					<li>Lorem ipsum dolor sit amet</li>
					<li>consectetur adipisicing elit. </li>
					<li>Nemo veritatis commodi iste optio </li>
					<li>ss</li>
				</ul>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta voluptatum ducimus quia velit quos dolores atque fugiat, sit fugit ex libero praesentium, eum, illo tempore reprehenderit cumque hic! Adipisci, voluptatem.
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus ad cumque cum facere dolorum reprehenderit quibusdam laborum doloribus sequi perspiciatis rem minima quia a quam deserunt tempore totam, id. Id?
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, quae aperiam nulla corporis libero soluta? Impedit sint corrupti modi illum, officia atque reiciendis nesciunt sapiente, autem natus perferendis, aperiam molestias.
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam officiis, iste beatae sint omnis rerum quia. Iure suscipit perferendis ad dolorem, quidem illum eum odio, optio alias aliquam, sed omnis.
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat, laboriosam excepturi dicta eveniet a, fugit error, qui ipsam fuga officia, maiores ex repudiandae. Quasi vitae illum delectus, illo placeat aut!
					<br>
					<br>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat, laboriosam excepturi dicta eveniet a, fugit error, qui ipsam fuga officia, maiores ex repudiandae. Quasi vitae illum delectus, illo placeat aut!

				</p>
				<div class="clearfix"></div>
			</div>
	
			<!-- Обычный текст в две колонки -->
			<div class="default-text text-columns">

				<p>
					<!-- Нужен спан. ели вставляется картика -->
					<span>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta voluptatum ducimus quia velit quos dolores atque fugiat, sit fugit ex libero praesentium, eum, illo tempore reprehenderit cumque hic! Adipisci, voluptatem.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus ad cumque cum facere dolorum reprehenderit quibusdam laborum doloribus sequi perspiciatis rem minima <b>quia a quam deserunt tempore</b> totam, id. Id?
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, quae aperiam nulla corporis libero soluta? Impedit sint corrupti modi illum, officia 	atque reiciendis nesciunt sapiente, autem natus perferendis, aperiam molestias.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam officiis, iste beatae sint omnis rerum quia. Iure suscipit perferendis ad dolorem, quidem illum eum odio, optio alias aliquam, sed omnis.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat, laboriosam excepturi dicta eveniet a, fugit error, qui ipsam fuga officia, maiores ex repudiandae. Quasi vitae illum delectus, illo placeat aut!
					</span>
					<span class="floatimg right">
						<span class='h3'>фото 5</span>
						<img src="/images/video_tmp.png" alt="" >
					</span>
				</p>
				<div class="clearfix"></div>
			</div>

			<!-- Обычный текст в две колонки -->
			<div class="default-text text-columns">
				<p>

					<span class="floatimg left">
						<img  src="/images/video_tmp.png" alt="" >
						<span class='h3'>фото 6</span>
					</span>
					<!-- Нужен спан. ели вставляется картика -->
					<span>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta voluptatum ducimus quia velit quos dolores atque fugiat, sit fugit ex libero praesentium, eum, illo tempore reprehenderit cumque hic! Adipisci, voluptatem.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus ad cumque cum facere dolorum reprehenderit quibusdam laborum doloribus sequi perspiciatis rem minima <b>quia a quam deserunt tempore</b> totam, id. Id?
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, quae aperiam nulla corporis libero soluta? Impedit sint corrupti modi illum, officia atque reiciendis nesciunt sapiente, autem natus perferendis, aperiam molestias.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam officiis, iste beatae sint omnis rerum quia. Iure suscipit perferendis ad dolorem, quidem illum eum odio, optio alias aliquam, sed omnis.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat, laboriosam excepturi dicta eveniet a, fugit error, qui ipsam fuga officia, maiores ex repudiandae. Quasi vitae illum delectus, illo placeat aut!
					</span>

				</p>
				<div class="clearfix"></div>
			</div>


			<!-- блок с приемуществами -->
			<div class="infoblock-default">
				<div class="infoblock-title"><span>Основные преимущества имплантации зубов:</span></div>
				<div class="default-text text-columns">
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet</li>
					</ul>
					
				</div>
				<div class="clearfix"></div>
			</div>

		

			<!-- Голубой блок с преимеществами и картинкй -->
			<div class="infoblock-default infoblock-blue">
				<div class="infoblock-title"><img src="/images/tooth.png" alt=""><span>Основные преимущества имплантации зубов:</span></div>
				<div class="default-text text-columns">
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet</li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>

			<!-- Голубой блок с преимеществами и картинкй -->
			<div class="infoblock-default infoblock-blue">
				<div class="infoblock-title"><img src="/images/caller.png" alt=""><span>Основные преимущества имплантации зубов:</span></div>
				<div class="default-text text-columns">
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet</li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>

			<!-- WARNING блок с преимеществами и картинкй -->
			<div class="infoblock-default infoblock-warn">
				<div class="infoblock-title"><img src="/images/warn.png" alt=""><span>Основные преимущества имплантации зубов:</span></div>
				<div class="default-text text-columns">
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, veniam cum magni obcaecati sint facilis perspiciatis qui eum itaque suscipit inventore provident dolores, maxime, unde id dignissimos ullam rem at?</li>
						<li>Lorem ipsum dolor sit amet</li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>


			<!-- Закрашенный блок с тектом и картинкой - ОРАНЖЕВЫЙ -->
			<div class="infoblock-bg infoblock-light">
				<div class="infoblock-bg-img">
					<img src="/images/doctor.png" alt="">
				</div>
				<div class="infoblock-bg-text">
					Специалисты центров ИНТАН предлагают широкий спектр услуг как по протезированию, так и по лечение зубов любой сложности. Обратившись к нам, вы обретете радость полноценной жизни и здоровую улыбку!
				</div>
				<div class="clearfix"></div>
			</div>

			<!-- Закрашенный блок с тектом и картинкой _ ГОЛУБО  -->
			<div class="infoblock-bg infoblock-dark">
				<div class="infoblock-bg-img">
					<img src="/images/coshel.png" alt="">
				</div>
				<div class="infoblock-bg-text">
					Цены на зубные имплантаты зависят от фирмы-производителя и особенностей конструкции искусственного зуба. Многообразие зубных имплантатов позволяет каждому пациенту выбрать наиболее оптимальный для себя вариант.
				</div>
				<div class="clearfix"></div>
			</div>



		</div>

	</div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
