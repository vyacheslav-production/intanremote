<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/krasnodar/kontacty/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/krasnodar/kontacty/index.php",
	),
	array(
		"CONDITION" => "#^/stomatologiya-ceny/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/stomatologiya-ceny/index.php",
	),
	array(
		"CONDITION" => "#^/krasnodar/aksii/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/krasnodar/aksii/index.php",
	),
	array(
		"CONDITION" => "#^/mobile/aksii/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/mobile/aksii/index.php",
	),
	array(
		"CONDITION" => "#^/personal/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/personal/index.php",
	),
	array(
		"CONDITION" => "#^/kontacty/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/kontacty/index.php",
	),
	array(
		"CONDITION" => "#^/aksii/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/aksii/index.php",
	),
    array(
        "CONDITION" => "#^/(.{3})/(.+)#",
        "RULE" => "selCity=$1",
        "PATH" => "/$2/index.php",
    ),
);

?>