;(function( $, window, undefined ) {

    /*!
    * jQuery stickTheFooter plugin
    *
    * Copyright (c) 2014
    *
    * @version 1.0.0
    */
    jQuery.fn.stickTheFooter = function ( options ) {
        var o = $.extend({
            header   :  $('#header'),
            content  :  $('#content'),
            footer   :  $('#footer'),
            offset   :  0
        }, options);
        return this.each(function(event){
            var headerHeight   =  o.header.eq(0).outerHeight(),
                footerHeight   =  o.footer.eq(0).outerHeight();
            function init(){
               o.content.css('minHeight', $(window).outerHeight() - footerHeight - headerHeight + o.offset);
            }
            init();
            $(window).resize(init);
        });
    }
    /* . */



    /*!
    * jQuery dropdown plugin
    *
    * Copyright (c) 2014
    *
    * @version 1.0.0
    */
    jQuery.fn.dropdown = function ( options ) {

        var defaults = {
            button: '.dropdown-button',
            menu: '.dropdown-menu',
            buttonTitle: '.dropdown-button-title',
            item: '.dropdown-menu-item',
            itemTitle: '.dropdown-menu-title',
            inputHidden: '.dropdown-hidden',
            active:'active',
            dataAttr:'value',
        }

        var o = $.extend(defaults, options);

        var $el = $(this);



        $(document.body).on('click', $el.selector+" "+o.button, function(e){



            var $dropdown = $(this).closest($el.selector);
            var $menu  = $dropdown.find(o.menu);

            if ( !$dropdown.hasClass(o.active) ) {

                $($el.selector).removeClass(o.active);
                $($el.selector).find(o.menu).hide();
                $dropdown.addClass(o.active);
                $menu.show();
                $(window).on('click', hideDropdown);

            } else {

                $dropdown.removeClass(o.active);
                $menu.hide();
                $(window).off('click', hideDropdown);

            }

            e.preventDefault();


        });

        $(document.body).on('click', $el.selector+" "+o.item, function(e){
            var $dropdown = $(this).closest($el.selector);
            var $menu  = $dropdown.find(o.menu);
            var $buttonTitle = $dropdown.find(o.buttonTitle);
            var $inputHidden = $dropdown.find(o.inputHidden);
            var $checkbox;
            var level;
            var i = 0;

            if( $dropdown.hasClass('dropdown-multi') ) {

                if ( $inputHidden.val() == "" ){
                    var hiddenArr = [];
                } else {
                    var hiddenArr = $inputHidden.val().split(',');
                }

                $(o.menu).hide();
                $menu.show();
                $dropdown.addClass('dropdown-changed');

                $checkbox = $(this).find("input[type='checkbox']");
                $checkbox.prop("checked") == false ? $checkbox.prop("checked", true) : $checkbox.prop("checked", false);


                //Если выбрали главный элемент уровня
                if( $(this).hasClass('dropdown-menu-item-higher') ) {
                    level = $(this).data('level');
                    if( $checkbox.prop("checked") == false ){
                        setSubLevel(level,false);
                    } else {
                        setSubLevel(level,true);
                    }
                }

                function setSubLevel(level, boo){
                    $menu
                        .find('.dropdown-menu-item-sub')
                        .filter("[data-level = "+level+"]")
                        .each(function(){
                            $(this).find("input[type='checkbox']").prop("checked", boo);
                        });
                }


                //Если выбрали подуровень
                if( $(this).hasClass('dropdown-menu-item-sub') ) {
                    level = $(this).data('level');
                    var arr = [];

                    $menu
                        .find('.dropdown-menu-item-sub')
                        .filter("[data-level = "+level+"]")
                        .each(function(){
                            if( $(this).find("input[type='checkbox']").prop("checked") == false ) {
                                arr.push(0);
                            } else {
                                arr.push(1);
                            }
                        });

                        if (arr.indexOf( 0 ) < 0 ) {
                            setHigherLevel(level, true);
                        } else if ( arr.indexOf( 0 ) >= 0  ) {
                            setHigherLevel(level, false);
                        }

                }

                function setHigherLevel(level, boo){
                    $menu
                        .find('.dropdown-menu-item-higher')
                        .filter("[data-level = "+level+"]").eq(0)
                        .find("input[type='checkbox']").prop("checked", boo);
                }



                //Обновить значения
                $inputHidden.val(''); hiddenArr = [];
                $menu.find(o.item).each(function(){
                    if ( $(this).find("input[type='checkbox']").prop("checked") == true ) {
                        hiddenArr.push( $(this).data('value') );
                    }
                });
                $inputHidden.val( hiddenArr );


                //Обновили счетчик
                $menu.find(o.item).each(function(){
                    if( $(this).find("input[type='checkbox']").prop("checked") == true ){
                        i++;
                    }
                });

                if ( i > 0) {
                    $buttonTitle
                        .find('.dropdown-button-count')
                        .html(" ("+i+")")
                } else {
                    $buttonTitle
                        .find('.dropdown-button-count')
                        .html("");

                    $dropdown.removeClass('dropdown-changed');
                }


                e.preventDefault();



            } else if ( $dropdown.hasClass('dropdown-lang') ) {
                $menu.hide();
                $dropdown.removeClass(o.active);
                $dropdown.addClass('dropdown-changed');
            } else {
                $menu.hide();
                $buttonTitle.html( $(this).find(o.itemTitle).html() );
                $inputHidden.val( $(this).data(o.dataAttr)).change();
                $dropdown.removeClass(o.active);
                $dropdown.addClass('dropdown-changed');
                e.preventDefault();
            }
        });

        function hideDropdown(e){
            if( $(e.target).is( $el.selector ) || $(e.target).is($el.selector + ' *')) return;
            $(o.menu).hide();
            $(window).off('click', hideDropdown);
            $( $el.selector ).removeClass(o.active);
        }




        this.refresh = function(el){

            var $el = $(el);

            $el.each(function(){

                var $thisEl = $(this),
                    $thisMenu = $thisEl.find('.dropdown-menu'),
                    $thisInputHidden = $thisEl.find('.dropdown-hidden'),
                    $thisInputHiddenArr = [],
                    $thisItem = $thisEl.find('.dropdown-menu-item'),
                    $thisButtonTitle = $thisEl.find('.dropdown-button-title'),
                    $thisItemHtml;

                if ( hasValue( $thisEl ) ) {
                    if ( $thisEl.hasClass('dropdown-multi') ) {
                        $thisInputHiddenArr = $thisInputHidden.val().split(',');
                        $thisInputHidden.val('');
                        for (var i = 0; i < $thisInputHiddenArr.length; i++) {
                            $thisItem.each(function(){
                                if( $(this).data('value') == $thisInputHiddenArr[i] ){
                                    $(this).trigger('click');
                                    $thisMenu.hide();
                                }
                            });
                        }
                    } else {
                        $thisItem.each(function(){
                            if( $(this).data('value') == $thisInputHidden.val() )
                               $thisEl.addClass('dropdown-changed');
                        });
                        $thisItemHtml = $thisItem.filter(" [ data-value = " + $thisInputHidden.val() + " ] ").eq(0).html();
                        $thisButtonTitle.html( $thisItemHtml );
                    }
                }

            });

            function hasValue(el){
                var val = el.find('.dropdown-hidden').val();
                return !val || /^\s*$/.test(val) ? false : true;
            }

        };

        this.refresh('.dropdown');



        return this.each(function(){
            var $el = $(this);
            $(this).data("dropdownOptions", o);
            $(this).data("dropdownButtonTitle", $(this).find(o.buttonTitle).html() );
        });


    }
    /* . */





    /*!
    * jQuery fancyConfirm plugin
    *
    * Copyright (c) 2014
    *
    * @version 1.0.0
    */
    $.fn.fancyConfirm = function ( options ) {

        var defaults = {
            msg:"Are you sure?",
            content: "<div class='dialog-confirm'> <div class='dialog-confirm-mes'> {msg} </div> <div class='dialog-confirm-sub'> <input id='fancyconfirm_cancel' type='button' class='custom-button custom-button-gray' value='Отмена'> <input id='fancyConfirm_ok' type='button' class='custom-button' value='Удалить'> </div> </div>",
            success:function(){},
            cancel:function(){},
            beforeSuccess:function(){ return true; },
            beforeCancel:function(){},
            beforeShow:function(){},
            afterShow:function(){},
            afterClose:function(){}
        }

        var o = $.extend(defaults, options);

        $(document.body).on('click', $(this).selector, function(e){

            var $el = $(this);

            $.fancybox({
                'modal' : true,
                'content': o.content.replace("{msg}",o.msg),
                afterShow: function(){
                    o.afterShow($el);
                },
                beforeShow: function(){
                    o.beforeShow($el);
                    $("#fancyConfirm_ok").click(function() {
                        if( o.beforeSuccess($el) ) {
                            o.success($el);
                            $.fancybox.close();
                        }
                    });
                    $("#fancyconfirm_cancel").click(function() {
                        o.beforeCancel($el);
                        $.fancybox.close($el);
                        o.cancel($el);
                    });
                },
                afterClose:function(){
                    o.afterClose($el);
                }
            });
            e.preventDefault();
            e.stopPropagation();
        });

        return this;

    }


    /*!
    * jQuery toggleBox plugin
    *
    * Copyright (c) 2014
    *
    * @version 1.0.0
    */
    $.fn.toggleBox = function( options ){

        var defaults = {
           buttonOpen:'.toggle-button-open',
           buttonClose:'.toggle-button-close',
           buttonToggle:'.toggle-button',
           buttonTitle:'.toggle-button-title',
           toggleContent:'.toggle-content',
           speed : 400,
           ease  : 'easeInOutQuad',
           beforeOpen:function(){},
           afterOpen: function(){},
           afterClose:function(){}
        }

        var o = $.extend(defaults, options);

        o.el = $(this).selector;

        function open(el, content){
            o.beforeOpen(el);
            el.addClass('active');
            content.stop(true,true).slideDown(o.speed, o.ease, function(){
                o.afterOpen(el);
            });

        }

        function close(el, content){
            el.removeClass('active');
            content.stop(true,true).slideUp(o.speed, o.ease, function(){
                o.afterClose(el);
            });
        }


        $(document.body).on('click', o.el+" "+o.buttonOpen, function(e){
            $box = $(this).closest(o.el),
            $content = $box.find(o.toggleContent);

            if( !$box.hasClass('active') ) {
                open($box, $content);
            }
            e.preventDefault();
        });

        $(document.body).on('click', o.el+" "+o.buttonClose, function(e){
            $box = $(this).closest(o.el),
            $content = $box.find(o.toggleContent);
            if( $box.hasClass('active') ) {
                close($box, $content);
            }
            e.preventDefault();
        });

        $(document.body).on('click', o.el+" "+o.buttonToggle, function(e){

            var $box = $(this).closest(o.el);
            var $content = $box.find(o.toggleContent);

            if( $box.hasClass('active') ){
                close($box, $content);

            } else {
                open($box, $content);

            }

            if( $(this).data('toggle-title-open') != '' &&  $(this).data('toggle-title-close') != '') {
                if( $box.hasClass('active') ){
                    $(this).html( $(this).data('toggle-title-close') );
                } else {
                    $(this).html( $(this).data('toggle-title-open') );
                }
            }
            e.preventDefault();


        });

        return this;

    }

})(jQuery, window); /*______________End_______________*/


function priceMenu(){
    $(".activeCategory").fadeIn("slow");
    var go_action = false;
    $(document).on('click',".price_menu a", function(event){
        event.preventDefault();

            if (go_action == false) {
                // Если анимации нет, то мы можем позволить ей начать совершаться.
                // Если go_action имеет значение true, то ничего не делаем 
                go_action = true;
                var pmenu = $(this).data("category");
                console.log(pmenu);
                var ptable= $(".price_book[data-select=" + pmenu+ "]");
                var active = $(".activeCategory");
                var category = $(".price_menu");
                // включай анимацию по сворачиванию и открыванию, если ты выбрал не текущую категорию
                if (!ptable.hasClass("activeCategory")) {
                    category.addClass("opacity default_cursor");
                    active.removeClass("activeCategory");
                    ptable.addClass("activeCategory");
                    active.fadeOut(500, function(){
                        ptable.fadeIn("slow", function(){
                            // тут снова переменной задаем значение false, мол анимация закончилась и есть готовность начать ее, а не делать все параллельно
                            go_action = false;
                            category.removeClass("opacity default_cursor");
                        });
                        
                    });

                }
                rollInOut();
                $(".price_menu a").removeClass("chose");   
                $(this).addClass("chose");
            }

    });

}

function lvls(){
    $(document).on('click','.lvl', function(){
        var first_lvl=$(this).data("first");
        console.log(first_lvl);
        var second_lvl= $(".lvl2[data-secondLvL=" + first_lvl + "]");
        $(this).toggleClass("price__select--active");
        // показывает или скрывает ответ
        if ($(this).hasClass("price__select--active")){
            // second_lvl.slideDown(1000);
            second_lvl.show(50).addClass("price_row2");
            console.log("add");

            // $(".price_row2:odd").css("background-color", "#D7D7D7");
        } else {
            // second_lvl.slideUp(1000);
            second_lvl.hide(50).removeClass("price_row2");
            console.log("remove");
        }
    });
}

function colorRows(){
    console.log("colorRows");
    $(".price_row:odd").css("background-color", "#ebebeb");
    $(".lvl2:odd").css("background-color", "#D7D7D7");
    // $(".price_row2:odd").css("background-color", "green");
}


function rollInOut(){
    var countRow = $(".activeCategory li").length;
    $(".activeCategory li").slice(15,countRow).addClass("hide loser");
    $(document).on("click",".roll", function(){
        $(".loser").toggleClass("hide");
        if($(".loser").hasClass("hide")){
            $(".roll span").text("развернуть список");
            // $(this).addClass("rollOut");
        } else {
            $(".roll span").text("свернуть список");
            // $(this).removeClass("rollIn");
        }

    });
}


window.srollable = function(){
    if( $(".head .head-nav").length ){
        $(".head .head-nav a").click(function(e){
            e.preventDefault();
            var top = $(this.hash).offset().top-$("header").height();
            $(document).scrollTo(top, 400);
        })
    }
}

function telvalid(){
$('.phone_field').mask('+7-(999)-999-99-99', { placeholder: '_'});
}