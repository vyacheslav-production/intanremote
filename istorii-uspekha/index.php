<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Истории успеха");
?>


  <section>
        <div class="wrap">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_page_vac", Array(

            ),
                false
            );?>  
            <?if ($_REQUEST['selCity'] == 'spb') {
                $id = 119337;
            }elseif ($_REQUEST['selCity'] == 'nvr') {
                $id = 119338;
            }elseif ($_REQUEST['selCity'] == 'krd') {
                $id = 119343;
            };
            GLOBAL $arrFilter;
            $arrFilter = array('PROPERTY_F_BINDCITY' => $id)?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "istorii_uspekha",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "spb",
                    "IBLOCK_ID" => "77",
                    "NEWS_COUNT" => "20",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilter",
                    "FIELD_CODE" => array("NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_TEXT", "DETAIL_PICTURE"),
                    "PROPERTY_CODE" => array("F_DOCTOR", "F_BINDCITY","IN_INTAN_YEAR","ID_VIDEO","RIGHT_BLOCK","RIGHT_IMG"),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "Y",
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );?> 
            
			

            <?include_once($_SERVER['DOCUMENT_ROOT'].'/include/sidebar_right_history.php');?>
        </div>
    </section>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>