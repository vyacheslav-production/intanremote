<section class="price_section price_book">
  <h1>Анестезиологические услуги</h1>
 
  <div class="price_subsec"> 
    <h2>Взрослое население</h2>
   
    <ul> 
      <li> 
        <p class="right">12600<span class="rouble">i</span></p>
       
        <div>Хирургическая стоматология - за каждый час работы</div>
       </li>
     
      <li> 
        <p class="right">8970<span class="rouble">i</span></p>
       
        <div>Терапевтическая стоматология - за каждый час работы</div>
       </li>
     
      <li> 
        <p class="right">10870<span class="rouble">i</span></p>
       
        <div>Ортопедическая стоматология - за каждый час работы</div>
       </li>
     
      <li> 
        <p class="right">5390<span class="rouble">i</span></p>
       
        <div>Наблюдение пациента анестезиологом во время манипуляций под местной анестезией за каждый час</div>
       </li>
     
      <li> 
        <p class="right">5390<span class="rouble">i</span></p>
       
        <div>Послеоперационное наблюдение (ведение) пациента анестезиологом за каждый час</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Детское население</h2>
   
    <ul> 
      <li> 
        <p class="right">14190<span class="rouble">i</span></p>
       
        <div>Первый час работы</div>
       </li>
     
      <li> 
        <p class="right">9350<span class="rouble">i</span></p>
       
        <div>За каждый последующий час работы</div>
       </li>
     
      <li> 
        <p class="right">5720<span class="rouble">i</span></p>
       
        <div>Наблюдение пациента анестезиологом в период медицинской манипуляции под местной анестезией за каждый час</div>
       </li>
     
      <li> 
        <p class="right">5720<span class="rouble">i</span></p>
       
        <div>Послеоперационное наблюдение (ведение) пациента анестезиологом за каждый час</div>
       </li>
     </ul>
   </div>
 </section>