<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Стоматология цены Спб. Цены стоматологии Интан в Петербурге.");
$APPLICATION->SetPageProperty("keywords", "стоматология цены, цены стоматологии");
$APPLICATION->SetPageProperty("description", "Интан - самые низкие цены и постоянные скидки. Расценки на перечень стоматологических услуг в Интане.");
$APPLICATION->SetTitle("Цены на стоматологические услуги");
CModule::IncludeModule("iblock");

//список услуг
$listServ = array();
$arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listmedservices']];
$arrFilter = getFilArrayByCity($arrFilter);
$res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'CODE', 'NAME', 'DETAIL_PAGE_URL'));
while($ar_res = $res->GetNext()){
    $listServ[] = $ar_res;
}

/*if(!$_GET['selCity']){
    LocalRedirect("/404.php", "404 Not Found");
}*/
?>
<section>
    <?if(!empty($_REQUEST['ELEMENT_CODE'])){
        $oneElem = array();
        $arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listprices'], 'PROPERTY_F_BINDSERV.CODE'=>$_REQUEST['ELEMENT_CODE']];
        $arrFilter = getFilArrayByCity($arrFilter);
        $res = CIBlockElement::GetList(array(), $arrFilter, false, false, array('IBLOCK_ID', 'ID', '*', 'PROPERTY_F_PRICE'));
        if($ar_res = $res->GetNext()){
            $oneElem = $ar_res;
        }else{
            LocalRedirect("/404.php", "404 Not Found");
        }
    }else{
        reset($listServ);
        $firstElem = current($listServ);
        LocalRedirect('/'.getCodeCity().'/stomatologiya-ceny/'.$firstElem['CODE'].'/');
    }?>
    <div class="wrap">

        <h1>Цены на стоматологические услуги</h1>
        <div class="mobile-inner-menu">
            <div class="menu-blok notitle">
                <ul>
                    <?foreach($listServ as $keyServ=>$oneServ){?>
                        <li><a class="<?=$oneServ['CODE']==$_REQUEST['ELEMENT_CODE'] ? 'active' : ''?>" href="/<?=getCodeCity()?>/stomatologiya-ceny/<?=$oneServ['CODE']?>/"><span><?=$oneServ['NAME']?></span></a></li>
                    <?}?>
                </ul>
            </div>

        </div>
        <div class="w65 tables ib">
             <?
                $listSales = array();
                $arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listsale'], 'PROPERTY_F_BINDCITY.CODE' => getCodeCity(),
                            array('LOGIC' => 'OR',
                                array('PROPERTY_F_SERVICE'=>$oneElem['ID']),
                                array('PROPERTY_F_SERVICE'=>false)
                            )
                ];
                $res = CIBlockElement::GetList(array('RAND'=>'ASC'), $arrFilter);
                while($ar_res = $res->GetNext()){
                    $listSales[] = $ar_res;
                }
               
                ?>
            <?if(!empty($listSales)){?>
                <div class="wrap">
                    <h1></h1>
                    <h2>Акции</h2>
                </div>
                <div class="b-slider mini">
                    <div class="controls">
                        <span  class="prev"></span>
                        <span class="next"></span>
                    </div>
                    <div class="swiper-container" >
                        <div class="swiper-wrapper">
                            <?foreach($listSales as $oneSale){?>
                                <?$img = CFile::ResizeImageGet($oneSale["PREVIEW_PICTURE"], array('width'=>427, 'height'=>240), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
                                <a href="<?=$oneSale['DETAIL_PAGE_URL']?>" class="img swiper-slide" style="background-image: url(<?=$img['src']?>)"></a>
                            <?}?>
                        </div>
                    </div>
                    <div class="swiper-scrollbar"></div>
                </div>
            <?}?>
            <h5><?=$oneElem['NAME']?></h5>

            <?=$oneElem['~DETAIL_TEXT']?>
        </div>
        <div class="w35 ib fixed-scrolling">
            <div class="menu-blok notitle">
                <ul>
                    <?foreach($listServ as $keyServ=>$oneServ){?>
                        <li><a class="<?=$oneServ['CODE']==$_REQUEST['ELEMENT_CODE'] ? 'active' : ''?>" href="/<?=getCodeCity()?>/stomatologiya-ceny/<?=$oneServ['CODE']?>/"><span><?=$oneServ['NAME']?></span></a></li>
                    <?}?>
                </ul>
            </div>
            <!--a  href="#" class="right-link calc-price"><span>Рассчитать стоимость</span></a-->
            <a  href="/lechenie-v-kredit/" class="right-link credit"><span>Лечение в кредит</span></a>
        </div>
    </div>

   
</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
