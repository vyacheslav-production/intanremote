<section class="price_section price_book" data-select="8">
  <h1>Эстетическая стоматология</h1>
 
  <div class="price_subsec"> 
    <h2>Реставрации</h2>
   
    <ul> 
      <li> 
        <p class="right">5610<span class="rouble">i</span></p>
       
        <div>Восстановление разрушеной коронки зуба</div>
       </li>
     
      <li> 
        <p class="right">4450<span class="rouble">i</span></p>
       
        <div>Изготовление винира из композитного материала</div>
       </li>
     
      <li> 
        <p class="right">680<span class="rouble">i</span></p>
       
        <div>Обработка под винир</div>
       </li>
     
      <li> 
        <div>Формирование культи из</div>
       
        <ul> 
          <li> 
            <p class="right">2300<span class="rouble">i</span></p>
           
            <div>Жидкотекучих материалов</div>
           </li>
         
          <li> 
            <p class="right">2250<span class="rouble">i</span></p>
           
            <div>Композита химического</div>
           </li>
         
          <li> 
            <p class="right">2590<span class="rouble">i</span></p>
           
            <div>Композита светового</div>
           </li>
         </ul>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Реставрации (ESTELITE)</h2>
   
    <ul> 
      <li> 
        <p class="right">3950<span class="rouble">i</span></p>
       
        <div>Восстановление угла центрального зуба</div>
       </li>
     
      <li> 
        <p class="right">5860<span class="rouble">i</span></p>
       
        <div>Восстановление разрушеной коронки зуба </div>
       </li>
     
      <li> 
        <p class="right">4720<span class="rouble">i</span></p>
       
        <div>Изготовление винира </div>
       </li>
     
      <li> 
        <div>Пломбы:</div>
       
        <ul> 
          <li> 
            <p class="right">2910<span class="rouble">i</span></p>
           
            <div>1 поверхность</div>
           </li>
         
          <li> 
            <p class="right">3150<span class="rouble">i</span></p>
           
            <div>2 поверхности</div>
           </li>
         
          <li> 
            <p class="right">3870<span class="rouble">i</span></p>
           
            <div>3 поверхности</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">2420<span class="rouble">i</span></p>
       
        <div>Реставрация металлокерамики</div>
       </li>
     </ul>
   </div>
 </section>