<section class="price_section price_book" data-select="6">
  <h1>Диагностика</h1>
 
  <div class="price_subsec"> 
    <h2>Рентгенологическое обследование</h2>
   
    <ul> 
      <li>
        <p class="right">220<span class="rouble">i</span></p>
      
        <div>Прицельный снимок</div>
      </li>
     
      <li>
        <p class="right">650<span class="rouble">i</span></p>
      
        <div>Ортопантомограмма (панорамный снимок)</div>
      </li>
     
      <li>
        <p class="right">330<span class="rouble">i</span></p>
      
        <div>Рентгенография придаточных пазух носа</div>
      </li>
     
      <li>
        <p class="right">1100<span class="rouble">i</span></p>
      
        <div>Компьютерная томография (1 челюсть)</div>
      </li>
     
      <li>
        <p class="right">1850<span class="rouble">i</span></p>
      
        <div>Компьютерная томография (2 челюсти)</div>
      </li>
     
      <li>
        <p class="right">550<span class="rouble">i</span></p>
      
        <div>Описание томограммы (одна челюсть)</div>
      </li>
     
      <li>
        <p class="right">940<span class="rouble">i</span></p>
      
        <div>Описание томограммы (две челюсти)</div>
      </li>
     
<!--<li><p class="right">1610<span class="rouble">i</span></p><div>Компьютерная томография гайморовых пазух</div></li>-->
 
      <li>
        <p class="right">30<span class="rouble">i</span></p>
      
        <div>Копирование снимка на дискету или носитель пациента</div>
      </li>
     
      <li>
        <p class="right">60<span class="rouble">i</span></p>
      
        <div>Копирование снимка на CD</div>
      </li>
     
      <li>
        <p class="right">30<span class="rouble">i</span></p>
      
        <div>Печать снимка с дискеты, CD, носителя пациента</div>
      </li>
     </ul>
   </div>
 </section>