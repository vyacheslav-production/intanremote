<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интан предлагает лечение зубов в кредит");
$APPLICATION->SetTitle("Заявка на лечение в кредит");
?> 
<!--уууее-->
 
<div id="requestWrap" style="display: none;"> <?$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.form",
	"zayavka_na_credit",
	Array(
		"IBLOCK_TYPE" => "on_line",
		"IBLOCK_ID" => "28",
		"STATUS_NEW" => "N",
		"LIST_URL" => "",
		"USE_CAPTCHA" => "N",
		"USER_MESSAGE_EDIT" => "",
		"USER_MESSAGE_ADD" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "N",
		"PROPERTY_CODES" => array(0=>"NAME",1=>"105",2=>"104",3=>"106",4=>"111",5=>"102",),
		"PROPERTY_CODES_REQUIRED" => array(0=>"NAME",1=>"105",),
		"GROUPS" => array(0=>"2",),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"MAX_USER_ENTRIES" => "100000",
		"MAX_LEVELS" => "100000",
		"LEVEL_LAST" => "Y",
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"SEF_MODE" => "N",
		"CUSTOM_TITLE_NAME" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => ""
	)
);?> </div>
 
<!---->
 <aside class="left_sidebar"> <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"tree1",
	Array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => "",
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "podmenu",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/inc_vert_list.php",
		"EDIT_TEMPLATE" => ""
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "inc_banners",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	)
);?> </aside> 
<div class="content_part"> <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"template1",
	Array(
	)
);?> <article> 
    <h1 class="main_h">Заявка на лечение в кредит</h1>
   
    <div> 
      <p class="credit_h moved">Банки-партнеры</p>
     
      <div style="margin-bottom: 20px;"> <img src="/images/letobank.png" style="float:left;margin:18px 25px 0 0;" title="Альфа-Банк" alt="Альфа-Банк"  /> <img src="/images/alphabank.png" style="float:left;margin:18px 0 0;" title="Альфа-Банк" alt="Альфа-Банк"  /> <img src="/images/homecredit.png" style="float:left;margin:19px 0 0 34px;" title="Банк Хоум Кредит" alt="Банк Хоум Кредит"  /> <input type="button" value="Отправить заявку" name="show_creditCalc" class="green_submit" style="float: right; margin: 20px 0px 0px;" /> 
        <div class="clear"></div>
       </div>
     </div>
   
    <!--<div> <img src="/images/crpl_1e2.jpg" style="margin:18px 0; width:675px;" width="675"  /> </div>-->
   
    <div class="credit_cols"> 
      <div class="first"> 
        <div> 
          <p class="credit_h">Преимущества</p>
         
          <ul> 
            <li>Переплата от 12% в год</li>
           
            <li>Без комиссий за предоставление и пользование кредитом</li>
           
            <li>Без справок о доходах и поручителей, при наличии одного документа</li>
           
            <li>Оформление заявки на кредит в медицинском Центре</li>
           
            <li>Денежные средства поступают на расчетный счет медицинского учреждения</li>
           </ul>
         </div>
       </div>
     
      <div class="last"> 
        <div> 
          <p class="credit_h">Условия</p>
         
          <ul> 
            <li>Максимальная сумма кредита 300 тыс. руб.</li>
           
            <li>Срок кредитования до 3-х лет</li>
           </ul>
         </div>
       
        <div class="credit_info"> <img src="/images/i-info.png" alt="i" style="float:left;width:17px;height:32px;margin:5px 0 0;" width="17" height="32"  /> 
          <div class="credit_info_text"> 
            <p><em>Информацию по кредитам можно получить <span style="display: block;">у руководителя отдела по работе</span> с ключевыми партнерами:</em></p>
           
            <p><b style="display: block;">Татьяна Сергеевна Антошко</b><span style="display: block;">Тел.: 942-66-13</span><span>(ежедневно с 11.00 до 20.00)</span></p>
           </div>
         </div>
       </div>
     
      <div class="clear"></div>
     </div>
   </article> </div>
 
<div class="clear"></div>
 
<script type="text/javascript">
    function credit(){

        var form = document.getElementById('intanRequest');
        var creditsum = document.getElementById('creditSum');
        var err = document.getElementById('err');

        function getChar(event) {
            if (event.which == null) {
                if (event.keyCode < 32) return null;
                return String.fromCharCode(event.keyCode) 
            }

            if (event.which!=0 && event.charCode!=0) {
                if (event.which < 32) return null;
                return String.fromCharCode(event.which)   
            }

            return null; 
        }

        creditsum.onkeypress = function(e) {
            e = e || event;
            var chr  = getChar(e);

            if (e.ctrlKey || e.altKey || chr == null) return; 
            if (chr < '0' || chr > '9') return false;
        }

        creditsum.oncut = maxFunc;
        creditsum.onkeyup = maxFunc;
        creditsum.oninput = maxFunc;
        creditsum.onpropertychange = function() { 
            event.propertyName == "value" && maxFunc();
        }


        function maxFunc() {
            var sum = +creditsum.value;
            if(sum >= 300001) {
                creditsum.style.color = "#bf3333";
                err.style.display = "block";
            } else {
                creditsum.style.color = "#000";
                err.style.display = "none";
            }
       
           
        }
    }

    //credit();



    (function($){
        $(document).ready(
        function(){
            $('.select').click(function(){
                $(this).parent().find('.options').css('display','block');
            });

            $('#sel_act').click(function(){
                $(this).parent().find('.options').css('display','block');
            });

            $('.options').mouseleave(function(){
                $(this).fadeOut(0);
            });
            $('.options > div').click(function(){
                $(this).closest('.pseudo-select').find('.select').html($(this).html());
                $(this).closest('.pseudo-select').find('input').attr('value', $(this).attr('value'));
                $.each($(this).parent().children('div.check'), function(){
                    $(this).removeClass('check');
                });
                $(this).addClass('check');
                $(this).parent().fadeOut(0);
            });
        }
    );
    })(jQuery);   

    $(function(){
        //show credit form
        $('input[name="show_creditCalc"]').live('click', function(){
            $('#requestWrap').show()
        })
        var inside_credit_form = false
        $('#intanRequest').live('mouseover mouseout', function(event) {
            if (event.type == 'mouseover') {
                inside_credit_form = true
            } else {
                inside_credit_form = false
            }
        });
        
        $("body").mouseup(function(){ 
            if(! inside_credit_form) $('#requestWrap').hide();
        });
        //credit ok msg
        if($('#credit_ok').length!=0){
            $('#requestWrap').show()
            $('#intanRequest').children().hide()
            $('#intanRequest').addClass('thankyou')
            setTimeout(function(){
                $("#intanRequest").removeClass("thankyou")
                $('#intanRequest').children().show()
                $('#requestWrap').hide()
                document.location.href = '/stomatologiya-ceny/lechenie-v-kredit/'
            }, 2000)
        }
        //credit error
        if($('#credit_error').length!=0){
            $('#requestWrap').show()
        }
    })


</script>
 <? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>