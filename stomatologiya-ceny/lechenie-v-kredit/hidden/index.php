<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интан предлагает лечение зубов в кредит");
$APPLICATION->SetTitle("Лечение в кредит");
?>

<!--уууее-->
<div id="requestWrap" style="display:none;">
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:iblock.element.add.form", "zayavka_na_credit", Array(
        "SEF_MODE" => "N",
        "IBLOCK_TYPE" => "on_line",
        "IBLOCK_ID" => "28",
        "PROPERTY_CODES" => array("102", "104", "105", "106", "107", "NAME"),
        "PROPERTY_CODES_REQUIRED" => array('106', 'NAME'),
        "GROUPS" => array("2"),
        "STATUS_NEW" => "N",
        "STATUS" => "ANY",
        "LIST_URL" => "",
        "ELEMENT_ASSOC" => "CREATED_BY",
        "MAX_USER_ENTRIES" => "100000",
        "MAX_LEVELS" => "100000",
        "LEVEL_LAST" => "Y",
        "USE_CAPTCHA" => "N",
        "USER_MESSAGE_EDIT" => "",
        "USER_MESSAGE_ADD" => "",
        "DEFAULT_INPUT_SIZE" => "30",
        "RESIZE_IMAGES" => "N",
        "MAX_FILE_SIZE" => "0",
        "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
        "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
        "CUSTOM_TITLE_NAME" => "",
        "CUSTOM_TITLE_TAGS" => "",
        "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
        "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
        "CUSTOM_TITLE_IBLOCK_SECTION" => "",
        "CUSTOM_TITLE_PREVIEW_TEXT" => "",
        "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
        "CUSTOM_TITLE_DETAIL_TEXT" => "",
        "CUSTOM_TITLE_DETAIL_PICTURE" => ""
            ), false
    );
    ?>
</div>
<!---->










<aside class="left_sidebar"> <?
    $APPLICATION->IncludeComponent(
            "bitrix:menu", "tree1", Array(
        "ROOT_MENU_TYPE" => "top",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => "",
        "MAX_LEVEL" => "2",
        "CHILD_MENU_TYPE" => "podmenu",
        "USE_EXT" => "N",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N"
            )
    );
    ?> <?
    $APPLICATION->IncludeComponent(
            "bitrix:main.include", "", Array(
        "AREA_FILE_SHOW" => "sect",
        "AREA_FILE_SUFFIX" => "inc_banners",
        "AREA_FILE_RECURSIVE" => "Y",
        "EDIT_TEMPLATE" => ""
            )
    );
    ?> </aside> 
<div class="content_part"> <?
    $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb", "template1", Array(
            )
    );
    ?> <article> 
        <h1 class="main_h">Лечение в кредит</h1>
        <img src="/images/kredit.jpg" class="flexible"  /> 
        <div class="credit1"> Сеть стоматологических клиник &laquo;Интан&raquo; предлагает школам Санкт-Петербурга участвовать в Программе «Интан &ndash; школам» (в рамках приоритетного национального проекта «Здоровье») по формированию у школьников культуры ухода за ротовой полостью, повышению мотивации к сохранению своего здоровья путем правильной гигиены и своевременного обнаружения причины болезней полости рта. </div>

        <div class="credit_cols"> 
            <div class="first"> 
                <div> 
                    <p class="credit_h">Преимущества</p>

                    <ul> 
                        <li>Недорогие кредитные программы</li>

                        <li>Без комиссий за предоставление и пользование кредитом</li>

                        <li>Без справок о доходах и поручителей, при наличии одного документа</li>

                        <li>Оформление заявки на кредит в медицинском Центре</li>

                        <li>Денежные средства поступают на расчетный счет медицинского учреждения</li>
                    </ul>
                </div>

                <div> 
                    <p class="credit_h moved">Банки-партнеры</p>

                    <div> <img src="/images/alphabank.png" style="float:left;margin:18px 0 0;" title="Альфа-Банк" alt="Альфа-Банк"  /> <img src="/images/homecredit.png" style="float:left;margin:19px 0 0 34px;" title="Банк Хоум Кредит" alt="Банк Хоум Кредит"  /> 
                        <div class="clear"></div>



                    </div>

                    <input type="button" value="Отправить заявку" name="show_creditCalc" class="green_submit" style="position:relative;left:300px;top:-50px">
                </div>
            </div>
            <div class="last"> 
                <div> 
                    <p class="credit_h">Условия</p>

                    <ul> 
                        <li>Максимальная сумма кредита 200 тыс. руб.</li>

                        <li>Срок кредитования до 3-х лет</li>
                    </ul>
                </div>

                <div class="credit_info"> <img src="/images/i-info.png" alt="i" style="float:left;width:17px;height:32px;margin:5px 0 0;" width="17" height="32"  /> 
                    <div class="credit_info_text"> 
                        <p><em>Информацию по кредитам можно получить <span style="display: block; ">у руководителя отдела по работе</span> с ключевыми партнерами:</em></p>

                        <p><b style="display: block; ">Макаренко Валентина Петровна</b><span style="display: block; ">Тел.: 764-56-68, 942-66-13</span><span>(ежедневно с 11.00 до 20.00)</span></p>
                    </div>
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <br />

        <div> <img src="/images/crpl_1e2.jpg" style="float:left;margin:18px 0 0; width:675px;" alt="" width="675"  /> 
            <div class="clear"></div>
        </div>
    </article></div>

<div class="clear"></div>





<script type="text/javascript">
    function credit(){

        var form = document.getElementById('intanRequest');
        var creditsum = document.getElementById('creditSum');
        var err = document.getElementById('err');

        function getChar(event) {
            if (event.which == null) {
                if (event.keyCode < 32) return null;
                return String.fromCharCode(event.keyCode) 
            }

            if (event.which!=0 && event.charCode!=0) {
                if (event.which < 32) return null;
                return String.fromCharCode(event.which)   
            }

            return null; 
        }

        creditsum.onkeypress = function(e) {
            e = e || event;
            var chr  = getChar(e);

            if (e.ctrlKey || e.altKey || chr == null) return; 
            if (chr < '0' || chr > '9') return false;
        }

        creditsum.oncut = maxFunc;
        creditsum.onkeyup = maxFunc;
        creditsum.oninput = maxFunc;
        creditsum.onpropertychange = function() { 
            event.propertyName == "value" && maxFunc();
        }


        function maxFunc() {
            var sum = +creditsum.value;
            if(sum >= 20001) {
                creditsum.style.color = "#bf3333";
                err.style.display = "block";
            } else {
                creditsum.style.color = "#000";
                err.style.display = "none";
            }
       
           
        }
    }

    credit();



    (function($){
        $(document).ready(
        function(){
            $('.select').click(function(){
                $(this).parent().find('.options').css('display','block');
            });

            $('#sel_act').click(function(){
                $(this).parent().find('.options').css('display','block');
            });

            $('.options').mouseleave(function(){
                $(this).fadeOut(0);
            });
            $('.options > div').click(function(){
                $(this).closest('.pseudo-select').find('.select').html($(this).html());
                $(this).closest('.pseudo-select').find('input').attr('value', $(this).attr('value'));
                $.each($(this).parent().children('div.check'), function(){
                    $(this).removeClass('check');
                });
                $(this).addClass('check');
                $(this).parent().fadeOut(0);
            });
        }
    );
    })(jQuery);   

    $(function(){
        //show credit form
        $('input[name="show_creditCalc"]').live('click', function(){
            $('#requestWrap').show()
        })
        var inside_credit_form = false
        $('#intanRequest').live('mouseover mouseout', function(event) {
            if (event.type == 'mouseover') {
                inside_credit_form = true
            } else {
                inside_credit_form = false
            }
        });
        
        $("body").mouseup(function(){ 
            if(! inside_credit_form) $('#requestWrap').hide();
        });
        //credit ok msg
        if($('#credit_ok').length!=0){
            $('#requestWrap').show()
            $('#intanRequest').children().hide()
            $('#intanRequest').addClass('thankyou')
            setTimeout(function(){
                $("#intanRequest").removeClass("thankyou")
                $('#intanRequest').children().show()
                $('#requestWrap').hide()
            }, 2000)
        }
        //credit error
        if($('#credit_error').length!=0){
            $('#requestWrap').show()
        }
    })


</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>