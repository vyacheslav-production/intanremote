<section class="price_section price_book" data-select="4">
  <h1>Ортодонтия</h1>
 
  <div class="price_subsec"> 
    <ul> 
      <li> 
        <p class="right">390<span class="rouble">i</span></p>
       
        <div>Консультация врача-ортодонта</div>
       </li>
     
      <li> 
        <p class="right">440<span class="rouble">i</span></p>
       
        <div>Профилактический осмотр</div>
       </li>
     
      <li> 
        <p class="right">2420<span class="rouble">i</span></p>
       
        <div>Расчет моделей и телерентгенограмм</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Съемная аппаратура</h2>
   
    <ul> 
      <li> 
        <p class="right">9350<span class="rouble">i</span></p>
       
        <div>Ортодонтические одночелюстные аппараты</div>
       </li>
     
      <li> 
        <p class="right">16390<span class="rouble">i</span></p>
       
        <div>Ортодонтический аппарат Андрезена-Гойпля</div>
       </li>
     
      <li> 
        <p class="right">18150<span class="rouble">i</span></p>
       
        <div>Ортодонтический аппарат Френкеля</div>
       </li>
     
      <li> 
        <p class="right">17050<span class="rouble">i</span></p>
       
        <div>Ортодонтический аппарат Pendulum</div>
       </li>
     
      <li> 
        <p class="right">12100<span class="rouble">i</span></p>
       
        <div>Ортодонтический аппарат Дерихсвайлера</div>
       </li>
     
      <li> 
        <p class="right">980<span class="rouble">i</span></p>
       
        <div>Небный бюгель Гожгарина</div>
       </li>
     
      <li> 
        <p class="right">14850<span class="rouble">i</span></p>
       
        <div>Аппарат для дисталяции моляров</div>
       </li>
     
      <li> 
        <p class="right">18480<span class="rouble">i</span></p>
       
        <div>Аппарат Herbst</div>
       </li>
     
      <li> 
        <p class="right">8250<span class="rouble">i</span></p>
       
        <div>Трейнер система</div>
       </li>
     
      <li> 
        <p class="right">8800<span class="rouble">i</span></p>
       
        <div>Система Миобрейс</div>
       </li>
     
      <li> 
        <p class="right">8800<span class="rouble">i</span></p>
       
        <div>LM активатор</div>
       </li>
     
      <li> 
        <p class="right">880<span class="rouble">i</span></p>
       
        <div>Активация одночелюстного ортодонтического аппарата</div>
       </li>
     
      <li> 
        <p class="right">1100<span class="rouble">i</span></p>
       
        <div>Активация двучелюстного ортодонтического аппарата </div>
       </li>
     
      <li> 
        <p class="right">1650<span class="rouble">i</span></p>
       
        <div>Починка ортодонтического аппарата</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Несъемная аппаратура</h2>
   
    <ul> 
      <li> 
        <div> 
          <p>Брекет-системы (цена указана за установку системы на одну челюсть)</p>
         
          <p>Металлические:</p>
         </div>
       
        <ul> 
          <li> 
            <p class="right">24750<span class="rouble">i</span></p>
           
            <div>Mini Master (США)</div>
           </li>
         
          <li> 
            <p class="right">27500<span class="rouble">i</span></p>
           
            <div>Ormco (США)</div>
           </li>
         
          <li> 
            <p class="right">27500<span class="rouble">i</span></p>
           
            <div>Empower (США)</div>
           </li>
         
          <li> 
            <p class="right">29700<span class="rouble">i</span></p>
           
            <div>Victory ЗМ Unitек (США)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">16500<span class="rouble">i</span></p>
       
        <div>Установка частичной металлической брекет-системы на одну челюсть</div>
       </li>
     
      <li> 
        <div> 
          <p>Сапфировые, керамические:</p>
         </div>
       
        <ul> 
          <li> 
            <p class="right">40700<span class="rouble">i</span></p>
           
            <div>Radiance (США)</div>
           </li>
         
          <li> 
            <p class="right">45650<span class="rouble">i</span></p>
           
            <div>Ormco (США)</div>
           </li>
         
          <li> 
            <p class="right">39600<span class="rouble">i</span></p>
           
            <div>Damon 3 (США)</div>
           </li>
         
          <li> 
            <p class="right">39600<span class="rouble">i</span></p>
           
            <div>Damon Q (США)</div>
           </li>
         
          <li> 
            <p class="right">39600<span class="rouble">i</span></p>
           
            <div>Empower Clear (США)</div>
           </li>
         
          <li> 
            <p class="right">49500<span class="rouble">i</span></p>
           
            <div>Damon Clear (США)</div>
           </li>
         
          <li> 
            <p class="right">46200<span class="rouble">i</span></p>
           
            <div>Clarity ЗМ Unitек (США)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div> 
          <p>Лингвальные:</p>
         </div>
       
        <ul> 
          <li> 
            <p class="right">180290<span class="rouble">i</span></p>
           
            <div>Incognito (Германия)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">21450<span class="rouble">i</span></p>
       
        <div>Установка частичной сапфировой, керамической брекет-системы на одну челюсть </div>
       </li>
     
      <li> 
        <p class="right">2200<span class="rouble">i</span></p>
       
        <div>Фиксация брекета при заболеваниях парадонта (цена за единицу)</div>
       </li>
     
      <li> 
        <p class="right">660<span class="rouble">i</span></p>
       
        <div>Фиксация брекета повторная при отрыве</div>
       </li>
     
      <li> 
        <p class="right">990<span class="rouble">i</span></p>
       
        <div>Фиксация нового металлического брекета Mini Master (Германия)</div>
       </li>
     
      <li> 
        <p class="right">990<span class="rouble">i</span></p>
       
        <div>Фиксация нового металлического брекета Ormco (США)</div>
       </li>
     
      <li> 
        <p class="right">1650<span class="rouble">i</span></p>
       
        <div>Фиксация нового сапфирового, керамического брекета Radiance (США)</div>
       </li>
     
      <li> 
        <p class="right">2200<span class="rouble">i</span></p>
       
        <div>Фиксация нового сапфирового, керамического брекета Ormco (США)</div>
       </li>
     
      <li> 
        <p class="right">1650<span class="rouble">i</span></p>
       
        <div>Фиксация нового сапфирового, керамического брекета Damon 3 (США)</div>
       </li>
     
      <li> 
        <p class="right">1650<span class="rouble">i</span></p>
       
        <div>Фиксация нового сапфирового, керамического брекета Damon Q (США)</div>
       </li>
<li> 
        <p class="right">1430<span class="rouble">i</span></p>
       
        <div>Фиксация нового сапфирового, керамического брекета Damon  3MX (США) </div>
       </li>
     
      <li> 
        <p class="right">2750<span class="rouble">i</span></p>
       
        <div>Фиксация нового сапфирового, керамического брекета Damon Clear (США)</div>
       </li>
     
      <li> 
        <p class="right">2200<span class="rouble">i</span></p>
       
        <div>Фиксация нового сапфирового, керамического брекета Clarity ЗМ Unitек (США) </div>
       </li>
     
      <li> 
        <p class="right">880<span class="rouble">i</span></p>
       
        <div>Фиксация кнопки</div>
       </li>
     
      <li> 
        <p class="right">1320<span class="rouble">i</span></p>
       
        <div>Фиксация замка Эндрюса</div>
       </li>
     
      <li> 
        <p class="right">1870<span class="rouble">i</span></p>
       
        <div>Фиксация замка саморегулирующего</div>
       </li>
     
      <li> 
        <p class="right">1160<span class="rouble">i</span></p>
       
        <div>Фиксация кольца</div>
       </li>
     
      <li> 
        <p class="right">1320<span class="rouble">i</span></p>
       
        <div>Активация металлической брекет-системы (одна челюсть)</div>
       </li>
     
      <li> 
        <p class="right">1870<span class="rouble">i</span></p>
       
        <div>Активация сапфировой, керамической брекет-системы (одна челюсть)</div>
       </li>
     
      <li> 
        <p class="right">2750<span class="rouble">i</span></p>
       
        <div>Активация лингвальной брекет-системы (одна челюсть)</div>
       </li>
     
      <li> 
        <p class="right">990<span class="rouble">i</span></p>
       
        <div>Активация брекет-системы Ormco (две челюсти)</div>
       </li>
     
      <li> 
        <p class="right">3300<span class="rouble">i</span></p>
       
        <div>Снятие брекет-системы (одна челюсть)</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <ul> 
      <li> 
        <p class="right">4950<span class="rouble">i</span></p>
       
        <div>Установка несъемного ретейнера (на одну челюсть)</div>
       </li>
     
      <li> 
        <p class="right">5500<span class="rouble">i</span></p>
       
        <div>Установка литого несъемного ретейнера (одна челюсть)</div>
       </li>
     
      <li> 
        <p class="right">8800<span class="rouble">i</span></p>
       
        <div>Установка ретейнера (съемного)</div>
       </li>
     
      <li> 
        <p class="right">3030<span class="rouble">i</span></p>
       
        <div>Снятие ретейнера (одна челюсть)</div>
       </li>
     
      <li> 
        <p class="right">1320<span class="rouble">i</span></p>
       
        <div>Повторная фиксация несъемного ретейнера (на одну челюсть)</div>
       </li>
     
      <li> 
        <p class="right">13750<span class="rouble">i</span></p>
       
        <div>Микроимплант ортодонтический</div>
       </li>
     
      <li> 
        <p class="right">880<span class="rouble">i</span></p>
       
        <div>Активация микроимпланта</div>
       </li>
     
      <li> 
        <p class="right">880<span class="rouble">i</span></p>
       
        <div>Кольца на моляры</div>
       </li>
     
      <li> 
        <p class="right">5500<span class="rouble">i</span></p>
       
        <div>Лицевая дуга</div>
       </li>
     
      <li> 
        <p class="right">720<span class="rouble">i</span></p>
       
        <div>Шейная повязка</div>
       </li>
     
      <li> 
        <p class="right">720<span class="rouble">i</span></p>
       
        <div>Головная повязка</div>
       </li>
     
      <li> 
        <p class="right">4400<span class="rouble">i</span></p>
       
        <div>Губной бампер</div>
       </li>
     
      <li> 
        <p class="right">2200<span class="rouble">i</span></p>
       
        <div>Силовые модули с шейной тягой</div>
       </li>
     
      <li> 
        <p class="right">9900<span class="rouble">i</span></p>
       
        <div>Лицевая маска</div>
       </li>
     
      <li> 
        <p class="right">550<span class="rouble">i</span></p>
       
        <div>Пенал для хранения пластинки</div>
       </li>
     
      <li> 
        <p class="right">880<span class="rouble">i</span></p>
       
        <div>Изготовление диагностических моделей (2 штуки)</div>
       </li>
     
      <li> 
        <p class="right">770<span class="rouble">i</span></p>
       
        <div>Замена кламмера</div>
       </li>
     
      <li> 
        <p class="right">440<span class="rouble">i</span></p>
       
        <div>Замена вестибулярной дуги</div>
       </li>
     
      <li> 
        <p class="right">280<span class="rouble">i</span></p>
       
        <div>Кольца эластичные</div>
       </li>
     
      <li> 
        <p class="right">440<span class="rouble">i</span></p>
       
        <div>Воск защитный</div>
       </li>
     
      <li> 
        <p class="right">610<span class="rouble">i</span></p>
       
        <div>Снятие слепков альгинатной массой (за единицу)</div>
       </li>
     
      <li> 
        <p class="right">660<span class="rouble">i</span></p>
       
        <div>Снятие слепков силиконовой массой (за единицу)</div>
       </li>
     
      <li> 
        <p class="right">1650<span class="rouble">i</span></p>
       
        <div>Пломба для временного завышения прикуса</div>
       </li>
     
      <li> 
        <p class="right">1650<span class="rouble">i</span></p>
       
        <div>Починка ортодонтического аппарата</div>
       </li>
     
      <li> 
        <p class="right">6490<span class="rouble">i</span></p>
       
        <div>Преортодонтический трейнер</div>
       </li>
     
      <li> 
        <p class="right">12100<span class="rouble">i</span></p>
       
        <div>Аппарат Дайрекс-Вайлера</div>
       </li>
     </ul>
   </div>
	<p class="roll"><span>развернуть список</span></p>
 </section>