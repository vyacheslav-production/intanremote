<section class="price_section price_book" data-select="9">
  <h1>Детская стоматология</h1>
 
  <div class="price_subsec"> 
    <ul> 
      <li> 
        <p class="right">390<span class="rouble">i</span></p>
       
        <div>Консультация врача стоматолога</div>
       </li>
     
      <li> 
        <p class="right">100<span class="rouble">i</span></p>
       
        <div>Анестезия аппликационная </div>
       </li>
     
      <li> 
        <p class="right">370<span class="rouble">i</span></p>
       
        <div>Анестезия</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Кариес молочных зубов</h2>
   
    <ul> 
      <li> 
        <p class="right">500<span class="rouble">i</span></p>
       
        <div>Обработка кариозной полости (дет.)</div>
       </li>
     
      <li> 
        <p class="right">460<span class="rouble">i</span></p>
       
        <div>Подкладка лечебная</div>
       </li>
     
      <li> 
        <p class="right">460<span class="rouble">i</span></p>
       
        <div>Подкладка изолирующая</div>
       </li>
     
      <li> 
        <div>Пломба (стеклоиномерный цемент) Vitremer</div>
       
        <ul> 
          <li> 
            <p class="right">900<span class="rouble">i</span></p>
           
            <div>1 поверхность</div>
           </li>
         
          <li> 
            <p class="right">1100<span class="rouble">i</span></p>
           
            <div>2 поверхности</div>
           </li>
         
          <li> 
            <p class="right">1570<span class="rouble">i</span></p>
           
            <div>3 поверхности</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>Пломба (цветная) Twinky Star</div>
       
        <ul> 
          <li> 
            <p class="right">1210<span class="rouble">i</span></p>
           
            <div>1 поверхность</div>
           </li>
         
          <li> 
            <p class="right">1450<span class="rouble">i</span></p>
           
            <div>2 поверхности</div>
           </li>
         
          <li> 
            <p class="right">1650<span class="rouble">i</span></p>
           
            <div>3 поверхности</div>
           </li>
         </ul>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Лечение пульпита молочного зуба, пульпита и периодонтитов постоянных зубов</h2>
   
    <ul> 
      <li> 
        <p class="right">610<span class="rouble">i</span></p>
       
        <div>Обработка кариозной полости (дет. пульпит)</div>
       </li>
     
      <li> 
        <p class="right">500<span class="rouble">i</span></p>
       
        <div>Трепанация зуба</div>
       </li>
     
      <li> 
        <p class="right">390<span class="rouble">i</span></p>
       
        <div>Ампутация пульпы и экстирпация из каналов</div>
       </li>
     
      <li> 
        <p class="right">760<span class="rouble">i</span></p>
       
        <div>Наложение девитализирующих паст</div>
       </li>
     
      <li> 
        <p class="right">440<span class="rouble">i</span></p>
       
        <div>Удаление распада пульпы из корневых каналов</div>
       </li>
     
      <li> 
        <div>Эндодонтическая обработка:</div>
       
        <ul> 
          <li> 
            <p class="right">940<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">1430<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">1930<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">2260<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>Пломбировка каналов с гуттаперчевыми штифтами и пломбировочным материалом</div>
       
        <ul> 
          <li> 
            <p class="right">900<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">1270<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">1430<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">1650<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">510<span class="rouble">i</span></p>
       
        <div>Подкладка изолирующая (дет. пульпит, периодонтит) </div>
       </li>
     
      <li> 
        <p class="right">500<span class="rouble">i</span></p>
       
        <div>Пломба временная (дет. пульпит, периодонтит)</div>
       </li>
     
      <li> 
        <div>Пломба (стеклоиномерный цемент) Vitremer</div>
       
        <ul> 
          <li> 
            <p class="right">900<span class="rouble">i</span></p>
           
            <div>1 поверхность</div>
           </li>
         
          <li> 
            <p class="right">1100<span class="rouble">i</span></p>
           
            <div>2 поверхности</div>
           </li>
         
          <li> 
            <p class="right">1570<span class="rouble">i</span></p>
           
            <div>3 поверхности</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>Пломба (цветная) Twinky Star</div>
       
        <ul> 
          <li> 
            <p class="right">1210<span class="rouble">i</span></p>
           
            <div>1 поверхность</div>
           </li>
         
          <li> 
            <p class="right">1450<span class="rouble">i</span></p>
           
            <div>2 поверхности</div>
           </li>
         
          <li> 
            <p class="right">1650<span class="rouble">i</span></p>
           
            <div>3 поверхности</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>Временная пломбировка</div>
       
        <ul> 
          <li> 
            <p class="right">500<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">790<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">1100<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">1430<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>2 посещение: Эндодонтическая обработка</div>
       
        <ul> 
          <li> 
            <p class="right">440<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">610<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">830<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">990<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Детская хирургия</h2>
   
    <ul> 
      <li> 
        <p class="right">610<span class="rouble">i</span></p>
       
        <div>Удаление молочного зуба с рассосавшимися корнями</div>
       </li>
     
      <li> 
        <p class="right">990<span class="rouble">i</span></p>
       
        <div>Удаление молочного зуба с нерассосавшимися корнями</div>
       </li>
     
      <li> 
        <p class="right">1540<span class="rouble">i</span></p>
       
        <div>Удаление постоянного зуба</div>
       </li>
     
      <li> 
        <p class="right">3030<span class="rouble">i</span></p>
       
        <div>Операция коррекции уздечки губы или языка</div>
       </li>
     
      <li> 
        <p class="right">170<span class="rouble">i</span></p>
       
        <div>Кюретаж лунки зуба</div>
       </li>
     
      <li> 
        <p class="right">240<span class="rouble">i</span></p>
       
        <div>Лечение альвеолита</div>
       </li>
     
      <li> 
        <p class="right">240<span class="rouble">i</span></p>
       
        <div>Остановка кровотечения</div>
       </li>
     
      <li> 
        <p class="right">240<span class="rouble">i</span></p>
       
        <div>Перевязка</div>
       </li>
     
      <li> 
        <p class="right">290<span class="rouble">i</span></p>
       
        <div>Разрез</div>
       </li>
     
      <li> 
        <p class="right">580<span class="rouble">i</span></p>
       
        <div>Иссечение капюшона</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Прочие манипуляции</h2>
   
    <ul> 
      <li> 
        <p class="right">150<span class="rouble">i</span></p>
       
        <div>Полировка одного зуба</div>
       </li>
     
      <li> 
        <p class="right">280<span class="rouble">i</span></p>
       
        <div>Серебрение одного зуба</div>
       </li>
     
      <li> 
        <p class="right">720<span class="rouble">i</span></p>
       
        <div>Герметизация фиссур одного молочного зуба</div>
       </li>
     
      <li> 
        <p class="right">980<span class="rouble">i</span></p>
       
        <div>Герметизация фиссур одного постоянного зуба</div>
       </li>
     
      <li> 
        <p class="right">330<span class="rouble">i</span></p>
       
        <div>Обучение гигиене полости рта</div>
       </li>
     
      <li> 
        <p class="right">130<span class="rouble">i</span></p>
       
        <div>Снятие зубных отложений ультразвуком (одного зуба)</div>
       </li>
     
      <li> 
        <p class="right">70<span class="rouble">i</span></p>
       
        <div>Покрытие поверхности одного зуба фторлаком</div>
       </li>
     
      <li> 
        <p class="right">60<span class="rouble">i</span></p>
       
        <div>Ретракционная нить (за зуб)</div>
       </li>
     
      <li> 
        <p class="right">1650<span class="rouble">i</span></p>
       
        <div>Компенсация потраченного времени в связи с психоэмоциональным состоянием ребенка</div>
       </li>
     
      <li> 
        <p class="right">1050<span class="rouble">i</span></p>
       
        <div>Использование препарата &quot;Цинкоксидэвгеноловая паста без формальдегида&quot; 1 канала</div>
       </li>
     
      <li> 
        <p class="right">1480<span class="rouble">i</span></p>
       
        <div>Использование препарата &quot;Цинкоксидэвгеноловая паста без формальдегида&quot; 2 канала</div>
       </li>
     
      <li> 
        <p class="right">1650<span class="rouble">i</span></p>
       
        <div>Использование препарата &quot;Цинкоксидэвгеноловая паста без формальдегида&quot; 3 канала</div>
       </li>
     
      <li> 
        <p class="right">1870<span class="rouble">i</span></p>
       
        <div>Использование препарата &quot;Цинкоксидэвгеноловая паста без формальдегида&quot; 4 канала</div>
       </li>
     
      <li> 
        <p class="right">170<span class="rouble">i</span></p>
       
        <div>Использование препарата Глуфторэд 1 зуб</div>
       </li>
     </ul>
   </div>
	<p class="roll"><span>развернуть список</span></p>
 </section>