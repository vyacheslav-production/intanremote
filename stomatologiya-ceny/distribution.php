<section class="price_section price_book">
  <h1>Дистрибьюция</h1>
 
  <div class="price_subsec"> 
    <h2>Продукция &laquo;Valplast&raquo;</h2>
   
    <ul> 
      <li> 
        <p class="right">1330<span class="rouble">i</span></p>
       
        <div>Ванночка ультразвуковая ValClean для чистки съемных протезов</div>
       </li>
     
      <li> 
        <p class="right">230<span class="rouble">i</span></p>
       
        <div>Таблетки для очистки съемных протезов DENTUR 6 штук</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Продукция «РОКС»</h2>
   
    <ul> 
      <li> 
        <p class="right">210<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС Кофе и Табак&quot; 74 гр</div>
       </li>
     
      <li> 
        <p class="right">210<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС Грейпфрут и Мята&quot; 74 гр</div>
       </li>
     
      <li> 
        <p class="right">210<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС Малина&quot; 74 гр</div>
       </li>
     
      <li> 
        <p class="right">210<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС Манго и Банан для чувствительных зубов&quot; 74 гр</div>
       </li>
     
      <li> 
        <p class="right">210<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС Вкус наслаждения. Шоколад и Мята&quot; 74 гр</div>
       </li>
     
      <li> 
        <p class="right">210<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС Бионика&quot; 74 гр</div>
       </li>
     
      <li> 
        <p class="right">210<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС Антитабак&quot; 74 гр</div>
       </li>
     
      <li> 
        <p class="right">190<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС Baby. Нежный уход. Душистая Ромашка&quot; 45 гр</div>
       </li>
     
      <li> 
        <p class="right">190<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС для детей Малина и Клубника&quot; 45 гр</div>
       </li>
     
      <li> 
        <p class="right">190<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС для детей Бабл Гам&quot; 45 гр</div>
       </li>
     
      <li> 
        <p class="right">190<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС для детей Фруктовый рожок (без фтора)&quot; 45 гр</div>
       </li>
     
      <li> 
        <p class="right">190<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС для школьников Кола и Лимон&quot; 74 гр</div>
       </li>
     
      <li> 
        <p class="right">190<span class="rouble">i</span></p>
       
        <div>з/п &quot;РОКС для школьников Земляника&quot; 74 гр</div>
       </li>
     
      <li> 
        <p class="right">100<span class="rouble">i</span></p>
       
        <div>з/щ &quot;Джордан для детей от 0 до 2 лет &quot;Шаг за шагом 1&quot;</div>
       </li>
     
      <li> 
        <p class="right">100<span class="rouble">i</span></p>
       
        <div>з/щ &quot;Джордан для детей от 3 до 5 лет &quot;Шаг за шагом 2&quot; </div>
       </li>
     
      <li> 
        <p class="right">100<span class="rouble">i</span></p>
       
        <div>з/щ &quot;Джордан для детей от 6 до 8 лет &quot;Шаг за шагом 3&quot; </div>
       </li>
     
      <li> 
        <p class="right">120<span class="rouble">i</span></p>
       
        <div>з/щ &quot;Джордан Клик для школьников&quot;</div>
       </li>
     
      <li> 
        <p class="right">300<span class="rouble">i</span></p>
       
        <div>Гель для укрепления зубов &quot;РОКС МЕДИКАЛ Минерале&quot; 40 мл </div>
       </li>
     
      <li> 
        <p class="right">250<span class="rouble">i</span></p>
       
        <div>Фиксирующий крем для зубных протезов «BONYplus Superior»</div>
       </li>
     
      <li> 
        <p class="right">160<span class="rouble">i</span></p>
       
        <div>Зубная нить (флосс) &quot;РОКС&quot; 50м</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Продукция «Colgate»</h2>
   
    <ul> 
      <li> 
        <p class="right">50<span class="rouble">i</span></p>
       
        <div>з/п Colgate Доктор заяц 50мл клубника</div>
       </li>
     
      <li> 
        <p class="right">50<span class="rouble">i</span></p>
       
        <div>з/п Colgate Доктор заяц 50мл жвачка</div>
       </li>
     
      <li> 
        <p class="right">150<span class="rouble">i</span></p>
       
        <div>з/п Colgate Sensitive Pro-relif 50 мл</div>
       </li>
     
      <li> 
        <p class="right">150<span class="rouble">i</span></p>
       
        <div>з/п Colgate Sensitive Pro-relif отбеливающая 50 мл</div>
       </li>
     
      <li> 
        <p class="right">150<span class="rouble">i</span></p>
       
        <div>з/п Colgate Sensitive Pro-relif восстанавление эмали 50 мл</div>
       </li>
     
      <li> 
        <p class="right">190<span class="rouble">i</span></p>
       
        <div>з/п Colgate Optic White New 50 мл</div>
       </li>
     
      <li> 
        <p class="right">120<span class="rouble">i</span></p>
       
        <div>з/п Colgate Total 12 Профессиональная для чувствительных 100 мл</div>
       </li>
     
      <li> 
        <p class="right">120<span class="rouble">i</span></p>
       
        <div>з/п Colgate Total 12 Прополис 100 мл</div>
       </li>
     
      <li> 
        <p class="right">60<span class="rouble">i</span></p>
       
        <div>з/п Colgate Total 12 Профессиональная Чистка 50мл</div>
       </li>
     
      <li> 
        <p class="right">160<span class="rouble">i</span></p>
       
        <div>з/п Элмекс Детская 50мл</div>
       </li>
     
      <li> 
        <p class="right">160<span class="rouble">i</span></p>
       
        <div>з/п Элмекс Юниор 75мл</div>
       </li>
     
      <li> 
        <p class="right">160<span class="rouble">i</span></p>
       
        <div>з/п Элмекс Защита от кариеса 75мл</div>
       </li>
     
      <li> 
        <p class="right">180<span class="rouble">i</span></p>
       
        <div>з/п Элмекс Сенситив Плюс 75мл</div>
       </li>
     
      <li> 
        <p class="right">210<span class="rouble">i</span></p>
       
        <div>з/щ Colgate 360 с/чист мягкая</div>
       </li>
     
      <li> 
        <p class="right">130<span class="rouble">i</span></p>
       
        <div>з/щ Colgate OPTO мягкая</div>
       </li>
     
      <li> 
        <p class="right">30<span class="rouble">i</span></p>
       
        <div>з/щ Colgate Экстра чистота</div>
       </li>
     
      <li> 
        <p class="right">380<span class="rouble">i</span></p>
       
        <div>з/щ Colgate электро 360 Sonic Power</div>
       </li>
     
      <li> 
        <p class="right">200<span class="rouble">i</span></p>
       
        <div>з/щ Элмекс Детская</div>
       </li>
     
      <li> 
        <p class="right">200<span class="rouble">i</span></p>
       
        <div>з/щ Элмекс Юниор</div>
       </li>
     
      <li> 
        <p class="right">150<span class="rouble">i</span></p>
       
        <div>Ополаскиватель Элмекс Защита от кариеса 400мл</div>
       </li>
     
      <li> 
        <p class="right">190<span class="rouble">i</span></p>
       
        <div>Ополаскиватель Элмекс Сенситив Плюс 400мл</div>
       </li>
     
      <li> 
        <p class="right">280<span class="rouble">i</span></p>
       
        <div>Ополаскиватель Colgate Optic White 250 мл</div>
       </li>
     
      <li> 
        <p class="right">320<span class="rouble">i</span></p>
       
        <div>Ополаскиватель Colgate Sensitive Pro-relif 400 мл</div>
       </li>
     
      <li> 
        <p class="right">130<span class="rouble">i</span></p>
       
        <div>Colgate Зубная нить Optic White</div>
       </li>
     
      <li> 
        <p class="right">130<span class="rouble">i</span></p>
       
        <div>Colgate Зубная лента</div>
       </li>
     
      <li> 
        <p class="right">180<span class="rouble">i</span></p>
       
        <div>Colgate Ершики 2 мл.</div>
       </li>
     
      <li> 
        <p class="right">180<span class="rouble">i</span></p>
       
        <div>Colgate Ершики набор</div>
       </li>
     
      <li> 
        <p class="right">130<span class="rouble">i</span></p>
       
        <div>Colgate Орто</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Средства для ухода за полостью рта CURADEN</h2>
   
    <ul> 
      <li> 
        <p class="right">590<span class="rouble">i</span></p>
       
        <div>Гель д/дезинфекции протезов &quot;Daily&quot; для ежедневного ухода</div>
       </li>
     
      <li> 
        <p class="right">590<span class="rouble">i</span></p>
       
        <div>Держатель алюминевый односторонний</div>
       </li>
     
      <li> 
        <p class="right">660<span class="rouble">i</span></p>
       
        <div>Набор &quot;дорожный&quot; (держатель с 4 ершиками)</div>
       </li>
     
      <li> 
        <p class="right">560<span class="rouble">i</span></p>
       
        <div>Ершик межзубный &quot;prime&quot; (0.6, 0.7, 0.8, 0.9, 1.1 мм)</div>
       </li>
     
      <li> 
        <p class="right">540<span class="rouble">i</span></p>
       
        <div>Ершик межзубный &quot;regular&quot; (1.0, 1.1, 1.2, 1.4, 1.5, 1.8 мм)</div>
       </li>
     
      <li> 
        <p class="right">540<span class="rouble">i</span></p>
       
        <div>Ершик межзубный &quot;strong &amp; implant&quot;, 2.8 мм</div>
       </li>
     
      <li> 
        <p class="right">720<span class="rouble">i</span></p>
       
        <div>Жидкость для полоскания полости рта, 0.05 % хлоргексидина (200 мл)</div>
       </li>
     
      <li> 
        <p class="right">720<span class="rouble">i</span></p>
       
        <div>Жидкость для полоскания полости рта, 0.12 % хлоргексидина (200 мл)</div>
       </li>
     
      <li> 
        <p class="right">720<span class="rouble">i</span></p>
       
        <div>Жидкость для полоскания полости рта, 0.2 % хлоргексидина (200 мл)</div>
       </li>
     
      <li> 
        <p class="right">550<span class="rouble">i</span></p>
       
        <div>Зубная паста гелеобразная, 0.05 % хлоргексидина (75 мл)</div>
       </li>
     
      <li> 
        <p class="right">550<span class="rouble">i</span></p>
       
        <div>Зубная паста гелеобразная, 0.12 % хлоргексидина (75 мл)</div>
       </li>
     
      <li> 
        <p class="right">550<span class="rouble">i</span></p>
       
        <div>Зубная паста гелеобразная, 0.2 % хлоргексидина (75 мл)</div>
       </li>
     
      <li> 
        <p class="right">610<span class="rouble">i</span></p>
       
        <div>Контейнер для протезов, красный</div>
       </li>
     
      <li> 
        <p class="right">280<span class="rouble">i</span></p>
       
        <div>Таблетки для индикации зубного налета (10 шт.)</div>
       </li>
     
      <li> 
        <p class="right">350<span class="rouble">i</span></p>
       
        <div>Щетка зубная &quot;soft&quot;, d 0,15мм</div>
       </li>
     
      <li> 
        <p class="right">880<span class="rouble">i</span></p>
       
        <div>Щетка зубная &quot;soft&quot;, d 0,15 мм (3шт.)</div>
       </li>
     
      <li> 
        <p class="right">350<span class="rouble">i</span></p>
       
        <div>Щетка зубная &quot;supersoft&quot;, d 0,12мм</div>
       </li>
     
      <li> 
        <p class="right">880<span class="rouble">i</span></p>
       
        <div>Щетка зубная &quot;supersoft&quot;, d 0,12 мм (3 шт.)</div>
       </li>
     
      <li> 
        <p class="right">350<span class="rouble">i</span></p>
       
        <div>Щетка &quot;ultrasoft&quot;, d 0,10мм </div>
       </li>
     
      <li> 
        <p class="right">880<span class="rouble">i</span></p>
       
        <div>Щетка &quot;ultrasoft&quot;, d 0,10 мм (3 шт.)</div>
       </li>
     
      <li> 
        <p class="right">260<span class="rouble">i</span></p>
       
        <div>Зубная щетка &quot;ultrasoft&quot; в целофане, d 0,10мм</div>
       </li>
     
      <li> 
        <p class="right">340<span class="rouble">i</span></p>
       
        <div>Щетка монопучковая с длиной пучка (6 мм, 9 мм)</div>
       </li>
     
      <li> 
        <p class="right">390<span class="rouble">i</span></p>
       
        <div>Ортодонтическая щетка &quot;ultrasoft&quot; с углублением</div>
       </li>
     
      <li> 
        <p class="right">330<span class="rouble">i</span></p>
       
        <div>Щетка зубная подростковая</div>
       </li>
     
      <li> 
        <p class="right">600<span class="rouble">i</span></p>
       
        <div>Щетка для ухода за протезом </div>
       </li>
     
      <li> 
        <p class="right">390<span class="rouble">i</span></p>
       
        <div>Щетка хирургическая &quot;mega soft&quot;</div>
       </li>
     
      <li> 
        <p class="right">300<span class="rouble">i</span></p>
       
        <div>Детская зубная щетка Curaprox с гумированной ручкой</div>
       </li>
     </ul>
   </div>
	<p class="roll"><span>развернуть список</span></p>
 </section>