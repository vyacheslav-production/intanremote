<section class="price_section price_book" data-select="7">
  <h1>Отбеливание</h1>
 
  <div class="price_subsec"> 
    <h2>Профессиональная гигиена, профилактика</h2>
   
    <ul> 
      <li> 
        <p class="right">330<span class="rouble">i</span></p>
       
        <div>Обучение гигиене полости рта</div>
       </li>
     
      <li> 
        <p class="right">130<span class="rouble">i</span></p>
       
        <div>Снятие зубных отложений ультразвуком (одного зуба)</div>
       </li>
     
      <li> 
        <p class="right">2810<span class="rouble">i</span></p>
       
        <div>Профессиональная гигиена полости рта (снятие зубных отложений+тонкая чистка Air-Flow) (уровень нормальный)</div>
       </li>
     
      <li> 
        <p class="right">3360<span class="rouble">i</span></p>
       
        <div>Профессиональная гигиена полости рта (снятие зубных отложений+тонкая чистка Air-Flow) (уровень обильный)</div>
       </li>
     
      <li> 
        <p class="right">1160<span class="rouble">i</span></p>
       
        <div>Профессиональная гигиена полости рта - снятие зубных отложений (уровень нормальный)</div>
       </li>
     
      <li> 
        <p class="right">1710<span class="rouble">i</span></p>
       
        <div>Профессиональная гигиена полости рта - снятие зубных отложений (уровень обильный)</div>
       </li>
     
      <li> 
        <p class="right">1650<span class="rouble">i</span></p>
       
        <div>Отбеливание Air-Flow</div>
       </li>
     
      <li> 
        <p class="right">60<span class="rouble">i</span></p>
       
        <div>Покрытие поверхности одного зуба фторлаком</div>
       </li>
     
      <li> 
        <p class="right">370<span class="rouble">i</span></p>
       
        <div>Аппликация бальзамом после снятия зубных отложений</div>
       </li>
     
      <li> 
        <p class="right">150<span class="rouble">i</span></p>
       
        <div>Профессиональная полировка десенситизирующей пастой Colgate&reg; Sensitive Pro-Relief&trade; (один зуб)</div>
       </li>
     
      <li> 
        <p class="right">4070<span class="rouble">i</span></p>
       
        <div>Глубокое фторирование APF система после проведения проф. гигиены</div>
       </li>
     
      <li> 
        <p class="right">17930<span class="rouble">i</span></p>
       
        <div>Отбеливание с помощью лампы &laquo;ZOOM 2&raquo;</div>
       </li>
     
      <li> 
        <p class="right">11610<span class="rouble">i</span></p>
       
        <div>Отбеливание с помощью системы Opalescence Xtra Boost</div>
       </li>
     
      <li> 
        <p class="right">8030<span class="rouble">i</span></p>
       
        <div>Отбеливающая система «DISCUS DENTAL» (дневная)</div>
       </li>
     
      <li> 
        <p class="right">8470<span class="rouble">i</span></p>
       
        <div>Отбеливающая система «DISCUS DENTAL» (ночная)</div>
       </li>
     
      <li> 
        <p class="right">720<span class="rouble">i</span></p>
       
        <div>Каппа для снятия чувствительности после отбеливания стандартная (за шт.)</div>
       </li>
     
      <li> 
        <p class="right">2050<span class="rouble">i</span></p>
       
        <div>Каппа для снятия чувствительности после отбеливания индивидуальная (за шт.)</div>
       </li>
     
      <li> 
        <p class="right">2670<span class="rouble">i</span></p>
       
        <div>Украшения для зубов «Skyce»</div>
       </li>
     </ul>
   </div>
 </section>