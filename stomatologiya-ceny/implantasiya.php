<section class="price_section price_book activeCategory" data-select="1">
  <h1>Имплантация</h1>

  <div class="price_subsec">
    <h2>Зубная имплантация</h2>

    <ul>
      <li>
        <div>Операция по установке одноэтапного имплантата (включая стоимость имплантата)</div>

        <ul>
          <li>
            <p class="right">10770<span class="rouble">i</span></p>

            <div>Mини имплантат &quot;C-TECH&quot; (Италия) за единицу </div>
           </li>

          <li>
            <p class="right">30690<span class="rouble">i</span></p>

            <div>Mини имплантат &quot;C-TECH&quot; (Италия) (3 ед.) </div>
           </li>

          <li>
            <p class="right">50070<span class="rouble">i</span></p>

            <div>Mини имплантат &quot;C-TECH&quot; (Италия) (5 ед.) </div>
           </li>
         </ul>
       </li>

      <li>
        <div>Операция по установке двухэтапного имплантата (включая стоимость имплантата)</div>

        <ul>
          <li>
            <p class="right">14500<span class="rouble">i</span></p>

            <div>&laquo;&quot;Swell&quot; производства MSI&raquo; ADIN (Израиль) за единицу</div>
           </li>

          <li>
            <p class="right">14500<span class="rouble">i</span></p>

            <div>«&quot;Touareg&quot; производства MSI» ADIN (Израиль) за единицу</div>
           </li>

          <li>
            <p class="right">18900<span class="rouble">i</span></p>

            <div>&quot;Vision, Logic, Implex&quot;, производитель HI-TEC (Израиль) за единицу</div>
           </li>

          <li>
            <p class="right">29700<span class="rouble">i</span></p>

            <div>&quot;XiVE&quot; производитель Friadent Dentsply (Германия) за единицу</div>
           </li>

          <li>
            <p class="right">31000<span class="rouble">i</span></p>

            <div>&quot;Nobel Replace Select&quot; производитель Nobel Biocare (Швеция) за единицу</div>
           </li>

          <li>
            <p class="right">31000<span class="rouble">i</span></p>

            <div>&quot;Nobel Groovy&quot; производитель Nobel Biocare (Швеция) за единицу</div>
           </li>

          <li>
            <p class="right">35000<span class="rouble">i</span></p>

            <div>&quot;Nobel Active&quot; производитель Nobel Biocare (Швеция) за единицу</div>
           </li>
         </ul>
       </li>

      <li>
        <p class="right">1900<span class="rouble">i</span></p>

        <div>Установка формирователя десны</div>
       </li>
     </ul>
   </div>
 <div class="to-hide">
	  <div class="price_subsec">
    <h2>Хирургическая стоматология</h2>

    <ul>
      <li>
        <p class="right">390<span class="rouble">i</span></p>

        <div>Консультация врача-хирурга </div>
       </li>

      <li>
        <p class="right">100<span class="rouble">i</span></p>

        <div>Анестезия аппликационная</div>
       </li>

      <li>
        <p class="right">330<span class="rouble">i</span></p>

        <div>Анестезия</div>
       </li>

      <li>
        <p class="right">610<span class="rouble">i</span></p>

        <div>Удаление зуба (подвижность III степени)</div>
       </li>

      <li>
        <p class="right">1210<span class="rouble">i</span></p>

        <div>Удаление зуба</div>
       </li>

      <li>
        <p class="right">2420<span class="rouble">i</span></p>

        <div>Удаление зуба (сложное)</div>
       </li>

      <li>
        <p class="right">3630<span class="rouble">i</span></p>

        <div>Удаление зуба (ретенированного)</div>
       </li>

      <li>
        <p class="right">2420<span class="rouble">i</span></p>

        <div>Удаление имплантата (винтового)</div>
       </li>

      <li>
        <p class="right">3630<span class="rouble">i</span></p>

        <div>Удаление имплантата (пластинчатого)</div>
       </li>

      <li>
        <p class="right">8470<span class="rouble">i</span></p>

        <div>Синус-лифтинг закрытый односторонний (без материала)</div>
       </li>

      <li>
        <p class="right">20570<span class="rouble">i</span></p>

        <div>Синус-лифтинг открытый односторонний (без материала)</div>
       </li>

      <li>
        <p class="right">4840<span class="rouble">i</span></p>

        <div>Операция по коррекции альвеолярного отростка на одной челюсти</div>
       </li>

      <li>
        <p class="right">18150<span class="rouble">i</span></p>

        <div>Операция по расщеплению костного гребня(без материала)</div>
       </li>

      <li>
        <p class="right">2420<span class="rouble">i</span></p>

        <div>Операция коррекции уздечки губы или языка</div>
       </li>

      <li>
        <p class="right">1210<span class="rouble">i</span></p>

        <div>Операция по удалению доброкачественного новообразования</div>
       </li>

      <li>
        <p class="right">1820<span class="rouble">i</span></p>

        <div>Операция снятия экзостозов </div>
       </li>

      <li>
        <p class="right">4840<span class="rouble">i</span></p>

        <div>Операция цистэктомия (резекция верхушки корня)</div>
       </li>

      <li>
        <p class="right">6660<span class="rouble">i</span></p>

        <div>Операция резекции верхушки корня двух рядом стоящих зубов</div>
       </li>

      <li>
        <p class="right">18150<span class="rouble">i</span></p>

        <div>Операция по пересадке костной ткани (аутотрансплантация) без материалов</div>
       </li>

      <li>
        <p class="right">3630<span class="rouble">i</span></p>

        <div>Операция по углублению предверия полости (пластика слизистой оболочки)</div>
       </li>

      <li>
        <p class="right">4840<span class="rouble">i</span></p>

        <div>Операция ретенция (зуб мудрости)</div>
       </li>

      <li>
        <p class="right">6050<span class="rouble">i</span></p>

        <div>Пластика альвеолярного гребня перед имплантацией</div>
       </li>

      <li>
        <p class="right">250<span class="rouble">i</span></p>

        <div>Использование материала «Альвожель»</div>
       </li>

      <li>
        <p class="right">250<span class="rouble">i</span></p>

        <div>Использование материала «Неоконус»</div>
      </li>

      <li>
        <p class="right">6580<span class="rouble">i</span></p>

        <div>Использование костного материала Cerabone&reg; Granulat 0,5 - 1,0 мм (0,25г) 0,5мл</div>
       </li>

      <li>
        <p class="right">8360<span class="rouble">i</span></p>

        <div>Использование костного материала Cerabone® Granulat 0,5 - 1,0 мм (0,5г) 1,0мл</div>
       </li>

      <li>
        <p class="right">14340<span class="rouble">i</span></p>

        <div>Использование костного материала Cerabone® Granulat 0,5 - 1,0 мм (1,0г) 2,0мл</div>
       </li>

      <li>
        <p class="right">27480<span class="rouble">i</span></p>

        <div>Использование костного материала Cerabone® Granulat 0,5 - 1,0 мм (2,5г) 5,0мл</div>
       </li>

      <li>
        <p class="right">6580<span class="rouble">i</span></p>

        <div>Использование костного материала Cerabone® Granulat 1,0 - 2,0 мм (0,25г) 0,5мл</div>
       </li>

      <li>
        <p class="right">8360<span class="rouble">i</span></p>

        <div>Использование костного материала Cerabone® Granulat 1,0 - 2,0 мм (0,5г) 1,0мл</div>
       </li>

      <li>
        <p class="right">14340<span class="rouble">i</span></p>

        <div>Использование костного материала Cerabone® Granulat 1,0 - 2,0 мм (1,0г) 2,0мл</div>
       </li>

      <li>
        <p class="right"><span class="rouble">27480i</span></p>

        <div>Использование костного материала Cerabone® Granulat 1,0 - 2,0 мм (2,5г) 5,0мл</div>
       </li>

      <li>
        <p class="right">8970<span class="rouble">i</span></p>

        <div>Использование костного материала «BIOSS» гранулы 0,25-1мм (0,25г)</div>
       </li>

      <li>
        <p class="right">10720<span class="rouble">i</span></p>

        <div>Использование костного материала «BIOSS» гранулы 0,25-1мм (0,5г)</div>
       </li>

      <li>
        <p class="right">10720<span class="rouble">i</span></p>

        <div>Использование костного материала «BIOSS» гранулы 1-2мм (0,5г)</div>
       </li>

      <li>
        <p class="right">11620<span class="rouble">i</span></p>

        <div>Использование костного материала «EASY-GRAFT» 0.15 мл</div>
       </li>

      <li>
        <p class="right">14250<span class="rouble">i</span></p>

        <div>Использование костного материала «EASY-GRAFT» 0.4 мл</div>
       </li>

      <li>
        <p class="right">5930<span class="rouble">i</span></p>

        <div>Использование костного материала «Остеоматрикс»</div>
       </li>

      <li>
        <p class="right">11360<span class="rouble">i</span></p>

        <div>Использование резорбирующей мембраны Jason® Collagen membrane 15 x 20 mm</div>
       </li>

      <li>
        <p class="right">14340<span class="rouble">i</span></p>

        <div>Использование резорбирующей мембраны Jason® Collagen membrane 20 x 30 mm</div>
       </li>

      <li>
        <p class="right">19140<span class="rouble">i</span></p>

        <div>Использование резорбирующей мембраны Jason® Collagen membrane 30 x 40 mm</div>
       </li>

      <li>
        <p class="right"><span class="rouble">12340i</span></p>

        <div>Использование резорбирующей мембраны «Bio-Gide» 16х22 mm</div>
       </li>

      <li>
        <p class="right">13850<span class="rouble">i</span></p>

        <div>Использование резорбирующей мембраны «Bio-Gide» 25х25 mm</div>
       </li>

      <li>
        <p class="right">18570<span class="rouble">i</span></p>

        <div>Использование резорбирующей мембраны «Bio-Gide» 30х40 mm</div>
       </li>

      <li>
        <p class="right">21150<span class="rouble">i</span></p>

        <div>Использование нерезорбирующей титановой  мембраны &quot; Frios&quot;</div>
       </li>

      <li>
        <p class="right">3190<span class="rouble">i</span></p>

        <div>Извлечение нерезорбирующей титановой мембраны (сетки)</div>
       </li>

      <li>
        <p class="right">1010<span class="rouble">i</span></p>

        <div>Применение препарата Коллапан</div>
       </li>

      <li>
        <p class="right">250<span class="rouble">i</span></p>

        <div>Разрез</div>
       </li>

      <li>
        <p class="right">370<span class="rouble">i</span></p>

        <div>Наложение шва</div>
       </li>

      <li>
        <p class="right">140<span class="rouble">i</span></p>

        <div>Кюретаж лунки зуба</div>
       </li>

      <li>
        <p class="right">190<span class="rouble">i</span></p>

        <div>Лечение альвеолита</div>
       </li>

      <li>
        <p class="right">190<span class="rouble">i</span></p>

        <div>Остановка кровотечения</div>
       </li>

      <li>
        <p class="right">190<span class="rouble">i</span></p>

        <div>Перевязка</div>
       </li>

      <li>
        <p class="right">190<span class="rouble">i</span></p>

        <div>Обработка хлоргексидином</div>
       </li>

      <li>
        <p class="right">330<span class="rouble">i</span></p>

        <div>Шовный материал</div>
       </li>

      <li>
        <p class="right">580<span class="rouble">i</span></p>

        <div>Иссечение капюшона</div>
       </li>

      <li>
        <p class="right">8250<span class="rouble">i</span></p>

        <div>Забор и подсадка костной аутостружки</div>
       </li>
     	 	 </ul>
   </div>

    <div class="price_subsec">
    <h2>3D планирование операции по имплантации с использованием Implant-Assistant</h2>

    <ul>
      <li>
        <p class="right">1100<span class="rouble">i</span></p>

        <div>Рентгеноконтрастный шаблон для КТ-исследования(1 имплантат)</div>
       </li>

      <li>
        <p class="right">1650<span class="rouble">i</span></p>

        <div>Рентгеноконтрастный шаблон для КТ-исследования(2-5 имплантатов)</div>
       </li>

      <li>
        <p class="right">2200<span class="rouble">i</span></p>

        <div>Рентгеноконтрастный шаблон для КТ-исследования(6 и более имплантатов)</div>
       </li>

      <li>
        <p class="right">10440<span class="rouble">i</span></p>

        <div>Индивидуальный имплантологический шаблон Implant-Guide® (1 имплантат)</div>
       </li>

      <li>
        <p class="right">17590<span class="rouble">i</span></p>

        <div>Индивидуальный имплантологический шаблон Implant-Guide® (2-5 имплантатов)</div>
       </li>

      <li>
        <p class="right">25310<span class="rouble">i</span></p>

        <div>Индивидуальный имплантологический шаблон Implant-Guide® (6 и более имплантатов)</div>
       </li>
     </ul>
   </div>
 </div>
	<p class="roll"><span>развернуть список</span></p>
 </section>