<section class="price_section price_book" data-select="5">
  <h1>Пародонтология</h1>
 
  <div class="price_subsec"> 
    <ul> 
      <li> 
        <p class="right">390<span class="rouble">i</span></p>
       
        <div>Консультация врача-пародонтолога</div>
       </li>
     
      <li> 
        <p class="right">100<span class="rouble">i</span></p>
       
        <div>Анестезия аппликационная</div>
       </li>
     
      <li> 
        <p class="right">330<span class="rouble">i</span></p>
       
        <div>Анестезия</div>
       </li>
     
      <li> 
        <p class="right">130<span class="rouble">i</span></p>
       
        <div>Снятие зубных отложений ультразвуком (одного зуба)</div>
       </li>
     
      <li> 
        <p class="right">370<span class="rouble">i</span></p>
       
        <div>Аппликация бальзамом после снятия зубных отложений</div>
       </li>
     
      <li> 
        <p class="right">730<span class="rouble">i</span></p>
       
        <div>Обработка аппаратом &quot;Вектор&quot; (1 зуб)</div>
       </li>
     
      <li> 
        <p class="right">7870<span class="rouble">i</span></p>
       
        <div>Обработка аппаратом &quot;Вектор&quot; (1 челюсть)</div>
       </li>
     
      <li> 
        <p class="right">610<span class="rouble">i</span></p>
       
        <div>Подбор индивидуальных средств CURADEN (обучение гигиене)</div>
       </li>
     
      <li> 
        <p class="right">4070<span class="rouble">i</span></p>
       
        <div>Глубокое фторирование APF система</div>
       </li>
     
      <li> 
        <p class="right">130<span class="rouble">i</span></p>
       
        <div>Медикаментозная обработка зубодесневого кармана</div>
       </li>
     
      <li> 
        <p class="right">130<span class="rouble">i</span></p>
       
        <div>Полировка зуба</div>
       </li>
     
      <li> 
        <p class="right">370<span class="rouble">i</span></p>
       
        <div>Десневая повязка</div>
       </li>
     
      <li> 
        <p class="right">970<span class="rouble">i</span></p>
       
        <div>Шинирование зуба</div>
       </li>
     
      <li> 
        <p class="right">730<span class="rouble">i</span></p>
       
        <div>Кюретаж зубодесневого кармана</div>
       </li>
     
      <li> 
        <p class="right">610<span class="rouble">i</span></p>
       
        <div>Вскрытие парадонтологического абсцесса</div>
       </li>
     
      <li> 
        <p class="right">370<span class="rouble">i</span></p>
       
        <div>Гингивотомия</div>
       </li>
     
      <li> 
        <p class="right">550<span class="rouble">i</span></p>
       
        <div>Гингивэктомия</div>
       </li>
     
      <li> 
        <p class="right">50<span class="rouble">i</span></p>
       
        <div>Снятие швов</div>
       </li>
     
      <li> 
        <p class="right">730<span class="rouble">i</span></p>
       
        <div>Лоскутная операция межзубного промежутка</div>
       </li>
     
      <li> 
        <p class="right">110<span class="rouble">i</span></p>
       
        <div>Устранение суперконтакта</div>
       </li>
     
      <li> 
        <p class="right">2420<span class="rouble">i</span></p>
       
        <div>Пластика уздечки и рубцовых тяжей</div>
       </li>
     
      <li> 
        <div>Инъекции</div>
       
        <ul> 
          <li> 
            <p class="right">130<span class="rouble">i</span></p>
           
            <div>Аскорбиновая кислота + лидокаин</div>
           </li>
         
          <li> 
            <p class="right">130<span class="rouble">i</span></p>
           
            <div>Линкомицин + лидокаин</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">2420<span class="rouble">i</span></p>
       
        <div>Пластика преддверия полости рта</div>
       </li>
     
      <li> 
        <p class="right">130<span class="rouble">i</span></p>
       
        <div>Аппликации лекарственных средств</div>
       </li>
     
      <li> 
        <p class="right">50<span class="rouble">i</span></p>
       
        <div>Покрытие поверхности одного зуба фторлаком</div>
       </li>
     
      <li> 
        <div>Физиотерапия</div>
       
        <ul> 
          <li> 
            <p class="right">190<span class="rouble">i</span></p>
           
            <div>Электрофорез</div>
           </li>
         
          <li> 
            <p class="right">210<span class="rouble">i</span></p>
           
            <div>Гелий-неоновый лазер</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">16100<span class="rouble">i</span></p>
       
        <div>Использование материала Emdogain&reg; 0,3 мл</div>
       </li>
     
      <li> 
        <p class="right">18030<span class="rouble">i</span></p>
       
        <div>Использование материала Emdogain® 0,7 мл</div>
       </li>
     
      <li> 
        <p class="right">250<span class="rouble">i</span></p>
       
        <div>Использование материала &laquo;Альвожель&raquo;</div>
       </li>
     
      <li> 
        <p class="right">250<span class="rouble">i</span></p>
       
        <div>Использование материала «Неоконус»</div>
       </li>
     
      <li> 
        <p class="right">1010<span class="rouble">i</span></p>
       
        <div>Применение препарата Коллапан</div>
       </li>
     
      <li> 
        <p class="right">1460<span class="rouble">i</span></p>
       
        <div>Применение мембраны «Парадонткол» в обл-ти одного промежутка</div>
       </li>
     </ul>
   </div>
	<p class="roll"><span>развернуть список</span></p>
 </section>