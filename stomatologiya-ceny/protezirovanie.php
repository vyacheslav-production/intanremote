<section class="price_section price_book" data-select="2">
  <h1>Протезирование</h1>
 
  <div class="price_subsec"> 
    <ul> 
      <li> 
        <p class="right">390<span class="rouble">i</span></p>
       
        <div>Консультация врача протезиста</div>
       </li>
     
      <li> 
        <p class="right">100<span class="rouble">i</span></p>
       
        <div>Анестезия аппликационная </div>
       </li>
     
      <li> 
        <p class="right">330<span class="rouble">i</span></p>
       
        <div>Анестезия</div>
       </li>
     
      <li> 
        <p class="right">1210<span class="rouble">i</span></p>
       
        <div>Составление плана протезирования</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Съемное протезирование</h2>
   
    <ul> 
      <li> 
        <div><b>Термопластические протезы &laquo;Valplast&raquo;</b></div>
       
        <ul> 
          <li> 
            <p class="right">26620<span class="rouble">i</span></p>
           
            <div>Съемный протез частичный «Valplast» (от 8 зубов)</div>
           </li>
         
          <li> 
            <p class="right">22440<span class="rouble">i</span></p>
           
            <div>Съемный протез частичный «Valplast» (4-7 зубов)</div>
           </li>
         
          <li> 
            <p class="right">18150<span class="rouble">i</span></p>
           
            <div>Съемный протез частичный «Valplast» (до 3-х зубов)</div>
           </li>
         
          <li> 
            <p class="right">850<span class="rouble">i</span></p>
           
            <div>Полировка протеза «Valplast»</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">15640<span class="rouble">i</span></p>
       
        <div><b>Съемный акриловый протез полный</b></div>
       </li>
     
      <li> 
        <p class="right">12840<span class="rouble">i</span></p>
       
        <div>Съемный акриловый протез частичный</div>
       </li>
     
      <li> 
        <p class="right">3630<span class="rouble">i</span></p>
       
        <div>Съемный акриловый протез 1 зуб</div>
       </li>
     
      <li> 
        <p class="right">5640<span class="rouble">i</span></p>
       
        <div>Съемный акриловый протез 2 или 3 зуба</div>
       </li>
     
      <li> 
        <p class="right">18760<span class="rouble">i</span></p>
       
        <div>Съемный протез акриловый с ответной частью на балке</div>
       </li>
     
      <li> 
        <p class="right">35200<span class="rouble">i</span></p>
       
        <div>Съемный протез гипоаллергенный безакриловый (акри-фри) полный</div>
       </li>
     
      <li> 
        <p class="right">33000<span class="rouble">i</span></p>
       
        <div>Съемный протез гипоаллергенный безакриловый (акри-фри) частичный</div>
       </li>
     
      <li> 
        <div>Дополнительная фиксация</div>
       
        <ul> 
          <li> 
            <p class="right">1210<span class="rouble">i</span></p>
           
            <div>Конструкция на мини имплантате &quot;C-TECH&quot; (за 1 зуб)</div>
           </li>
         
          <li> 
            <p class="right">11620<span class="rouble">i</span></p>
           
            <div>Конструкция на мини имплантатах &quot;C-TECH&quot; (нижняя челюсть)</div>
           </li>
         
          <li> 
            <p class="right">13680<span class="rouble">i</span></p>
           
            <div>Конструкция на мини имплантатах &quot;C-TECH&quot; (верхняя челюсть)</div>
           </li>
         
          <li> 
            <p class="right">29530<span class="rouble">i</span></p>
           
            <div>изготовление титановой балки на уровне имплантатов (CAD/CAM)</div>
           </li>
         
          <li> 
            <p class="right">59040<span class="rouble">i</span></p>
           
            <div>изготовление титановой балки на уровне имплантатов (Procera)</div>
           </li>
         
          <li> 
            <p class="right">1820<span class="rouble">i</span></p>
           
            <div>на «Локаторах»</div>
           </li>
         
          <li> 
            <p class="right">1820<span class="rouble">i</span></p>
           
            <div>на «Шариковых абатментах»</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">6420<span class="rouble">i</span></p>
       
        <div>Литое армирование съемного протеза</div>
       </li>
     
      <li> 
        <p class="right">610<span class="rouble">i</span></p>
       
        <div>Использование прозрачной пластмассы базиса в съемном протезе</div>
       </li>
     
      <li> 
        <div><b>Кламмер</b></div>
       
        <ul> 
          <li> 
            <p class="right">240<span class="rouble">i</span></p>
           
            <div>Гнутый (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">430<span class="rouble">i</span></p>
           
            <div>Пелот (за единицу)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">27500<span class="rouble">i</span></p>
       
        <div><b>Бюгельный протез</b></div>
       </li>
     
      <li> 
        <p class="right">29040<span class="rouble">i</span></p>
       
        <div>Бюгельный протез с литым нёбом</div>
       </li>
     
      <li> 
        <div>Дополнительный аттачмен (цена за единицу)</div>
       
        <ul> 
          <li> 
            <p class="right">1820<span class="rouble">i</span></p>
           
            <div>Кламмер литой </div>
           </li>
         
          <li> 
            <p class="right">3510<span class="rouble">i</span></p>
           
            <div>Кнопочный</div>
           </li>
         
          <li> 
            <p class="right">3760<span class="rouble">i</span></p>
           
            <div>Рельсовый</div>
           </li>
         
          <li> 
            <p class="right">3880<span class="rouble">i</span></p>
           
            <div>Балочный</div>
           </li>
         
          <li> 
            <p class="right">3880<span class="rouble">i</span></p>
           
            <div>АСЦ 52</div>
           </li>
         
          <li> 
            <p class="right">4240<span class="rouble">i</span></p>
           
            <div>Кламмер многозвеньевой литой</div>
           </li>
         
          <li> 
            <p class="right">7750<span class="rouble">i</span></p>
           
            <div>Ригельный</div>
           </li>
         
          <li> 
            <p class="right">7750<span class="rouble">i</span></p>
           
            <div>Активируемый</div>
           </li>
         
          <li> 
            <p class="right">7750<span class="rouble">i</span></p>
           
            <div>МК1</div>
           </li>
         
          <li> 
            <p class="right">7750<span class="rouble">i</span></p>
           
            <div>Запирающий замок</div>
           </li>
         
          <li> 
            <p class="right">7750<span class="rouble">i</span></p>
           
            <div>ЗЛ</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">27500<span class="rouble">i</span></p>
       
        <div>Съемный бюгельный протез под телескопические коронки</div>
       
        <ul> 
          <li> 
            <p class="right">16500<span class="rouble">i</span></p>
           
            <div>Коронка телескопическая (цена за основную и ответную часть)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div><b>Починка</b></div>
       
        <ul> 
          <li> 
            <p class="right">1820<span class="rouble">i</span></p>
           
            <div>перелома протеза</div>
           </li>
         
          <li> 
            <p class="right">182<span class="rouble">i</span></p>
           
            <div>приварка зуба (до 3-х единиц)</div>
           </li>
         
          <li> 
            <p class="right">1820<span class="rouble">i</span></p>
           
            <div>приварка кламмера (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">2060<span class="rouble">i</span></p>
           
            <div>перебазировка акрилового протеза (холодная)</div>
           </li>
         
          <li> 
            <p class="right">4480<span class="rouble">i</span></p>
           
            <div>перебазировка акрилового протеза (горячая)</div>
           </li>
         
          <li> 
            <p class="right">1210<span class="rouble">i</span></p>
           
            <div>установка матриц (за единицу)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">550<span class="rouble">i</span></p>
       
        <div>Коррекция протеза</div>
       </li>
     
      <li> 
        <div><b>Каппы</b></div>
       
        <ul> 
          <li> 
            <p class="right">2050<span class="rouble">i</span></p>
           
            <div>Каппа для снятия чувствительности после отбеливания индивидуальная (за шт.)</div>
           </li>
         
          <li> 
            <p class="right">2790<span class="rouble">i</span></p>
           
            <div>Каппа от бруксизма</div>
           </li>
         
          <li> 
            <p class="right">2670<span class="rouble">i</span></p>
           
            <div>Каппа для повышения окклюзии</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">940<span class="rouble">i</span></p>
       
        <div>Индивидуальная ложка</div>
       </li>
     
      <li> 
        <p class="right"><span class="rouble">760i</span></p>
       
        <div>Изготовление диагностических моделей (за еденицу)</div>
       </li>
     
      <li> 
        <p class="right">760<span class="rouble">i</span></p>
       
        <div>Изготовление восковых шаблонов с прикусными валиками (за единицу)</div>
       </li>
     
      <li> 
        <p class="right">1340<span class="rouble">i</span></p>
       
        <div>Изготовление прикусного шаблона на жёстком базисе (за единицу)</div>
       </li>
     
      <li> 
        <p class="right">6360<span class="rouble">i</span></p>
       
        <div>Изготовление диагностического воскового протеза (за единицу)</div>
       </li>
     
      <li> 
        <p class="right">430<span class="rouble">i</span></p>
       
        <div>Снятие слепков альгинатной массой (за единицу)</div>
       </li>
     
      <li> 
        <p class="right">570<span class="rouble">i</span></p>
       
        <div>Снятие слепков силиконовой массой (за единицу)</div>
       </li>
     
      <li> 
        <p class="right">970<span class="rouble">i</span></p>
       
        <div>Снятие слепков BISICO (за единицу)</div>
       </li>
     
      <li> 
        <p class="right">970<span class="rouble">i</span></p>
       
        <div>Снятие слепков полиэфирной массой (за единицу)</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Несъемное протезирование</h2>
   
    <ul> 
      <li> 
        <div><b>Сплав NiCr</b></div>
       
        <ul> 
          <li> 
            <p class="right">8800<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из металла NiCr «функциональная» (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">11740<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из металла NiCr одноэтапный имплантат (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">11740<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из металла NiCr двухэтапный имплантат (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">7950<span class="rouble">i</span></p>
           
            <div>Коронка цельнолитая из металла NiCr (за единицу)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div><b>Сплав CoCr</b></div>
       
        <ul> 
          <li> 
            <p class="right">9570<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из металла CoCr «функциональная» (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">12650<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из металла «функционально-эстетическая» (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">11980<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из металла CoCr одноэтапный имплантат (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">11980<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из металла CoCr двухэтапный имплантат (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">8070<span class="rouble">i</span></p>
           
            <div>Коронка цельнолитая из металла CoCr (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">8190<span class="rouble">i</span></p>
           
            <div>Коронка цельнолитая из металла CoCr на имплантате (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">14190<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из сплава кобальта (CoCr) изготовленная по CAD/CAM технологии на уровне абатментов, зубов (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">11980<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из сплава кобальта (CoCr) изготовленная по CAD/CAM технологии на уровне имплантата, промежуточная часть (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">14410<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из сплава кобальта (CoCr) изготовленная по CAD/CAM технологии на уровне имплантата, опорная часть (за единицу)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div><b>Сплав Драгоценных металлов</b></div>
       
        <ul> 
          <li> 
            <p class="right">23100<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из драгоценного металла</div>
           </li>
         
          <li> 
            <p class="right">26950<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из драгоценного металла двухэтапный имплантат (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">20570<span class="rouble">i</span></p>
           
            <div>Коронка цельнолитая из драгоценного металла</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div><b>Диоксид циркония</b></div>
       
        <ul> 
          <li> 
            <p class="right">18340<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из диоксида циркония (материал производства Германия) единичная коронка</div>
           </li>
         
          <li> 
            <p class="right">18950<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из диоксида циркония (материал производства Германия) за единицу в мосте</div>
           </li>
         
          <li> 
            <p class="right">22610<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из диоксида циркония на уровне имплантата на титановой основе для любой имплантационной системы (материал производства Германия) за единицу</div>
           </li>
         
          <li> 
            <p class="right">21110<span class="rouble">i</span></p>
           
            <div>Коронка цельноциркониевая для любой имплантационной системы, за единицу</div>
           </li>
         
          <li> 
            <p class="right">22610<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из диоксида циркония «Procera» (Швеция) единичная коронка</div>
           </li>
         
          <li> 
            <p class="right">18890<span class="rouble">i</span></p>
           
            <div>Коронка цельноциркониевая за единицу</div>
           </li>
         
          <li> 
            <p class="right">23230<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из диоксида циркония «Procera» (Швеция) за единицу в мосте</div>
           </li>
         
          <li> 
            <p class="right">27490<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из диоксида циркония «Procera» на уровне имплантата «Nobel» (Швеция) за единицу</div>
           </li>
         
          <li> 
            <p class="right">23600<span class="rouble">i</span></p>
           
            <div>Коронка на основе каркаса из оксида алюминия «Procera» (Швеция) за единицу в мосте</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div><b>Пресс керамика</b></div>
       
        <ul> 
          <li> 
            <p class="right">17110<span class="rouble">i</span></p>
           
            <div>Винир на основе безметалловой керамики (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">14670<span class="rouble">i</span></p>
           
            <div>Коронка на основе безметалловой керамики (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">14520<span class="rouble">i</span></p>
           
            <div>Вкладка ИНЛЕЙ (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">23890<span class="rouble">i</span></p>
           
            <div>Ультратонкий винир(УЛЬТРАНИР), за единицу</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div><b>Временные коронки</b></div>
       
        <ul> 
          <li> 
            <p class="right">1090<span class="rouble">i</span></p>
           
            <div>Коронка пластмассовая (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">3030<span class="rouble">i</span></p>
           
            <div>Коронка пластмассовая на имплантате (за единицу)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div><b>Абатменты изготовленные на заводе производителя (оригинальные)</b></div>
       
        <ul> 
          <li> 
            <p class="right">2420<span class="rouble">i</span></p>
           
            <div>Абатмент временный (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">7700<span class="rouble">i</span></p>
           
            <div>Абатмент типа &quot;Локатор&quot; (за единицу) </div>
           </li>
         
          <li> 
            <p class="right">6930<span class="rouble">i</span></p>
           
            <div>Абатмент типа &quot;Локатор SGS&quot; (за единицу) </div>
           </li>
         
          <li> 
            <p class="right">6050<span class="rouble">i</span></p>
           
            <div>Абатмент из титана шариковый &quot;SGS&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">5860<span class="rouble">i</span></p>
           
            <div>Абатмент из титана &quot;ADIN&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">9170<span class="rouble">i</span></p>
           
            <div>Абатмент из циркония &quot;ADIN&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">6050<span class="rouble">i</span></p>
           
            <div>Абатмент эстетический прямой и угловой &quot;HI-TEC&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">7700<span class="rouble">i</span></p>
           
            <div>Абатмент из циркония &quot;HI-TEC&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">4070<span class="rouble">i</span></p>
           
            <div>Абатмент CAD-CAM &quot;HI-TEC&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">6050<span class="rouble">i</span></p>
           
            <div>Абатмент Multi Unit прямой &quot;HI-TEC&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">7150<span class="rouble">i</span></p>
           
            <div>Абатмент Multi Unit угловой &quot;HI-TEC&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">4070<span class="rouble">i</span></p>
           
            <div>Абатмент шаровидный &quot;HI-TEC&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">8800<span class="rouble">i</span></p>
           
            <div>Абатмент выжигаемый золотой &quot;HI-TEC&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">3300<span class="rouble">i</span></p>
           
            <div>Абатмент выжигаемый титановый &quot;HI-TEC&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">2970<span class="rouble">i</span></p>
           
            <div>Абатмент выжигаемый беззолно-пластмассовый &quot;HI-TEC&quot; (за единицу)</div>
           </li>
         
          <li> 
            <p class="right"><span class="rouble">9570i</span></p>
           
            <div>Абатмент из титана «Xive» (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">12430<span class="rouble">i</span></p>
           
            <div>Абатмент из циркония «Xive» (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">10530<span class="rouble">i</span></p>
           
            <div>Абатмент из титана шариковый «Xive» (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">9080<span class="rouble">i</span></p>
           
            <div>Абатмент из титана «Nobel Replace» (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">11500<span class="rouble">i</span></p>
           
            <div>Абатмент из циркония «Nobel Replace» (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">10410<span class="rouble">i</span></p>
           
            <div>Абатмент из титана «Nobel Active» (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">13190<span class="rouble">i</span></p>
           
            <div>Абатмент из циркония «Nobel Active» (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">6130<span class="rouble">i</span></p>
           
            <div>Абатмент из титана «Nobel Replace» BALL ABUT с ответной частью (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">13190<span class="rouble">i</span></p>
           
            <div>Абатмент из титана &quot;ADIN&quot; для &quot;Touareg с поверхностью Osseo Fix&quot; (за единицу)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div><b>Абатменты изготовленные в лаборатории (неоригинальные)</b></div>
       
        <ul> 
          <li> 
            <p class="right">9680<span class="rouble">i</span></p>
           
            <div>Абатмент из титана индивидуальный (за единицу)</div>
           </li>
         
          <li> 
            <p class="right">11500<span class="rouble">i</span></p>
           
            <div>Абатмент из диоксида циркония индивидуальный (за единицу)</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">470<span class="rouble">i</span></p>
       
        <div>Временная пломба</div>
       </li>
     
      <li> 
        <p class="right">60<span class="rouble">i</span></p>
       
        <div>Ретракционная нить (за зуб)</div>
       </li>
     
      <li> 
        <p class="right">250<span class="rouble">i</span></p>
       
        <div>Снятие коронки штампованной за единицу</div>
       </li>
     
      <li> 
        <p class="right">370<span class="rouble">i</span></p>
       
        <div>Снятие коронки литой или металлокерамики за единицу</div>
       </li>
     
      <li> 
        <div>Цементировка одной коронки</div>
       
        <ul> 
          <li> 
            <p class="right">380<span class="rouble">i</span></p>
           
            <div>импортным стеклоиномерным цементом</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>Вкладка</div>
       
        <ul> 
          <li> 
            <p class="right">2150<span class="rouble">i</span></p>
           
            <div>Однокорневая NiCr</div>
           </li>
         
          <li> 
            <p class="right">2640<span class="rouble">i</span></p>
           
            <div>Двухкорневая NiCr</div>
           </li>
         
          <li> 
            <p class="right">3700<span class="rouble">i</span></p>
           
            <div>Разборная NiCr</div>
           </li>
         
          <li> 
            <p class="right">2420<span class="rouble">i</span></p>
           
            <div>Однокорневая CoCr</div>
           </li>
         
          <li> 
            <p class="right">2780<span class="rouble">i</span></p>
           
            <div>Двухкорневая CoCr</div>
           </li>
         
          <li> 
            <p class="right">3830<span class="rouble">i</span></p>
           
            <div>Разборная CoCr</div>
           </li>
         
          <li> 
            <p class="right">10880<span class="rouble">i</span></p>
           
            <div>Однокорневая диоксид циркония</div>
           </li>
         
          <li> 
            <p class="right">11130<span class="rouble">i</span></p>
           
            <div>Двухкорневая диоксид циркония</div>
           </li>
         
          <li> 
            <p class="right">11250<span class="rouble">i</span></p>
           
            <div>Разборная диоксид циркония</div>
           </li>
         
          <li> 
            <p class="right">12100<span class="rouble">i</span></p>
           
            <div>Однокорневая из драгоценного металла</div>
           </li>
         
          <li> 
            <p class="right">12350<span class="rouble">i</span></p>
           
            <div>Двухкорневая из драгоценного металла</div>
           </li>
         
          <li> 
            <p class="right">12350<span class="rouble">i</span></p>
           
            <div>Разборная из драгоценного металла</div>
           </li>
         
          <li> 
            <p class="right">11000<span class="rouble">i</span></p>
           
            <div>Вкладка из безметалловой керамики</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>Изготовление хирургического шаблона для имплантации:</div>
       
        <ul> 
          <li> 
            <p class="right">6420<span class="rouble">i</span></p>
           
            <div>до 4-х имплантатов</div>
           </li>
         
          <li> 
            <p class="right">8720<span class="rouble">i</span></p>
           
            <div>более 4-х имплантатов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">550<span class="rouble">i</span></p>
       
        <div>Закрытие шахты винта</div>
       </li>
     
      <li> 
        <p class="right">1.5</p>
       
        <div>Коэффициент за сокращение срока</div>
       </li>
     </ul>
   </div>
	<p class="roll"><span>развернуть список</span></p>
 </section>