<section class="price_section price_book" data-select="3">
  <h1>Лечение</h1>
 
  <div class="price_subsec"> 
    <ul> 
      <li> 
        <p class="right">390<span class="rouble">i</span></p>
       
        <div>Консультация врача стоматолога-терапевта</div>
       </li>
     
      <li> 
        <p class="right">100<span class="rouble">i</span></p>
       
        <div>Анестезия аппликационная</div>
       </li>
     
      <li> 
        <p class="right">330<span class="rouble">i</span></p>
       
        <div>Анестезия</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Кариес средний и клиновидный дефект</h2>
   
    <ul> 
      <li> 
        <p class="right">440<span class="rouble">i</span></p>
       
        <div>Обработка кариозной полости(ср.)</div>
       </li>
     
      <li> 
        <p class="right">470<span class="rouble">i</span></p>
       
        <div>Подкладка</div>
       </li>
     
      <li> 
        <p class="right">1430<span class="rouble">i</span></p>
       
        <div>Пломба 1 поверхность(кариес средний)</div>
       </li>
     
      <li> 
        <p class="right">1690<span class="rouble">i</span></p>
       
        <div>Пломба 2 поверхности(кариес средний)</div>
       </li>
     
      <li> 
        <p class="right">1780<span class="rouble">i</span></p>
       
        <div>Пломба при 3-х и более поверхностях (кариес средний)</div>
       </li>
     
      <li> 
        <p class="right">3590<span class="rouble">i</span></p>
       
        <div>Пломба, при разрушении зуба более 1/2 коронки(кариес средний)</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Кариес глубокий и кариес ранее депульпированного зуба</h2>
   
    <ul> 
      <li> 
        <p class="right">540<span class="rouble">i</span></p>
       
        <div>Обработка кариозной полости (гл.)</div>
       </li>
     
      <li> 
        <p class="right">470<span class="rouble">i</span></p>
       
        <div>Подкладка лечебная</div>
       </li>
     
      <li> 
        <p class="right">470<span class="rouble">i</span></p>
       
        <div>Подкладка изолирующая </div>
       </li>
     
      <li> 
        <p class="right">1570<span class="rouble">i</span></p>
       
        <div>Пломба 1 поверхность(кариес глубокий)</div>
       </li>
     
      <li> 
        <p class="right">1830<span class="rouble">i</span></p>
       
        <div>Пломба 2 поверхности(кариес глубокий)</div>
       </li>
     
      <li> 
        <p class="right">1940<span class="rouble">i</span></p>
       
        <div>Пломба при 3-х и более поверхностях (кариес глубокий)</div>
       </li>
     
      <li> 
        <p class="right">3600<span class="rouble">i</span></p>
       
        <div>Пломба, при разрушении зуба более 1/2 коронки(кариес глубокий)</div>
       </li>
     
      <li> 
        <p class="right">2350<span class="rouble">i</span></p>
       
        <div>Восстановление угла центрального зуба</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Лечение пульпита и периодонтитов</h2>
   
    <ul> 
      <li> 
        <p class="right">610<span class="rouble">i</span></p>
       
        <div>Обработка кариозной полости (пульпит)</div>
       </li>
     
      <li> 
        <p class="right">410<span class="rouble">i</span></p>
       
        <div>Ампутация пульпы и экстирпация из каналов</div>
       </li>
     
      <li> 
        <p class="right">660<span class="rouble">i</span></p>
       
        <div>Наложение девитализирующих паст</div>
       </li>
     
      <li> 
        <p class="right">690<span class="rouble">i</span></p>
       
        <div>Распломбировка канала</div>
       </li>
     
      <li> 
        <p class="right">950<span class="rouble">i</span></p>
       
        <div>Распломбировка канала с применением эндодонтического мотора</div>
       </li>
     
      <li> 
        <p class="right">400<span class="rouble">i</span></p>
       
        <div>Распломбировка канала (частичная)</div>
       </li>
     
      <li> 
        <p class="right">550<span class="rouble">i</span></p>
       
        <div>Распломбировка канала (частичная) с применением эндодонтического мотора</div>
       </li>
     
      <li> 
        <p class="right">370<span class="rouble">i</span></p>
       
        <div>Удаление дентикля</div>
       </li>
     
      <li> 
        <p class="right">410<span class="rouble">i</span></p>
       
        <div>Удаление распада пульпы из корневых каналов</div>
       </li>
     
      <li> 
        <p class="right">790<span class="rouble">i</span></p>
       
        <div>Удаление из канала инородного тела</div>
       </li>
     
      <li> 
        <div>Эндодонтическая обработка:</div>
       
        <ul> 
          <li> 
            <p class="right">810<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">1310<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">1740<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">2090<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>Эндодонтическая обработка с применением эндомотора с никель-титановыми файлами К3:</div>
       
        <ul> 
          <li> 
            <p class="right">1290<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">2070<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">2770<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">3460<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>Эндодонтическая обработка с применением эндодонтического мотора с никель-титановыми файлами Pro Taper:</div>
       
        <ul> 
          <li> 
            <p class="right">1790<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">2800<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">3810<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">4700<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>Пломбировка каналов с гуттаперчевыми штифтами и пломбировочным материалом</div>
       
        <ul> 
          <li> 
            <p class="right">810<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">1040<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">1280<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">1420<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>Пломбировка каналов горячей гуттаперчей</div>
       
        <ul> 
          <li> 
            <p class="right">1050<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">1920<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">2670<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">2860<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">470<span class="rouble">i</span></p>
       
        <div>Подкладка изолирующая (пульпит, периодонтит)</div>
       </li>
     
      <li> 
        <p class="right">460<span class="rouble">i</span></p>
       
        <div>Пломба временная (пульпит, периодонтит)</div>
       </li>
     
      <li> 
        <p class="right">1620<span class="rouble">i</span></p>
       
        <div>Пломба 1 поверхность(пульпит, периодонтит)</div>
       </li>
     
      <li> 
        <p class="right">1900<span class="rouble">i</span></p>
       
        <div>Пломба 2 поверхности(пульпит, периодонтит)</div>
       </li>
     
      <li> 
        <p class="right">2070<span class="rouble">i</span></p>
       
        <div>Пломба при 3-х и более поверхностях (пульпит, периодонтит)</div>
       </li>
     
      <li> 
        <p class="right">3600<span class="rouble">i</span></p>
       
        <div>Пломба, при разрушении зуба более 1/2 коронки(пульпит, периодонтит)</div>
       </li>
     
      <li> 
        <div>Временная пломбировка</div>
       
        <ul> 
          <li> 
            <p class="right">460<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">720<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">1010<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">1200<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <div>2 посещение: Эндодонтическая обработка</div>
       
        <ul> 
          <li> 
            <p class="right">400<span class="rouble">i</span></p>
           
            <div>1 канала</div>
           </li>
         
          <li> 
            <p class="right">550<span class="rouble">i</span></p>
           
            <div>2 каналов</div>
           </li>
         
          <li> 
            <p class="right">750<span class="rouble">i</span></p>
           
            <div>3 каналов</div>
           </li>
         
          <li> 
            <p class="right">880<span class="rouble">i</span></p>
           
            <div>4 каналов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">280<span class="rouble">i</span></p>
       
        <div>Лазеротерапия</div>
       </li>
     </ul>
   </div>
 
  <div class="price_subsec"> 
    <h2>Прочие манипуляции</h2>
   
    <ul> 
      <li> 
        <p class="right">810<span class="rouble">i</span></p>
       
        <div>Внутриканальный фиксатор из титана</div>
       </li>
     
      <li> 
        <p class="right">980<span class="rouble">i</span></p>
       
        <div>Герметизация фиссур</div>
       </li>
     
      <li> 
        <p class="right">1340<span class="rouble">i</span></p>
       
        <div>Закрытие перфорационного отверстия (Pro-Root)</div>
       </li>
     
      <li> 
        <p class="right">690<span class="rouble">i</span></p>
       
        <div>Трепанация искусственной коронки</div>
       </li>
     
      <li> 
        <p class="right">330<span class="rouble">i</span></p>
       
        <div>Обучение гигиене полости рта</div>
       </li>
     
      <li> 
        <p class="right">1380<span class="rouble">i</span></p>
       
        <div>Отбеливание терапевтическим способом (1 единицы)</div>
       </li>
     
      <li> 
        <p class="right">60<span class="rouble">i</span></p>
       
        <div>Покрытие поверхности одного зуба фторлаком</div>
       </li>
     
      <li> 
        <p class="right">1370<span class="rouble">i</span></p>
       
        <div>Применение стекловолокна (вкл. материал для фиксации)</div>
       </li>
     
      <li> 
        <p class="right">460<span class="rouble">i</span></p>
       
        <div>Трепанация зуба</div>
       </li>
     
      <li> 
        <p class="right">310<span class="rouble">i</span></p>
       
        <div>Удаление дефектной пломбы</div>
       </li>
     
      <li> 
        <p class="right">460<span class="rouble">i</span></p>
       
        <div>Наложение коффердама</div>
       </li>
     
      <li> 
        <p class="right">60<span class="rouble">i</span></p>
       
        <div>Ретракционная нить (за зуб)</div>
       </li>
     
      <li> 
        <p class="right">790<span class="rouble">i</span></p>
       
        <div>Подкладка (стеклоиномерный цемент)</div>
       </li>
     
      <li> 
        <p class="right">1430<span class="rouble">i</span></p>
       
        <div>Пломба (стеклоиномерный цемент)</div>
       </li>
     
      <li> 
        <p class="right">140<span class="rouble">i</span></p>
       
        <div>Полировка пломбы (за единицу)</div>
       </li>
     
      <li> 
        <div>Замещение дефекта зубного ряда с использованием нити &laquo;Гласспан&raquo;</div>
       
        <ul> 
          <li> 
            <p class="right">5610<span class="rouble">i</span></p>
           
            <div>Замещение одного зуба</div>
           </li>
         
          <li> 
            <p class="right">7480<span class="rouble">i</span></p>
           
            <div>Замещение двух зубов</div>
           </li>
         </ul>
       </li>
     
      <li> 
        <p class="right">1040<span class="rouble">i</span></p>
       
        <div>Депофорез (3 сеанса) 1 канал</div>
       </li>
     </ul>
   </div>
	<p class="roll"><span>развернуть список</span></p>
 </section>