<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Записаться на приём");
?>

    <section>
        <div class="wrap">

            <!-- <a href="#" class="back-to"><i></i><span>к списку акций</span></a> -->
            <div class="">
                <h1>Запись на приём</h1>
                <div class="appointment">
                    <!-- <div class="h2">Запись на приём</div> -->
                    <p>
                        Для того, чтобы записаться на прием в один из центров «Интан», позвоните <a class="ph" href="tel:+7(812)7770202">+7 (812) 777 02 02</a> или заполните, пожалуйста, форму ниже. <br> Наш администратор обязательно позвонит Вам и предложит оптимальное время посещения.<br><br>Спасибо за доверие к нашему центру!
                    </p>

                </div>
                <div class="enroll-2-doctor  ">
                    <h2>Заявка на прием</h2>
                    <form class="sb_form" action="" method="post" onsubmit="solowayEvent(); yaCounter9957124.reachGoal('zapis_na_priyom'); _gaq.push(['_trackEvent', 'form','form_priem']); return true;">
                        <input name="form_subm" value="<?=$arIblockAccord['add_reception']?>" type="hidden" />
                        <input type="hidden" name="sbsource" value="<?$sbsource;?>"
                        <div class="table">
                            <div class="row">
                                <div class="cell label">
                                    Ваше имя
                                </div>
                                <div class="cell inputs">
                                    <input name="name" required type="text">
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell label">
                                    Контактный телефон
                                </div>
                                <div class="cell inputs">
                                    <input name="kontakt_phone" required type="text" title="Введите телефон вформате +7 999 9999999"  placeholder="+7 9__ _______" class="phone">
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell label" style="padding-right: 15px;">
                                    Удобное время для звонка
                                </div>
                                <div class="cell inputs">
                                    <select name="vremya_zvonka" id="" class="time">
                                        <option value="сейчас">Сейчас</option>
                                        <option value="9-12">9-12</option>
                                        <option value="13-16">13-16</option>
                                        <option value="16-19">16-19</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell label">
                                    Выберите клинику
                                </div>
                                <div class="cell inputs">
                                    <select name="tsentr" id="" class=""  data-placeholder="Выберите клинику">
                                        <option value="">Выберите клинику</option>
                                        <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['dental_centers']), false, false, array('ID', 'NAME'));
                                        while($ar_res = $res->GetNext()){?>
                                            <option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                                        <?}?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell label"></div>
                                <div class="cell button">
                                    <input onclick="yaCounter9957124.reachGoal('zapis_na_priyom'); kCall1(); return true;" type="submit" value="Отправить">
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>