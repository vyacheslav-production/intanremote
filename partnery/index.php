<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Партнеры");
?>

    <section>


        <div class="wrap">


            <div class="w65 text ib">
                <h1>Партнеры нашей стоматологии</h1>
                <div class="text-item ">
                    <h2>Банки-партнеры</h2>
                    <blockquote>
                        <!-- можно юзать спаны, если ссылки не нужны -->
							<span href="#" style="background-image: url(/images/afb.png);">
								<h3>Альфа-банк</h3>
							</span>
                        <!-- можно юзать спаны, если ссылки не нужны -->
							<span href="#"  style="background-image: url(/images/hcrd.png);">
								<h3>Home Credit Bank</h3>
							</span>
                        <!-- можно юзать спаны, если ссылки не нужны -->
                    </blockquote>
                    <h2>Страховые компании</h2>
                    <blockquote>
                        <!-- можно юзать спаны, если ссылки не нужны -->
							<span href="#" class="text-block"  style="background-image: url(http://intan.ru/upload/iblock/4d9/4d97de556e5c9574d502e67334dcf514.jpg);">
								<h3>Сургутнефтегаз</h3>
								<p>
                                    191040 Санк-Петербург,<br> Лиговский пр. д. 43/45<br>т. 600-76-00
                                </p>
							</span>
                        <!-- можно юзать спаны, если ссылки не нужны -->
							<span href="#"  class="text-block"  style="background-image: url(http://intan.ru/upload/iblock/6b9/6b95922a32625cdd4a8be402e53aa40e.jpg);">
								<h3>Ингосстрах</h3>
								<p>
                                    197110 Санкт-Петербург <br>
                                    Песочная набережная д. 40 <br>
                                    т. 332-10-10
                                </p>
							</span>
                        <!-- можно юзать спаны, если ссылки не нужны -->
							<span href="#"  class="text-block"  style="background-image: url(http://intan.ru/upload/iblock/16c/16c9081c0c129cb0328cbcfedcf6d40f.jpg);">
								<h3>Оздоровительный фонд «Мединеф»</h3>
								<p>
                                    191040 Санк-Петербург,<br>
                                    Лиговский пр. д. 43/45<br>
                                    т. 702-16-49,603-28-45
                                </p>
							</span>
							<span href="#"  class="text-block"  style="background-image: url(http://intan.ru/upload/iblock/35c/35cf4a9d3969e695b63498e8b3741569.jpg);">
								<h3>Капитал-Полис</h3>
								<p>
                                    190013 Московский пр. д. 22<br> т. 320-65-34
                                </p>
							</span>
							<span href="#"  class="text-block"  style="background-image: url(http://intan.ru/upload/iblock/73d/73da1d02c18ad3e8dd2be5e4aac339f0.jpg);">
								<h3>Капитал-Полис</h3>
								<p>
                                    197110 Санкт-Петербург<br>
                                    Песочная набережная д. 40<br>
                                    т. 332-10-10
                                </p>
							</span>
							<span href="#" class="text-block"   style="background-image: url(http://intan.ru/upload/iblock/993/993b9d6545db4cba3354c6aec5da7040.jpg);">
								<h3>СПФ ОСАО «РОССИЯ»</h3>
								<p>
                                    197061, СПб, ул. Дивенская, д.3<br>т. 336-34-62, 327-39-84,<br>327-39-85
                                </p>
							</span>
                    </blockquote>

                </div>
            </div>
            <div class="w35 ib">
                <div class="stood-intan">
                    <div class="intan-stood-logo"><img src="/images/stood-intan.png" alt=""></div>
                    <div class="table">
                        <div class="row">
                            <div class="cell"><a href="#"><i class="vrach"></i><span>Врачам</span></a></div>
                            <div class="cell"><a href="#"><i class="assistent"></i><span>Ассистентам</span></a></div>
                        </div>
                        <div class="row">
                            <div class="cell"><a href="#"><i class="directors"></i><span>Управляющим</span></a></div>
                            <div class="cell"><a href="#"><i class="admines"></i><span>Администраторам</span></a></div>
                        </div>
                    </div>
                </div>
                <div class="reviews">
                    <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv'], 'PROPERTY_F_BIND'=>$oneElem['ID']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
                    if($ar_res = $res->GetNext()){?>
                        <a href="/otzyv/" class="title">
                            <div class="h2"><i></i><span>Отзывы о лечении зубов</span></div>
                        </a>
                        <div class="review-item">
                            <div class="review-user-name" data-date="<?//=date('d.m.Y', strtotime($ar_res['DATE_CREATE']))?>"><?=$ar_res['NAME']?></div>
                            <div class="def-toggle">
                            <p>
                                <?=$ar_res['DETAIL_TEXT']?>
                            </p>
                                </div>
                        </div>
                    <?}?>
                    <a href="/vakansii/" class="right-link consultant"><span>Вакансии</span></a>
                    <a href="/intan_shkola/" class="right-link school"><span>Интан-школам</span></a>
                </div>
            </div>
        </div>

    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>