<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

if(isset($_POST['form_subm']) && !empty($_POST['form_subm'])){
    //если добавляем отзыв
    if(is_array($_POST['F_BIND'])){
        $newFBind = array_diff($_POST['F_BIND'], array(''));
        $_POST['F_BIND'] = array_pop($newFBind);
    }

    $el = new CIBlockElement();
    $data = array(
        'IBLOCK_ID' => (int)$_POST['form_subm'],
        'NAME' => htmlspecialchars($_POST['name']),
        'PROPERTY_VALUES'=>array(),
        'ACTIVE' => 'N'
    );
    $black = array('iblock', 'form_subm');
    $main = array('NAME', 'DETAIL_TEXT');
    foreach($_POST as $key => $value){
        if(in_array(mb_strtoupper($key), $main)) {
            $data[mb_strtoupper($key)] = $value;
            continue;
        }elseif(!in_array(mb_strtolower($key), $black)) {
            $data['PROPERTY_VALUES'][$key] = $value;
        }
    }

    //если резюме
    if($_POST['form_subm'] == $arIblockAccord['reqforjob']){
        $fid = CFile::SaveFile($_FILES['rezyme'], 'resume');
        $data['PROPERTY_VALUES']['rezyme'] = $fid;
    }

    $data['PROPERTY_VALUES']['F_BINDCITY'] = getIdCity(getCodeCity());

    $elemID = $el->Add($data);
    if(!empty($el->LAST_ERROR)) {
        $arResult['error'] = $el->LAST_ERROR;
    }else{?>
        <script>
            $(function() {
                $('.fancy').click();
            });
        </script>
        <?
        $captcha = $_POST['g-recaptcha-response'];
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LdpcgoTAAAAAPQOeUX5psdEKT1lHkOMC4LDFQWI&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        if ($_POST['form_subm'] == $arIblockAccord['add_reception']): ?>
            <script type="text/javascript">
                (function (h) {
                    function k() {
                        var a = function (d, b) {
                            if (this instanceof AdriverCounter)d = a.items.length || 1, a.items[d] = this, b.ph = d, b.custom && (b.custom = a.toQueryString(b.custom, ";")), a.request(a.toQueryString(b)); else return a.items[d]
                        };
                        a.httplize = function (a) {
                            return (/^\/\//.test(a) ? location.protocol : "") + a
                        };
                        a.loadScript = function (a) {
                            try {
                                var b = g.getElementsByTagName("head")[0], c = g.createElement("script");
                                c.setAttribute("type", "text/javascript");
                                c.setAttribute("charset", "windows-1251");
                                c.setAttribute("src", a.split("![rnd]").join(Math.round(1E6 * Math.random())));
                                c.onreadystatechange = function () {
                                    /loaded|complete/.test(this.readyState) && (c.onload = null, b.removeChild(c))
                                };
                                c.onload = function () {
                                    b.removeChild(c)
                                };
                                b.insertBefore(c, b.firstChild)
                            } catch (f) {
                            }
                        };
                        a.toQueryString = function (a, b, c) {
                            b = b || "&";
                            c = c || "=";
                            var f = [], e;
                            for (e in a)a.hasOwnProperty(e) && f.push(e + c + escape(a[e]));
                            return f.join(b)
                        };
                        a.request = function (d) {
                            var b = a.toQueryString(a.defaults);
                            a.loadScript(a.redirectHost + "/cgi-bin/erle.cgi?" + d + "&rnd=![rnd]" + (b ? "&" + b : ""))
                        };
                        a.items = [];
                        a.defaults = {tail256: document.referrer || "unknown"};
                        a.redirectHost = a.httplize("//ad.adriver.ru");
                        return a
                    }

                    var g = document;
                    "undefined" === typeof AdriverCounter && (AdriverCounter = k());
                    new AdriverCounter(0, h)
                })
                ({"sid": 208327, "sz": "action_lead", "bt": 62, "custom": {"150": "lead_id", "153": "user_id"}});
            </script>
        <?
        elseif ($_POST['form_subm'] == $arIblockAccord['zvonok']):?>
            <script type="text/javascript">
                (function (h) {
                    function k() {
                        var a = function (d, b) {
                            if (this instanceof AdriverCounter)d = a.items.length || 1, a.items[d] = this, b.ph = d, b.custom && (b.custom = a.toQueryString(b.custom, ";")), a.request(a.toQueryString(b)); else return a.items[d]
                        };
                        a.httplize = function (a) {
                            return (/^\/\//.test(a) ? location.protocol : "") + a
                        };
                        a.loadScript = function (a) {
                            try {
                                var b = g.getElementsByTagName("head")[0], c = g.createElement("script");
                                c.setAttribute("type", "text/javascript");
                                c.setAttribute("charset", "windows-1251");
                                c.setAttribute("src", a.split("![rnd]").join(Math.round(1E6 * Math.random())));
                                c.onreadystatechange = function () {
                                    /loaded|complete/.test(this.readyState) && (c.onload = null, b.removeChild(c))
                                };
                                c.onload = function () {
                                    b.removeChild(c)
                                };
                                b.insertBefore(c, b.firstChild)
                            } catch (f) {
                            }
                        };
                        a.toQueryString = function (a, b, c) {
                            b = b || "&";
                            c = c || "=";
                            var f = [], e;
                            for (e in a)a.hasOwnProperty(e) && f.push(e + c + escape(a[e]));
                            return f.join(b)
                        };
                        a.request = function (d) {
                            var b = a.toQueryString(a.defaults);
                            a.loadScript(a.redirectHost + "/cgi-bin/erle.cgi?" + d + "&rnd=![rnd]" + (b ? "&" + b : ""))
                        };
                        a.items = [];
                        a.defaults = {tail256: document.referrer || "unknown"};
                        a.redirectHost = a.httplize("//ad.adriver.ru");
                        return a
                    }

                    var g = document;
                    "undefined" === typeof AdriverCounter && (AdriverCounter = k());
                    new AdriverCounter(0, h)
                })
                ({"sid": 208327, "sz": "order_call", "bt": 62, "custom": {"150": "lead_id", "153": "user_id"}});
            </script>
        <?endif;?>
        <?
    //отправка формы перенесена в init.php
    }
}
?>