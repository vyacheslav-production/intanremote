<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Интан - школам");
$APPLICATION->SetTitle("О компании");
?>

    <section>


        <div class="wrap">


            <div class="w65 text ib">
                <h1>Интан — школам!</h1>
                <div class="text-item ">
                    <p>
                        Сеть стоматологических клиник «Интан» предлагает школам Санкт-Петербурга участвовать в Программе «Интан – школам» (в рамках приоритетного национального проекта «Здоровье») по формированию у школьников культуры ухода за ротовой полостью, повышению мотивации к сохранению своего здоровья путем правильной гигиены и своевременного обнаружения причины болезней полости рта.
                    </p>
                    <h2>План мероприятий</h2>
                    <ol>
                        <li>Проведение игровых занятий для учеников начальных классов о стоматологических заболеваниях, последствиях и методах профилактики болезней полости рта</li>

                        <li>Обучение правильной гигиене полости рта учащихся школы</li>

                        <li>Проведение на безвозмездной основе, на базе школы и в учебное время, профилактического осмотра состояния полости рта у школьников с 1 по 11 класс, с выдачей рекомендаций</li>

                        <li>Оказание бесплатной услуги «герметизация фиссур« для школьников с 1 по 11 класс. Данный вид стоматологической помощи помогает предотвратить развитие таких стоматологических заболеваний, как кариес и пульпит</li>

                        <li>Формирование статистических данных о состоянии здоровья учащихся Вашей школы. Красочный отчет предоставляет школе в электронном виде и в виде журнала</li>
                    </ol>
                    <blockquote>
                        <p style="width: 100%; text-align: left;">
                            Более полная информация (проект
                            «Программы», фотографии, отчеты
                            о работе) и вопросы сотрудничества:
                            Тел.: +7 (812) 777-02-02
                            Или обращайтесь к управляющему ближайшего к Вам центра ИНТАН
                        </p>
                    </blockquote>

                </div>
            </div>
            <div class="w35 ib">
                <div class="stood-intan">
                    <div class="intan-stood-logo"><img src="/images/stood-intan.png" alt=""></div>
                    <div class="table">
                        <div class="row">
                            <div class="cell"><a style="cursor: default;" ><i class="vrach"></i><span>Врачам</span></a></div>
                            <div class="cell"><a style="cursor: default;" ><i class="assistent"></i><span>Ассистентам</span></a></div>
                        </div>
                        <div class="row">
                            <div class="cell"><a style="cursor: default;" ><i class="directors"></i><span>Управляющим</span></a></div>
                            <div class="cell"><a  style="cursor: default;"><i class="admines"></i><span>Администраторам</span></a></div>
                        </div>
                    </div>
                </div>
                <div class="reviews">
                    <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv'], 'PROPERTY_F_BIND'=>$oneElem['ID']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
                    if($ar_res = $res->GetNext()){?>
                        <a href="/otzyv/" class="title">
                            <div class="h2"><i></i><span>Отзывы о лечении зубов</span></div>
                        </a>
                        <div class="review-item">
                            <div class="review-user-name"><?=$ar_res['NAME']?></div>
                            <div class="def-toggle">
                            <p>
                                <?=$ar_res['DETAIL_TEXT']?>
                            </p>
                                </div>
                        </div>
                    <?}?>
                    <a href="/vakansii/" class="right-link consultant"><span>Вакансии</span></a>
                    <a href="/partnery/" class="right-link parnter"><span>Партнеры</span></a>
                </div>
            </div>
        </div>

    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>