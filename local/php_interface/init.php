<?
require_once($_SERVER['DOCUMENT_ROOT']."/ipgeobase/ipgeobase.php");

global $arIblockAccord;
$arIblockAccord = [
    'blogers'=>52,
    'zvonok'=>49,
    'credit_calc'=>51,
    'listarticles'=>46,
    'listfaq'=>47,
    'listencyc'=>48,
    'listvideo'=>45,
    'otzyiv'=>44,
    'reqforjob'=>43,
    'doplistmedserv'=>39,
    'dental_centers'=>37,
    'listsale'=>33,
    'listmedservices'=>38,
    'static_pages'=>36,
    'listvacan'=>35,
    'add_reception'=>50,
    'listthes'=>71,
    'listbenef'=>70,
    'listcities'=>74,
    'listprices'=>75,
    'pricesfrom'=>76
];



define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/log.txt");

AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("RalegionIntanAfterIBlockElementAddClass", "OnAfterIBlockElementAddHandler"));

class RalegionIntanAfterIBlockElementAddClass {


    function OnAfterIBlockElementAddHandler(&$arFields) {

        CModule::IncludeModule("iblock");
        global $arIblockAccord;

        if($arFields["IBLOCK_ID"] == $arIblockAccord['otzyiv']){

            $res = CIBlockElement::GetList(array(), array('IBLOCK_ID'=>$arFields["IBLOCK_ID"], 'ID'=>$arFields["ID"]), false, false, array('*', 'PROPERTY_otzyv_email', 'PROPERTY_otzyv_otzyv'));
            if($ar_res = $res->GetNext()){
                $arMess = array(
                    'AUTHOR'     => $ar_res['NAME'],
                    'AUTHOR_EMAIL'   => $ar_res['PROPERTY_OTZYV_EMAIL_VALUE'],
                    'MESSAGE' => $ar_res['DETAIL_TEXT'],
                    'SOURCE' => $_POST['source'],
                    'MEDIUM' => $_POST['medium'],
                    'CAMPAIGN' => $_POST['campaign'],
                );
                CEvent::Send('REVIEW_FORM', SITE_ID, $arMess);
            }

        }elseif($arFields["IBLOCK_ID"] == $arIblockAccord['add_reception']){

            $res = CIBlockElement::GetList(array(), array('IBLOCK_ID'=>$arFields["IBLOCK_ID"], 'ID'=>$arFields["ID"]), false, false, array('*', 'PROPERTY_kontakt_phone', 'PROPERTY_vremya_zvonka', 'PROPERTY_tsentr.NAME', 'PROPERTY_F_COMMENT', 'PROPERTY_tsentr.ID', 'PROPERTY_DOCTOR'));
            if($ar_res = $res->GetNext()){

                //нужен список адресов, массив arSelectFields не возвращает массив, только строку
                $resTs = CIBlockElement::GetList(array(), array('IBLOCK_ID'=>$arIblockAccord["dental_centers"], 'ID'=>$ar_res["PROPERTY_TSENTR_ID"]), false, false, array('PROPERTY_F_EMAILADDREC', 'NAME', 'ID'));
                if($ar_resTs = $resTs->GetNext()){
                    $listEmailCenters = $ar_resTs['PROPERTY_F_EMAILADDREC_VALUE'];
                }

                $arMess = array(
                    'AUTHOR'     => $ar_res['NAME'],
                    'AUTHOR_PHONE'   => $ar_res['PROPERTY_KONTAKT_PHONE_VALUE'],
                    'CALL_TIME'   => $ar_res['PROPERTY_VREMYA_ZVONKA_VALUE'],
                    'CLINIC'   => $ar_res['PROPERTY_TSENTR_NAME'],
                    'MESSAGE'   => $ar_res['PROPERTY_F_COMMENT_VALUE'],
                    'EMAIL'   => implode(', ', $listEmailCenters),
                    'DOCTOR' => $ar_res['PROPERTY_DOCTOR_VALUE'],
                    'SOURCE' => $_POST['source'],
                    'MEDIUM' => $_POST['medium'],
                    'CAMPAIGN' => $_POST['campaign'],
                );
                CEvent::Send('MEV_ZAYAVKA', SITE_ID, $arMess);
            }

        }elseif($arFields["IBLOCK_ID"] == $arIblockAccord['reqforjob']) {

            $res = CIBlockElement::GetList(array(), array('IBLOCK_ID'=>$arFields["IBLOCK_ID"], 'ID'=>$arFields["ID"]), false, false, array('*', 'PROPERTY_dolgnost.NAME', 'PROPERTY_email', 'PROPERTY_telefon', 'PROPERTY_rezyme', 'PROPERTY_F_COMMENT'));
            if($ar_res = $res->GetNext()){
                $arMess = array(
                    "DOLGNOST" => $ar_res['PROPERTY_DOLGNOST_NAME'],
                    "EMAIL" => $ar_res['PROPERTY_EMAIL_VALUE'],
                    "TELEFON" => $ar_res['PROPERTY_TELEFON_VALUE'],
                    "IMYA" => $ar_res["NAME"],
                    "FILE" => CFile::GetPath($ar_res['PROPERTY_REZYME_VALUE']),
                    'COMMENT' => $ar_res['PROPERTY_F_COMMENT_VALUE'],
                    'SOURCE' => $_POST['source'],
                    'MEDIUM' => $_POST['medium'],
                    'CAMPAIGN' => $_POST['campaign'],
                );
                CEvent::Send("REZYME", SITE_ID, $arMess);
            }

        }elseif($arFields["IBLOCK_ID"] == $arIblockAccord['credit_calc']) {

            $res = CIBlockElement::GetList(array(), array('IBLOCK_ID'=>$arFields["IBLOCK_ID"], 'ID'=>$arFields["ID"]), false, false, array('*', 'PROPERTY_F_TIME', 'PROPERTY_F_COMMENT', 'PROPERTY_F_PHONE'));
            if($ar_res = $res->GetNext()){
                $arMess = array(
                    "NAME" => $arFields["NAME"],
                    "F_PHONE" => $ar_res['PROPERTY_F_PHONE_VALUE'],
                    "F_TIME" => $ar_res['PROPERTY_F_TIME_VALUE'],
                    "F_COMMENT" => $ar_res["PROPERTY_F_COMMENT_VALUE"],
                );
                CEvent::Send("CREDIT", SITE_ID, $arMess);
            }

        }

        ///////////////////////////////////////////////////////////////
    }

}

function plural($number, $one, $two, $five){
    if (($number - $number % 10) % 100 != 10) {
        if ($number % 10 == 1) {
            $result = $one;
        } elseif ($number % 10 >= 2 && $number % 10 <= 4) {
            $result = $two;
        } else {
            $result = $five;
        }
    } else {
        $result = $five;
    }
    return $result;
};

function getUrlByCookie($url = null, $siteId = 's1'){

    if(!isset($_POST['checkStatusIrl']) && !$_POST['checkStatusIrl']){

        $url = str_replace('/krasnodar/', '', $url);
        $url = ltrim($url, '/');

        $rsSites = CSite::GetByID($siteId);
        $arSite = $rsSites->Fetch();
		
		return ($arSite['DIR']);

        $ch = curl_init($arSite['SERVER_NAME'].$arSite['DIR'].$url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "checkStatusIrl=true");
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if($httpcode == 200){
            return $arSite['DIR'].$url;
        }else{
            return $arSite['DIR'];
        }


    }
}

function getCodeCity(){
	$selectedCity = $_REQUEST['selCity'];
	
	if ($selectedCity == "s4" || $selectedCity == "s1") {
		return "spb";
	}
	
    if($selectedCity && !empty($selectedCity)){
        return $selectedCity;
    }

    return $_COOKIE['selCity'] ? $_COOKIE['selCity'] : 'spb';
}

function getIdCity($codeCity){

    global $arIblockAccord;

    $arrFilter = ['IBLOCK_ID'=>$arIblockAccord['listcities'], 'ACTIVE'=>'Y', 'CODE'=>$codeCity];
    $res = CIBlockElement::GetList(array('NAME'=>'ASC'), $arrFilter);
    if($ar_res = $res->fetch()){
        return $ar_res['ID'];
    }

    return null;
}

function getCities(){

    global $arIblockAccord;

    $listCity = [];
    if(CModule::IncludeModule("iblock")){
        $arrFilter = ['IBLOCK_ID'=>$arIblockAccord['listcities'], 'ACTIVE'=>'Y'];
        $res = CIBlockElement::GetList(array('NAME'=>'ASC'), $arrFilter, false, false, array('IBLOCK_ID', 'ID', 'NAME'));
        while($ar_res = $res->fetch()){
            $listCity[] = $ar_res;
        }
    }

    return $listCity;
}

function getFilArrayByCity($arr = []){

    $arrLogic[] = array('LOGIC' => 'OR',
        array('PROPERTY_F_BINDCITY.CODE'=>getCodeCity()),
        array('PROPERTY_F_BINDCITY.CODE'=>false)
    );

    return array_merge($arr, $arrLogic);
}

function addUrlGeo($param, $codeCity){
    if($param){
        return '/'.$codeCity;
    }
    return null;
}
?>
