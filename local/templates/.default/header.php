<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<html>
<head>
<?$APPLICATION->ShowHead();?>
<title><?$APPLICATION->ShowTitle()?></title>

<script type="text/javascript">var kavanga={};
kavanga.cl=247;
kavanga.protocol=('https:' == document.location.protocol ? 'https://' : 'http://');
kavanga.domain="l.kavanga.ru";
kavanga.s = document.createElement('script'); kavanga.s.type = 'text/javascript'; kavanga.s.async = true;
kavanga.rand_version = Math.floor((Math.random()*100)+1);
kavanga.s.src = kavanga.protocol + kavanga.domain +'/js/leads_'+kavanga.cl+'.js?v='+kavanga.rand_version;
kavanga.s0 = document.getElementsByTagName('script')[0]; kavanga.s0.parentNode.insertBefore(kavanga.s, kavanga.s0);
function kCall1(){
window._ktIsLead = 1;
kavanga.ready();
}
function kCall2(){
window._ktIsLead = 2;
kavanga.ready();
}
</script>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF">

<?$APPLICATION->ShowPanel()?>

<?if($USER->IsAdmin()):?>

<div style="border:red solid 1px">
	<form action="/bitrix/admin/site_edit.php" method="GET">
		<input type="hidden" name="LID" value="<?=SITE_ID?>" />
		<p><font style="color:red"><?echo GetMessage("DEF_TEMPLATE_NF")?> </font></p>
		<input type="submit" name="set_template" value="<?echo GetMessage("DEF_TEMPLATE_NF_SET")?>" />
	</form>
</div>

<?endif?>