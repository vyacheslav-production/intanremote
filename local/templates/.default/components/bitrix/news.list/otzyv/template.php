<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); ?>
<? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?><br />
<? endif; ?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="response_item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
        <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
            <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img class="preview_picture" border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" style="float:left" /></a>
            <? else: ?>
                <img class="preview_picture" border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" style="float:left" />
            <? endif; ?>
            <? endif ?>
        <p class="response_item_h">
            <? if ($arParams["DISPLAY_NAME"] != " N" && $arItem["NAME"]): ?>
                <? if ($arItem["DISPLAY_PROPERTIES"]["otzyv_fio"]["VALUE"]) { ?>
                    <span class="response_name"><? echo $arItem["DISPLAY_PROPERTIES"]["otzyv_fio"]["VALUE"] ?></span>
                <? } else { ?>
                    <span class="response_name"><? echo $arItem["NAME"] ?></span>
                <? } ?>
            <? endif; ?>
            <? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]): ?>
                <span class="response_date"><? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
        <? endif ?>
        </p>
        <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
            <? echo $arItem["PREVIEW_TEXT"]; ?>
        <? endif; ?>
        <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
            <div style="clear:both"></div>
        <? endif ?>
        <? foreach ($arItem["FIELDS"] as $code => $value): ?>

            <?= GetMessage("IBLOCK_FIELD_" . $code) ?>:&nbsp;<?= $value; ?>
            <br />
            <? endforeach; ?>
        <div class="response_text">
    <? echo $arItem["DISPLAY_PROPERTIES"]["otzyv_otzyv"]["VALUE"]; ?>
        </div>
    </div>
<? endforeach; ?>
    <div id="to_top"><p class="left"><span>Наверх</span></p><span class="to_top_arr"></span><div class="clear"></div></div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<nav class="square_pager"> 
		<p>Страницы:</p><?=$arResult["NAV_STRING"]?>
	</nav>
<?endif;?>
