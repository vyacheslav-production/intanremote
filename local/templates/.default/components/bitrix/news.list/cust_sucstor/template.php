<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="w65 text ib">
    <div class="text-item vacancy-item history ">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?
            $arrFilter = ['IBLOCK_ID'=>$arIblockAccord['listcities'], 'ID'=>$arItem['DISPLAY_PROPERTIES']['F_DOCTOR']['VALUE']];
            $res = CIBlockElement::GetList(array('NAME'=>'ASC'), $arrFilter, false, false, array('ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_bloger_position', 'PREVIEW_PICTURE'));
            if($ar_res = $res->fetch()){
                $docInfo = $ar_res;
            }?>
            <div class="history-item">
                <div class="history-item-head">
                    <div class="table">
                        <div class="row">
                            <div class="cell history-item-left">
                                <img src="/images/intan-logo-history.png" alt="">
                            </div>
                            <div class="cell history-item-right">
                                <?=$docInfo['NAME'];?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="history-item-body">
                    <div class="table">
                        <div class="row">
                            <div class="cell history-item-left">
                                <?$img = CFile::ResizeImageGet($docInfo["PREVIEW_PICTURE"], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);?>
                                <img src="<?=$img['src']?>" alt="">
                            </div>
                            <div class="cell history-item-right">
                                <div class="history-item-main-tite"><?=$docInfo['NAME'];?></div>
                                <div class="history-item-main-text">
                                    <?=$arItem['~PREVIEW_TEXT']?>
                                </div>
                                <a href="#" data-closed="Читать историю"  data-opened="Скрыть историю"></a>

                            </div>
                        </div>
                    </div>
                    <div class="history-item-main-history">
                        <div>
                            <?=$arItem['~DETAIL_TEXT']?>
                        </div>
                    </div>
                </div>
            </div>

        <?endforeach;?>
    </div>
</div>
