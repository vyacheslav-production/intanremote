<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)die(); ?>

<? $a = 1; ?>

	
		<? foreach ($arResult["ITEMS"] as $arItem): ?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			
			<?
			$i = getVidId($arItem['PROPERTIES']['video']['VALUE']['TEXT']);
			?>


			<?(($a) % 2 == 0) ? ($f = "last") : ($f = "clear") ?>
			<div class="video_list_item <?= $f; ?>">
				<div class="video_prev_wrap">
					<div class="video_prev">
						<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><img src="http://i2.ytimg.com/vi/<?= $i ?>/default.jpg" alt="" /></a>
					</div>
					<p class="video_prev_shadow"></p>
				</div>
				<div class="video_list_item_text">
					<p><? echo $arItem["NAME"] ?></p>
					<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">Смотреть видео</a>
				</div>
				<div class="clear"></div>
			</div>
			<? $a++ ?>
		<? endforeach; ?>
		
		<nav class="video_pager">
			<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
				<br /><?= $arResult["NAV_STRING"] ?>
			<? endif; ?>
			<div class="clear"></div>
		</nav>
