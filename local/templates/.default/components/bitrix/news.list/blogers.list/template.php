<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="vrachi_main">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="vrach-div">
		<?$file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>260, 'height'=>330), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
		<?//print_r($arItem);?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img class="vrach-img" src="<?=$file['src']?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
			<?else:?>
				<img border="0" src="<?=$file['src']?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" class="vrach-img" />
			<?endif;?>
		<?endif?>
		<p class="vrach-name"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></p>
		<?if ($arItem["DISPLAY_PROPERTIES"]["bloger_info"]):?>
		<p class="post"><?=$arItem["DISPLAY_PROPERTIES"]["bloger_info"]["DISPLAY_VALUE"]?> ,</p>
		<?endif;?>
		<p class="post"><?=$arItem["DISPLAY_PROPERTIES"]["bloger_position"]["DISPLAY_VALUE"]?></p>

	</div>
<?endforeach;?>

</div>
