<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="search_page">
    <?if($arParams["DISPLAY_TOP_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
    <form action="index.php" method="get">
        <?if($arParams["USE_SUGGEST"] === "Y"):
                if(strlen($arResult["REQUEST"]["~QUERY"]) && is_object($arResult["NAV_RESULT"]))
                {
                        $arResult["FILTER_MD5"] = $arResult["NAV_RESULT"]->GetFilterMD5();
                        $obSearchSuggest = new CSearchSuggest($arResult["FILTER_MD5"], $arResult["REQUEST"]["~QUERY"]);
                        $obSearchSuggest->SetResultCount($arResult["NAV_RESULT"]->NavRecordCount);
                }
                ?>
                <?$APPLICATION->IncludeComponent(
                        "bitrix:search.suggest.input",
                        "",
                        array(
                                "NAME" => "q",
                                "VALUE" => $arResult["REQUEST"]["~QUERY"],
                                "INPUT_SIZE" => 40,
                                "DROPDOWN_SIZE" => 10,
                                "FILTER_MD5" => $arResult["FILTER_MD5"],
                        ),
                        $component, array("HIDE_ICONS" => "Y")
                );?>
        <?else:?>
            <div class="page_seach_input_wrap">
            <span class="field_sides left"></span>
            <span class="field_sides right"></span>
            <div class="field_value_inner">
                <input type="text" name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" />
            </div>
        </div>
        <div class="right">
            <input type="submit" class="search_go" value="<?=GetMessage("SEARCH_GO")?>" />
            <input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
        </div>
        <?endif;?>
        <?if($arParams["SHOW_WHERE"]):?>
            <div class="page_seach_select_wrap">
                <p class="left">Искать в разделе:</p>
                <div class="first">
                    <span class="field_sides left"></span>
                    <span class="field_sides right"></span>
                    <div class="field_value_inner">
                        <select class="cusel" name="where">
                        <option value=""><?=GetMessage("SEARCH_ALL")?></option>
                        <?foreach($arResult["DROPDOWN"] as $key=>$value):?>
                        <option value="<?=$key?>"<?if($arResult["REQUEST"]["WHERE"]==$key) echo " selected"?>><?=$value?></option>
                        <?endforeach?>
                        </select>
                    </div>
                </div>
        </div>
      
        <?endif;?>
                
        <?if($arParams["SHOW_WHEN"]):?>
                <script>
                var switch_search_params = function()
                {
                        var sp = document.getElementById('search_params');
                        var flag;

                        if(sp.style.display == 'none')
                        {
                                flag = false;
                                sp.style.display = 'block'
                        }
                        else
                        {
                                flag = true;
                                sp.style.display = 'none';
                        }

                        var from = document.getElementsByName('from');
                        for(var i = 0; i < from.length; i++)
                                if(from[i].type.toLowerCase() == 'text')
                                        from[i].disabled = flag

                        var to = document.getElementsByName('to');
                        for(var i = 0; i < to.length; i++)
                                if(to[i].type.toLowerCase() == 'text')
                                        to[i].disabled = flag

                        return false;
                }
                </script>
                <a class="search-page-params" href="#" onclick="return switch_search_params()"><?echo GetMessage('CT_BSP_ADDITIONAL_PARAMS')?></a>
                <div id="search_params" class="search-page-params" style="display:<?echo $arResult["REQUEST"]["FROM"] || $arResult["REQUEST"]["TO"]? 'block': 'none'?>">
                        <?$APPLICATION->IncludeComponent(
                                'bitrix:main.calendar',
                                '',
                                array(
                                        'SHOW_INPUT' => 'Y',
                                        'INPUT_NAME' => 'from',
                                        'INPUT_VALUE' => $arResult["REQUEST"]["~FROM"],
                                        'INPUT_NAME_FINISH' => 'to',
                                        'INPUT_VALUE_FINISH' =>$arResult["REQUEST"]["~TO"],
                                        'INPUT_ADDITIONAL_ATTR' => 'size="10"',
                                ),
                                null,
                                array('HIDE_ICONS' => 'Y')
                        );?>
                </div>
        <?endif?>
    </form>
    <div class="search_sort_by">
                <?if($arResult["REQUEST"]["HOW"]=="d"):?>
                        <a href="<?=$arResult["URL"]?>&amp;how=r<?echo $arResult["REQUEST"]["FROM"]? '&amp;from='.$arResult["REQUEST"]["FROM"]: ''?><?echo $arResult["REQUEST"]["TO"]? '&amp;to='.$arResult["REQUEST"]["TO"]: ''?>"><?=GetMessage("SEARCH_SORT_BY_RANK")?></a><span class="seach_separator">|</span><span class="active"><?=GetMessage("SEARCH_SORTED_BY_DATE")?></span>
                <?else:?>
                        <span class="active"><?=GetMessage("SEARCH_SORTED_BY_RANK")?></span><span class="seach_separator">|</span><a href="<?=$arResult["URL"]?>&amp;how=d<?echo $arResult["REQUEST"]["FROM"]? '&amp;from='.$arResult["REQUEST"]["FROM"]: ''?><?echo $arResult["REQUEST"]["TO"]? '&amp;to='.$arResult["REQUEST"]["TO"]: ''?>"><?=GetMessage("SEARCH_SORT_BY_DATE")?></a>
                <?endif;?>
    </div>
    <?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):?>
            <div class="search-language-guess">
                    <?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
            </div><?endif;?>

    <?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
    <?elseif($arResult["ERROR_CODE"]!=0):?>
            <p><?=GetMessage("SEARCH_ERROR")?></p>
            <?ShowError($arResult["ERROR_TEXT"]);?>
            <p><?=GetMessage("SEARCH_CORRECT_AND_CONTINUE")?></p>
            
            <p><?=GetMessage("SEARCH_SINTAX")?><br /><b><?=GetMessage("SEARCH_LOGIC")?></b></p>
            <table class="how_to_seach">
                    <tr>
                            <td><?=GetMessage("SEARCH_OPERATOR")?></td><td><?=GetMessage("SEARCH_SYNONIM")?></td>
                            <td><?=GetMessage("SEARCH_DESCRIPTION")?></td>
                    </tr>
                    <tr>
                            <td><?=GetMessage("SEARCH_AND")?></td><td>and, &amp;, +</td>
                            <td><?=GetMessage("SEARCH_AND_ALT")?></td>
                    </tr>
                    <tr>
                            <td><?=GetMessage("SEARCH_OR")?></td><td>or, |</td>
                            <td><?=GetMessage("SEARCH_OR_ALT")?></td>
                    </tr>
                    <tr>
                            <td><?=GetMessage("SEARCH_NOT")?></td><td>not, ~</td>
                            <td><?=GetMessage("SEARCH_NOT_ALT")?></td>
                    </tr>
                    <tr>
                            <td>( )</td>
                            <td>&nbsp;</td>
                            <td><?=GetMessage("SEARCH_BRACKETS_ALT")?></td>
                    </tr>
            </table>
    <?elseif(count($arResult["SEARCH"])>0):?>
            <div class="search_result_items">
                <?foreach($arResult["SEARCH"] as $arItem):?>
                        <p class="search_result_link"><a href='<?echo $arItem["URL"]?>'><?echo $arItem["TITLE_FORMATED"]?></a></p>
                        <div class="search_result_desc"><?echo $arItem["BODY_FORMATED"]?></div>
                        <?if (
                                $arParams["SHOW_RATING"] == "Y"
                                && strlen($arItem["RATING_TYPE_ID"]) > 0
                                && $arItem["RATING_ENTITY_ID"] > 0
                        ):?>
                                <div class="search-item-rate"><?
                                        $APPLICATION->IncludeComponent(
                                                "bitrix:rating.vote", $arParams["RATING_TYPE"],
                                                Array(
                                                        "ENTITY_TYPE_ID" => $arItem["RATING_TYPE_ID"],
                                                        "ENTITY_ID" => $arItem["RATING_ENTITY_ID"],
                                                        "OWNER_ID" => $arItem["USER_ID"],
                                                        "USER_VOTE" => $arItem["RATING_USER_VOTE_VALUE"],
                                                        "USER_HAS_VOTED" => $arItem["RATING_USER_VOTE_VALUE"] == 0? 'N': 'Y',
                                                        "TOTAL_VOTES" => $arItem["RATING_TOTAL_VOTES"],
                                                        "TOTAL_POSITIVE_VOTES" => $arItem["RATING_TOTAL_POSITIVE_VOTES"],
                                                        "TOTAL_NEGATIVE_VOTES" => $arItem["RATING_TOTAL_NEGATIVE_VOTES"],
                                                        "TOTAL_VALUE" => $arItem["RATING_TOTAL_VALUE"],
                                                        "PATH_TO_USER_PROFILE" => $arParams["~PATH_TO_USER_PROFILE"],
                                                ),
                                                $component,
                                                array("HIDE_ICONS" => "Y")
                                        );?>
                                </div>
                        <?endif;?>
                        <p class="search_modifed_date"><?=$arItem["DATE_CHANGE"]?></p>
                <?endforeach;?>
            </div>
            <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
            
    <?else:?>
            <p class="seach_nothing_found"><?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?></p>
    <?endif;?>
</div>