<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<form class="search_form" method="get" action="<?=$arResult["FORM_ACTION"]?>" enctype="application/x-www-form-urlencoded">
	<div class="search_string"><input type="text" class="search_string" name="q" /></div>
	<input type="submit" name="s" value="" class="search-button" />
	<div class="br"></div>
</form>