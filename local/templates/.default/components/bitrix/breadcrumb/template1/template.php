<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (empty($arResult))
    return "";

$strReturn = '<nav class="breadcrumb-navigation"><ul>';
for ($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++) {

    if ($arResult[$index]["TITLE"] != $arResult[$index - 1]["TITLE"]) {
        if ($index > 0) {
            $strReturn .= '<li><span class="sep"></span></li>';
        }
        $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
        if ($arResult[$index]["LINK"] <> "" && $index < (count($arResult) - 1)) {
            $strReturn .= '<li><a href="' . $arResult[$index]["LINK"] . '" title="' . $title . '">' . $title . '</a></li>';
        } else {
            $strReturn .= '<li><span>' . $title . '</span></li>';
        }
    }
}
$strReturn .= '</ul></nav><div class="clear" style="padding:0 0 20px;"></div>';
return $strReturn;
?>