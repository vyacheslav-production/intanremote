<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="get" style="display:none;">
    <?
    foreach ($arResult["ITEMS"] as $arItem):
        if (array_key_exists("HIDDEN", $arItem)):
            echo $arItem["INPUT"];
        endif;
    endforeach;
    ?>
    <table class="data-table">
        <thead>
            <tr>
                <td colspan="2" style="text-align:center;"><?= GetMessage("IBLOCK_FILTER_TITLE") ?></td>
            </tr>
        </thead>
        <tbody>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <? if (!array_key_exists("HIDDEN", $arItem)): ?>
                    <tr>
                        <td><?= $arItem["NAME"] ?>:</td>
                        <td><?= $arItem["INPUT"] ?></td>
                    </tr>
    <? endif ?>
<? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">
                    <input type="submit" name="set_filter" value="<?= GetMessage("IBLOCK_SET_FILTER") ?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?= GetMessage("IBLOCK_DEL_FILTER") ?>" /></td>
            </tr>
        </tfoot>
    </table>
</form>
