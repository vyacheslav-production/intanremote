<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); ?>
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br />
    <? endif; ?>
        <p class="secondary_h"><?
        $ansCnt;
        $ansFilter = array(
            "IBLOCK_ID" => "18",
            "ACTIVE" => "Y"
        );
        if($_GET["arrFilter_ff"]["NAME"]){
            $ansFilter["?NAME"] = $_GET["arrFilter_ff"]["NAME"];
        }
        $arrAnsDb = CIBlockElement::GetList(array("SORT" => "ASC"), $ansFilter, false, false, array("ID"));
        while($arrAnsob = $arrAnsDb->GetNext()){
            $arrAns[] = $arrAnsob;
        }
        echo count($arrAns);
        ?> ответов</p>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
<div class="faq_item"  id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
        <div class="faq_q"><p><span class="faq_quot left">&laquo;</span><? echo $arItem["NAME"] ?><span class="faq_quot right">&raquo;</span></p></div>

        <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
            <div class="faq_a">
                <div class="faq_a_ico"></div>
                <div class="faq_a_text">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["PREVIEW_TEXT"]; ?></a>
                </div>
                <div class="clear"></div>
            </div>
        <? endif; ?>
        </div>
    <? endforeach; ?>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <nav class="square_pager"> 
            <p>Страницы:</p><?= $arResult["NAV_STRING"] ?>
        </nav>
    <? endif; ?>
    <?
//    $curtitle = CMain::GetTitle(false, false);
//    echo $curtitle;
//    $curtitle.= " - стр." . $arResult['PAGEN'];
//    $APPLICATION->SetTitle($curtitle);
//    $APPLICATION->SetPageProperty("title", $arResult['NAV_RESULT']["NavPageNomer"]);
    ?>
<?
$uri = $APPLICATION->GetCurUri();
if (strpos($uri, "?ELEMENT_ID") !== true) {
    $ps = strpos($uri, "PAGEN_1=");
    $ps = $ps + 8;
    $i = 1;
    $curpage = substr($uri, $ps, $i);
    $i++;
    if (is_numeric(substr($uri, $ps, $i)))
        $curpage = substr($uri, $ps, $i);
    $i++;
    if (is_numeric(substr($uri, $ps, $i)))
        $curpage = substr($uri, $ps, $i);
//echo $curpage;
    $title = "Часто задаваемые вопросы о стоматологии";
    if ($curpage != 0 && is_numeric($curpage)) {
        $title.=" стр. " . $curpage;
    }
    $APPLICATION->SetTitle($title);
    unset($i, $curpage);
}
?> 