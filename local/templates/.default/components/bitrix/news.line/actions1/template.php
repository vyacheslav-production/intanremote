<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if(count($arResult["ITEMS"]) > 0){ ?>
<div class="rheader1">Акции</div>
	<? foreach($arResult["ITEMS"] as $arKey => $arItem): ?>
            <div class="r_action" id="actions_item_<?=$arKey; ?>" onclick="$('#actions_item_text_<?=$arKey; ?>').toggle(); $('#actions_item_<?=$arKey; ?>').toggleClass('r_action_active');">
            	<? echo $arItem["NAME"]; ?>
            	<div class="r_action_text" id="actions_item_text_<?=$arKey; ?>">
                	<? echo $arItem["PREVIEW_TEXT"]; ?>
                </div>
            </div>
    <? endforeach; ?>
<? } ?>
