<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<nav class="top_menu"><ul>

<?
$a = count($arResult);
$b = 1;
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
        
	<?if($arItem["SELECTED"]):?>
		<li><a href="<?=$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a></li>
                <?if($b!=$a):?>
                <li><span class="top_menu_decor"></span></li>
                <?endif;?>
	<?else:?>
		<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
                <?if($b!=$a):?>
                <li><span class="top_menu_decor"></span></li>
                <?endif;?>
	<?endif?>
	
<? $b++; endforeach;?>
</ul></nav>
<?endif?>