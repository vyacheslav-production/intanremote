<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/*function addUrlGeo($param, $codeCity){
	if($param){
		return '/'.$codeCity;
	}
	return null;
}*/
?>
<?if (!empty($arResult)):?>
<div class="vacancy-bread">
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if(stristr($_SERVER['PHP_SELF'], $arItem["LINK"])):?>
		<span class="active"><?=$arItem["TEXT"]?></span>
	<?else:?>
		<a href="<?=addUrlGeo($arItem['PARAMS']['geo'], getCodeCity()).$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
	<?endif?>
	
<?endforeach?>

</div>
<?endif?>
