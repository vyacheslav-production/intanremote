<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="main_cmenu2">
	<div class="undertop2">
		<div class="cmenu2">
<? if (!empty($arResult)){
		foreach($arResult as $arItem){
			if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;
			if($arItem["SELECTED"]){ ?>
        	<div class="cmenu2_item cmenu2_item_active"><a href="<?=$arItem["LINK"]?>" class="cmenu2"><?=$arItem["TEXT"]?></a></div>
		<? }else{ ?>
			<div class="cmenu2_item"><a href="<?=$arItem["LINK"]?>" class="cmenu2"><?=$arItem["TEXT"]?></a></div>
		<? }
		}
	} ?>
        	<div class="br"></div>
    	</div>
    </div>
</div>