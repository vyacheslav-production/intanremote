<?
                if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
                IncludeTemplateLangFile(__FILE__);
                ?> 

                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "page",
	"AREA_FILE_SUFFIX" => "inc_for_enc",
	"EDIT_TEMPLATE" => ""
	),
	false
);?>
                </div><!--content-->    
                <footer id="page_footer">
                    <div class="footer_shadow"></div>
                    <div class="footer_inner">
                        <div class="footer_copyright">
                            &copy; <!--2012- --><?php echo date(Y);?> ООО &laquo;ВебАудитПро&raquo;
                        </div>
                        <div class="footer_menu_wrap">
                            <?$APPLICATION->IncludeComponent("bitrix:menu", "footer_menu2", array(
                                    "ROOT_MENU_TYPE" => "footer_menu",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array(
                                    ),
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "left",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "N"
                                    ),
                                    false
                            );?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </footer>
            </div><!--container-->
        </div>
<!-- Yandex.Metrika counter -->
<div style="display:none;"><script type="text/javascript">
(function(w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter12879412 = new Ya.Metrika({id:12879412, enableAll: true, webvisor:true});
        }
        catch(e) { }
    });
})(window, "yandex_metrika_callbacks");
</script></div>
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script>
<noscript><div><img src="//mc.yandex.ru/watch/12879412" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    </body>
</html>