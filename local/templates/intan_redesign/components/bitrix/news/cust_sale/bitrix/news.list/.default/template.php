<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="action-list">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="action-list-item">
            <?$img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>387, 'height'=>230), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
            <span class="img" style="background-image: url(<?=$img['src']?>)"></span>
            <span class="title"><?=$arItem["NAME"]?></span>
            <span class="txt">
                <div>
                    <?if($arItem["DATE_ACTIVE_TO"]):?>
                        Срок действия акции: <strong>до <?=$arItem["DATE_ACTIVE_TO"]?>.</strong>
                    <?endif;?>
                </div>
                <div>
                    <?=$arItem["PREVIEW_TEXT"]?>
                </div>
            </span>
        </a>

    <?endforeach;?>
</div>
