<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $arIblockAccord;
?>
<h1><?=$arResult["NAME"]?></h1>
<a href="/<?=getCodeCity()?>/aksii/" class="back-to"><i></i><span>к списку акций</span></a>
<div class="w65 ib">
    <?$img = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]["ID"], array('width'=>832, 'height'=>390), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
    <img  class="action-banner-imga"  src="<?=$img['src']?>"  alt="">
    <h6>        <?=$arResult['PREVIEW_TEXT']?></h6>
    <p>
        <?=$arResult['DETAIL_TEXT']?>
        <?if($arResult["DATE_ACTIVE_TO"]):?>
            Акция проходит <a  class="orange-link">до <?=$arResult["DATE_ACTIVE_TO"]?>.
        <?endif;?></a>
       <!-- <span class="link-btn-wrap"><a href="#call-me2" class="appointment link-btn">Записаться на прием</a></span> -->
    </p>
    <?=$arResult['DISPLAY_PROPERTIES']['F_ADDINFO']['~VALUE']['TEXT']?>

</div>
<div class="w35 ib">
    <div class="enroll-2-doctor  ">
        <h2>Заявка на прием</h2>
        <form class="sb_form" action="" method="post" onsubmit="kCall2(); <?=$arResult['PROPERTIES']['F_GOAL']['VALUE']?>;">
            <input name="form_subm" value="<?=$arIblockAccord['add_reception']?>" type="hidden" />
            <input name="name" required type="text" placeholder="Имя">
            <input name="kontakt_phone" required type="text" title="Введите телефон в формате +7 999 999 9999" class="phone" placeholder="Телефон">
            <select name="vremya_zvonka" id="" class="time" data-placeholder="Время">
                <option value="">Время</option>
                <option value="Сейчас">Сейчас</option>
                <option value="9-12">9-12</option>
                <option value="13-16">13-16</option>
                <option value="17-20">17-20</option>
            </select>
            <select name="tsentr" id="" class=""  data-placeholder="Выберите клинику">
                <option value="">Выберите клинику</option>
                <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['dental_centers']), false, false, array('ID', 'NAME'));
                while($ar_res = $res->GetNext()){?>
                    <option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                <?}?>
            </select>
            <textarea name="F_COMMENT" placeholder="Комментарий"></textarea>
            <?if ($arResult['ID'] == 82994) :?>
                <input type="submit" on click="yaCounter28119171.reachGoal('zapis_implant_akcia'); return true;" value="Отправить" />
            <?else :?>
                <input type="submit" value="Отправить">
            <?endif; ?>
        </form>
    </div>
    <?if($arResult['DISPLAY_PROPERTIES']['F_CENTER']['VALUE']){?>
        <h2>Акция действует по адресу</h2>
        <nav>
            <?$res = CIBlockElement::GetList(array(), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['dental_centers'], 'ID'=>$arResult['DISPLAY_PROPERTIES']['F_CENTER']['VALUE']), false, false, array('*', 'PROPERTY_F_HOWTOGET', 'PROPERTY_F_COORD', 'PROPERTY_F_ADDR'));
            while($ar_res = $res->GetNext()){?>
                <a href="<?=$ar_res['DETAIL_PAGE_URL']?>" ><span><?=$ar_res['NAME']?></span></a>
            <?}?>
        </nav>
    <?}?>
    <div class="reviews">
        <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv'], 'PROPERTY_F_BIND'=>$arResult['ID']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
        if($ar_res = $res->GetNext()){?>
            <div class="h2"><i></i><span>Отзывы</span></div>
            <div class="review-item">
                <div class="review-user-name" data-date="<?//=date('d.m.Y', strtotime($ar_res['DATE_CREATE']))?>"><?=$ar_res['NAME']?></div>
                <div class="def-toggle">
                    <p>
                        <?=$ar_res['DETAIL_TEXT']?>
                    </p>
                </div>
            </div>
        <?}?>
    </div>
</div>