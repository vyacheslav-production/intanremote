<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $arIblockAccord;
?>

<div class="wrap">
    <h1 class="center"><?=$arResult["NAME"]?></h1>
    <a href="/<?=getCodeCity()?>/kontacty/" class="back-to-docs"><i></i><span>к списку центров</span></a>
    <div class="w65 ib">
        <div class="contacts-block-mobile table">
            <div>
                <div class="cell"><i class="icon-tyte-1"></i>Телефон</div>
                <div class="cell <?=$arResult['DISPLAY_PROPERTIES']['F_CLASS']['VALUE'];?>">
                    <?if(!empty($arResult['DISPLAY_PROPERTIES']['F_PHONE']['VALUE'])){
                        foreach($arResult['DISPLAY_PROPERTIES']['F_PHONE']['VALUE'] as $onePhone){?>
                            <a href="tel:<?=$onePhone?>"><?=$onePhone?></a><br>
                        <?}
                    }?>
                </div>
            </div>
            <div>
                <div class="cell"><i class="icon-tyte-2"></i>Адрес</div>
                <div class="cell">
                    <?=$arResult['DISPLAY_PROPERTIES']['F_ADDR']['VALUE']?><br>
                    <?if(!empty($arResult['DISPLAY_PROPERTIES']['F_METRO']['VALUE'])){
                        foreach($arResult['DISPLAY_PROPERTIES']['F_METRO']['VALUE'] as $oneMetro){?>
                            <?=$oneMetro?><br>
                        <?}
                    }?>
                </div>
            </div>
            <div>
                <div class="cell"><i class="icon-tyte-3"></i>Режим работы</div>
                <div class="cell">
                    <?if(!empty($arResult['DISPLAY_PROPERTIES']['F_MODE']['VALUE'])){
                        foreach($arResult['DISPLAY_PROPERTIES']['F_MODE']['VALUE'] as $oneMode){?>
                            <?=$oneMode?><br>
                        <?}
                    }?>
                </div>
            </div>
        </div>
        <div class="contacts-block table">
            <div class="thead">
                <div class="row">
                    <div class="cell"><i class="icon-tyte-1"></i>Телефон</div>
                    <div class="cell"><i class="icon-tyte-2"></i>Адрес</div>
                    <div class="cell"><i class="icon-tyte-3"></i>Режим работы</div>
                </div>
            </div>
            <div class="tbody">
                <div class="row">
                    <div class="cell <?=$arResult['DISPLAY_PROPERTIES']['F_CLASS']['VALUE'];?>">
                        <?if(!empty($arResult['DISPLAY_PROPERTIES']['F_PHONE']['VALUE'])){
                            foreach($arResult['DISPLAY_PROPERTIES']['F_PHONE']['VALUE'] as $onePhone){?>
                                <a href="tel:<?=$onePhone?>"><?=$onePhone?></a><br>
                            <?}
                        }?>
                    </div>
                    <div class="cell">
                        <?=$arResult['DISPLAY_PROPERTIES']['F_ADDR']['VALUE']?><br>
                        <?if(!empty($arResult['DISPLAY_PROPERTIES']['F_METRO']['VALUE'])){
                            foreach($arResult['DISPLAY_PROPERTIES']['F_METRO']['VALUE'] as $oneMetro){?>
                                <span class="metro"><?=$oneMetro?></span><br>
                            <?}
                        }?>
                    </div>
                    <div class="cell">
                        <?if(!empty($arResult['DISPLAY_PROPERTIES']['F_MODE']['VALUE'])){
                            foreach($arResult['DISPLAY_PROPERTIES']['F_MODE']['VALUE'] as $oneMode){?>
                                <?=$oneMode?><br>
                            <?}
                        }?>
                    </div>
                </div>
            </div>

        </div>
        <?
        $listSales = array();
        $res = CIBlockElement::GetList(array('RAND'=>'ASC'),
            array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listsale'], 'PROPERTY_F_BINDCITY.CODE' => getCodeCity(),
                array('LOGIC' => 'OR',
                    array('PROPERTY_F_CENTER'=>$arResult['ID']),
                    array('PROPERTY_F_CENTER'=>false)
                ))
        );
        while($ar_res = $res->GetNext()){
            $listSales[] = $ar_res;
        }
        if(!empty($listSales)){?>
            <div class="action-centre">
                <h2>Акции центра</h2>
                <?for($i = 0; $i < 3; $i++){?>
                    <?$img = CFile::ResizeImageGet($listSales[$i]["PREVIEW_PICTURE"], array('width'=>280, 'height'=>168), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
                    <a href="<?=$listSales[$i]['DETAIL_PAGE_URL']?>" target=blank class="img"><img src="<?=$img['src']?>" alt=""></a>
                <?}?>
            </div>
        <?}?>
        <?=$arResult["DETAIL_TEXT"]?>
    </div>
    <div class="w35 ib">

        <?if($arResult['DISPLAY_PROPERTIES']['F_CHDOC']['VALUE']){
            $res = CIBlockElement::GetByID($arResult['DISPLAY_PROPERTIES']['F_CHDOC']['VALUE']);
            if($ar_res = $res->GetNext()){?>
                <div class="head-doc">
                    <a href="<?=$ar_res['DETAIL_PAGE_URL']?>">
                        <?$imgDoc = $file = CFile::ResizeImageGet($ar_res["PREVIEW_PICTURE"], array('width'=>96, 'height'=>96), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                        <span class="img" style="background-image: url(<?=$imgDoc['src']?>)"></span>
                        <span class="i">
                            <span class="prof">Главный врач центра</span>
                            <span class="name"><?=$ar_res['NAME']?></span>
                        </span>
                    </a>
                    <a href="/<?=getCodeCity()?>/personal/?clinic=<?=$arResult['ID']?>" class="all-c">все врачи центра</a>
                </div>
            <?}
        }?>

        <?if($arResult['DISPLAY_PROPERTIES']['F_TOUR']['VALUE']){
            $imageDoc = $file = CFile::ResizeImageGet($arResult['PROPERTIES']['PANORAMA']['VALUE'], array('width'=>400, 'height'=>240), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>

            <a target="_blank" class="d3d-pane" href="<?=$arResult['DISPLAY_PROPERTIES']['F_TOUR']['VALUE']?>"><img src="<?=$imageDoc['src']?>" alt=""></a>
        <? }
        else
            if ($arResult['PROPERTIES']['PANORAMA']):
                $imageDoc = $file = CFile::ResizeImageGet($arResult['PROPERTIES']['PANORAMA']['VALUE'], array('width'=>408, 'height'=>240), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                <img src="<?=$imageDoc['src']?>" alt="">
            <?endif;?>

        <div class="map" id="ymaps-map-container"></div>
        <script type="text/javascript" src="http://api-maps.yandex.ru/2.0/?coordorder=latlong&amp;load=package.full&amp;wizard=constructor&amp;lang=ru-RU"></script>
        <script type="text/javascript">
            ymaps.ready(function() {
                var map = new ymaps.Map("ymaps-map-container", {
                    center: [<?=$arResult['DISPLAY_PROPERTIES']['F_COORD']['VALUE']?>],
                    zoom: 15,
                    type: "yandex#map"
                });
                map.controls
                    .add("smallZoomControl")
                    .add(new ymaps.control.TypeSelector(["yandex#map", "yandex#satellite", "yandex#hybrid", "yandex#publicMap"]));
                // Создаем метку и задаем изображение для ее иконки
                myPlacemark = new ymaps.Placemark([<?=$arResult['DISPLAY_PROPERTIES']['F_COORD']['VALUE']?>], {
                    balloonContent: '<?=$arResult['DISPLAY_PROPERTIES']['F_ADDR']['VALUE']?>'
                }, {
                    iconImageHref: '/images/map_mark.png', // картинка иконки
                    iconImageSize: [43, 36], // размеры картинки
                    iconImageOffset: [-13, -32] // смещение картинки
                });
                // Добавление метки на карту
                map.geoObjects.add(myPlacemark);

                map.events.add('click', function (e) {
                    map.balloon.close();
                })
            });
        </script>
        <div class="how-2-get">
            <a class="list-o open" href="javascript:;"><i></i><span>Как добраться</span></a>
            <p>
                <?=$arResult['DISPLAY_PROPERTIES']['F_HOWTOGET']['~VALUE']['TEXT']?>
            </p>
        </div>
        <div class="reviews">
            <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv'], 'PROPERTY_F_BIND'=>$arResult['ID']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
            if($ar_res = $res->GetNext()){?>
                <a href="/otzyv/?clinic=<?=$arResult['ID']?>" class="title">
                    <div class="h2"><i></i><span>Отзывы о центре</span></div>
                </a>
                <div class="review-item">
                    <div class="review-user-name" data-date="<?//=date('d.m.Y', strtotime($ar_res['DATE_CREATE']))?>"><?=$ar_res['NAME']?></div>
                    <div class="def-toggle">
                        <p>
                            <?=$ar_res['DETAIL_TEXT']?>
                        </p>
                    </div>
                </div>
            <?}?>
        </div>

    </div>
</div>
