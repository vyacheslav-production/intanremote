<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $arIblockAccord;
$arMapCenter = array(
	's1' => '59.933286, 30.333057',
	's4' => '45.06148367, 38.96222000'
);

$resElem = [];
$arrFilter = ['IBLOCK_ID'=>$arIblockAccord['listcities'], 'ACTIVE'=>'Y', 'CODE'=>$_REQUEST['selCity']];
$res = CIBlockElement::GetList(array('NAME'=>'ASC'), $arrFilter, false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_F_COORD', 'PROPERTY_F_METRO'));
if($ar_res = $res->fetch()){
	$resElem = $ar_res;
}
?>
<div class="wrap">
	<h1 class="center">Адреса центров стоматологии «Интан»</h1>
	<div class="list-center">
		<div class="top-pane">
			<div class="tabs">
				<a id="map" href="#">На карте</a>
				<?if($resElem['PROPERTY_F_METRO_VALUE']['TEXT']):?>
					<a id="metro" href="#">По станциям метро</a>
				<?endif;?>
				<a id="list-only" href="#">Списком</a>
			</div>
			<div class="data-time">Время работы: Пн - Сб: <b>9:00—22:00</b> Воскресенье: <b>10:00—20:00</b></div>
		</div>
	</div>
</div>

<div class="list-center">
	<div class="content">
		<div id="map" class="list-content">
			<div id="ymaps-map-container" style="width: 100%; height: 590px;"></div>
			<script type="text/javascript" src="http://api-maps.yandex.ru/2.0/?coordorder=latlong&load=package.full&wizard=constructor&lang=ru-RU"></script>

			<script type="text/javascript">
				$(function(){
					$('#map').click(function(e){
						var $cont = $("#ymaps-map-container");

						if ($cont.attr("init") != "true") {
							ymaps.ready(init);
							$cont.attr("init", "true");
						} else {
							e.preventDefault();
						}
					});

					if ($("a#map").hasClass("active")) {
						$('#map').click();
					}

				});

				function init () {
					var map = new ymaps.Map("ymaps-map-container", {
						center: [<?=$resElem['PROPERTY_F_COORD_VALUE']?>],
						zoom: 10,
						type: "yandex#map"
					});
					map.controls
						.add("smallZoomControl")
						.add(new ymaps.control.TypeSelector(["yandex#map", "yandex#satellite", "yandex#hybrid", "yandex#publicMap"]));
					map.events.add('click', function (e) {
						map.balloon.close();
					});

					myCollection = new ymaps.GeoObjectCollection();

					<?$i = 1;
                    foreach($arResult["ITEMS"] as $arItem){
                        $strPhone = '';
                        if(!empty($arItem['DISPLAY_PROPERTIES']['F_PHONE']['VALUE'])){
                            foreach($arItem['DISPLAY_PROPERTIES']['F_PHONE']['VALUE'] as $onePhone){
                                $strPhone.= '<a href="tel: '.$onePhone.'">'.$onePhone.'</a>';
                            }
                        }?>
					myPlcmrk<?=$i?> = new ymaps.Placemark([<?=$arItem['DISPLAY_PROPERTIES']['F_COORD']['VALUE']?>], {
						balloonContent: '<a id="bxid_723423" href="<?=$arItem["DETAIL_PAGE_URL"]?>" ><?=$arItem["NAME"]?></a> <div class="phones"><?=$strPhone?></div>'
					}, {
						iconImageHref: '/images/map_mark.png', // картинка иконки
						iconImageSize: [43, 36], // размеры картинки
						iconImageOffset: [-13, -32] // смещение картинки
					});
					myCollection.add(myPlcmrk<?=$i?>);
					<?$i++;
                }?>

					// Добавление коллекции на карту
					map.geoObjects.add(myCollection);

					//удаление карты
					$('#w_by_subway, #w_as_list').click(function(){
						map.destroy();
					})
				};
			</script>
		</div>
		<?if($resElem['PROPERTY_F_METRO_VALUE']['TEXT']):?>
			<div id="metro" class="list-content">
				<div class="wrap">
					<div id="contacts_subway">
						<div class="subway_map">
							<?=$resElem['PROPERTY_F_METRO_VALUE']['TEXT']?>
						</div>
					</div>
				</div>
			</div>
		<?endif;?>

		<div class="wrap">
			<div  class="list-content">
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<div class="list-item">
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="title"><?=$arItem["NAME"]?></a>
						<?foreach($arItem['DISPLAY_PROPERTIES']['F_METRO']['VALUE'] as $oneMetro){?>
							<div class="metro"><?=$oneMetro?></div>
						<?}?>
						<div class="phones <?=$arItem['DISPLAY_PROPERTIES']['F_CLASS']['VALUE'];?>">
							<?if(!empty($arItem['DISPLAY_PROPERTIES']['F_PHONE']['VALUE'])){
								foreach($arItem['DISPLAY_PROPERTIES']['F_PHONE']['VALUE'] as $onePhone){?>
									<a href="tel: <?=$onePhone?>"><?=$onePhone?></a>
								<?}
							}?>
						</div>
						<?if($arItem['DISPLAY_PROPERTIES']["F_NEW"]['VALUE']):?>
							<a data-new="Открытие"></a>
						<?endif;?>
						<?if($arItem['DISPLAY_PROPERTIES']["F_SALE"]['VALUE']):?>
							<?$resSale = CIBlockElement::GetList(array('SORT'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listsale'], 'ID'=>$arItem['DISPLAY_PROPERTIES']["F_SALE"]['VALUE']), false, false, array('ID', 'DETAIL_PAGE_URL'));
							if($ar_resSale = $resSale->GetNext()){?>
								<a data-action="акция!" href="<?=$ar_resSale['DETAIL_PAGE_URL']?>"></a>
							<?}?>
						<?endif;?>
					</div>

				<?endforeach;?>
			</div>
		</div>
	</div>
</div>