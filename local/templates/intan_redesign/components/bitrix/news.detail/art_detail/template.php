<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $arIblockAccord;
?>

<h1><?=$arResult["NAME"]?></h1>
<a href="<?=SITE_DIR?>stati/" class="back-to"><i></i><span>к списку статей</span></a>
<div class="w65 text ib">
    <div class="text-item ">
        <div class="date-b">
            <?=$arResult['DISPLAY_ACTIVE_FROM']?>
        </div>
        <?=$arResult["DETAIL_TEXT"]?>
    </div>
</div>
<div class="w35 ib">
    <div class="reviews">
        <?
        //забираем данные по услуге
        $arMedServ = array();
        $res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listmedservices'], 'ID'=>$arResult['DISPLAY_PROPERTIES']['F_CATEGORY']['VALUE']), false, false, array('ID', 'DETAIL_PAGE_URL', 'PROPERTY_F_NPREPOSIT', 'PROPERTY_F_NACCUSAT'));
        if($ar_res = $res->GetNext()){
            $arMedServ = $ar_res;
        }

        //на случай, если не указаны значения
        $arMedServ['PAGE_URL'] = $arMedServ['DETAIL_PAGE_URL'] ? $arMedServ['DETAIL_PAGE_URL'].'tseny/' : '/stomatologiya-ceny/';
        $arMedServ['PROPERTY_F_NACCUSAT_VALUE'] = $arMedServ['PROPERTY_F_NACCUSAT_VALUE'] ? $arMedServ['PROPERTY_F_NACCUSAT_VALUE'] : 'наши услуги';
        $arMedServ['PROPERTY_F_NPREPOSIT_VALUE'] = $arMedServ['PROPERTY_F_NPREPOSIT_VALUE'] ? $arMedServ['PROPERTY_F_NPREPOSIT_VALUE'] : 'нашиx услугах';
        ?>
        <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv'], 'PROPERTY_F_BIND'=>$arResult['DISPLAY_PROPERTIES']['F_CATEGORY']['VALUE']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
        if($ar_res = $res->GetNext()){?>
            <div class="h2"><i></i><span>Отзывы</span></div>
            <div class="review-item">
                <div class="review-user-name" data-date="<?//=date('d.m.Y', strtotime($ar_res['DATE_CREATE']))?>"><?=$ar_res['NAME']?></div>
                <div class="def-toggle">
                <p>
                    <?=$ar_res['DETAIL_TEXT']?>
                </p>
                    </div>
                <?if($arMedServ['DETAIL_PAGE_URL']):?>
                    <a href="<?=$arMedServ['DETAIL_PAGE_URL']?>" class="right-link info-lech"><span>Информация о <?=$arMedServ['PROPERTY_F_NPREPOSIT_VALUE']?></span></a>
                    <a href="<?=$arMedServ['PAGE_URL']?>" class="right-link price-price"><span>Цены на <?=$arMedServ['PROPERTY_F_NACCUSAT_VALUE']?></span></a>
                <?endif;?>
            </div>
        <?}else{?>
            <?if($arMedServ['DETAIL_PAGE_URL']):?>
                <a href="<?=$arMedServ['DETAIL_PAGE_URL']?>" class="right-link info-lech"><span>Информация о <?=$arMedServ['PROPERTY_F_NPREPOSIT_VALUE']?></span></a>
                <a href="<?=$arMedServ['PAGE_URL']?>" class="right-link price-price"><span>Цены на <?=$arMedServ['PROPERTY_F_NACCUSAT_VALUE']?></span></a>
            <?endif;?>
        <?}?>
    </div>
</div>