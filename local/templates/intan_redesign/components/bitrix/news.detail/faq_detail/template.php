<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
global $arIblockAccord;
?>

    <h1 class="main_h">Вопрос-ответ</h1>

    <div class="w65 faq">
        <a href="<?=SITE_DIR?>voprosy/" class="back-to-docs"><i></i><span> Вернуться к списку</span></a>

        <div class="faq_item">
            <div class="faq_q"><h2><span class="faq_quot left">&laquo;</span><?= $arResult["NAME"] ?><span
                        class="faq_quot right">&raquo;</span></h2></div>
            <div class="faq_a">
                <div class="faq_a_ico"></div>
                <div class="faq_a_text">
                    <?= $arResult['DETAIL_TEXT'] ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>

    <div class="w35">
        <div class="enroll-2-doctor right">
            <div class="h2">Задать вопрос</div>
            <form class="sb_form" action="" method="post" onsubmit="kCall2();">
                <input name="form_subm" value="<?= $arIblockAccord['listfaq'] ?>" type="hidden"/>
                <input name="F_NAME" required type="text" placeholder="Имя">
                <input name="F_EMAIL" required title="example@mail.ru" type="text"
                       pattern="[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,4}" placeholder="Email">
                <select name="F_CATEGORY" id="" data-placeholder="Тема вопроса">
                    <option value=""></option>
                    <? $res = CIBlockElement::GetList(array('SORT' => 'ASC'),
                        array('IBLOCK_ID' => $arIblockAccord['listmedservices'], 'ACTIVE' => 'Y'));
                    while ($ar_res = $res->GetNext()) {
                        ?>
                        <option value="<?= $ar_res['ID'] ?>"><?= $ar_res['NAME'] ?></option>
                    <? } ?>
                </select>
                <textarea name="name" required placeholder="Вопрос"></textarea>
                <input type="submit" value="Отправить">
            </form>
        </div>
    </div>

