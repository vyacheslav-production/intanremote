<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $arIblockAccord;
?>

<h1 class="center">Врачи стоматологии «Интан»</h1>

<div class="w65 doc-item ib">
    <a href="<?='/'.getCodeCity()?>/personal/" class="back-to-docs"><i></i><span>к списку врачей</span></a>
    <div class="doc-item">
        <?$img = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]["ID"], array('width'=>245, 'height'=>368), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
        <div class="doc-photo"><img src="<?=$img["src"]?>" alt=""></div>
        <div class="doc-info">
            <h3><?=$arResult["NAME"]?></h3>
            <h4>Должность</h4>
            <p>
                <?=$arResult['DISPLAY_PROPERTIES']['bloger_position']['VALUE']?>
            </p>
            <h4>Краткая биография</h4>
            <p>
                <?=$arResult['DETAIL_TEXT']?>
            </p>
        </div>
    </div>
</div>
<div class="w35 ib">
    <div class="reviews">
        <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv'], 'PROPERTY_F_BIND'=>$arResult['ID']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
        if($ar_res = $res->GetNext()){?>
            <a class="title">
                <div class="h2"><i></i><span>Отзывы о враче</span></div>
            </a>
            <div class="review-item">

                <div class="review-user-name" data-date="<?//=date('d.m.Y', strtotime($ar_res['DATE_CREATE']))?>"><?=$ar_res['NAME']?></div>

                <div class="def-toggle">
                    <p>
                        <?=$ar_res['DETAIL_TEXT']?>
                    </p>
                </div>
            </div>
        <?}?>
        <?if ($arResult['PROPERTIES']['F_CENTRE']) {
//            var_dump($arResult['PROPERTIES']['F_CENTRE']['VALUE']);
            $clinics = CIBlockElement::GetList(array('SORT' => 'ASC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $arIblockAccord['dental_centers'], 'ID' => $arResult['PROPERTIES']['F_CENTRE']['VALUE']), false, false, array('ID', 'NAME'));
            while ($ob = $clinics->GetNextElement())
            {
                $arCln = $ob->GetFields();
//                var_dump($arCln['NAME']);
            }
            $cntClinics = CIBlockElement::GetList(array('SORT' => 'ASC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $arIblockAccord['dental_centers'], 'ID' => $arResult['PROPERTIES']['F_CENTRE']['VALUE']), array(), false, array('ID', 'NAME'));


          //  echo $cntClinics;
        }
        //var_dump($arResult['PROPERTIES']['bloger_clinic']['VALUE']);
        /*$docclinics = CIBlockElement::GetList(array('SORT' => 'ASC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $arIblockAccord['dental_centers'], 'NAME' => $arResult['DISPLAY_PROPERTIES']['bloger_clinic']['VALUE']), false, false, array('ID', 'NAME'));
        var_dump(CIBlockElement::GetProperty($arIblockAccord['blogers'], 'bloger_clinic'));
        while ($doc = $docclinics->GetNext()){ var_dump($doc['NAME']);}*/ ?>
        <div class="enroll-2-doctor">
            <div class="h2">Записаться на прием к врачу</div>
            <?global $yaId?>
            <form class="sb_form" action="" method="post" onsubmit="yaCounter<?=$yaId?>.reachGoal('record_dantist'); kCall2(); ga('send', 'event', 'form', 'form_priem_doc'); return true;">
                <?
                //Выбираем клиники, в которых работает врач
                $clinics = CIBlockElement::GetList(array('SORT' => 'ASC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $arIblockAccord['dental_centers'], 'ID' => $arResult['PROPERTIES']['F_CENTRE']['VALUE']), false, false, array('ID', 'NAME'));
                //Считаем их количество
                $cntClinics = CIBlockElement::GetList(array('SORT' => 'ASC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $arIblockAccord['dental_centers'], 'ID' => $arResult['PROPERTIES']['F_CENTRE']['VALUE']), array(), false, array('ID', 'NAME'));
                ?>
                <?if ($cntClinics != 1){
                ?>
                <select name="tsentr" id="" data-placeholder="Выберите клинику">
                    <option value=""></option>
                   <? while($ar_res = $clinics->GetNextElement()){
                        $clinic = $ar_res->GetFields();?>
                        <option value="<?=$clinic['ID']?>"><?=$clinic['NAME']?></option>
                    <?}?>
                </select>
                <?} else {
                    $ar_res = $clinics->GetNextElement();
                    $clinic = $ar_res->GetFields();?>
                        <input type="text" value="<?=$clinic['NAME']?>" readonly="readonly">
                        <input type="hidden" value="<?=$clinic['ID']?>" name="tsentr">
                <?}?>
                <input name="form_subm" value="<?=$arIblockAccord['add_reception']?>" type="hidden" />
                <input name="name" required type="text" placeholder="Имя">
                <input name="kontakt_phone" required type="text" title="Введите телефон в формате +7 999 999 9999" class="phone" placeholder="Телефон">
                <textarea name="F_COMMENT" placeholder="Комментарий" required></textarea>
                <input onclick="yaCounter9957124.reachGoal('application_for_admission_doctors');  return true;" type="submit" value="Отправить">
            </form>
        </div>
    </div>
</div>
