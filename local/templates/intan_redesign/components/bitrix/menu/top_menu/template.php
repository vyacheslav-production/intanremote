<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/*function addUrlGeo($param, $codeCity){
    if($param){
        return '/'.$codeCity;
    }
    return null;
}*/
?>
<?if (!empty($arResult)):?>
    <div class="head-nav">

        <?
        foreach($arResult as $arItem):
            if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;
            ?>
            <?if($arItem["SELECTED"]):?>
            <a href="<?=addUrlGeo($arItem['PARAMS']['geo'], getCodeCity()).$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a>
        <?else:?>
            <a href="<?=addUrlGeo($arItem['PARAMS']['geo'], getCodeCity()).$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
        <?endif?>

        <?endforeach?>

    </div>
<?endif?>