<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):
	require_once($_SERVER['DOCUMENT_ROOT'].'/include/mobdetect/Mobile_Detect.php');
	$mobDetect = new Mobile_Detect;

	if($mobDetect->isMobile()):?>
		<style>
			div.b-slider, .b-slider .swiper-container, div.b-slider .swiper-slide{
				height: 195px !important;
			}

			div.b-slider .swiper-slide{
				width: 320px !important;
				background-size: 100%;
				background-repeat: no-repeat;
			}

			.b-slider .prev, .b-slider .next{
				top: 117px !important;
			}
		</style>
	<?endif;?>
	<div class="b-slider">
		<div class="controls">
			<span  class="prev"></span>
			<span class="next"></span>
		</div>
		<div class="swiper-container" >
			<div class="swiper-wrapper">
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					$sizeThumb = $mobDetect->isMobile() ? array('width'=>320, 'height'=>300) : array('width'=>640, 'height'=>386);
					?>
					<?$img = CFile::ResizeImageGet($arItem['DISPLAY_PROPERTIES']['F_PHOTOSLIDER']['VALUE'], $sizeThumb, BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="img swiper-slide" style="background-image: url(<?=$img['src']?>)"></a>

				<?endforeach;?>
			</div>
		</div>
		<div class="swiper-scrollbar"></div>

	</div>
<?endif;?>
