<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="vakanci-list">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
	    <div class="item-list" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	        <div class="vakanci-inner">
	            <h3><?=$arItem['NAME']?></h3>
	            <?if ($arItem['PROPERTIES']['REQUIRE']['~VALUE']['TEXT']) {?>
		            <div class="title">Требования</div>
		            <div class="text"><?=$arItem['PROPERTIES']['REQUIRE']['~VALUE']['TEXT']?></div>
	            <?}?>
	            <div class="adres">
		            <?if ($arItem['PROPERTIES']['WORK_PLACE']['VALUE']) {?>
		            	<?=$arItem['PROPERTIES']['WORK_PLACE']['VALUE']?>
		            <?}?>
	            <a href="#" data-closed="Читать полностью"  data-opened="Скрыть"></a></div>
	        </div>
	        <?if ($arItem['PROPERTIES']['DUTIES']['~VALUE']['TEXT'] || $arItem['PROPERTIES']['CONDITIONS']['~VALUE']['TEXT']) {?>
	        	<div class="vakanci-item-main-vakanci">
		            <div>
		            	<?if ($arItem['PROPERTIES']['DUTIES']['~VALUE']['TEXT']) {?>
		            		<p><i><span>Обязанности</span></i></p>
		                	<?=$arItem['PROPERTIES']['DUTIES']['~VALUE']['TEXT']?>
		            	<?}?>
		            	<?if ($arItem['PROPERTIES']['CONDITIONS']['~VALUE']['TEXT']) {?>
		            		<p><i><span>Условия</span></i></p>
		                	<?=$arItem['PROPERTIES']['CONDITIONS']['~VALUE']['TEXT']?>
		            	<?}?>
		                <a href="#" class="sender">Откликнуться</a>
		            </div>
		        </div>
			<?}?>
	    </div>  
	<?endforeach;?>    
</div>
