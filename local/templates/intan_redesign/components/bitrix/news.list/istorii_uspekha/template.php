<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="w65 text ib">
    <div class="text-item vacancy-item history ">
		<?foreach($arResult["ITEMS"] as $key => $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
	        <div class="history-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	            <div class="history-item-body">
	                <div class="table">
	                    <div class="row">
	                    	<?CModule::IncludeModule("iblock");
							$res = CIBlockElement::GetByID($arItem["PROPERTIES"]['F_DOCTOR']['VALUE']);
							$ar_res = $res->GetNext();
							$db_props = CIBlockElement::GetProperty($ar_res['IBLOCK_ID'], $ar_res['ID'], array("sort" => "asc"), Array("CODE"=>"bloger_position"));
							$ar_props = $db_props->Fetch();
							?>
	                        <div class="cell history-item-left">
	                            <img src="<?=CFile::GetPath($ar_res['DETAIL_PICTURE'])?>" alt="<?=$arItem['NAME']?>" width="256">
	                            <a href="#">Увеличить фото</a>
	                        </div>
	                        <div class="cell history-item-right">
	                        	<?if ($arItem['PROPERTIES']['IN_INTAN_YEAR']['VALUE']) {?>
	                        		<div class="history-item-main-date">В «ИНТАН» с <?=$arItem['PROPERTIES']['IN_INTAN_YEAR']['VALUE']?></div>
	                        	<?}?>                        	
	                            <div class="history-item-main-tite">
	                            	<?=$arItem['NAME']?>
	                            	<?if ($ar_props['VALUE']) {?>
		                        		<span><?=$ar_props['VALUE']?></span>
		                        	<?}?>                             	
	                            </div>
	                            <?if ($arItem['PREVIEW_TEXT']) {?>
	                            	<div class="history-item-main-text">
		                                <?=$arItem['PREVIEW_TEXT']?>
		                            </div>
	                            <?}?>    
	                            <?if ($arItem['DETAIL_TEXT'] || $arItem['PROPERTIES']['ID_VIDEO']['VALUE'] || $arItem['PROPERTIES']['RIGHT_IMG']['VALUE'] || $arItem['PROPERTIES']['RIGHT_BLOCK']['~VALUE']['TEXT']) {?>
	                            	<a href="#" data-closed="Читать историю"  data-opened="Скрыть историю"></a>
	                            <?}?>                        
	                            
	                        </div>
	                    </div>
	                </div>
	                <div class="history-item-main-history">
	                	<?if ($arItem['DETAIL_TEXT']) {?>
							<div class="history-item-main-history-inner">
								 <?=$arItem['DETAIL_TEXT']?>
							</div>
	                	<?}?>
						<?if ($arItem['PROPERTIES']['ID_VIDEO']['VALUE']) {?>
		            		<div class="history-item-video" id="vid<?=$arItem['ID']?>" data-video="<?=$arItem['PROPERTIES']['ID_VIDEO']['VALUE']?>"></div>	
		            	<?}?>	                    
	                </div>
	                <div class="history-item-to-right">
	                	<?if ($arItem['PROPERTIES']['RIGHT_IMG']['VALUE']) {?>
	                		<div class="history-pic">
						        <img src="<?=CFile::GetPath($arItem['PROPERTIES']['RIGHT_IMG']['VALUE'])?>" alt="" />
						    </div>
		            	<?}?>                	
	                	<?if ($arItem['PROPERTIES']['RIGHT_BLOCK']['~VALUE']['TEXT']) {?>
	                		<div class="history-about-work">
						        <?=$arItem['PROPERTIES']['RIGHT_BLOCK']['~VALUE']['TEXT']?>
						    </div>
		            	<?}?>			    
	                </div>
	            </div>
	        </div>
        <?endforeach;?>	                        	    
	</div>
</div>

