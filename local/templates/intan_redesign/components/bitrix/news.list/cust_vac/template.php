<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>

    <div class="slide-toggles">
        <div class="title">
            <a href="#" class="orange-link"><?=$arItem["NAME"]?></a>
        </div>
        <article>
            <h2>Требования</h2>
            <p><?echo $arItem["PREVIEW_TEXT"];?></p>

            <h2>Условия</h2>
            <p><?echo $arItem["DETAIL_TEXT"];?></p>
        </article>
    </div>

<?endforeach;?>