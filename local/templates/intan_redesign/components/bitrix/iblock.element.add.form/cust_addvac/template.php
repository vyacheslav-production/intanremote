<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
?>

<h2>Отправить резюме</h2>
<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data" class="vakansii">
    <?= bitrix_sessid_post() ?>
    <? if ($arParams["MAX_FILE_SIZE"] > 0): ?><input type="hidden" name="MAX_FILE_SIZE" value="<?= $arParams["MAX_FILE_SIZE"] ?>" /><? endif ?>


    <input name="PROPERTY[NAME][0]" required type="text" placeholder="Имя">
    <input name="PROPERTY[48][0]" required type="Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="example@mail.ru" placeholder="Email">
    <input name="PROPERTY[49][0]" required type="text"  placeholder="Телефон" title="Введите телефон вформате +7 (999) 999-99-99" placeholder="+7 (9__)-___-__-__" class="phone">
    <select name="PROPERTY[50]" id="" class="time">
        <option value="">Специализация</option>
        <option value="19">Хирург-имплантолог</option>
        <option value="67">Зубной техник (керамист)</option>
        <option value="66">Ассистент стоматолога</option>
        <option value="68">Терапевт-пародонтолог</option>
        <option value="17">Стоматолог</option>
        <option value="18">Парадонтолог</option>
    </select>
    <div class="file">
        <span>
            <a class="orange-link">Загрузить резюме</a>
        </span>
        <ins></ins>
        <input type="hidden" name="PROPERTY[51][0]" value="" />
        <input type="file" name="PROPERTY_FILE_51_0" />
    </div>
    <textarea placeholder="Комментарий"></textarea>
    <input type="submit" value="Отправить">
</form>