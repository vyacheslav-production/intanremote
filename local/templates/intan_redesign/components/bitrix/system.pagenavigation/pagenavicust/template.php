<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
    {
        return;
    }
}

$strNavQueryString     = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

if($arResult['NavPageNomer'] > 1){
    $APPLICATION->AddHeadString('<meta name="robots" content="noindex, follow" />', true);
}
?>
<div class="pagination">
    <? if ($arResult["NavPageNomer"] > 1): ?>
        <a class="word" data-pagenum="<?= ($arResult["NavPageNomer"] - 1) ?>">< Назад</a>
    <?endif ?>
    <? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>

        <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
            <span class="active"><?= $arResult["nStartPage"] ?></span>
        <? else: ?>
            <a data-pagenum="<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
        <?endif ?>

        <? $arResult["nStartPage"]++ ?>
    <? endwhile ?>
    <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
        <a class="word" data-pagenum="<?= ($arResult["NavPageNomer"] + 1) ?>">вперед ></a>
    <?endif ?>
</div>