<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
    {
        return;
    }
}

$strNavQueryString     = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

if($arResult['NavPageNomer'] > 1){
	$APPLICATION->AddHeadString('<meta name="robots" content="noindex, follow" />', true);
}
?>
<div class="search-result-pager">
    Страницы

    <ul class="c-pager">
		<? if ($arResult["NavPageNomer"] > 1): ?>
				<li>
					<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>#search-result-upper">
						<i class="ico ico-min-arr ico-min-arr-left"></i>
					</a>
				</li>
		<?endif ?>
		<? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>

			<? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
				<li class="current"><a><?= $arResult["nStartPage"] ?></a></li>
			<? else: ?>
				<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>#search-result-upper"><?= $arResult["nStartPage"] ?></a></li>
			<?endif ?>

			<? $arResult["nStartPage"]++ ?>
		<? endwhile ?>
		<? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
			<li>
				<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>#search-result-upper">
					<i class="ico ico-min-arr ico-min-arr-right"></i>
				</a>
			</li>
		<?endif ?>

    </ul>
</div>