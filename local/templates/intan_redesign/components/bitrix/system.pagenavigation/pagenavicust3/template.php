<?php

if (!defined('B_PROLOG_INCLUDED') || (B_PROLOG_INCLUDED !== true)) {
    die();
}

if (!$arResult["NavShowAlways"]) {
    if (
        (0 == $arResult["NavRecordCount"])
        ||
        ((1 == $arResult["NavPageCount"]) && (false == $arResult["NavShowAll"]))
    ) {
        return;
    }
}
$navQueryString      = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$navQueryStringFull  = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

if($arResult['NavPageNomer'] > 1){
    $APPLICATION->AddHeadString('<meta name="robots" content="noindex, follow" />', true);
}
?>
<?$urlString = $_GET;?>
<div class="pagination">
    <a class="word" data-pagenum="<?= ($arResult["NavPageNomer"] - 1) ?>">< Назад</a>
    <a data-pagenum="1" class="<?=($arResult["NavPageNomer"]==1) ? 'active' : ''?>">1</a>
    <?=(($arResult["NavPageNomer"] > 3) && ($arResult['NavPageCount'] > 5)) ? '<span>...</span>' : ''?>
    <?$curP = 2;//начинаем со второй страницы?>
    <?$endP = $arResult["NavPageCount"];?>

    <?while($curP <= $endP):?>
        <?if($arResult['NavPageCount'] > 5):// 6 и более страниц?>

            <?if($arResult["NavPageNomer"] <= 3)://первые 3 страницы?>
                <?if($curP <= 4):?>
                    <a data-pagenum="<?=$curP?>" class="<?=($curP == $arResult["NavPageNomer"]) ? 'active' : ''?>"><?=$curP;?></a>
                <?else:?>
                    <?$curP = $endP;//завершаем цикл?>
                <?endif?>
            <?elseif(($arResult['NavPageCount']-$arResult["NavPageNomer"]) < 3)://последние 3 страницы?>

                <?if($curP < ($arResult["NavPageNomer"]-1)):?>
                    <?$curP = $arResult["NavPageNomer"]-1;//текущая страница равна предыдущей?>
                    <a data-pagenum="<?=$curP?>" class="<?=($curP == $arResult["NavPageNomer"]) ? 'active' : ''?>"><?=$curP;?></a>
                <?else:?>
                    <a data-pagenum="<?=$curP?>" class="<?=($curP == $arResult["NavPageNomer"]) ? 'active' : ''?>"><?=$curP;?></a>
                <?endif;?>

            <?else://все остальные?>

                <?if($curP < ($arResult["NavPageNomer"]-1)):?>
                    <?$endP = $arResult["NavPageNomer"]+2?>
                    <?$curP = $arResult["NavPageNomer"]-1;//текущая страница равна предыдущей?>
                    <a data-pagenum="<?=$curP?>" class="<?=($curP == $arResult["NavPageNomer"]) ? 'active' : ''?>"><?=$curP;?></a>
                <?else:?>
                    <a data-pagenum="<?=$curP?>" class="<?=($curP == $arResult["NavPageNomer"]) ? 'active' : ''?>"><?=$curP;?></a>
                <?endif;?>

            <?endif;?>

        <?else:?>
            <a data-pagenum="<?=$curP?>" class="<?=($curP == $arResult["NavPageNomer"]) ? 'active' : ''?>"><?=$curP;?></a>
        <?endif;?>

        <?$curP++?>
    <?endwhile;?>
    <?=((($arResult['NavPageCount']-$arResult["NavPageNomer"]) > 3) && $arResult['NavPageCount'] > 5) ? '<span>...</span>' : ''?>
    <?if($arResult['NavPageCount'] > 5 && (($arResult['NavPageCount']-$arResult["NavPageNomer"]) > 2)):?>
        <a data-pagenum="<?=$arResult["NavPageCount"]?>" class="<?=($arResult["NavPageNomer"]==$arResult["NavPageCount"]) ? 'active' : ''?>"><?=$arResult["NavPageCount"]?></a>
    <?endif;?>

    <a  class="word" data-pagenum="<?= ($arResult["NavPageNomer"] + 1) ?>">вперед ></a>
</div>