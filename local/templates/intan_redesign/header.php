<!doctype html>
<html>
<head>
	<title><? $APPLICATION->ShowTitle() ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content='True' name='HandheldFriendly' />
	<meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport' />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <? $APPLICATION->ShowMeta("robots") ?>
    <? $APPLICATION->ShowMeta("keywords") ?>
    <? $APPLICATION->ShowMeta("description") ?>
    <? $APPLICATION->ShowCSS(); ?>
    <? $APPLICATION->ShowHeadStrings() ?>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link  type="text/css" rel="stylesheet" href="/css/stylesheets/styles.css"  charset="utf-8">
		<link  type="text/css" rel="stylesheet" href="/css/stylesheets/adaptive.css"  charset="utf-8">
    <script type="text/javascript" src="/js/global.js"></script>
    <!--[if lt IE 9]>
    <script  type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script type="text/javascript">
        (function(w, d, e) {
            var a = 'all', b = 'tou'; var src = b + 'c'
                +'h'; src = 'm' + 'o' + 'd.c' + a + src;
            var jsHost = (("https:" ==
                d.location.protocol) ? "https://" : "http://")+ src;
            s = d.createElement(e); p =
                d.getElementsByTagName(e)[0]; s.async = 1; s.src =
                jsHost
                +"."+"r"+"u/d_client.js?param;ref"+escape(d.referrer)
                +";url"+escape(d.URL)+";cook"+escape(d.cookie)+";";
            if(!w.jQuery) { jq = d.createElement(e);
                jq.src = jsHost  +"."+"r"+'u/js/jquery-1.5.1.min.js';
                p.parentNode.insertBefore(jq, p);}
            p.parentNode.insertBefore(s, p);
        }(window, document, 'script'));
    </script>

    <script src='https://www.google.com/recaptcha/api.js'></script>
	<?php if ( getCodeCity() == "spb" ) : ?>
		<script type="text/javascript">
			var __cs = __cs || [];
			__cs.push(["setAccount", "rY9fRtkswDGnUgzTG7rgw6gVcxi8FTq7"]);
			__cs.push(["setHost", "//server.comagic.ru/comagic"]);
		</script>
		<script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>
	<?php endif ?>
	<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=Ed*aBplwvBPBAnB7LboxkNVh9UwFj1b2t7xgYC0fxGWai7*HJz8Mb3TZ23Ox1Ro93mzl7W6iTgCEc8f1omlusr1OjdExrBKstoGzku71nedaj4UWM6PeUlQWWuonYsfbXMinlIyrYGidVyrFuFslao6kytxIYtqET77JRAsJMN8-';</script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	
	fbq('init', '628528517248435');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=628528517248435&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
<!-- VK Pixel Code -->
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=IkjlJdVuiPoVQd4DanrmNR4Y/xRSHivVQlQE09wxL19hSXQllM/Hj6OjSUXclQ1/JF1KngVzxJpaOJCVtKKz2Gp7yLgFLy7ZdFliLLp5ULa2ILv3aoW6ed/riduKfhL3FWb0dKCnLOlgCVLvsbf2yFoQvn1xwXwOC49tfcyt6wo-&pixel_id=1000016795';</script>
<!-- end VK Pixel Code -->
</head>
<?  if (SITE_DIR == '/krasnodar/'){?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-56125435-13', 'auto');
        ga('send', 'pageview');

    </script>
<?}
else {?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-56125435-11', 'auto');
    ga('send', 'pageview');

</script>
<?}?>
<script src="/js/sourcebuster.min.js"></script>
<script>

    sbjs.init(),
        sbjs.get.current.typ,// тип трафика

        sbjs.get.current.src,// источник (utm_source)

        sbjs.get.current.mdm, // канал (utm_medium)

        sbjs.get.current.cmp,  // кампания (utm_campaign)

        sbjs.get.current.cnt, // контент (utm_content)

        sbjs.get.current.trm

</script>
<body>
<!-- Kavanga.kTrack START -->
<!-- intan.ru -->
<!-- ZeroPixel -->
<script type="text/javascript">var kavanga={};
kavanga.cl=247;
kavanga.protocol=('https:' == document.location.protocol ? 'https://' : 'http://');
kavanga.domain="l.kavanga.ru";
kavanga.s = document.createElement('script'); kavanga.s.type = 'text/javascript'; kavanga.s.async = true;
kavanga.rand_version = Math.floor((Math.random()*100)+1);
kavanga.s.src = kavanga.protocol + kavanga.domain +'/js/leads_'+kavanga.cl+'.js?v='+kavanga.rand_version;
kavanga.s0 = document.getElementsByTagName('script')[0]; kavanga.s0.parentNode.insertBefore(kavanga.s, kavanga.s0);
function kCall1(){
window._ktIsLead = 1;
kavanga.ready();
}
function kCall2(){
window._ktIsLead = 2;
kavanga.ready();
}
</script>
<!-- Kavanga.kTrack END -->
<? $APPLICATION->ShowPanel(); ?>
<?CModule::IncludeModule("iblock");?>
<? global $yaId; ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            <? if (SITE_DIR != '/krasnodar/')
            {
            $yaId = 9957124;
            ?>
                try {
                    w.yaCounter9957124 = new Ya.Metrika({id:9957124,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
                } catch(e) { }
          <?  }
            else
            {
            $yaId = 28119171;
            ?>
                try {
                    w.yaCounter28119171 = new Ya.Metrika({id:28119171,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
                } catch(e) { }
          <?  }
            ?>
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<!--Soloway -->
<script type="text/javascript">
    (function(h){function k(){var a=function(d,b){if(this instanceof AdriverCounter)d=a.items.length||1,a.items[d]=this,b.ph=d,b.custom&&(b.custom=a.toQueryString(b.custom,";")),a.request(a.toQueryString(b));else return a.items[d]};a.httplize=function(a){return(/^\/\//.test(a)?location.protocol:"")+a};a.loadScript=function(a){try{var b=g.getElementsByTagName("head")[0],c=g.createElement("script");c.setAttribute("type","text/javascript");c.setAttribute("charset","windows-1251");c.setAttribute("src",a.split("![rnd]").join(Math.round(1E6*Math.random())));c.onreadystatechange=function(){/loaded|complete/.test(this.readyState)&&(c.onload=null,b.removeChild(c))};c.onload=function(){b.removeChild(c)};b.insertBefore(c,b.firstChild)}catch(f){}};a.toQueryString=function(a,b,c){b=b||"&";c=c||"=";var f=[],e;for(e in a)a.hasOwnProperty(e)&&f.push(e+c+escape(a[e]));return f.join(b)};a.request=function(d){var b=a.toQueryString(a.defaults);a.loadScript(a.redirectHost+"/cgi-bin/erle.cgi?"+d+"&rnd=![rnd]"+(b?"&"+b:""))};a.items=[];a.defaults={tail256:document.referrer||"unknown"};a.redirectHost=a.httplize("//ad.adriver.ru");return a}var g=document;"undefined"===typeof AdriverCounter&&(AdriverCounter=k());new AdriverCounter(0,h)})
    ({"sid":208327,"bt":62,"custom":{"153":"user_id"}});
</script>

<!--Soloway event -->
<script type="text/javascript">
    function solowayEvent(){
        (function (h)
        {function k() {
            var a = function (d,b) {
                if (this instanceof AdriverCounter) d = a.items.length || 1,
                    a.items[d] = this, b.ph = d,
                b.custom && (b.custom = a.toQueryString(b.custom,";")),
                    a.request(a.toQueryString(b));
                else return a.items[d]};
            a.httplize = function (a) {return (/^\/\//.test(a)?location.protocol:"")+a};
            a.loadScript = function (a) {
                try {
                    var b = g.getElementsByTagName("head")[0],
                        c = g.createElement("script");
                    c.setAttribute("type", "text/javascript");
                    c.setAttribute("charset", "windows-1251");
                    c.setAttribute("src",a.split("![rnd]").join(Math.round(1E6*Math.random())));
                    c.onreadystatechange = function () {
                        /loaded|complete/.test(this.readyState)&&(c.onload=null,b.removeChild(c))};
                    c.onload = function () {b.removeChild(c)};
                    b.insertBefore(c,b.firstChild)} catch (f) {}};
            a.toQueryString = function (a,b,c) {
                b = b || "&";c = c || "=";var f = [],e;
                for (e in a) a.hasOwnProperty(e) && f.push(e+c+escape(a[e]));
                return f.join(b)};
            a.request = function (d) {var b = a.toQueryString(a.defaults);
                a.loadScript(a.redirectHost+"/cgi-bin/erle.cgi?"+d+"&rnd=![rnd]"+(b?"&"+b:""))};
            a.items = [];
            a.defaults = { tail256: document.referrer || "unknown" };
            a.redirectHost = a.httplize("//ad.adriver.ru");return a}
            var g = document;
            "undefined" === typeof AdriverCounter && (AdriverCounter = k());
            new AdriverCounter(0, h)})
        ({"sid":208327,"bt":62,"custom":{"153":"user_id"}});
    }
</script>

<?$listCity = [];
$arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listcities']];
$res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME', 'CODE', 'PROPERTY_F_COORD', 'PROPERTY_F_METRO', 'PROPERTY_F_PHONE', 'PROPERTY_F_ADVERBIAL'));
while($ar_res = $res->GetNext()){
    $listCity[$ar_res['CODE']] = $ar_res;
}
?>

<noscript><div><img src="//mc.yandex.ru/watch/<?=$yaId?>" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<div class="modal-wrapper">
	<div id="appointment" class="appointment">
        <div class="h2">Записаться на прием</div>
		<p>Мы подберем подходящий вам вариант: <br> Позвоните <a class="ph" href="tel:<?=$listCity[getCodeCity()]['PROPERTY_F_PHONE_VALUE']?>"><?=$listCity[getCodeCity()]['PROPERTY_F_PHONE_VALUE']?></a> или <a href="#call-me2" class="order-call-call">оставьте номер</a></p>
	</div>
    <div id="call-me-thx" >
        <a class="fancy" href="#call-me-thx"></a>
        <div class='thx '><div class='table'><div class='row'><div class='cell'>Ваша заявка успешно отправлена!<br>Спасибо!</div></div></div></div>
    </div>
	<div id="call-me" class="call-me">
        <div class="h2">Заказать звонок</div>
		<form class="sb_form" action="" method="post" onsubmit="solowayEvent(); yaCounter<?=$yaId?>.reachGoal('call_order'); ga('send', 'event', 'form', 'form_call_back'); return true;">
            <input name="form_subm" value="<?=$arIblockAccord['zvonok']?>" type="hidden" />
			<div class="table">
				<div class="row">
					<div class="cell label">
						Ваше имя
					</div>
					<div class="cell inputs">
						<input name="name" required type="text">
					</div>
				</div>
				<div class="row">
					<div class="cell label">
						Контактный телефон
					</div>
					<div class="cell inputs">
						<input name="phone" required type="text" title="Введите телефон вформате +7 (999) 999-99-99"  placeholder="+7 (9__)-___-__-__" class="phone">
					</div>
				</div>
				<div class="row">
					<div class="cell label">
						Удобное время для звонка
					</div>
					<div class="cell inputs">
                        <select name="time" id="" class="time">
                            <option value="сейчас">Сейчас</option>
                            <option value="9-12">9-12</option>
                            <option value="13-16">13-16</option>
                            <option value="16-19">16-19</option>
                        </select>
					</div>
				</div>
				<div class="row">
					<div class="cell label">
						Выберите клинику
					</div>
					<div class="cell inputs">
                        <select name="klinika" id="" class=""  data-placeholder="Выберите клинику">
                            <option value="">Выберите клинику</option>
                            <?$arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['dental_centers']];
                            $arrFilter = getFilArrayByCity($arrFilter);
                            $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME'));
                            while($ar_res = $res->GetNext()){?>
                                <option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                            <?}?>
                        </select>
					</div>
				</div>
				<div class="row">
					<div class="cell label"></div>
					<div class="cell button">
                        <input onclick="yaCounter9957124.reachGoal('call_back'); kCall1(); return true;" type="submit" value="Отправить">
                    </div>
				</div>
			</div>

			<img src="/images/orenge-intan.png" alt="">
		</form>
    </div>
    <div id="call-me2" class="call-me">
        <div class="h2">Записаться на приём</div>
        <form class= "sb_form" action="" method="post" onsubmit="solowayEvent(); yaCounter<?=$yaId?>.reachGoal('forma_zapisi_done');  kCall1(); ga('send', 'event', 'form', 'form_priem'); return true;">
            <input name="form_subm" value="<?=$arIblockAccord['add_reception']?>" type="hidden" />
            <div class="table">
                <div class="row">
                    <div class="cell label">
                        Ваше имя
                    </div>
                    <div class="cell inputs">
                        <input name="name" required type="text" placeholder="Имя" >
                    </div>
                </div>
                <div class="row">
                    <div class="cell label">
                        Контактный телефон
                    </div>
                    <div class="cell inputs">
                        <input name="kontakt_phone" required type="text" title="Введите телефон вформате +7 (999) 999-99-99"  placeholder="+7 (9__)-___-__-__" class="phone">
                    </div>
                </div>
                <div class="row">
                    <div class="cell label">
                        Удобное время для звонка
                    </div>
                    <div class="cell inputs">
                        <select name="vremya_zvonka" id="" class="time" data-placeholder="Удобное время для звонка">
							<option value="">Удобное время для звонка</option>
                            <option value="сейчас">Сейчас</option>
                            <option value="9-12">9-12</option>
                            <option value="13-16">13-16</option>
                            <option value="16-19">16-19</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="cell label">
                        Выберите клинику
                    </div>
                    <div class="cell inputs">
                        <select name="tsentr" id="" class=""  data-placeholder="Выберите клинику">
                            <option value="">Выберите клинику</option>
                            <?$arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['dental_centers']];
                            $arrFilter = getFilArrayByCity($arrFilter);
                            $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME'));
                            while($ar_res = $res->GetNext()){?>
                                <option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="cell label"></div>
                    <div class="cell button">
                        <input  type="submit" value="Отправить">
                    </div>
                </div>
            </div>

            <img src="/images/orenge-intan.png" alt="">
        </form>
	</div>

	<div id="call-me-otzivForm" class="call-me">
			<div class="h2">Отправить отзыв</div>
			<form class="sb_form" action="" method="post">
					<input name="form_subm" value="<?=$arIblockAccord['otzyiv']?>" type="hidden" />
					<div class="table">
							<div class="row">
									<div class="cell label">
											Ваше имя
									</div>
									<div class="cell inputs">
											<input name="name" required type="text" placeholder="Имя">
									</div>
							</div>
							<div class="row">
									<div class="cell label">
											Email
									</div>
									<div class="cell inputs">
											<input name="otzyv_email" required  type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email">
									</div>
							</div>
							<div class="row">
									<div class="cell label">
											Центр
									</div>
										<div class="cell inputs">
								<select name="F_BIND[]" id="form_dent">
										<option value="0">Центр не выбран</option>
										<?$arrFilter = ['IBLOCK_ID'=>$arIblockAccord['dental_centers'], 'ACTIVE'=>'Y'];
                                        $arrFilter = getFilArrayByCity($arrFilter);
                                        $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter);
										while($ar_res = $res->GetNext()){?>
												<option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
										<?}?>
								</select>
								</div>
						</div>
						<div class="row">
								<div class="cell label">
										Врач
								</div>
									<div class="cell inputs">
										<select name="F_BIND[]" id="form_doc">
												<option value="">Врач не выбран</option>
												<?$arrFilter = ['IBLOCK_ID'=>$arIblockAccord['blogers'], 'ACTIVE'=>'Y'];
                                                $arrFilter = getFilArrayByCity($arrFilter);
                                                $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter);
												while($ar_res = $res->GetNext()){?>
														<option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
												<?}?>
										</select>
							</div>
						</div>
						<div class="row">
								<div class="cell label">
										Отзыв
								</div>
									<div class="cell inputs">
								<textarea name="DETAIL_TEXT" placeholder="Отзыв"></textarea>
							</div>
						</div>
							<div class="row">
									<div class="cell label"></div>
									<div class="cell button">
											<input onclick="yaCounter9957124.reachGoal('zapis_na_priyom'); kCall1(); return true;" type="submit" value="Отправить">
									</div>
							</div>
					</div>

					<img src="/images/orenge-intan.png" alt="">
			</form>
	</div>

</div>
<header>
	<div class="head">
		<div class="wrap">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                "ROOT_MENU_TYPE" => "top_menu",	// Тип меню для первого уровня
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
                    "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "Y",	// Разрешить несколько активных пунктов одновременно
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                ),
                false
            );?>
			<a href="/<?=$listCity[getCodeCity()]["CODE"]?>/karera/" onclick="yaCounter9957124.reachGoal('rabota_v_intan'); kCall1(); return true;" class="appointment fr"><span class="toggler">Работа в Интан</span></a>
		</div>
	</div>
	<div class="toggle-head_small"><i class="fa fa-bars"></i></div>
	<div class="head_small">

		<div class="menu-adaptive">
			<ul>
				<li><a href="#"><span class="toggler">Услуги</span></a>
					<ul class="under">
							<?$arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listmedservices']];
                            $arrFilter = getFilArrayByCity($arrFilter);
                            $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME', 'DETAIL_PAGE_URL'));
							while($ar_res = $res->GetNext()){?>
									<li class="<?=stristr($_SERVER['REQUEST_URI'], $ar_res['DETAIL_PAGE_URL']) ? 'active' : ''?>" >
										<a  href="<?=$ar_res['DETAIL_PAGE_URL']?>"><span><?=$ar_res['NAME']?></span></a>
									</li>
							<?}?>
					</ul>
				</li>
			<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu_small", Array(
					"ROOT_MENU_TYPE" => "top_menu",	// Тип меню для первого уровня
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"ALLOW_MULTI_SELECT" => "Y",	// Разрешить несколько активных пунктов одновременно
							"MENU_CACHE_TYPE" => "N",	// Тип кеширования
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
					),
					false
			);?>
			<li><a href="/otzyv/"><span>Отзывы</span></a></li>
			<li><a href="/voprosy/"><span>Вопрос-ответ</span></a></li>
			<li><a href="/search/"><span>Поиск по сайту</span></a></li>
            <li><a href="/spb/karera/"><span>Карьера</span></a></li>
		</ul>
		</div>
	</div>
	<div class="wrap">
        <div class="social-icons">
            <noindex><a target="_blank" class="vk" rel="nofollow" href="http://vk.com/intan"><i></i></a></noindex>
            <noindex><a target="_blank" class="fb" rel="nofollow" href="https://www.facebook.com/pages/%D0%98%D0%9D%D0%A2%D0%90%D0%9D/200327416812479"><i></i></a></noindex>
            <noindex><a target="_blank" class="ok" rel="nofollow" href="http://ok.ru/group/52894927552599"><i></i></a></noindex>
        </div>
		<div class="logo ib">
			<a href="/" class="logo"><img src="/images/logo.png" alt=""></a>
			<div class="order-call">
                <span class="call_phone_1"><a href="tel:<?=$listCity[getCodeCity()]['PROPERTY_F_PHONE_VALUE']?>" class="tel"><?=$listCity[getCodeCity()]['PROPERTY_F_PHONE_VALUE']?></a></span>
				<a href="#call-me" class="order-call-call"><span>Заказать звонок</span></a>
				<a href="#call-me2" onclick="yaCounter9957124.reachGoal('zapis_na_priem_new'); kCall1(); return true;" class="appointment"><span>Записаться на прием</span></a>
			</div>
		</div>
		<div class="addres">
            <?$listClinic = [];
            $arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['dental_centers']];
            $arrFilter = getFilArrayByCity($arrFilter);
            $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME', 'DETAIL_PAGE_URL', 'PROPERTY_F_SALE', 'PROPERTY_F_SALE.DETAIL_PAGE_URL', 'PROPERTY_F_NEW'));
            while($ar_res = $res->GetNext()){
                $listClinic[] = $ar_res;
            }?>
			<div class="title">
				<b><?=count($listClinic)?></b>
				<span><?=plural(count($listClinic), 'центр', 'центра', 'центров')?> стоматологии в</span>
                <a href="javascript:;" class="change-city"><span><?=$listCity[getCodeCity()]['PROPERTY_F_ADVERBIAL_VALUE'] ? $listCity[getCodeCity()]['PROPERTY_F_ADVERBIAL_VALUE'] : $listCity[getCodeCity()]['NAME']?></span></a>
                <div class="change-city-drop-is_saintp">
                    <?
                    $whCity = '';
                    $gb = new IPGeoBase();
                    $data = $gb->getRecord($_SERVER['REMOTE_ADDR']);

                    if($data['city'] == 'Краснодар' || $data['region'] == 'Краснодарский край'){
                        $whCity = 'Краснодар';
                        $codeCity = 'krd';
                    }elseif($data['city'] == 'Санкт-Петербург' || $data['region'] == 'Ленинградская область'){
                        $whCity = 'Санкт-Петербург';
                        $codeCity = 'spb';
                    }else{
                        $whCity = 'Санкт-Петербург';
                        $codeCity = 'spb';
                    }
                    ?>
                    <div class="h2">Ваш город — <?=$whCity?>?</div>
                    <a href="/" class="no" onclick="$.cookie('selCity', '<?=$codeCity?>', { expires: 365, path: '/' })">Да</a>
                    <a href="#"  class="yes">Выбрать другой город</a>
                    <div class="clearfix"></div>

                    <div class="change-city-drop-select">
                        <div class="h2">Выберите город</div>
                        <?foreach($listCity as $oneCity):?>
                            <a href="/" onclick="$.cookie('selCity', '<?=$oneCity['CODE']?>', { expires: 365, path: '/' })"><span><?=$oneCity['NAME']?></span></a>
                        <?endforeach?>
                        <div class="clearfix"></div>
                    </div>
                 </div>
			</div>
			<ul>
                <?$new == 0;?>
                <?foreach($listClinic as $oneClinic){?>
                    <li>
                        <a class="<?=stristr($_SERVER['REQUEST_URI'], $oneClinic['DETAIL_PAGE_URL']) ? 'active' : ''?>" href="<?=$oneClinic['DETAIL_PAGE_URL']?>"><?=$oneClinic['NAME']?></a>
                        <?if($oneClinic['PROPERTY_F_NEW_VALUE'] == 1): $new = 1;?>
                            <a data-new="Открытие"></a>
                        <?endif;?>
                        <?if ($new == 0) {?>
                            <?if($oneClinic['PROPERTY_F_SALE_DETAIL_PAGE_URL']):?>
                                <a data-action="акция!" href="<?=str_replace('//', '/', $oneClinic['PROPERTY_F_SALE_DETAIL_PAGE_URL'])?>"></a>
                            <?endif;?>
                        <?}?>
                        <?$new = 0;?>                        
                    </li>
                <?}?>
			</ul>
		</div>
		<div class="address-small">
			<?if(SITE_ID == 's4'):?>
					<a href="tel:+78612770202" class="tel">+7 (861) 277-02-02</a>
			<?else:?>
					<a href="tel:+78127770202" class="tel">+7 (812) 777-02-02</a>
			<?endif;?>
			<div class="button-city">
					<a href="javascript:;" class="change-city-mobile">
					<span class="dashed"><?=$listCity[getCodeCity()]['NAME']?></span>
				</a>
			</div>
		</div>
		<div class="writeToAppointment">
			<a href="#call-me2" class="appointment"><span class="toggler">Записаться на прием</span></a>
		</div>
	</div>
	<nav>
		<div class="wrap">
            <?$arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listmedservices']];
            $arrFilter = getFilArrayByCity($arrFilter);
            $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME', 'DETAIL_PAGE_URL'));
            while($ar_res = $res->GetNext()){?>
                <a class="<?=stristr($_SERVER['REQUEST_URI'], $ar_res['DETAIL_PAGE_URL']) ? 'active' : ''?>" href="<?=$ar_res['DETAIL_PAGE_URL']?>"  class="active"><?=$ar_res['NAME']?></a>
            <?}?>
		</div>
	</nav>
</header>
