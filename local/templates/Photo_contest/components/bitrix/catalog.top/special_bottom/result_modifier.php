<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
foreach($arResult["ITEMS"] as $key => $arItem) {

   // работаем с фотками
   
   if(!empty($arItem["PREVIEW_PICTURE"])){
       $mod = $arItem["PREVIEW_PICTURE"]['WIDTH'] - $arItem["PREVIEW_PICTURE"]['HEIGHT'];
        if ($mod >= 0) {
            $arResult["ITEMS"][$key]['PHOTO'] = CFile::ResizeImageGet(
                 $arItem["PREVIEW_PICTURE"], array('width'=>94, 'height'=>94), BX_RESIZE_IMAGE_EXACT, false
            );
        } else {
            $arResult["ITEMS"][$key]['PHOTO'] = CFile::ResizeImageGet(
                 $arItem["PREVIEW_PICTURE"], array('width'=>94, 'height'=>94), BX_RESIZE_IMAGE_EXACT, false
            );
        }
   }
}
?>