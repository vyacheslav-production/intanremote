<!doctype html>
<html lang="ru" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<title><? $APPLICATION->ShowTitle() ?></title>
	<? $APPLICATION->ShowHead() ?>

	<link rel="stylesheet" href="/css/reset.css" />
	<?//link rel="stylesheet" href="/css/special.css" /?>
	<link rel="stylesheet" href="/css/special-prod.min.css" />
	<!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
	<!--[if lt IE 8]>
            <link rel="stylesheet" href="/css/specialie7.css" />
        <![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>
        <script src="/bitrix/templates/Photo_contest/script.js" type="text/javascript"></script>
        <script src="/bitrix/templates/Photo_contest/js/jquery.inputmask.js" type="text/javascript"></script>
        <script src="/bitrix/templates/Photo_contest/js/jquery.zclip.min.js" type="text/javascript"></script>
        <? //$APPLICATION->AddHeadScript('/bitrix/templates/Photo_contest/script.js'); ?>


</head>
<body>
<!-- Kavanga.kTrack START -->
<!-- intan.ru -->
<!-- ZeroPixel -->
<script language="JavaScript">
    <!--
    (function() {
        var kref = '';
        try {kref = escape(document.referrer);} catch(e){};
        var kt = document.createElement('script'); kt.type = 'text/javascript'; kt.async = true;
        kt.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'dsp.kavanga.ru/pixel-get.js?site_id=6598&prr=' + kref + '&rnd=' + Math.round(Math.random()*1000000);
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(kt, s);
    })();
    //-->
</script>
<!-- Kavanga.kTrack END -->
<? $APPLICATION->ShowPanel(); ?>
<header class="b-sp-header" id="top">
	<div class="l-cont">
		<h1 class="b-sp-logo"><a href="/">Премия ИНТАН за лучшую улыбку</a></h1>
		<nav class="b-sp-menu cf">
                    <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "menu",
                                Array(
                                        "ROOT_MENU_TYPE" => "special",
                                        "MAX_LEVEL" => "1",
                                        "CHILD_MENU_TYPE" => "left",
                                        "USE_EXT" => "N",
                                        "DELAY" => "N",
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "MENU_CACHE_TYPE" => "N",
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "MENU_CACHE_GET_VARS" => ""
                                )
                        );?>
                </nav>
	</div>
</header>