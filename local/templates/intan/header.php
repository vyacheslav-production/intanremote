<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <meta name='yandex-verification' content='6916f87e4b68667a' />
        <link rel="stylesheet" type="text/css" media="all" href="/css/reset.css">
        <? $APPLICATION->ShowHead() ?>
        <!--[if lt IE 9]>
        <script src="/js/html5.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" media="all" href="/css/ie8.css" />
        <![endif]-->
        <!--[if lte IE 7]>
        <link rel="stylesheet" type="text/css" media="all" href="/css/ie7.css" />
        <![endif]-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js" type="text/javascript"></script>
        <script src="/js/scripts.js" type="text/javascript"></script>
        <script src="/js/slides.min.jquery.js" type="text/javascript"></script>
        <script src="/js/jcarousellite_1.0.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="/js/jScrollPane.js"></script>
        <script type="text/javascript" src="/js/jquery.mousewheel.js"></script>

        <script type="text/javascript" src="/js/scrollTo.js"></script>
        <script type="text/javascript" src="/js/jquery.cookie.js"></script>



        <link rel="shortcut icon" href="/favicon.png" type="image/png" />
    </head>
    <body>
    <!-- Kavanga.kTrack START -->
    <!-- intan.ru -->
    <!-- ZeroPixel -->
    <script language="JavaScript">
        <!--
        (function() {
            var kref = '';
            try {kref = escape(document.referrer);} catch(e){};
            var kt = document.createElement('script'); kt.type = 'text/javascript'; kt.async = true;
            kt.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'dsp.kavanga.ru/pixel-get.js?site_id=6598&prr=' + kref + '&rnd=' + Math.round(Math.random()*1000000);
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(kt, s);
        })();
        //-->
    </script>
    <!-- Kavanga.kTrack END -->
        <? $APPLICATION->ShowPanel(); ?>
        <div id="skip-link" style="display:none;">
            <a href="#main_content">Skip to main content</a>
        </div>
        <div id="zvonok_prostynya">
            <div id="zvonok_inc">
                <h2>Заказать звонок</h2>
                <div id="zvonok_close"></div>
                <?
                $APPLICATION->IncludeComponent("bitrix:iblock.element.add.form", "zakaz_zvonok", array(
                    "IBLOCK_TYPE" => "on_line",
                    "IBLOCK_ID" => "27",
                    "STATUS_NEW" => "N",
                    "LIST_URL" => "",
                    "USE_CAPTCHA" => "N",
                    "AJAX_MODE" => "Y",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "Y",
                    "USER_MESSAGE_EDIT" => "Спасибо, заявка отправлена.",
                    "USER_MESSAGE_ADD" => "Спасибо, заявка отправлена.",
                    "DEFAULT_INPUT_SIZE" => "30",
                    "RESIZE_IMAGES" => "N",
                    "PROPERTY_CODES" => array(
                        0 => "NAME",
                        1 => "99",
                        2 => "100",
                        3 => "101",
                    ),
                    "PROPERTY_CODES_REQUIRED" => array(
                        0 => "NAME",
                        1 => "99",
                    ),
                    "GROUPS" => array(
                        0 => "2",
                    ),
                    "STATUS" => "ANY",
                    "ELEMENT_ASSOC" => "CREATED_BY",
                    "MAX_USER_ENTRIES" => "100000",
                    "MAX_LEVELS" => "100000",
                    "LEVEL_LAST" => "Y",
                    "MAX_FILE_SIZE" => "0",
                    "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                    "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                    "SEF_MODE" => "N",
                    "SEF_FOLDER" => "",
                    "CUSTOM_TITLE_NAME" => "Имя",
                    "CUSTOM_TITLE_TAGS" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                    "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                    "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                    "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                    "CUSTOM_TITLE_DETAIL_TEXT" => "",
                    "CUSTOM_TITLE_DETAIL_PICTURE" => ""
                        ), false
                );
                ?>

            </div>
        </div>
        <div class="container">
            <header id="page_header">
                <div class="h_phone">
                    <h1 id="page_h"><a href="/" title="на главную">Интан - Центры имплантации и стоматологии</a></h1>
                    <div class="header_phone" style="text-align:left"> +7(812) 777-02-02 <!-- +7 (812) 305-02-02 --></div>
                    <div id="header_zvonok"><span>Заказать звонок</span></div>
                </div>
                <div class="header_right">
                    <div class="cen_summ_num">
                        13
                    </div>
                    <div class="cen_summ_text">
                        <p>центров стоматологии в Санкт-Петербурге</p>
                        <div class="cen_summ_col">
                            <a href="http://intan.ru/aksii/68516/" class="href_action" ><!-- <img src="/images/green_special_icon.png">--></a>
							<p><a href="/stomatologiya_na_komendantskom42/">Комендантский пр., д. 42, корп. 1</a></p>
                            <p><a href="/somatologiya_na_prospekte_lunacharskogo/">Луначарского пр., дом 11, к.3</a></p>
                            <p><a href="/stomatologiya_na_buharestskoy/">Славы пр./Бухарестская, 96</a></p>
                            <!-- <p><a href="/stomatologiya_na_buharestskoy/">Бухарестская ул., дом 96</a></p> -->
                            
                            <p><a href="/stomatologiya_na_ligovskom/">Лиговский пр., дом 125</a></p>

                        </div>
                        <div class="cen_summ_col">
                            <p><a href="/stomatologiya_na_bogatyrskom/">Богатырский пр., дом 51, корп.1</a></p>
                            <p><a href="/stomatologiya_na_rossiyskom/">Российский пр., дом 8</a></p>
                            <p><a href="/stomatologiya_na_budapeshtskoy/">Будапештская ул., дом 97, к.2</a></p>
                            <p><a href="/stomatologiya_na_bolshoy_pushkarskoy/">Большая Пушкарская ул., дом 41</a></p>
                       
                        </div>
                        <div class="cen_summ_col last">
                           <p><a href="/stomatologiya_na_voznesenskom/">Вознесенский пр. д. 21</a></p>
                            <p><a href="/stomatologiya_na_prosvesheniya/">пр. Просвещения, дом 87, к.2</a></p>
                            <p><a href="/stomatologiya_na_stachek/">пр. Стачек, дом 69 А</a></p>
							<p><a href="/stomatologiya_na_komendantskom/">Комендантский пр., дом 7, к.1</a></p>
                            
                            <p><a href="/stomatologiya_novyj_okkervil/">ул.Ленинградская д.3</a></p> 
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="top_nemu_wrap">
                    <div class="top_menu_left"></div>
                    <div class="top_menu_right"></div>
                    <?
                    $APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                        "ROOT_MENU_TYPE" => "top_menu", // Тип меню для первого уровня
                        "MAX_LEVEL" => "1", // Уровень вложенности меню
                        "CHILD_MENU_TYPE" => "left", // Тип меню для остальных уровней
                        "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "DELAY" => "N", // Откладывать выполнение шаблона меню
                        "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
                        "MENU_CACHE_TYPE" => "N", // Тип кеширования
                        "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                        "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
                            ), false
                    );
                    ?>
                </div>
            </header>
        </div>
        <div class="green_bump"></div>
        <section id="main_content">
            <div class="container">
