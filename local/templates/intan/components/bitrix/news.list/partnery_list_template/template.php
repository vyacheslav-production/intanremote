<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); ?>
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br />
    <? endif; ?>
    <?
    $pSectP = $arResult["ITEMS"]["0"]["IBLOCK_SECTION_ID"];
    ?>
    <? foreach ($arResult["ITEMS"] as $pInd => $arItem): ?>
        <? if ($pInd == 0) { ?>
            <div class="partners_list">
                <h2 class="secondary_h"><?
                $pCnt = 0;
        $p_type = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"])->GetNext();
        echo $p_type["NAME"];
            ?></h2>
            <? } else if ($pSectP != $arItem["IBLOCK_SECTION_ID"]) { ?>
                <div class="clear"></div>
                </div>
                <div class="partners_list">
                    <h2 class="secondary_h"><?
                    $pCnt = 0;
        $p_type = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"])->GetNext();
        echo $p_type["NAME"];
                ?></h2>
                    <?
                    $pSectP = $arItem["IBLOCK_SECTION_ID"];
                }
                ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                    <?
                    $pClass = "";
                    (($pCnt+1)%3==0)?($pClass = " last"):($pClass = "");
                    if($pCnt==0){$pClass = " clear";}
                    ?>
                    <div class="partners_list_item<?=$pClass?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                        <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                            <div class="partners_img"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img class="preview_picture" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"  /></a></div>
                        <? else: ?>
                            <div class="partners_img"><img class="preview_picture" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" /></div>
                        <? endif; ?>
                    <? endif ?>
                    <? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]): ?>
                        <span class="news-date-time"><? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
                    <? endif ?>
                    <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                        <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                            <p class="rartners_h"><a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a></p>
                        <? else: ?>
                            <p class="rartners_h"><? echo $arItem["NAME"] ?></p>
                        <? endif; ?>
                    <? endif; ?>
                    <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                        <div class="partners_adress"><? echo $arItem["PREVIEW_TEXT"]; ?></div>
                    <? endif; ?>
                </div>
                <?$pCnt++; if (($pInd == (count($arResult["ITEMS"]) - 1))) { ?>
                    <div class="clear"></div>
                </div>
            <? }
            ?>
        <? endforeach; ?>
        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
            <?= $arResult["NAV_STRING"] ?>
        <? endif; ?>
