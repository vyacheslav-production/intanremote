<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
?>

<? if (count($arResult["ERRORS"])): ?>
    <?= ShowError(implode("<br />", $arResult["ERRORS"])) ?>
<? endif ?>
<? if (strlen($arResult["MESSAGE"]) > 0): ?>
    <?//= ShowNote($arResult["MESSAGE"]) ?>
<span id="h_zapis" style="display:none;"></span>
<? endif ?>
<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" onsubmit="yaCounter9957124.reachGoal('zapis_na_priyom'); kCall2(); _gaq.push(['_trackEvent', 'form','form_priem']); return true;" enctype="multipart/form-data">

<?= bitrix_sessid_post() ?>

    <div class="f_zapisi_top">
        <p class="forma_zapisi_h">Оставьте ваши данные</p>
        <div class="left">
            <p>Имя:*</p><p>Телефон:*</p>
            <!--<p>email:</p>-->
        </div>
        <div class="fields">
            <p><input name="PROPERTY[NAME][0]" type="text" style="height:24px;width:285px;padding:0;" /></p>
            <p><input name="PROPERTY[57][0]" type="text" style="height:24px;width:285px;padding:0;" /></p>
           <!-- <p><input name="PROPERTY[59][0]" type="text" style="height:24px;width:285px;padding:0;" /></p>-->
        </div>
        <div class="clear"></div>
    </div>
    <div class="f_zapisi_center">
        <p class="forma_zapisi_h">Выберите ближайший центр</p>
        <div class="left"><p>Центр</p>

          <!--  <p>Услуга</p>-->

        </div>
        <div class="fields">
            <div class="select_wrap">
                <select name="PROPERTY[66]" id="forma_zapisi_adress" style="width:200px;height:22px;">
                    <option value="55">Стачек проспект</option>
                    <option value="49">Комендантском пр. 7, к.1</option>
                    <option value="90">Комендантский пр. 42, к. 1</option>
                    <option value="50">Лиговский проспект</option>
                    <option value="52">Просвещения проспект</option>
                    <option value="48">Будапештская улица</option>
                    <option value="53">Бухарестская улица</option>
                    <option value="54">Российский проспект</option>
                    <option value="56">Большая Пушкаркая ул</option>
                    <option value="87">Вознесенский пр</option>
                    <option value="88">Луначарского пр</option>
                    <option value="89">Богатырский проспект</option>
                    <option value="92">Ленинградская улица</option>
                </select>
            </div>
           
        <!--    <div class="select_wrap">
                <select name="PROPERTY[61]" id="forma_zapisi_servise" style="width:200px;height:22px;">
                    <option value="24">Только консультация</option>
                    <option value="25">Имплантация</option>
                    <option value="26">Протезирование</option>
                    <option value="27">Исправление прикуса</option>
                    <option value="28">Лечение</option>
                    <option value="29">Отбеливание</option>
                    <option value="30">Детская стоматология</option>
                    <option value="31">Пародонтология</option>
                    <option value="32">Эстетическая косметология</option>
                    <option value="33">Диагностика</option>
                    <option value="71">Хирургия</option>
                </select>
            </div>
        -->

        </div>
        <div class="clear"></div>
    </div>
    <div class="f_zapisi_bottom">
        <p class="forma_zapisi_h">Удобное время звонка</p>
        <div class="radio_wrap">
            <div class="radio_item"><label><input type="radio" name="PROPERTY[58]" value="20" />9&mdash;12</label></div>
            <div class="radio_item"><label><input type="radio" name="PROPERTY[58]" value="21" />12&mdash;15</label></div>
            <div class="radio_item"><label><input type="radio" name="PROPERTY[58]" value="22" />15&mdash;18</label></div>
            <div class="radio_item"><label><input type="radio" name="PROPERTY[58]" value="23" />18&mdash;21</label></div>
            <div class="clear"></div>
        </div>
    </div>
    <!-- <div class="f_zapisi_capcha">
        <div class="plz_capcha">Пожалуйста, введите код с картинки:</div>

        <div class="capcha_wrap">
            <script type="text/javascript">
				$(function(){					
					/*RELOAD CAPTCHA*/ 
					var symbols ="abcdefghjklmnopqrstuvwxyz0123456789" 
					var length = 32; 
					function generatePassword(symbols, length) { 
					var result = ""; 
					for (var i=0; i<length; i++) { 
					result += symbols.charAt(Math.floor(Math.random()*symbols.length)); 
					}; 
					return result; 
					} 

					$("#smenit").click(function(){ 
					var captcha_code = generatePassword(symbols, length); 
					var c = "/bitrix/tools/captcha.php?captcha_sid=" + captcha_code ; 
					$("#code").attr("src", c) 
					$("#sid").attr("value", captcha_code) 
					})					
				});
			</script>

			<input id="sid" type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
            <img id="code" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" alt="" />			
		</div>
-->

       <!-- <div class="capcha_input"><input type="text" name="captcha_word" style="width:60px;height:24px;padding:0;" /></div> 
        <div class="clear"></div>
		<div id="smenit" style="cursor:pointer; position:absolute; bottom:-20px; left:190px; font: normal 12px Arial, sans-serif; color:gray; border-bottom: 1px dashed;">сменить картинку</div>
    </div>-->
    <div class="f_zapisi_submit">
        <input type="submit" class="green_submit" name="iblock_submit" value="Отправить" />
    </div>
</form>
