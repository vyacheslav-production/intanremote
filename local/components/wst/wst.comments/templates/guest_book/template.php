<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
if(empty($arResult))
	return "";
?>
<?if ($arResult["ALLOW_COMMENT"]=="Y"):?>
<script type="text/javascript">
function do_submit(form) {
  if (form.wst_comment.value=="") {
    alert('<?=GetMessage("REPLY_EMPTY_COMMENT")?>');
		return;
  }
	form.action="";
	form.submit();
}
</script>
<noscript>
<?// C�������� ����� �������� ������ ��� ����������� �������� ?>
<span style="color:#FF0000;">
<?=GetMessage("NO_JS")?>
</span>
</noscript>

<div id="respond">
<?=($arResult["REPLY_NOTE"]) ? '<p>'.ShowError($arResult["REPLY_NOTE"]).'</p>' : '';?>
<?=($arResult["REPLY_ERROR"]) ? '<p>'.ShowError($arResult["REPLY_ERROR"]).'</p>' : '';?>
<h3><?=GetMessage("LEAVE_A_REPLY")?></h3>
<form name="replyform" action="about:blank" method="post" id="commentform" onsubmit="return false;">
<input type="hidden" name="wst_nospam" value="nospam" />
<?if ($USER->IsAuthorized()):?>
<p><strong><?=$arResult["USER"]["FULL_NAME"]?></strong> <small><a href="<?=$APPLICATION->GetCurPage()."?logout=yes"?>"><?=GetMessage("REPLY_LOGOUT")?></a></small></p>
<?else:?>
<p><input type="text" name="wst_author" id="author" value="" size="22" tabindex="1" />
<label for="author"><small><?=GetMessage("REPLY_NAME")?></small></label></p>
<p><input type="text" name="wst_email" id="email" value="" size="22" tabindex="2" />
<label for="email"><small><?=GetMessage("REPLY_EMAIL")?></small></label></p>
<p><input type="text" name="wst_url" id="url" value="" size="22" tabindex="3" />
<label for="url"><small><?=GetMessage("REPLY_WEB")?></small></label></p>
<?endif;//if (!$USER->IsAuthorized())?>
<p><textarea name="wst_comment" id="comment" cols="100%" rows="10" tabindex="4"></textarea></p>
<p><input type="button" value="<?=GetMessage("REPLY_SUBMIT")?>" onclick="do_submit(this.form);" tabindex="5" class="submit"/>
<?=bitrix_sessid_post();?>
</p>
</form>
<script type="text/javascript">
  <?// ��������� ���������� �������� ���������� ����?>
  document.replyform.wst_nospam.value="OK";
</script> 
</div>
<?else:?>
<p><?=GetMessage("REPLY_NOT_ALLOWED");?></p>
<?endif;//if ($arResult["ALLOW_COMMENT"]=="Y")?>
<?if (count($arResult["ITEMS"])>0):?>
	<h3 id="comments"><?=GetMessage("RESPONSES_TO")?><?=isset($arResult["ELEMENT"]["NAME"]) ? ' &#8220;'.$arResult["ELEMENT"]["NAME"].'&#8221;' : ''?>:</h3>
	<ol class="commentlist">
	<?$cc=0;foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem["COMMENT"]['ID'], $arItem["COMMENT"]['EDIT_LINK'], CIBlock::GetArrayByID($arItem["COMMENT"]["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem["COMMENT"]['ID'], $arItem["COMMENT"]['DELETE_LINK'], CIBlock::GetArrayByID($arItem["COMMENT"]["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('COMMENT_DELETE_CONFIRM')));
			?>
			<li class="comment<?=((($cc % 2) == 1) ? " odd" : " even" )?>" id="comment-<?=$arItem["COMMENT"]["ID"]?>">
			<div class="comment-body" id="<?=$this->GetEditAreaId($arItem["COMMENT"]['ID']);?>">
				<div class="comment-author vcard">
					<?if ($arItem["USER"]["AVATAR_URL"]):?>
					<img alt='<?=$arItem["USER"]["FULL_NAME"]?>' src='<?=$arItem["USER"]["AVATAR_URL"]?>' class='avatar photo' height='<?=$arParams["AVATARS_SIZE"]?>' width='<?=$arParams["AVATARS_SIZE"]?>' />
					<?endif;?>
					<?=($arItem["COMMENT"]["ACTIVE"]=="N") ? "<div>".GetMessage("NONE_ACTIVE")."</div>" : ""?>
					<cite class="fn"><?if ($arItem["USER"]["PERSONAL_WWW"]):?><a href="<?=$arItem["USER"]["PERSONAL_WWW"]?>" rel="external nofollow" class="url"><?endif;?><?=$arItem["USER"]["FULL_NAME"]?><?if ($arItem["USER"]["PERSONAL_WWW"]):?></a><?endif;?></cite> <span class="says"><?=GetMessage("SAYS")?>:</span>					
				</div>
				<div class="comment-meta commentmetadata">
					<a href="<?=$arResult["ELEMENT"]["DETAIL_PAGE_URL"]?>#comment-<?=$arItem["COMMENT"]["ID"]?>"><?=$arItem["COMMENT"]["DATE_CREATE"]?></a>
				</div>
				<div><?=$arItem["COMMENT"]["PREVIEW_TEXT"]?></div>
			</div>
		</li>
	<?$cc++;endforeach;?>
	</ol>
<?if((count($arResult["ITEMS"])>0)&&(trim($arResult["NAV_STRING"]))):?>
	<div class="navigation">
	<?=$arResult["NAV_STRING"]?>
	</div>
<?endif;?>
<?endif;//if (count($arResult["ITEMS"])>0)?>