<?
$MESS["RESPONSES_TO"] = 'Ваши отзывы';
$MESS["SAYS"] = 'ответил';
$MESS["LEAVE_A_REPLY"] = 'Оставить сообщение';
$MESS["REPLY_EMPTY_COMMENT"] = 'Текст сообщения не заполнен';
$MESS["REPLY_NAME"] = 'Представьтесь';
$MESS["REPLY_EMAIL"] = 'Е-мейл (не публикуется)';
$MESS["REPLY_WEB"] = 'Сайт (публикуется с параметром nofollow)';
$MESS["REPLY_SUBMIT"] = 'Отправить';
$MESS["REPLY_LOGOUT"] = 'Выйти';
$MESS["NO_JS"] = 'Ваш браузер не поддерживает JavaScript или в нем отключена поддержка скриптов. Вы не сможете оставить комментарий.';
$MESS["COMMENT_DELETE_CONFIRM"] = 'Удалить комментарий?';
$MESS["NONE_ACTIVE"] = 'Комментарий не активен';
$MESS["REPLY_NOT_ALLOWED"] = 'Вы не можете оставлять сообщения';
?>