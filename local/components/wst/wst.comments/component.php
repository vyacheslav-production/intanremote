<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!CModule::IncludeModule('wst.comments'))
	return;
/**
 * ��������� ���������:
 * ����� �������� ����� - ������������� ��� ��������������� �������� - �������
 * ������� ��������� ����� ������� - �������
 * ������ ������ �� ����� - �������� ����������� ������
 * ���������� ��� ���������� � ���������� ����������� - �������
 * �������� ���������� ��������, ���� ��� ������ ��� ������� �����
 * ����� ��� �������� - ����� �� �������� ������
 * ������� �������� ������ ��� ���������� ������
 * ������������ ��� ������ - ����� �������� ������� ������ ��������� �� ����� - �������
 * ���-�� ������� � ��������� � Facebook (?)
 * ����������� ����� Loginza
 */
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//echo "<pre>Template POST: "; print_r($_POST); echo "</pre>";
//echo "<pre>Template SESSION: "; print_r($_SESSION); echo "</pre>";
//exit();
CPageOption::SetOptionString("main", "nav_page_in_session", "N");
$arResult = array();
$arParamsToDelete = array(
	"login",
	"logout",
	"register",
	"forgot_password",
	"change_password",
	"confirm_registration",
	"confirm_code",
	"confirm_user_id",
	"clear_cache",
	"bitrix_include_areas"
);
/*************************************************************************
��������� � ������������� ������� ���������
*************************************************************************/
//������, �������� ����������� ������������
//$arParams["USER_GROUPS"])
//���������� ������������ �� ��������
$arParams["NUM"] = isset($arParams["NUM"]) ? intval($arParams["NUM"]) : 10;
//����� "�������� �����", ���� �������, �� ������������� � ��������������� ��������.
$arParams["GUESTBOOK_MODE"] = ($arParams["GUESTBOOK_MODE"]=="Y") ? $arParams["GUESTBOOK_MODE"] : $arParams["GUESTBOOK_MODE"]="N";
//����� ���������� Y - ����������� �����, ����� - ������������
$arParams["PUBLISH_MODE"] = ($arParams["PUBLISH_MODE"]=="Y") ? $arParams["PUBLISH_MODE"] : $arParams["PUBLISH_MODE"]="N";
//������ ��� ��������� �������
$arParams["MODER_MAIL_EVENT_TYPE"] = "WST_POST_COMMENTED";
//id ��������� ������������
$arParams["IBLOCK_ID"] = isset($arParams["IBLOCK_ID"]) ? intval($arParams["IBLOCK_ID"]) : 0;
//�������� �����������
$arParams["COMMENTED_ELEMENT_PROP"] = trim($arParams["COMMENTED_ELEMENT_PROP"]) ? trim($arParams["COMMENTED_ELEMENT_PROP"]) : "ARTICLE";
$arParams["GUEST_NAME_PROP"] = trim($arParams["GUEST_NAME_PROP"]) ? trim($arParams["GUEST_NAME_PROP"]) : "GUEST_NAME";
$arParams["GUEST_EMAIL_PROP"] = trim($arParams["GUEST_EMAIL_PROP"]) ? trim($arParams["GUEST_EMAIL_PROP"]) : "GUEST_EMAIL";
$arParams["GUEST_URL_PROP"] = trim($arParams["GUEST_URL_PROP"]) ? trim($arParams["GUEST_URL_PROP"]) : "GUEST_URL";
//��������
$arAvatarsTypes = array("GRAVATAR","USER_PHOTO","NONE");
if (!in_array($arParams["AVATARS_TYPE"],$arAvatarsTypes))
	$arParams["AVATARS_TYPE"] = "GRAVATAR";
$arParams["AVATARS_SIZE"] = (intval($arParams["AVATARS_SIZE"])>0) ? intval($arParams["AVATARS_SIZE"]) : 32;
$arParams["AVATARS_SIZE"] = (intval($arParams["AVATARS_SIZE"])>512) ? 512 : intval($arParams["AVATARS_SIZE"]);
//��������� ������������ ���������
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]) ? trim($arParams["PAGER_TITLE"]) : GetMessage("WST_PAGER_TITLE_DEF");
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]) ? trim($arParams["PAGER_TEMPLATE"]) : "";
$arParams["PAGER_SHOW_ALWAYS"] = false;
//����� ���� �� ����������
if (!in_array($arParams["CACHE_TYPE"],array("N","Y","A"))) $arParams["CACHE_TYPE"] = "A";
if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600000;
else $arParams["CACHE_TIME"] = intval($arParams["CACHE_TIME"]);//�� ������ ������
//�������� ����� ��� � �������� id �������� [� �������]
if ($arParams["GUESTBOOK_MODE"]!="Y"){
	if ($arParams["SEF_MODE"] == "Y"){
		$arDefaultUrlTemplates404 = array(
			"element" => "#SECTION_ID#/#ELEMENT_ID#/",
		);
		$arVariables = array();
		$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
		//$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);
		$componentPage = CComponentEngine::ParseComponentPath(
			$arParams["SEF_FOLDER"],
			$arUrlTemplates,
			$arVariables
		);
		$arResult["ELEMENT_ID"] = $arVariables["ELEMENT_ID"];
		$arResult["SECTION_ID"] = $arVariables["SECTION_ID"];
	}else{
		$arResult["ELEMENT_ID"] = intval($_REQUEST[$arParams["VARIABLE_ALIASES"]["ELEMENT_ID"]]);
		$arResult["SECTION_ID"] = intval($_REQUEST[$arParams["VARIABLE_ALIASES"]["SECTION_ID"]]);
	}//if($arParams["SEF_MODE"] == "Y")

	if (!$arResult["ELEMENT_ID"]){
		//�� ���������� �������������� �������
		//ShowError(GetMessage("IBLOCK_ELEMENT_ERROR"));
		return;
	}else{
		$element = GetIBlockElement($arResult["ELEMENT_ID"]);
		$arResult["ELEMENT"] = array_intersect_key(
			$element,
			array(
				"ID"=>false,
				"CODE"=>false,
				"NAME"=>false,
				"DETAIL_PAGE_URL"=>false
			)
		);
	}
}//if ($arParams["GUESTBOOK_MODE"]!="Y")

$arNavParams = array(
	"nPageSize" => $arParams["NUM"],
);
$arNavigation = CDBResult::GetNavParams($arNavParams);
/*** ������� ��������� ��������� **********************************/

/****************************************************************************
����������� �� ���� � �������, ������� ��������� �������������� � ���������� ����������
****************************************************************************/
$allow_show_list = true; //���������� ������ ������������, ���� ���, �� $arResult["ALLOW_COMMENT"] ���� =N
if(CModule::IncludeModule('iblock')) {
	$iblock_permission = CIBlock::GetPermission($arParams["IBLOCK_ID"]);
	if ($iblock_permission<"R")
		$allow_show_list = false;
	if ($arParams["GUESTBOOK_MODE"]!="Y") {
		$commented_iblock_permission = CIBlock::GetPermission($element["IBLOCK_ID"]);
		if ($commented_iblock_permission<"R")
			$allow_show_list = false;
	}
}
if ((((count(array_intersect($arParams["USER_GROUPS"],$USER->GetUserGroupArray()))>0)&&($iblock_permission>"D"))||($iblock_permission>="W")) && ($allow_show_list===true)){
	$arResult["ALLOW_COMMENT"] = "Y";
	if ($USER->IsAuthorized()) {
		$rsUser = CUser::GetByID($USER->GetID());
		$arResult["USER"] = array_intersect_key(
			$rsUser->Fetch(),
			array(
				"ID"=>false,
				"LOGIN"=>false,
				"NAME"=>false,
				"LAST_NAME"=>false,
				"EMAIL"=>false,
				"PERSONAL_WWW"=>false,
				"PERSONAL_PHOTO"=>false
			)
		);
		$arResult["USER"]["FULL_NAME"] = trim($USER->GetFullName())  ? trim($USER->GetFullName() ) : $USER->GetLogin();
		if ($arParams["AVATARS_TYPE"]=="NONE"){
			$arResult["USER"]["AVATAR_URL"]="";
		}elseif(($arParams["AVATARS_TYPE"]=="USER_PHOTO")&&($arResult["USER"]["PERSONAL_PHOTO"])){
			$arResult["USER"]["AVATAR_URL"] = CWSTComments::checkResizeAvatar($arResult["USER"]["PERSONAL_PHOTO"],$arParams["AVATARS_SIZE"]);
		}else{
			$arResult["USER"]["AVATAR_URL"] = CWSTComments::get_gravatar($arResult["USER"]["EMAIL"],$arParams["AVATARS_SIZE"]);
		}
	}else{
		//������ ��������� �������� �������
	}//if ($USER->IsAuthorized()) 
}else{
	$arResult["ALLOW_COMMENT"] = "N";
}//if (count(array_intersect( ...

/*** �������:
���������� ��� ��� ����������/�������������� ����������� - $this->ClearResultCache($cache_ar, false);
������������� ������� ���� �� ������ ��� ��������� - $this->AbortResultCache();
***/

//������� �����������
if ($arParams["GUESTBOOK_MODE"]!="Y"){
	$cache_ar = array($USER->GetGroups(),$arNavigation,bitrix_sessid(),$allow_show_list,$iblock_permission,$commented_iblock_permission,$arResult["SECTION_ID"],$arResult["ELEMENT_ID"],$arResult["ALLOW_COMMENT"]);
}else{
	$cache_ar = array($USER->GetGroups(),$arNavigation,bitrix_sessid(),$allow_show_list,$iblock_permission,$arResult["ALLOW_COMMENT"]);
}

/*************************************************************************
������������ ���������� �����������
*************************************************************************/
if (check_bitrix_sessid() && ($_POST['wst_nospam']=="OK") && ($arResult["ALLOW_COMMENT"]=="Y")){
	if(CModule::IncludeModule('iblock')){
		$el = new CIBlockElement;
		$prop = array();
		if ($arParams["GUESTBOOK_MODE"]!="Y")
			$prop[$arParams["COMMENTED_ELEMENT_PROP"]] = $arResult["ELEMENT_ID"];
		if (!$USER->IsAuthorized()) {
			$prop[$arParams["GUEST_NAME_PROP"]] = (trim($_POST["wst_author"]) ? trim($_POST["wst_author"]) : GetMessage("GUEST"));
			$arSend["AUTHOR_NAME"] = $prop[$arParams["GUEST_NAME_PROP"]];
			$prop[$arParams["GUEST_EMAIL_PROP"]] = trim($_POST["wst_email"]);
			$arSend["AUTHOR_EMAIL"] = $prop[$arParams["GUEST_EMAIL_PROP"]];
			$prop[$arParams["GUEST_URL_PROP"]] = trim($_POST["wst_url"]);
			$arSend["AUTHOR_URL"] = $prop[$arParams["GUEST_URL_PROP"]];
		}else{
			$arSend["AUTHOR_NAME"] = $arResult["USER"]["NAME"];
			$arSend["AUTHOR_EMAIL"] = $arResult["USER"]["EMAIL"];;
			$arSend["AUTHOR_URL"] = $arResult["USER"]["PERSONAL_WWW"];;
		}
		$arSend["COMMENT"] = trim($_POST["wst_comment"]);
		$arComment = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"NAME" => GetMessage("COMMENT_ELNAME"),
			"PREVIEW_TEXT" => $arSend["COMMENT"],
			"ACTIVE" => ($arParams["PUBLISH_MODE"]=="Y" ? "Y" : "N"),
			"PROPERTY_VALUES"=> $prop,
		);
		if($NEW_ID = $el->Add($arComment)){
			//���� � ��� ������������� ������� ��� ��������� ������� � �������� ������.
			if (check_email($arParams["MODER_MAIL"]))
				$readyForMail = CWSTComments::wstContentPostCommentsCheckAddMailEvent($arParams["MODER_MAIL_EVENT_TYPE"],LANGUAGE_ID,SITE_ID);
			else
				$readyForMail = false;
			if ($readyForMail){
				$arSend["POST_LINK"] = $APPLICATION->GetCurPageParam("",$arParamsToDelete);
				if (CEvent::Send($arParams["MODER_MAIL_EVENT_TYPE"], SITE_ID, array(
					"MODER_MAIL" => $arParams["MODER_MAIL"],
					"COMMENT" => trim($_POST["wst_comment"]),
					"AUTHOR_NAME" => $arSend["AUTHOR_NAME"],
					"AUTHOR_EMAIL" => $arSend["AUTHOR_EMAIL"] ? $arSend["AUTHOR_EMAIL"] : "-",
					"AUTHOR_URL" => $arSend["AUTHOR_URL"] ? $arSend["AUTHOR_URL"] : "-",
					"COMMENT" => $arSend["COMMENT"],
					"POST_LINK" => $arSend["POST_LINK"],
					)
				)) $eventSendError="success"; else $eventSendError="error";
			}//readyForMail
			$this->ClearResultCache($cache_ar, false);
			$_POST=array();
			if ($arParams["PUBLISH_MODE"]!="Y")
				$arResult["REPLY_NOTE"] = GetMessage("AFTER_MODERATION");
			LocalRedirect($APPLICATION->GetCurPageParam("",$arParamsToDelete)."#comment-".$NEW_ID);
		}else{
			$arResult["REPLY_ERROR"] = $el->LAST_ERROR;
		}
	}//CModule
}elseif ((!check_bitrix_sessid()) && ($_POST['wst_nospam']=="OK")){
	$arResult["REPLY_ERROR"] = GetMessage("OLD_SESSION");
}//if (check_bitrix_sessid() && ($_POST['wst_nospam']=="OK"))

/*************************************************************************
������ � �����
*************************************************************************/
if ($this->StartResultCache(false, $cache_ar)){
	if (($arParams["IBLOCK_ID"]>0) && $allow_show_list){
		if(CModule::IncludeModule('iblock')){
			if ($iblock_permission>="W") $activeOnly=""; else $activeOnly="Y";
			if ($arParams["GUESTBOOK_MODE"]!="Y"){
				$arFilter = array("ACTIVE"=>$activeOnly,"IBLOCK_TYPE"=>$arParams["IBLOCK_TYPE"],"IBLOCK_ID"=>$arParams["IBLOCK_ID"],"PROPERTY_".$arParams["COMMENTED_ELEMENT_PROP"]=>$arResult["ELEMENT_ID"]);
			}else{
				$arFilter = array("ACTIVE"=>$activeOnly,"IBLOCK_TYPE"=>$arParams["IBLOCK_TYPE"],"IBLOCK_ID"=>$arParams["IBLOCK_ID"]);
			}
			$arElements = CIBlockElement::GetList(
				array("sort"=>"asc","id"=>"desc"),
				$arFilter,
				false,
				$arNavParams,
				array(
					"ACTIVE","ID","IBLOCK_ID","CREATED_BY","DATE_CREATE","NAME","PREVIEW_TEXT",
					"PROPERTY_".$arParams["GUEST_NAME_PROP"],"PROPERTY_".$arParams["GUEST_EMAIL_PROP"],"PROPERTY_".$arParams["GUEST_URL_PROP"]
				)
			);
			if ($arElements->SelectedRowsCount()>0){
				$arResult["NAV_STRING"] = $arElements->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
				$arResult["ELEMENT"]["DETAIL_PAGE_URL"] = $APPLICATION->GetCurPageParam("",$arParamsToDelete);
				while ($item = $arElements->GetNextElement()){
					$arItem = array();
					$fields = $item->GetFields();
					//��������� ��� ������ �������������� ��������� � ��������
					$arButtons = CIBlock::GetPanelButtons(
						$fields["IBLOCK_ID"],
						$fields["ID"],
						0,
						array("SECTION_BUTTONS"=>false, "SESSID"=>false)
					);
					$fields["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
					$fields["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
					if ($fields["CREATED_BY"]){
						$rsUser = CUser::GetByID($fields["CREATED_BY"]);
						//�������� ������ �������� ����
						$arItem["USER"] = array_intersect_key(
							$rsUser->Fetch(),
							array(
								"ID"=>false,
								"LOGIN"=>false,
								"NAME"=>false,
								"LAST_NAME"=>false,
								"EMAIL"=>false,
								"PERSONAL_WWW"=>false,
								"PERSONAL_PHOTO"=>false
							)
						);
						$fullname=trim(implode(' ',array($arItem["USER"]["NAME"],$arItem["USER"]["LAST_NAME"])));
						$arItem["USER"]["FULL_NAME"] = $fullname ? $fullname : $arItem["USER"]["LOGIN"];
						if ($arParams["AVATARS_TYPE"]=="NONE"){
							$arItem["USER"]["AVATAR_URL"]="";
						}elseif(($arParams["AVATARS_TYPE"]=="USER_PHOTO")&&($arItem["USER"]["PERSONAL_PHOTO"])){
							$arItem["USER"]["AVATAR_URL"] = CWSTComments::checkResizeAvatar($arItem["USER"]["PERSONAL_PHOTO"],$arParams["AVATARS_SIZE"]);
						}else{
							$arItem["USER"]["AVATAR_URL"] = CWSTComments::get_gravatar($arItem["USER"]["EMAIL"],$arParams["AVATARS_SIZE"]);
						}
					}else{
						//������� ������
						$arItem["USER"]["FULL_NAME"] = (trim($fields["PROPERTY_".$arParams["GUEST_NAME_PROP"]."_VALUE"]) ? trim($fields["PROPERTY_".$arParams["GUEST_NAME_PROP"]."_VALUE"]) : GetMessage("GUEST"));
						$arItem["USER"]["PERSONAL_WWW"] = trim($fields["PROPERTY_".$arParams["GUEST_URL_PROP"]."_VALUE"]);
						$arItem["USER"]["AVATAR_URL"] = CWSTComments::get_gravatar($fields["PROPERTY_".$arParams["GUEST_EMAIL_PROP"]."_VALUE"],$arParams["AVATARS_SIZE"]);
					}//if ($fields["CREATED_BY"])
					$arItem["COMMENT"] = array_intersect_key(
						$fields,
						array(
							"ACTIVE"=>false,
							"ID"=>false,
							"DATE_CREATE"=>false,
							"PREVIEW_TEXT"=>false,
							"IBLOCK_ID"=>false,
							"EDIT_LINK"=>false,
							"DELETE_LINK"=>false,
						)
					);
					$arResult["ITEMS"][] = $arItem;
				}//while
			}else{
				//������������ ���
				$this->AbortResultCache();
			}//if ($arElements->SelectedRowsCount()>0)
		}else{
			//�� ���������� ������ "���������"
			$this->AbortResultCache();
		}//if(CModule::IncludeModule('iblock'))
	}else{
		//�� ����� ID ���������, ���� �� ������ ���������� ������������ �� ��������
		$this->AbortResultCache();
	}//if ($arParams["IBLOCK_ID"])>0)
	$this->IncludeComponentTemplate();
}//if ($this->StartResultCache(false, $cache_ar))
//echo "readyForMail=".$readyForMail;
//echo '***cib='.$commented_iblock_permission.'***ib='.$iblock_permission.'***allow='.$allow_show_list.'###';
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($element); echo "</pre>";
?>