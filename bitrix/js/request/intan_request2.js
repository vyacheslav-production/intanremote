function credit(){

  var err = document.getElementById('err');

    function getChar(event) {
      if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode) // IE
      }

      if (event.which!=0 && event.charCode!=0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which)   // остальные
      }

      return null; // специальная клавиша
    }

    var form = document.forms.intan_request;
    var creditsum = form.elements.credit_sum;

    creditsum.onkeypress = function(e) {
      e = e || event;
      var chr  = getChar(e);

      if (e.ctrlKey || e.altKey || chr == null) return; 
      if (chr < '0' || chr > '9') return false;


    }


    creditsum.oncut = maxFunc;
    creditsum.onkeyup = maxFunc;
    creditsum.oninput = maxFunc;
    creditsum.onpropertychange = function() { 
      event.propertyName == "value" && maxFunc();
    }


    function maxFunc() {
      var sum = +creditsum.value;
      if(sum >= 20001) {
        creditsum.style.color = "#bf3333";
        err.style.display = "block";
      } else {
        creditsum.style.color = "#000";
        err.style.display = "none";
      }
   
       
    }



}
