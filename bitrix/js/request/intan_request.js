
  (function($){
      $(document).ready(
              function(){
                  $('.select').click(function(){
                      $(this).parent().find('.options').css('display','block');
                  });

                  $('#sel_act').click(function(){
                      $(this).parent().find('.options').css('display','block');
                  });

                  $('.options').mouseleave(function(){
                      $(this).fadeOut(0);
                  });
                  $('.options > div').click(function(){
                      $(this).closest('.pseudo-select').find('.select').html($(this).html());
                      $(this).closest('.pseudo-select').find('input').attr('value', $(this).attr('value'));
                      $.each($(this).parent().children('div.check'), function(){
                          $(this).removeClass('check');
                      });
                      $(this).addClass('check');
                      $(this).parent().fadeOut(0);
                  });
              }
      );
  })(jQuery);