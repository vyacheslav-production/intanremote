<?

define("STOP_STATISTICS", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!empty($_FILES) && !empty($_REQUEST['el_id']) && !empty($_REQUEST['iblock_id']) && !empty($_REQUEST['prop_id'])  && !empty($_REQUEST['folder']) ): 

	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
	$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];
		
	CheckDirPath($targetPath);
		
	move_uploaded_file($tempFile,$targetFile);
	
	CModule::IncludeModule('iblock');

	$arFile = CFile::MakeFileArray($targetFile);
	$arFile["MODULE_ID"] = "iblock";
	CIBlockElement::SetPropertyValues($_REQUEST['el_id'], $_REQUEST['iblock_id'], $arFile, $_REQUEST['prop_id']);
	unlink($targetFile);

	echo 'ok' ;
else:
	
	if(defined("BX_UTF"))
		echo " ���� �� ��������. ����������� ����������� ������. " ;
	else
		echo iconv('cp1251', 'utf8' , " ���� �� ��������. ����������� ����������� ������. ") ;

endif;
?>