
arButtons['typograf'] = ['BXButton',
	{
		id : 'typograf',
		iconkit : 'typograf.gif',
		name : 'typograf',
		title : 'Типографировать',
		handler : function ()
		{
		
			this.pMainObj.pMainObj.executeCommand('SelectAll');
			var sel  = this.pMainObj;
			var sel_tags = sel['sLastContent'].trim();
			var sel_text =  sel_tags.replace(/&/g, '#a#');
			
			if( sel_tags.length > 0 && sel_tags.indexOf('__bxtagname="php"') == -1 && sel_tags.indexOf('__bxtagname="component2"') == -1 )	
			{		
			
				tid = jsAjax.InitThread(); 
				jsAjax.AddAction(tid, 
				function(str){ 
					sel.insertHTML(str);
				}); 
				jsAjax.Post(tid, '/bitrix/tools/defatools/typograf/typograf.php', {'text': sel_text })
		
			}	
			else
			{
				alert(" Невозможно типографировать. В тексте присутствует код или ничего не введено." );
			}
		
		}//function
	}
];

	if(arGlobalToolbar == undefined)
	{
		var last = arToolbars['standart'][1].length; 
		arToolbars['standart'][1][last] = arToolbars['standart'][1][last-1];
		arToolbars['standart'][1][last-1] =  arButtons['typograf'];
		
	}
	else
	{
		var last = arGlobalToolbar.length;
		arGlobalToolbar[last] = arGlobalToolbar[last-1];
		arGlobalToolbar[last-1] =  arButtons['typograf'];
	}
		
