<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;
	
$arGroups=Array();
$db_groups = CGroup::GetList(($by="c_sort"), ($order="asc"), Array("ACTIVE"=>"Y"));
while($arRes=$db_groups->Fetch()) 
	$arGroups[$arRes["ID"]] = $arRes["NAME"];

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"),Array("SITE_ID"=>$_REQUEST["site"]));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"AJAX_MODE" => array(),
		"USER_GROUPS" => Array(
			"NAME"=>GetMessage("WST_USER_GROUPS"),
			"PARENT" => "BASE",
			"TYPE" => "LIST",
			"MULTIPLE"=>"Y",
			"VALUES"=>$arGroups,
			"DEFAULT"=>array(),
		),
		"GUESTBOOK_MODE" => Array(
			"NAME"=>GetMessage("WST_GUESTBOOK_MODE"),
			"PARENT" => "BASE",
			"TYPE" => "CHECKBOX",
			"DEFAULT"=>"N",
		),
		"NUM" => Array(
			"NAME"=>GetMessage("WST_NUM"),
			"PARENT" => "BASE",
			"TYPE" => "STRING",
			"DEFAULT"=>"10",
		),
		"PUBLISH_MODE" => Array(
			"NAME"=>GetMessage("WST_PUBLISH_MODE"),
			"PARENT" => "BASE",
			"TYPE" => "CHECKBOX",
			"DEFAULT"=>"Y",
		),
		"MODER_MAIL" => Array(
			"NAME"=>GetMessage("WST_MODER_MAIL"),
			"PARENT" => "BASE",
			"TYPE" => "STRING",
			"DEFAULT"=>"",
		),
		"IBLOCK_ID" => Array(
			"NAME"=>GetMessage("WST_IBLOCK_ID"),
			"PARENT" => "DATA_SOURCE",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => "",
		),
		"COMMENTED_ELEMENT_PROP" => Array(
			"NAME"=>GetMessage("WST_COMMENTED_ELEMENT"),
			"PARENT" => "DATA_SOURCE",
			"TYPE" => "STRING",
			"DEFAULT"=>"ARTICLE",
		),
		"GUEST_NAME_PROP" => Array(
			"NAME"=>GetMessage("WST_GUEST_NAME"),
			"PARENT" => "DATA_SOURCE",
			"TYPE" => "STRING",
			"DEFAULT"=>"GUESTNAME",
		),
		"GUEST_EMAIL_PROP" => Array(
			"NAME"=>GetMessage("WST_GUEST_EMAIL"),
			"PARENT" => "DATA_SOURCE",
			"TYPE" => "STRING",
			"DEFAULT"=>"GUESTEMAIL",
		),
		"GUEST_URL_PROP" => Array(
			"NAME"=>GetMessage("WST_GUEST_URL"),
			"PARENT" => "DATA_SOURCE",
			"TYPE" => "STRING",
			"DEFAULT"=>"GUESTURL",
		),
		"AVATARS_TYPE" => array(
			"NAME" => GetMessage("WST_AVATARS_TYPE"),
			"PARENT" => "VISUAL",
			"TYPE" => "LIST",
			"VALUES" => array(
				"GRAVATAR" => GetMessage("WST_GRAVATAR"), 
				"USER_PHOTO" => GetMessage("WST_USER_PHOTO_FIRST"),
				"NONE" => GetMessage("WST_NO_AVATARS"),
				//"FORUM_AVATAR" => GetMessage("ASD_CMP_PARAM_PIC_FROM_DET"))
			),
			"DEFAULT"=>"GRAVATAR",
		),
		"AVATARS_SIZE" => array(
			"NAME" => GetMessage("WST_AVATARS_SIZE"),
			"PARENT" => "VISUAL",
			"TYPE" => "STRING",
			"DEFAULT" => "32"
		),
		"PAGER_TITLE" => Array(
			"NAME"=>GetMessage("WST_PAGER_TITLE"),
			"PARENT" => "ADDITIONAL_SETTINGS",
			"TYPE" => "STRING",
			"DEFAULT"=>GetMessage("WST_PAGER_TITLE_DEF"),
		),
		"PAGER_TEMPLATE" => Array(
			"NAME"=>GetMessage("WST_PAGER_TEMPLATE"),
			"PARENT" => "ADDITIONAL_SETTINGS",
			"TYPE" => "STRING",
			"DEFAULT"=>"",
		),
		"CACHE_TYPE" => Array(
			"NAME"=>GetMessage("WST_CACHE_TYPE"),
			"PARENT" => "CACHE_SETTINGS",
			"TYPE" => "STRING",
			"DEFAULT"=>"A",
		),
		"CACHE_TIME" => Array(
			"NAME"=>GetMessage("WST_CACHE_TIME"),
			"PARENT" => "CACHE_SETTINGS",
			"TYPE" => "STRING",
			"DEFAULT"=>"3600000",
		),
		"VARIABLE_ALIASES" => Array(
			"ELEMENT_ID" => Array("NAME" => GetMessage("WST_ELEMENT_ID_DESC")),
			"SECTION_ID" => Array("NAME" => GetMessage("WST_SECTION_ID_DESC")),
		),
		"SEF_MODE" => Array(
			"section" => array(
				"NAME" => GetMessage("WST_SEF_SECTION_PAGE"),
				"DEFAULT" => "#SECTION_ID#/",
			),
			"element" => array(
				"NAME" => GetMessage("WST_SEF_DETAIL_PAGE"),
				"DEFAULT" => "#SECTION_ID#/#ELEMENT_ID#/",
			)
		),
	)
);
?>
