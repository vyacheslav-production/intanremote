<?
$MESS["MODER_MAIL_EVENT_TYPE_NAME"] = "уведомление о новом комментарии";
$MESS["MODER_MAIL_EVENT_TYPE_DESC_MODER_MAIL"] = "емейл модератора";
$MESS["MODER_MAIL_EVENT_TYPE_DESC_AUTHOR_NAME"] = "автор комментария";
$MESS["MODER_MAIL_EVENT_TYPE_DESC_AUTHOR_EMAIL"] = "емейл автора";
$MESS["MODER_MAIL_EVENT_TYPE_DESC_AUTHOR_URL"] = "сайт автора";
$MESS["MODER_MAIL_EVENT_TYPE_DESC_COMMENT"] = "текст комментария";
$MESS["MODER_MAIL_EVENT_TYPE_DESC_POST_LINK"] = "ссылка на публикацию";
$MESS["MODER_MAIL_EVENT_TEMPLATE_TOPIC"] = "добавлен комментарий";
$MESS["MODER_MAIL_EVENT_TEMPLATE_MSG1"] = "Информационное сообщение сайта";
$MESS["MODER_MAIL_EVENT_TEMPLATE_MSG2"] = "На сайте добавлен комментарий от";
$MESS["MODER_MAIL_EVENT_TEMPLATE_MSG3"] = "Текст сообщения";
$MESS["MODER_MAIL_EVENT_TEMPLATE_MSG4"] = "Адрес публикации";
$MESS["MODER_MAIL_EVENT_TEMPLATE_MSG5"] = "Сообщение сгенерировано автоматически";
?>