<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

//echo "<pre>"; print_r($arResult);echo "</pre>";

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

?>


<?if($arResult["bDescPageNumbering"] === true):?>

	<div class="clients_nav_string">

	<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?if($arResult["bSavePage"]):?>
			
			<span class="nav_srt_elem"><a class="a_img" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><img src="/images/nav_prev_active.png" class="nav_arr" alt="&lt;" /></a></span>
			|
		<?else:?>
			
			<?if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
				<span class="nav_srt_elem"><a class="a_img" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><img src="/images/nav_prev_active.png" class="nav_arr"  alt="&lt;" /></a></span>
			<?else:?>
				<span class="nav_srt_elem"><a class="a_img" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><img src="/images/nav_prev_active.png" class="nav_arr"  alt="&lt;" /></a></span>
			<?endif?>
		<?endif?>
	<?else:?>
		<span class="nav_srt_elem"><img src="/images/nav_prev_dis.png" class="nav_arr"  alt="&lt;" /></span>
	<?endif?>

	<?while($arResult["nStartPage"] >= $arResult["nEndPage"]):?>
		<?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<span class="nav_srt_elem"><?=$NavRecordGroupPrint?></span>
		<?elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
			<span class="nav_srt_elem"><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$NavRecordGroupPrint?></a></span>
		<?else:?>
			<span class="nav_srt_elem"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$NavRecordGroupPrint?></a></span>
		<?endif?>

		<?$arResult["nStartPage"]--?>
	<?endwhile?>

	|

	<?if ($arResult["NavPageNomer"] > 1):?>
		<span class="nav_srt_elem"><a class="a_img" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><img src="/images/nav_next_active.png" class="nav_arr"  alt="&gt;" /></a></span>
		
	<?else:?>
		
	<?endif?>

<?else:?>

	<div class="clients_nav_string">

	<?if ($arResult["NavPageNomer"] > 1):?>

		<?if($arResult["bSavePage"]):?>
			
			<span class="nav_srt_elem"><a class="a_img" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><img src="/images/nav_prev_active.png" class="nav_arr"  alt="&lt;" /></a></span>
			|
		<?else:?>
			
			<?if ($arResult["NavPageNomer"] > 2):?>
				<span class="nav_srt_elem"><a class="a_img" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><img src="/images/nav_prev_dis.png"  alt="&lt;" class="nav_arr" /></a></span>
			<?else:?>
				<span class="nav_srt_elem"><a class="a_img" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><img src="/images/nav_prev_active.png" class="nav_arr"  alt="&lt;" /></a></span>
			<?endif?>
		<?endif?>

	<?else:?>
		<span class="nav_srt_elem"><img src="/images/nav_prev_dis.png" class="nav_arr"  alt="&lt;" /></span>
	<?endif?>

	<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<span class="nav_srt_elem"><?=$arResult["nStartPage"]?></span>
		<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
			<span class="nav_srt_elem"><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a></span>
		<?else:?>
			<span class="nav_srt_elem"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a></span>
		<?endif?>
		<?$arResult["nStartPage"]++?>
	<?endwhile?>

	<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<a class="a_img" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><img src="/images/nav_next_active.png" class="nav_arr"  alt="&gt;" /></a>
	<?else:?>
		<span class="nav_srt_elem"><img src="/images/nav_next_dis.png" class="nav_arr"  alt="&gt;" /></span>
	<?endif?>

<?endif?>

        </div>

