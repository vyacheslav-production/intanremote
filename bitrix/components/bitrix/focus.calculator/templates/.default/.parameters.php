<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "SET_JQUERY" => Array(
        "NAME" => GetMessage("SET_JQUERY"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N",
    ),
);

?>
