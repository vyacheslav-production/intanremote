<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
	"NAME" => GetMessage("WST_COMMENTS_NAME"),
	"DESCRIPTION" => GetMessage("WST_COMMENTS_DESC"),
	"PATH" => array (
		"ID" => "content",
		"CHILD" => array(
			"ID" => "wst_content",
			"NAME" => GetMessage("WST_FOLDER_NAME"),
		),
	),
	"ICON" => "/images/component.gif",
	"COMPLEX" => "N",
);
?>