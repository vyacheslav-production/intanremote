<?
$MESS ['WST_USER_GROUPS'] = "Разрешено комментировать группам";
$MESS ['WST_NUM'] = "Количество комментариев на странице";
$MESS ['WST_MODER_MAIL'] = "Отправлять уведомления на емейл";
$MESS ['WST_PUBLISH_MODE'] = "Публиковать сразу";
$MESS ['WST_GUESTBOOK_MODE'] = "Режим \"гостевая книга\"";
$MESS ['WST_IBLOCK_ID'] = "ID инфоблока комментариев";
$MESS ['WST_COMMENTED_ELEMENT'] = "Свойство 'Комментируемый элемент'";
$MESS ['WST_GUEST_NAME'] = "Свойство 'Имя гостя'";
$MESS ['WST_GUEST_EMAIL'] = "Свойство 'Е-мейл гостя'";
$MESS ['WST_GUEST_URL'] = "Свойство 'Сайт гостя'";
$MESS ['WST_AVATARS_TYPE'] = "Тип аватара";
$MESS ['WST_AVATARS_SIZE'] = "Размер аватара, пкс";
$MESS ['WST_GRAVATAR'] = "Только GRAVATAR";
$MESS ['WST_USER_PHOTO_FIRST'] = "Фото из профиля, если нет, то GRAVATAR";
$MESS ['WST_NO_AVATARS'] = "Не использовать аватары";
$MESS ['WST_PAGER_TITLE'] = "Заголовок постраничной навигации";
$MESS ['WST_PAGER_TITLE_DEF'] = "Страница";
$MESS ['WST_PAGER_TEMPLATE'] = "Шаблон постраничной навигации";
$MESS ['WST_CACHE_TYPE'] = "Тип кэширования";
$MESS ['WST_CACHE_TIME'] = "Время кэширования";
$MESS ['WST_SEF_DETAIL_PAGE'] = "Шаблон детальной страницы комментируемого элемента";
$MESS ['WST_SEF_SECTION_PAGE'] = "Шаблон страницы раздела комментируемого элемента";
$MESS ['WST_ELEMENT_ID_DESC'] = "Идентификатор комментируемого элемента";
$MESS ['WST_SECTION_ID_DESC'] = "Идентификатор раздела комментируемого элемента";
?>