<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); 
//print_r($arResult);
?>

    <div class="mdoc_filter">
        <div class="sp">
            <p class="mdoc_fil_lab">Искать по центру:</p>
            <select id="filter_addr" class="st_sel">
				<option value="all">Все центры</option>
                <?  
                foreach ($arResult["CLINIC"] as $arItem) { 
					if ($arItem['ID'] == $_GET['arrFilter_pf']['bloger_clinic']) {
						echo ' <option value="'.$arItem['ID'].'" selected>'.$arItem['NAME'].'</option>';
					} else { ?>
						<option value="<?= $arItem['ID']; ?>"><?= $arItem['NAME'] ?></option>
                <? 	}
                } ?>
            </select>
        </div>
        <div>
            <p class="mdoc_fil_lab">Искать по специализации:</p>
            <select id="filter_spec" class="st_sel">
<?
				$arrSpec = array('Любая','Ортопед','Терапевт','Хирург','Имплантолог','Пародонтолог','Ортодонт',);
				foreach ($arrSpec as $value) {
					if(!empty($_GET['arrFilter_pf']['bloger_position']) and $_GET['arrFilter_pf']['bloger_position'] == $value) {
						echo '<option value="'.$value.'" selected>'.$value.'</option>';
					} else {
						echo '<option value="'.$value.'">'.$value.'</option>';
					}
				}
?>
            </select>
        </div>
    </div>
<div class="doc_header">Врачи центров стоматологии «Интан»</div>

<div class="doc_list">
    <?
    $g = 0;
    foreach($arResult['ITEMS'] as $Item) {
		$class = '';
        if ($g == 0) {
            $class.=' first';
        } elseif ($g == 2) {
            $class.=' last';
        };
        ?>
        <div class="doc_item<? echo($class); ?>">
            <img src="<?= $Item['PREVIEW_PICTURE']['SRC'] ?>" alt="<?=$Item['NAME']?>" class="doc_img" />
            <div class="doc_name"><?=$Item['NAME']?></div>
            <div class="doc_post"><?=$Item['PROPERTIES']['bloger_position']['VALUE']?></div>
        </div>
        <?
        $g++;
        if ($g == 3) {
            $g = 0;
        }
    }
    ?>
    <div class="clear"></div>
</div>

