<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $APPLICATION->SetTitle($arResult["NAME"]);
$APPLICATION->AddChainItem($APPLICATION->GetTitle()); ?>
<div class="sp_detail">
    <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>" class="sp_img" />
    <div class="spt">
        <div class="sptr">
            <div class="spc1"><div class="sp_tooth"></div></div>
            <div class="spc2"><?=$arResult["NAME"]?></div>
        </div>
    </div>
    <div class="sp_det_text">
        <p><?=$arResult["DETAIL_TEXT"];?></p>
    </div>
</div>
<a href="/mobile/zapisatsya_na_priyom/" class="cont_zapis">Записаться на прием</a>
<script type="text/javascript">
    function hfix() {
        var h = ($(window).height())-($('.mph').height())-60;
        $('.sp_detail').css({'min-height':h});
    }
    $(document).ready(function(){
        hfix();
    });
    $(window).load(function(){
        hfix();
    });
</script>
