<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?//print_r($arResult);?>
<div class="spec_wrap cf">
    <?
    //выводим 6 штук на страницу
    foreach($arResult["ITEMS"] as $arItem) {

        //это не сильно то надо, но на всякий случай, что бы пустить элементы по 2
        $class = '';
        if (($i + 1) % 2 == 0) {
            $class.=' even';
        } else {
            $class.=' odd';
        };
        ?>
        <div class="spec_item<?=$class;?>">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" class="sp_img" /></a>
            <div class="spt">
                <div class="sptr">
                    <div class="spc1"><div class="sp_tooth"></div></div>
                    <div class="spc2"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                </div>
            </div>
        </div>

    <? }; ?>
</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<nav> 
		<?=$arResult["NAV_STRING"]?>
	</nav>
<?endif;?>
