<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?//print_r($arResult);?>
<?

// Удаляем «Интан» на пр. Тореза 112 - он не активный!
unset($arResult["PROPERTY_LIST_FULL"]["66"]["ENUM"]['51']);

$chose = 'none';
$dform = 'block';
$cid = '';
if (isset($arResult["ELEMENT_PROPERTIES"]["66"]["0"]["VALUE"] )) {
	$cid =  $arResult["ELEMENT_PROPERTIES"]["66"]["0"]["VALUE"];
}elseif (!empty($_GET['centerId'])) {
    if ($_GET['centerId'] == 'buhar') {
        $cid = '53';
    } elseif ($_GET['centerId'] == 'komend') {
        $cid = '49';
    } elseif ($_GET['centerId'] == 'ligov') {
        $cid = '50';
    } elseif ($_GET['centerId'] == 'ross') {
        $cid = '54';
    } elseif ($_GET['centerId'] == 'buda') {
        $cid = '48';
    } elseif ($_GET['centerId'] == 'biggun') {
        $cid = '56';
    } elseif ($_GET['centerId'] == 'voznes') {
        $cid = '87';
    } elseif ($_GET['centerId'] == 'prosv') {
        $cid = '52';
    } elseif ($_GET['centerId'] == 'stach') {
        $cid = '55';
    }
} else {
    $chose = 'block';
    $dform = 'none';
}
?>
<script type="text/javascript">
    var cid = '<?= $cid; ?>';
    var conttitle = '';
    var contaddr = '';
    function cltx() {
        $('.fademask, .z_tx').remove();
    }

    function sel() {
        if (cid == '53') {
            conttitle = 'Стоматология ИНТАН во Фрунзенском районе';
            contaddr = 'Бухарестская ул., дом 96';
        } else if (cid == '49') {
            conttitle = 'Стоматология ИНТАН в Приморском районе (м. Комендантский проспект)';
            contaddr = 'Комендантский пр., дом 7, к.1';
        } else if (cid == '50') {
            conttitle = 'Стоматология ИНТАН в Центральном районе (Лиговский проспект)';
            contaddr = 'Лиговский пр., дом 125';
        } else if (cid == '54') {
            conttitle = 'Стоматология ИНТАН в Невском районе (м. Проспект Большевиков)';
            contaddr = 'Российский пр., дом 8';
        } else if (cid == '48') {
            conttitle = 'Стоматология Интан во Фрунзенском районе (м. Купчино)';
            contaddr = 'ул. Будапештская, дом 97, к.2';
        } else if (cid == '56') {
            conttitle = 'Стоматология на Большой Пушкарской улице';
            contaddr = 'ул. Большая Пушкарская, дом 41';
        } else if (cid == '87') {
            conttitle = 'Стоматология в Адмиралтейском районе на Вознесенском пр.';
            contaddr = 'Вознесенский пр., дом 21';
        } else if (cid == '52') {
            conttitle = 'Стоматология ИНТАН в Калининском районе (м. Гражданский проспект)';
            contaddr = 'пр. Просвещения, дом 87, к.2';
        } else if (cid == '55') {
            conttitle = 'Стоматология в Кировском районе на проспекте Стачек';
            contaddr = 'пр. Стачек, дом 69 «А»';
        }
        $('.conttitle').text(conttitle);
        $('.contaddr').text(contaddr);

        var item = $('#none option');
        for (var i = 0; i < (item.length); i++) {
            var it = $(item[i]);
            if (it.attr('data-id') == cid) {
				it.prop("selected", true);
			} else {
				it.prop("selected", false);
			}
            /*
            if (it.attr('data-id') == cid) {
                it.hide();
            }*/
        }
    }
    function reselHide() {
        var item = $('.zapis_resel');
        for (var i = 0; i < (item.length); i++) {
            var it = $(item[i]);
            if (it.attr('data-id') == cid) {
                it.hide();
            }
        }
    }

    $(document).ready(function() {
        sel();
        reselHide();
        $('.zap_chsel').click(function() {
            var newCid = $(this).attr('data-id');
            cid = newCid;
            sel();
            $('.zapis_prim_chose').hide();
            $('.zapis_f').show();
        });

        $('.cont_sel_active').click(function() {
            if ($('.cont_sel_drop').is(':visible')) {
                $('.cont_sel_drop').slideUp('100');
                $('.white_ar').removeClass('up');
            } else {
                $('.cont_sel_drop').slideDown('200');
                $('.white_ar').addClass('up');
            }
        });
        $('.cont_main, .cont_header, .mph').click(function() {
            if ($('.cont_sel_drop').is(':visible')) {
                $('.cont_sel_drop').slideUp('100');
                $('.white_ar').removeClass('up');
            }
        });
        $('.zapis_resel').click(function() {
            var newCid = $(this).attr('data-id');
            cid = newCid;
            $('.cont_sel_drop').slideUp('100');
            $('.white_ar').removeClass('up');
            sel();
            $('.zapis_resel').show();
            reselHide();
        });
<?
        if (!empty($arResult["MESSAGE"])) {?>
                var tx = '<div class="fademask" onclick="cltx();"></div><div class="z_tx" onclick="cltx();"><div class="z_tx_in"><div class="tx_tooth"></div><div class="tx_text"><p class="first">Спасибо!</p><p>Ваша заявка принята</p></div><div class="clear"></div></div></div>';
                $('body').append(tx);
        <? }?>
        <?
        if (!empty($arResult["ERRORS"])) {
			$str = str_replace("'", "&#039;", implode("<br />", $arResult["ERRORS"]));?>
                var tx = '<div class="fademask" onclick="cltx();"></div><div class="z_tx" onclick="cltx();"><div class="z_tx_in"><div class="tx_tooth"></div><div class="tx_text"><p class="error"><?=$str?></p></div><div class="clear"></div></div></div>';
                $('body').append(tx);
        <? }?>

    });
     
 </script> 
       
<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data" 
      class="zapis_f" style="display:<?= $dform; ?>">
      <?=bitrix_sessid_post()?>
      
        <select name="PROPERTY[66]" id="none">
<?
			foreach ($arResult["PROPERTY_LIST_FULL"]["66"]["ENUM"] as $key => $arEnum) {
			
					if ($arEnum["DEF"] == "Y") $checked = true;
?>
				<option class='zapis_resel'  data-id="<?= $key ?>" value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
<?
			}?>
        </select>
        
    <div class="cont_header">
        <span class="msico icplacemark"></span>
        <div class="conth_in">
            <p class="conttitle">&ndsp;</p>
            <p class="contaddr">&ndsp;</p>
        </div>
    </div>
    <div class="cont_select">
        <div class="cont_sel_active">
            <span class="white_ar msico"></span>
            <span class="cont_sel_at">Выбрать другой центр</span>
        </div>


        <ul class="cont_sel_drop">
            <? foreach ($arResult["PROPERTY_LIST_FULL"]["66"]["ENUM"] as $key => $arEnum) { ?>
                <li class='zapis_resel' data-id="<?= $key ?>">
                    <span><?=$arEnum["VALUE"]?></span>
                </li>
            <? }; ?>
        </ul>
    </div>
    <div class="zapf_winp even">
        <p class="zapf_label">Имя:<span class="grn">*</span></p>
        <input type="text" name="PROPERTY[NAME][0]"  
        <?
        if (!empty($arResult["ELEMENT"]["NAME"])) {
				echo ' value="'.$arResult["ELEMENT"]["NAME"].'" ';
		} else {
			echo ' value="" ';
		}
        if (!empty($arResult["ERRORS"])) {
			foreach ($arResult['ERRORS'] as $str) {
				if (strpos($str, "'Имя'") !== false) {
					echo 'class="error"';
				}
			}
		}
        ?> />
    </div>
    <div class="zapf_winp odd">
        <p class="zapf_label">Телефон:<span class="grn">*</span></p>
        
         <input type="text" name="PROPERTY[57][0]" 
<?
        if (!empty($arResult["ERRORS"])) {
			foreach ($arResult['ERRORS'] as $str) {
				if (strpos($str, "'Телефон'") !== false) {
					echo ' class="error" ';
				}
			}
		}
		if (!empty($arResult["ELEMENT_PROPERTIES"]["57"]["0"]["VALUE"])) {
				echo ' value="'.$arResult["ELEMENT_PROPERTIES"]["57"]["0"]["VALUE"].'" ';
		} else {
			echo ' value="" ';
		}
        ?> />
        
    </div>
    <div class="zapf_winp even">
        <p class="zapf_label sp10">Удобное время звонка:</p>
        
        <p class="sp10"><label for="property_20"><input type="radio" name="PROPERTY[58]" value="20"
<?
        if (!empty($arResult["ELEMENT_PROPERTIES"]["58"]["0"]["VALUE"]) and $arResult["ELEMENT_PROPERTIES"]["58"]["0"]["VALUE"] == 20) {
				echo ' checked ';
		} 
		?>
		id="property_20"> 9-12</label></p>
		
        <p class="sp10"><label for="property_21"><input type="radio" name="PROPERTY[58]" value="21" <?
        if (!empty($arResult["ELEMENT_PROPERTIES"]["58"]["0"]["VALUE"]) and $arResult["ELEMENT_PROPERTIES"]["58"]["0"]["VALUE"] == 21) {
				echo ' checked ';
		} 
		?> id="property_21"> 12-15</label></p>
		
        <p class="sp10"><label for="property_22"><input type="radio" name="PROPERTY[58]" value="22"<?
        if (!empty($arResult["ELEMENT_PROPERTIES"]["58"]["0"]["VALUE"]) and $arResult["ELEMENT_PROPERTIES"]["58"]["0"]["VALUE"] == 22) {
				echo ' checked ';
		} 
		?> id="property_22"> 15-18</label></p>
        
        <p><label for="property_23"><input type="radio" name="PROPERTY[58]" value="23"<?
        if (!empty($arResult["ELEMENT_PROPERTIES"]["58"]["0"]["VALUE"]) and $arResult["ELEMENT_PROPERTIES"]["58"]["0"]["VALUE"] == 23) {
				echo ' checked ';
		} 
		?> id="property_23"> 18-21</label></p>
    </div>
    <? //ниже инпут для вывода сообщения об успехе.  ?>
    <input type="submit" name="iblock_submit" value="Отправить" class="zapf_sub" />
</form>
<div class="zapis_prim_chose" style="display:<?= $chose; ?>">
    <div class="zapis_prim_chose_h">Выберите центр</div>
    <ul class="mai_conlist">
        <?

        foreach ($arResult["PROPERTY_LIST_FULL"]["66"]["ENUM"] as $key => $arEnum) { ?>
            <li>
                <span class='zap_chsel' data-id="<?= $key ?>">
                    <span class="mcico icplace"></span>
                    <span class="mai_conlit"><?=$arEnum["VALUE"]?></span>
                </span>
            </li>
<? 			
        } ?>
    </ul>
</div>
