</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.topnav_active').click(function() {
            if ($('.topnav_drop').is(':visible')) {
                $('.topnav_drop').slideUp('100');
            } else {
                $('.topnav_drop').slideDown('200');
            }
        });
        $('.mpbody').click(function() {
            if ($('.topnav_drop').is(':visible')) {
                $('.topnav_drop').slideUp('100');
            }
        });

        var swiper = new Swiper('.mob_slider', {
            //pagination: '.slider3pag',
            loop: true,
            grabCursor: true,
            autoPlay: 5000
        });
        //Navigation arrows
        $('.mob_slid_prev').click(function(e) {
            e.preventDefault();
            swiper.swipePrev();
        });
        $('.mob_slid_next').click(function(e) {
            e.preventDefault();
            swiper.swipeNext();
        });
    });
</script>
</body>
</html>