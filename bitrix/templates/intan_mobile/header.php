<!DOCTYPE HTML>
<html lang="ru" xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <meta charset="UTF-8"/>
        <meta name="MobileOptimized" content="width"/>
        <meta name="HandheldFriendly" content="true"/>
        <meta name="apple-mobile-web-app-title" content="Интан" />
        <link rel="apple-touch-icon" href="/bitrix/templates/intan_mobile/img/apple-touch-icon-57x57-precomposed.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="/bitrix/templates/intan_mobile/img/apple-touch-icon-72x72-precomposed.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="/bitrix/templates/intan_mobile/img/apple-touch-icon-114x114-precomposed.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="/bitrix/templates/intan_mobile/img/apple-touch-icon-144x144-precomposed.png" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <!--

             /\ /\
        All  =OxO=   we hail!
             .. ..
        -->
        <link rel="stylesheet" type="text/css" href="/css/reset.css" media="all" />
        <?
        $APPLICATION->ShowHead();
        $mbase = '/mobile/';
        $mzapis = '/mobile/zapisatsya_na_priyom/';
        $mcost = '/mobile/rasschitat_stoimost_lecheniya/';
        $mprice = '/mobile/stomatologiya-ceny/';
        $mvrach = '/mobile/personal/';
        $mcred = '/mobile/stomatologiya-ceny/lechenie-v-kredit/';
        $mspecial = '/mobile/aksii/';
        $center_adrr = array(
            array('cid' => 'biggun', 'adr' => 'Большая Пушкарская ул., д. 41', 'url' => '/mobile/stomatologiya_na_bolshoy_pushkarskoy/'),
            array('cid' => 'buda', 'adr' => 'Будапештская ул., д. 97, к.2', 'url' => '/mobile/stomatologiya_na_budapeshtskoy/'),
            array('cid' => 'buhar', 'adr' => 'Бухарестская ул., д. 96', 'url' => '/mobile/stomatologiya_na_buharestskoy/'),
            array('cid' => 'voznes', 'adr' => 'Вознесенский пр. д. 21', 'url' => '/mobile/stomatologiya_na_voznesenskom/'),
            array('cid' => 'komend', 'adr' => 'Комендантский пр., д. 7, к.1', 'url' => '/mobile/stomatologiya_na_komendantskom/'),
            array('cid' => 'ligov', 'adr' => 'Лиговский пр., д. 125', 'url' => '/mobile/stomatologiya_na_ligovskom/'),
            array('cid' => 'prosv', 'adr' => 'Просвещения пр., д. 87, к. 2', 'url' => '/mobile/stomatologiya_na_prosvesheniya/'),
            array('cid' => 'ross', 'adr' => 'Российский пр., д. 8', 'url' => '/mobile/stomatologiya_na_rossiyskom/'),
            array('cid' => 'stach', 'adr' => 'Стачек пр., д. 69 &laquo;А&raquo;', 'url' => '/mobile/stomatologiya_na_stachek/')
        );
        ?>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript"  src="/js/idangerous.swiper.js"></script>
        <script type="text/javascript"  src="/bitrix/templates/intan_mobile/intan_mobile.js"></script>
        <link rel="shortcut icon" href="/favicon.png" type="image/png" />
    </head>
    <body>
        <? $APPLICATION->ShowPanel(); ?>
        <div class="cage">
        <header class="mph">
            <h1 class="mphl hidetext"><a href="/mobile/">Стоматология Интан</a></h1>
            <div class="mph_phone">
                <div>Телефон для справок:</div>
                <div class="last">+7 (812) 777-0202</div>
            </div>
            <nav class="topnav">
                <div class="topnav_active">
                    <div class="tm_lines"><i></i></div>
                    <div class="tm_text">
                        <?
                        if ($_SERVER['REQUEST_URI'] == $mzapis):
                            echo 'Записаться на приём';
                        elseif ($_SERVER['REQUEST_URI'] == $mprice):
                            echo 'Цены';
                        elseif ($_SERVER['REQUEST_URI'] == $mcost):
                            echo 'Рассчитать стоимость';
                        elseif ($_SERVER['REQUEST_URI'] == $mvrach):
                            echo 'Врачи';
                         elseif ($_SERVER['REQUEST_URI'] == $mspecial):
                            echo 'Акции';
                        elseif ($_SERVER['REQUEST_URI'] == $mcred):
                            echo 'Лечение в кредит';
                        else: echo 'Меню'; //вдруг забредет чувак незнамо куда
                        endif;
                        ?>
                    </div>
                </div>
                <ul class="topnav_drop">
                    <li <?
                    if ($_SERVER['REQUEST_URI'] == $mbase) {
                        echo " class='hdn'";
                    };
                    ?>><a href="<?=$mbase;?>">Главная</a></li>
                    <li <?
                    if ($_SERVER['REQUEST_URI'] == $mzapis) {
                        echo " class='hdn'";
                    };
                    ?>><a href="<?=$mzapis;?>">Записаться на приём</a></li>
                    <li <?
                        if ($_SERVER['REQUEST_URI'] == $mprice) {
                            echo " class='hdn'";
                        };
                        ?>><a href="<?=$mprice;?>">Цены</a></li>
                    <li <?
                    if ($_SERVER['REQUEST_URI'] == $mcost) {
                        echo " class='hdn'";
                    };
                    ?>><a href="<?=$mcost;?>">Рассчитать стоимость</a></li>
                    <li <?
                    if ($_SERVER['REQUEST_URI'] == $mvrach) {
                        echo " class='hdn'";
                    };
                    ?>><a href="<?=$mvrach;?>">Врачи</a></li>
                    <li <?
                    if ($_SERVER['REQUEST_URI'] == $mspecial) {
                        echo " class='hdn'";
                    };
                    ?>><a href="<?=$mspecial;?>">Акции</a></li>
                    <li <?
                    if ($_SERVER['REQUEST_URI'] == $mcred) {
                        echo " class='hdn'";
                    };
                    ?>><a href="<?= $mcred ?>">Лечение в кредит</a></li>
                </ul>
            </nav>
        </header>
<div class="mpbody">
