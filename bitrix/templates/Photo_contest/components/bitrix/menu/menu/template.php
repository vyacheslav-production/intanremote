<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?print_r($arResult)?>
<?if (!empty($arResult)):?>
<ul>

<?
$count = count($arResult);
foreach($arResult as $key => $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li class="b-sp-menu__li"><a href="<?=$arItem["LINK"]?>" class="b-sp-menu__link act"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li class="b-sp-menu__li"><a href="<?=$arItem["LINK"]?>" class="b-sp-menu__link"><?=$arItem["TEXT"]?></a></li>
	<?endif;
        if (($key+1) != $count) {
            ?> <li class="b-sp-menu__sep b-sp-menu__li"></li> <?
        }
	
endforeach;?>

</ul>
<?endif?>