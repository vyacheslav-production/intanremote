<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
//print_r($arResult);
?>
<div class="l-cont"> 		

    <?if (count($arResult["ERRORS"])):?>
            <?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
    <?endif?>
    <?if (strlen($arResult["MESSAGE"]) > 0):?>
            <?=ShowNote($arResult["MESSAGE"])?>
    <?endif?>
    <form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data" class="b-regform cf">

        <?=bitrix_sessid_post()?>
        <?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
        <input type="hidden" name="PROPERTY[117][0]" value="0" /> 

        <div class="b-regform-bulge"> 				 
            <div class="b-regform-bulge__top"><i></i></div>
            <div class="b-regform-bulge__bg"> 					 
                <div class="b-regform-bulge__heading"> 						
                    <span>Заполни анкету</span> для участия в конкурсе: 					
                </div>
                <div class="b-regform-bulge__intro"> 	
                    Все пункты, отмеченные <span class="b-regform-bulge__star">*</span> обязательны к заполнению 
                </div>
                <div class="b-regform-field"> 						 
                    <p class="b-regform-field__label"><span class="b-regform-bulge__star">*</span> Имя:</p>
                    <input type="text" required="required" class="b-regform-field__input" name="PROPERTY[NAME][0]" /> 	
                </div>
                <div class="b-regform-field"> 						 
                    <p class="b-regform-field__label"><span class="b-regform-bulge__star">*</span> Фамилия:</p>
                    <input type="text" required="required" class="b-regform-field__input" name="PROPERTY[111][0]" /> 	
                </div>

                <div class="b-regform-field"> 						 
                    <p class="b-regform-field__label"><span class="b-regform-bulge__star">*</span> Телефон:</p>
                    <input type="tel" id="tel" required="required" class="b-regform-field__input"  name="PROPERTY[112][0]" /> 			
                </div>
                <div class="b-regform-field"> 						 
                    <p class="b-regform-field__label"><span class="b-regform-bulge__star">*</span> E-mail:</p>
                    <input type="email" required="required" class="b-regform-field__input"  name="PROPERTY[113][0]" /> 				
                </div>
                <label class="b-regform-chekline">
                    <input type="checkbox" required="required" class="b-regform-chekline__checkbox" name="PROPERTY[114][0][VALUE]" value="1" />
          
                    <i class="b-regform-chekline__check"></i> Я являюсь клиентом ИНТАН
                </label> 					
                <label class="b-regform-chekline">
                    <input type="checkbox" required="required" class="b-regform-chekline__checkbox"  name="PROPERTY[115][0][VALUE]" value="1" />
                    <i class="b-regform-chekline__check"></i> Я согласен с <a href="/special/terms/" >условиями конкурса</a>
                </label> 					
                <label class="b-regform-chekline">
                    <input type="checkbox" required="required" class="b-regform-chekline__checkbox"  name="PROPERTY[116][0][VALUE]" value="1" />
                    <i class="b-regform-chekline__check"></i> Я разрешаю использовать свое изображение в рамках конкурса
                </label>
            </div>
        </div>

        <div class="b-regform-img-desc"> 				 
            <div class="b-regform-img-adding cf"> 					 
                <div class="b-regform-img-upload"> 						
                    <i class="b-regform-img-upload__l"></i><i class="b-regform-img-upload__r"></i> 	
                    <span class="b-regform-img-upload__text">Загрузить фотографию</span> 		
                    <input id="b-regform-img-upload__input" required="required" class="b-regform-img-upload__input" type="file" name="PROPERTY_FILE_DETAIL_PICTURE_0" />
                    <input type="hidden" name="PROPERTY[DETAIL_PICTURE][0]" value="">                    
                </div>
                <div class="b-regform-img-upload_attach" id="b-regform-img-upload_attach"></div>

                <script type="text/javascript">
                    $(document).ready(function() {
                        $('b-regform-img-upload__text').click(function(){$('#b-regform-img-upload__input').click();})
                        $('#b-regform-img-upload__input').change(function()
                                {
                                    $('#b-regform-img-upload__input').each(function() {
                                        var name = this.value;
                                        reWin = /.*\\(.*)/;
                                        var fileTitle = name.replace(reWin, "$1");
                                        reUnix = /.*\/(.*)/;
                                        fileTitle = fileTitle.replace(reUnix, "$1");
                                        $('#b-regform-img-upload_attach').html('Приложенный файл: ' + fileTitle);
                                });
                                });
                    });
                </script>
                
            </div>
            
            <div class="b-regform-img-upload_note">Требования к файлу: размер не более 5 МБ, 
                <br />
                формат: jpg, png, gif. 				
            </div>

            <div class="b-regform-field"> 					 
                <p class="b-regform-field__label">Описание к фотографии:</p>
                <textarea class="b-regform-field__tarr" name="PROPERTY[DETAIL_TEXT][0]"></textarea> 				
            </div>
            <input type="submit" name="iblock_submit" value="Участвовать" class="b-regform-field__submit" /> 			
        </div>
    </form>
</div>