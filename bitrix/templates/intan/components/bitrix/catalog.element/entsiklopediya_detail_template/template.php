<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
        $this->AddEditAction($arrEnc['ID'], $arResult['EDIT_LINK'], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arResult['ID'], $arResult['DELETE_LINK'], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <article class="rly_art" id="<?=$this->GetEditAreaId($arResult['ID']);?>">
            <h1 class="secondary_h"><?=$arResult["NAME"]?></h1>
                <div class="rly_art_body">
                    <div class="rly_art_img"><img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="" /></div>
                    <div class="rly_art_text">
                        <?=$arResult["DETAIL_TEXT"]?>
                    </div>
                    <div class="clear"></div>
                </div>
        </article>