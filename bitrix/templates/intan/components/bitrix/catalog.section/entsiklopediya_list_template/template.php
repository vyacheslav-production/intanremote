<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); ?>
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br />
    <? endif; ?>
    

    <? foreach ($arResult["ITEMS"] as $arrEnc) { ?>
        <?
        $this->AddEditAction($arrEnc['ID'], $arrEnc['EDIT_LINK'], CIBlock::GetArrayByID($arrEnc["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arrEnc['ID'], $arrEnc['DELETE_LINK'], CIBlock::GetArrayByID($arrEnc["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <article class="rly_art" id="<?=$this->GetEditAreaId($arrEnc['ID']);?>">
            <h1><a href="<?= $arrEnc["DETAIL_PAGE_URL"] ?>"><?= $arrEnc["NAME"] ?></a></h1>
            <div class="rly_art_body">
<div class="rly_art_img"><img src="<?=$arrEnc["PREVIEW_PICTURE"]["SRC"]?>" alt="" /></div>
<div class="rly_art_text">
    <?=$arrEnc["PREVIEW_TEXT"]?>
</div>

                
                
                <div class="clear"></div>
            </div>

        </article>
    <? }
    ?>


    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br /><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>