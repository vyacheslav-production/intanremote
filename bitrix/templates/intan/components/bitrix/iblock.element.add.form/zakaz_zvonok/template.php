<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
?>

<script>
$(function () {
    //Заказать звонок
	$(function(){
		$("#header_zvonok").click(function(){
			$("#zvonok_prostynya").show();
		});
		$("#zvonok_close, #zvonok_close1").click(function(){
			$("#zvonok_prostynya").hide();
		});
	});
});	
</script>

<?if (count($arResult["ERRORS"])):?>
	<?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">

	<?=bitrix_sessid_post()?>

	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>

	<div class="zvonok_l_name">Имя*</div>
	<div class="zvonok_r_input"><input type="text" name="PROPERTY[NAME][0]" value="" style="width:378px;" /></div>
	<div class="clear"></div>
	<div class="zvonok_l_name">Телефон*</div>
	<div class="zvonok_r_input" style="margin:0 0 35px 0;"><input type="text" name="PROPERTY[99][0]" value="" /></div>
	<div class="clear"></div>
	<div class="zvonok_l_name" style="line-height: 12px;">Удобное время <div style="margin:6px 0 0 0;">для звонка</div></div>
	<div class="zvonok_r_input">
		<div class="radio_item"><label><input type="radio" name="PROPERTY[100]" checked value="72">9—12</label></div>
		<div class="radio_item"><label><input type="radio" name="PROPERTY[100]" value="73">12—15</label></div>
		<div class="radio_item"><label><input type="radio" name="PROPERTY[100]" value="74">15—18</label></div>
		<div class="radio_item"><label><input type="radio" name="PROPERTY[100]" value="75">18—21</label></div>
		<div class="clear"></div>
	</div>
	<div class="zvonok_l_name" style="line-height: 12px;">Выберите клинику</div>
	<div class="zvonok_r_input zvonok_r_input1">
		<label><input type="radio" name="PROPERTY[101]" checked value="76">Стачек проспект</label>
		<label><input type="radio" name="PROPERTY[101]" value="77">Комендантский проспект</label>
		<label><input type="radio" name="PROPERTY[101]" value="78">Лиговский проспект</label>
		<label><input type="radio" name="PROPERTY[101]" value="79">Просвещения проспект</label>
		<label><input type="radio" name="PROPERTY[101]" value="80">Будапештская улица</label>
		<label><input type="radio" name="PROPERTY[101]" value="81">Бухарестская улица</label>
		<label><input type="radio" name="PROPERTY[101]" value="82">Луначарского пр</label>
		<label><input type="radio" name="PROPERTY[101]" value="83">Российский проспект</label>
		<label><input type="radio" name="PROPERTY[101]" value="84">Большая Пушкаркая ул</label>
		<label><input type="radio" name="PROPERTY[101]" value="85">Вознесенский пр</label>		
	</div>
	
	<input type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
	<input type="button" name="close" value="Закрыть" id="zvonok_close1" />
	<?if (strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):?><input type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" /><?endif?>
	
	<?if (strlen($arParams["LIST_URL"]) > 0):?><a href="<?=$arParams["LIST_URL"]?>"><?=GetMessage("IBLOCK_FORM_BACK")?></a><?endif?>
</form>