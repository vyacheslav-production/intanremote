<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
?>
<?
//get list of intan centers
$db = CIBlockElement::GetList(Array("SORT" => "ASC"), array('IBLOCK_ID' => 5), false, false, array('NAME', 'ID'));
while ($ob = $db->GetNextElement()) {
    $arClinic[] = $ob->GetFields();
}
foreach ($arClinic as $v) {
    $selOpt.='<div value="' . $v['ID'] . '">' . $v['NAME'] . '</div>';
}
?>

<form name="iblock_add" id="intanRequest" action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <!--<form name="intan_request" >-->
    <h1>
        Заявка на лечение в кредит
    </h1>
    <? if (count($arResult["ERRORS"])): ?>
        <span id="credit_error">Поля отмеченные '*' обязательны для заполнения</span>
    <? endif ?>
    <? if (strlen($arResult["MESSAGE"]) > 0): ?>
        <span id="credit_ok"></span>
    <? endif ?>
    <p>
        Оставьте ваши данные
    </p> 

    <div class="t f">
		<?/*
        <div class="tr">
            <div class="td">       
                Фамилия     
            </div> 
            <div class="td">  
                <input type="text" name="PROPERTY[102][0]" maxlength="27" />        
            </div>                
        </div>*/?>
        <div class="tr">
            <div class="td">       
                Как Вас зовут?*     
            </div> 
            <div class="td">  
                <input type="text" name="PROPERTY[NAME][0]" maxlength="27" />         
            </div>                
        </div>
        <?/*
        <div class="tr">
            <div class="td">       
                Отчество     
            </div> 
            <div class="td">  
                <input type="text" name="PROPERTY[104][0]" maxlength="27" />         
            </div>                
        </div>*/?>
        <div class="tr">
            <div class="td">       
                Телефон для связи*     
            </div> 
            <div class="td">  
                <input type="text" name="PROPERTY[105][0]" maxlength="27" />         
            </div>                
        </div>
        <div class="f_zapisi_bottom">
        <p class="forma_zapisi_h">Удобное время звонка</p>
        <div class="radio_wrap">
            <div class="radio_item"><label><input type="radio" name="PROPERTY[111]" value="88" />9&mdash;12</label></div>
            <div class="radio_item"><label><input type="radio" name="PROPERTY[111]" value="89" />12&mdash;15</label></div>
            <div class="radio_item"><label><input type="radio" name="PROPERTY[111]" value="90" />15&mdash;18</label></div>
            <div class="radio_item"><label><input type="radio" name="PROPERTY[111]" value="91" />18&mdash;21</label></div>
            <div class="clear"></div>
        </div>
    </div>
    </div>  <!-- t-->
    <?/*
    <p>
        Информация по кредиту
    </p>  
    <div class="t">
        <div class="tr">
            <div class="td">       
                Сумма кредита, руб.*    
            </div> 
            <div class="td">  
                <p><input type="text" name="PROPERTY[106][0]" id="creditSum" maxlength="27" /></p>
                <p><span id="err">Максимальный размер кредита 300000 руб.</span></p>          
            </div>                
        </div>
        <div class="tr">
            <div class="td" >       
                Выберите центр «ИНТАН»     
            </div> 
            <div class="td" style="position:relative" id="targetDiv">          

                <div class="pseudo-select">
                    <div class="select">Любой</div>
                    <div class="options">
                        <div value="0">Любой</div><!--
                        <div value="second" class="check">Будапештская ул., дом 97, к.2</div>-->
                        <?= $selOpt; ?>
                    </div>
                    <input type="hidden" name="PROPERTY[107][0]" value="first">
                    <span id="sel_act"></span>
                </div>
            </div>                
        </div>
    </div> */?> <!-- t-->  
    <input type="submit" name="iblock_submit" value="Отправить запрос" /> 
</form>