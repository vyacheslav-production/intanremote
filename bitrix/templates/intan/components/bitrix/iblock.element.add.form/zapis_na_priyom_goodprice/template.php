<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" onsubmit="yaCounter9957124.reachGoal('zapis_landing_implants'); return true;" class="appoint-form">
    <?= bitrix_sessid_post() ?>
    <h4>
        ЗАПИШИТЕСЬ НА ПРИЁМ
    </h4>
    <div class="appoint-form-row">
        <div class="input-box">
            <i class="ico ico-human"></i>
            <input name="PROPERTY[NAME][0]" type="text" placeholder="ваше имя">
        </div>
    </div>
    <div class="appoint-form-row">
        <div class="input-box">
            <i class="ico ico-phone"></i>
            <input name="PROPERTY[57][0]" type="text" placeholder="контактный телефон">
        </div>
    </div>
    <div class="appoint-form-row dropdown">
        <div class="input-box dropdown-button">
            <i class="ico ico-point"></i>
            <span class="dropdown-arr"></span>
            <span class="dropdown-button-title">
                выберите центр
            </span>
        </div>
<ul class="dropdown-menu">
     <li data-value="55">
         <i class="ico ico-metro-red"></i>
         <span class="dropdown-menu-title">Стачек проспект</span>
     </li>
     <li data-value="49">
         <i class="ico ico-metro-purple"></i>
         <span class="dropdown-menu-title">Комендантский пр. 7, к.1</span>
     </li>
	  <li data-value="90">
         <i class="ico ico-metro-purple"></i>
         <span class="dropdown-menu-title">Комендантский пр. 42, к. 1</span>
     </li>
     <li data-value="50">
         <i class="ico ico-metro-purple-orange"></i>
         <span class="dropdown-menu-title">Лиговский проспект</span>
     </li>
     <li data-value="52">
         <i class="ico ico-metro-red"></i>
         <span class="dropdown-menu-title">Просвещения проспект</span>
     </li>
     <li data-value="48">
         <i class="ico ico-metro-blue"></i>
         <span class="dropdown-menu-title">Будапештская улица</span>
     </li>
     <li data-value="53">
         <i class="ico ico-metro-purple"></i>
         <span class="dropdown-menu-title">Бухарестская улица</span>
     </li>
     <li data-value="54">
         <i class="ico ico-metro-orange"></i>
         <span class="dropdown-menu-title">Российский проспект</span>
     </li>
     <li data-value="56">
         <i class="ico ico-metro-blue"></i>
         <span class="dropdown-menu-title">Большая Пушкаркая</span>
     </li>
     <li data-value="87">
         <i class="ico ico-metro-blue-orange-purple"></i>
         <span class="dropdown-menu-title">Вознесенский пр</span>
     </li>
     <li data-value="88">
         <i class="ico ico-metro-blue"></i>
         <span class="dropdown-menu-title">Луначарского пр</span>
     </li>
     <li data-value="89">
         <i class="ico ico-metro-purple"></i>
         <span class="dropdown-menu-title">Богатырский проспект</span>
     </li>
	 <li data-value="92">
         <i class="ico ico-metro-orange"></i>
         <span class="dropdown-menu-title">Ленинградская улица</span>
     </li>
</ul>
         <input name="PROPERTY[66]" type="hidden" value="0">
    </div>
    <div class="appoint-form-row appoint-form-row-submit">
        <input name="iblock_submit" class="custom-form" type="submit" value="ОТПРАВИТЬ ЗАЯВКУ">
    </div>
</form>
