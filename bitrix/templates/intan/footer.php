<?
                if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
                    die();
                IncludeTemplateLangFile(__FILE__);
                ?>

                <?
                $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                    "AREA_FILE_SHOW" => "page",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => ""
                        ), false, array(
                    "ACTIVE_COMPONENT" => "N"
                        )
                );
                ?>

            </div>
        </section>
        <footer id="page_footer">
            <div class="container">

                <div class="footer_content">
                    <?
                    $APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", Array(
                        "ROOT_MENU_TYPE" => "bottom_menu", // Тип меню для первого уровня
                        "MAX_LEVEL" => "1", // Уровень вложенности меню
                        "CHILD_MENU_TYPE" => "left", // Тип меню для остальных уровней
                        "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "DELAY" => "N", // Откладывать выполнение шаблона меню
                        "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
                        "MENU_CACHE_TYPE" => "N", // Тип кеширования
                        "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                        "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
                            ), false
                    );
                    ?>
                    <div class="foot_made_by" style="margin:0 0 0 25px;">
                        <p style="font-style:normal; margin: 26px 0 0;">Лицензия на осуществление медицинской деятельности  № 78-01-004451 выдана 28.02.2014 комитетом по здравоохранению г. Санкт-Петербурга</p>
                    </div>
                    <p class="foot_copy">ООО «ИНТАН», ИНН 7805105288, ОГРН 1027802717800</p>
                    <p>Адрес: (Юридический, фактический) г. Санкт-Петербург,</p>
                    <p>пр. Стачек д.69, литер А</p>
                    <div class="clear"></div>
                    <div class="foot_made_by">
                        <p>&laquo;Легион&raquo;</p>
                        <span class="legion_logo"></span>
                        <p><a href="http://www.ralegion.ru/" title="Легион - создание сайтов в Санкт-Петербурге">Разработка сайта</a></p>
                    </div>
                    <p class="foot_copy">&copy; 1998 - <?php echo date(Y); ?> &laquo;ИНТАН&raquo;</p>
                    <p>О возможных противопоказаниях проконсультируйтесь с нашими специалистами.</p>
                </div>
                <div class="clear"></div>
            </div>
        </footer>
        <div class="fademask"></div>
        <div class="pop_window">
            <div class="popwin_in">
                <? /*div class="popw_top">
                    <div class="popw_title">Интервью с главным врачом</div>
                    <div class="popw_close">Закрыть</div>
                    <div class="clear"></div>
                </div>
                <div>

                </div */ ?>
            </div>
        </div>
        <!-- Google-analitics counter -->
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-25730239-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <!-- /Google-analitics counter -->
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-56125435-11', 'auto');
		  ga('require', 'displayfeatures');
		  ga('send', 'pageview');

		</script>
		
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function(d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter9957124 = new Ya.Metrika({id: 9957124,
                            webvisor: true,
                            clickmap: true,
                            accurateTrackBounce: true});
                    } catch (e) {
                    }
                });

                var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function() {
                    n.parentNode.insertBefore(s, n);
                };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else {
                    f();
                }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="//mc.yandex.ru/watch/9957124" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        <script type="text/javascript"><!-- /* build:::5 */ -->
            var liveTex = true,
                    liveTexID = 20341,
                    liveTex_object = true;
            (function() {
                var lt = document.createElement('script');
                lt.type = 'text/javascript';
                lt.async = true;
                lt.src = 'http://cs15.livetex.ru/js/client.js';
                var sc = document.getElementsByTagName('script')[0];
                sc.parentNode.insertBefore(lt, sc);
            })();
        </script>
<!-- Код тега ремаркетинга Google -->
<!--------------------------------------------------
С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 969714583;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/969714583/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
    </body>

</html>