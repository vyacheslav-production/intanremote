<!DOCTYPE html>
<html>
    <head>
        <title><?$APPLICATION->ShowTitle()?> - WebAuditPro</title>
        <link rel="stylesheet" type="text/css" media="all" href="/css/reset.css">
        <?$APPLICATION->ShowHead()?>
        <!--[if lt IE 9]>
        <script src="/js/html5.js" type="text/javascript"></script>
        <![endif]-->
        <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" media="all" href="/css/ie8.css">
        <![endif]-->
        <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" media="all" href="/css/ie9.css">
        <![endif]-->
        <!--[if lte IE 7]>
        <link rel="stylesheet" type="text/css" media="all" href="/css/ie7.css">
        <![endif]-->
        <script src="/js/jquery.js" type="text/javascript"></script>
        <script src="/js/scripts.js" type="text/javascript"></script>
        <script src="/js/jquery-ui-1.8.17.custom.min.js" type="text/javascript"></script>
        <script src="/js/cusel_min2.5.js" type="text/javascript"></script>
        <link rel="shortcut icon" href="/favicon.png" type="image/png">
    </head>
    <body>
        <?$APPLICATION->ShowPanel();?>
        <div id="skip-link">
            <a href="#main-content">Skip to main content</a>
        </div>
        <div class="background">
            <div id="container">
                <header id="page_header">
                    <div class="header_top">
                        <div class="header_top_aside">
                            <div class="header_map">
                                <a href="mailto:info@webauditpro.ru" class="header_map_mail" title="Написать письмо">Написать письмо</a>
                                <a href="/sitemap/" class="header_map_sitemap" title="Карта сайта">Карта сайта</a>
                                <a href="/" class="header_map_home" title="На главную">На главную</a>
                                <div class="clear"></div>
                            </div>
                            <div class="h_phone">
                                <span style="color: #666666; font-size: 10px">т. (812)</span> 925-00-66
                            </div>
                            <div class="header_order">
                                <a href="/otpravit_zayavku/" class="send_an_order">
                                    <span>Отправить заявку</span>
                                </a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <hgroup>
                            <h1 id="page_h">
                                <a href="/">WebAuditPro - профессиональный аудит сайтов</a>
                            </h1>
                        </hgroup>
                        <div class="header_top_center">
                                <?$APPLICATION->IncludeComponent("bitrix:search.form", "webpro_search1", Array(
                                        "USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
                                        "PAGE" => "#SITE_DIR#search/index.php",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
                                        ),
                                        false
                                        );?>
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "top_menu",
                                    Array(
                                            "ROOT_MENU_TYPE" => "top",
                                            "MENU_CACHE_TYPE" => "N",
                                            "MENU_CACHE_TIME" => "3600",
                                            "MENU_CACHE_USE_GROUPS" => "Y",
                                            "MENU_CACHE_GET_VARS" => array(),
                                            "MAX_LEVEL" => "1",
                                            "CHILD_MENU_TYPE" => "left",
                                            "USE_EXT" => "N",
                                            "DELAY" => "N",
                                            "ALLOW_MULTI_SELECT" => "N"
                                    )
                                );?>
                                <div class="clear"></div>
                            </div>
                        <div class="clear"></div>
                    </div>
                    <div class="header_shadow"></div>
                    <div class="header_bottom">
                        <div class="header_bottom_right">
                            <p class="first">Мы осознанно отказались от предоставления любых услуг, кроме консалтинга и аудита.</p>
                            <p>Вы всегда можете быть уверены в непредвзятости и достоверности предоставляемой нами информации.</p>
                        </div>
                        <div class="header_bottom_left">
                            <div class="first">
                                <div class="header_bottom_icon">
                                    <img src="/images/tz_help_icon.png" alt="" />
                                </div>
                                <div class="header_bottom_moved">   
                                <?php if($_SERVER['REQUEST_URI'] == '/pomoshch_v_sostavlenii_tz/'):?><a class="selected" href="/pomoshch_v_sostavlenii_tz/"><span>Помощь в составлении</span><span>технических заданий</span></a>
                                    <?php else:?><a href="/pomoshch_v_sostavlenii_tz/"><span><span>Помощь в составлении</span></span><span><span>технических заданий</span></span></a>  
                                    <?php endif;?>
                                </div>    
                                <div class="clear"></div>
                            </div>
                            <div>
                                <div class="header_bottom_icon">
                                    <img src="/images/control_podr_icon.png" alt="" />
                                </div>
                                <div class="header_bottom_moved">
                                    <?php if($_SERVER['REQUEST_URI'] == '/kontrol_effektivnosti_podryadchikov/'):?><a class="selected" href="/kontrol_effektivnosti_podryadchikov/"><span>Контроль эффективности</span><span>подрядчиков</span></a>
                                    <?php else:?><a href="/kontrol_effektivnosti_podryadchikov/"><span><span>Контроль эффективности</span></span><span><span>подрядчиков</span></span></a>
                                    <?php endif;?> 
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="header_bottom_center">
                            <div>
                                <div class="header_bottom_icon">
                                    <img src="/images/globe_search_icon.png" alt="" />
                                </div>
                                <div class="header_bottom_moved">
                                    <span>WebAuditPro не сотрудничает с маркетинговыми и web-студиями.</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </header>

                <div id="main-content" class="clear">
                