<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

//delayed function must return a string
if (empty($arResult))
    return "";

$strReturn = '<nav class="breadcrumb-navigation"><ul><li><a href="/">Услуги</a></li><li><span>&nbsp;&gt;&nbsp;</span></li>';
//$sameNC = false;
//if($arResult[(count($arResult) - 1)]["TITLE"]==$arResult[(count($arResult) - 2)]["TITLE"]){
//    $sameNC = true;
//}
for ($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++) {

    if ($arResult[$index]["TITLE"] != $arResult[$index - 1]["TITLE"]) {
        if ($index > 0) {
            $strReturn .= '<li><span>&nbsp;&gt;&nbsp;</span></li>';
        }
        $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
        if ($arResult[$index]["LINK"] <> "" && $index < (count($arResult) - 1)) {
            $strReturn .= '<li><a href="' . $arResult[$index]["LINK"] . '" title="' . $title . '">' . $title . '</a></li>';
        } else {
            $strReturn .= '<li><span>' . $title . '</span></li>';
        }
    }
}
$strReturn .= '</ul></nav>';
return $strReturn;
?>