<div class="header_search">
    <form action="<?=$arResult["FORM_ACTION"]?>">
        <input name="s" type="submit" class="search_go" value="Поиск" />
        <div class="header_searchbar">
            <span class="searchbar_sides left"></span>
            <span class="searchbar_sides right"></span>
            <div class="searchbar_inner">
                <?if($arParams["USE_SUGGEST"] === "Y"):?><?$APPLICATION->IncludeComponent(
				"bitrix:search.suggest.input",
				"",
				array(
					"NAME" => "q",
					"VALUE" => "",
					"INPUT_SIZE" => 15,
					"DROPDOWN_SIZE" => 10,
				),
				$component, array("HIDE_ICONS" => "Y")
			);?><?else:?><input type="text" name="q" class="seach_field" /><?endif;?>
            </div>
        </div>
        <div class="clear"></div>
    </form>
</div>