<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); ?>
<div class="news-list">
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br />
    <? endif; ?>
    <div class="articles_row">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>

            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <article class="articles_item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <h2 class="post_h2"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></h2>
                <div>
                    <?= $arItem['PREVIEW_TEXT'] ?>
                </div>
            </article>

        <? endforeach; ?>
        <div style="clear:left"></div>
    </div>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"] && count($arResult["ITEMS"]) > 4): ?>
        <br /><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>