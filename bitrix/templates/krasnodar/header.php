

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=980" />
	<title>Intan</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
	<link rel="shortcut icon" href="/assets/images/favicon.png" type="favicon.png">
	<link rel="stylesheet" href="/assets/css/styles.css" charset="utf-8">
	<!--         <link  type="text/css" rel="stylesheet" cssWatch="no"  href="js/swiper-slider/idangerous.swiper.css">
			<link  type="text/css" rel="stylesheet" cssWatch="no"  href="js/swiper-scrollbar/lib/idangerous.swiper.scrollbar.css">
			<link  type="text/css" rel="stylesheet" cssWatch="no"  href="js/jscrollpane/style/jquery.jscrollpane.css"> -->
	<!-- <link   rel="stylesheet" type="text/css"  cssWatch="no"  href="js/fancybox-2.1.5/source/jquery.fancybox.css"> -->
	<link rel="stylesheet" type="text/css"  cssWatch="no"  href="/assets/js/libs/fancybox/jquery.fancybox.css">
	<!--[if lt IE 9]>
	<script  type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<!-- <header id="header" class="header"> -->
<header>
	<div class="head">
		<div class="wrap">
			<div class="head-nav">
				<a href="#promo"><span>Начало</span></a>
				<a href="#price"><span>Цены</span></a>
				<a href="#doctors"><span>Врачи</span></a>
				<a href="#map"><span>Контакты</span></a>
				<a href="#make_an_appointment" class="appointment fr fancybox"><span>Записаться на прием</span></a>
			</div>

		</div>
	</div>
	<!-- Fancybox    -->
	<div id="make_an_appointment" style="display: none; width: 480px;">
		<form action="">
			<h3 class="form_title"><span>Записаться на прием</span></h3>

			<div class="field_box">
				<input type="text" required placeholder="Ваше имя" id="name" name="name" class="name_field">
				<label id="name-error" class="error" for="name" style="display: none;"></label>

				<input pattern=".{1,}" required title="Укажите телефон" class="phone_field" id="phone" type="text" placeholder="Телефон" name="phone">
				<label id="phone-error" class="error" for="phone" style="display: none;"></label>

				<select name="" id="" required>
					<option  value="" disabled selected style="font-style:italic;">Выберите клинику</option>
					<option value="">Восточно-Кругликовская, д. 46/11</option>
					<option value="">70-летия Октября, д. 15</option>
				</select>

				<input type="submit" value="Отправить" class="button">
			</div>

		</form>
		<div class="form_bottom">
			<img src="/assets/images/logo_w.png" alt="">
		</div>
	</div>
	<!-- /fancybox -->

</header>

<section id="content" class="content">

	<!-- START CONTENT-->




	<section class="promo" id="promo">
		<div class="wrap">
			<img src="/assets/images/eat_apple.png" alt="chiks">
			<div class="promo_text">
				<img src="/assets/images/logo.png" alt="logo">
				<p class="promo_sity"><span>Новые центры ИНТАН</span> <br> <span>в Краснодаре</span></p>
				<p class="promo_phone">Телефон: +7 (861) 277-02-02</p>
			</div>
		</div>
	</section>

	<section class="event">
		<div class="wrap">
			<img src="/assets/images/we_open.jpg" alt="we are open">
			<img src="/assets/images/discount.jpg" alt="discount">
			<p class="event_text">Успей поучаствовать в акции стоматологии ИНТАН! Только до 1 февраля 2015 года – скидка на все услуги 7%</p>
		</div>
	</section>

	<section class="price" id="price">
		<div class="wrap">
			<h2>Цены</h2>


			<div class="table_wrap">

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/implantasiya.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/protezirovanie.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/lechenie.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/ortodontiya.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/paradontologiya.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/diagnostika.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/otbelivanie.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/estetic.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/detskaya.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/anestezia.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>

				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/stomatologiya-ceny/distribution.php",
					"EDIT_TEMPLATE" => ""
				), false
				);
				?>
<?/*

				<!-- таблица прайса -->
				<!-- table.price_book>tr.price_row>td.priceb_descr+td.priceb_price -->
				<table class="price_book activeCategory" data-select="1">
					<tr class="price_row" >
						<td class="priceb_descr"><span>Консультация врача протезиста </span></td>
						<td class="priceb_price">390 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Анестезия аппликационная</span></td>
						<td class="priceb_price">90 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Анестезия</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Составление плана протезирования </span></td>
						<td class="priceb_price">1100 Р</td>
					</tr>

					<tr class=" price_book--title">
						<td><p class="">Съемное протезирование </p></td>
					</tr>

					<tr class="price_row lvl" data-first="valplast">
						<td class="priceb_descr"> <span>Термопластические протезы «Valplast»</span></td>
						<td class="priceb_price"> </td>
					</tr>
					<tr class=" hide lvl2" data-secondLvL="valplast">
						<td class="priceb_descr"> <span>111</span></td>
						<td class="priceb_price"> 30 Р</td>
					</tr>
					<tr class=" hide lvl2" data-secondLvL="valplast">
						<td class="priceb_descr"> <span>222</span></td>
						<td class="priceb_price"> 60 Р</td>
					</tr>
					<tr class=" hide lvl2" data-secondLvL="valplast">
						<td class="priceb_descr"> <span>33</span></td>
						<td class="priceb_price"> 60 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Анестезия</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Анестезия</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Анестезия</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Анестезия</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Анестезия</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Анестезия</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>тут</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Анестезия</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>не видно</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row lvl" data-first="valplast1">
						<td class="priceb_descr"> <span>Термопластические протезы «Valplast»</span></td>
						<td class="priceb_price"> </td>
					</tr>
					<tr class=" hide lvl2" data-secondLvL="valplast1">
						<td class="priceb_descr"> <span>111</span></td>
						<td class="priceb_price"> 30 Р</td>
					</tr>
					<tr class=" hide lvl2" data-secondLvL="valplast1">
						<td class="priceb_descr"> <span>222</span></td>
						<td class="priceb_price"> 60 Р</td>
					</tr>
					<tr class=" hide lvl2" data-secondLvL="valplast1">
						<td class="priceb_descr"> <span>33</span></td>
						<td class="priceb_price"> 60 Р</td>
					</tr>
				</table>


				<!-- 	    		<table class="price_book" data-select="2">
									<tr class="price_row">
										<td class="priceb_descr"><span>2222</span></td>
										<td class="priceb_price">2222</td>
									</tr>
								</table> -->
				<table class="price_book" data-select="2">
					<tr class="price_row">
						<td class="priceb_descr">Консультация врача протезиста </td>
						<td class="priceb_price">390 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr">Анестезия аппликационная</td>
						<td class="priceb_price">90 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr">Анестезия</td>
						<td class="priceb_price">300 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr">Составление плана протезирования </td>
						<td class="priceb_price">1100 Р</td>
					</tr>

					<tr class=" price_book--title">
						<td><p class="">Съемное протезирование </p></td>
					</tr>

					<tr class="price_row lvl" data-first="valplast">
						<td class="priceb_descr"> <span>Термопластические протезы «Valplast»</span></td>
						<td class="priceb_price"> </td>
					</tr>
					<tr class=" hide lvl2" data-secondLvL="valplast">
						<td class="priceb_descr"> <span>111</span></td>
						<td class="priceb_price"> 30 Р</td>
					</tr>
					<tr class=" hide lvl2" data-secondLvL="valplast">
						<td class="priceb_descr"> <span>222</span></td>
						<td class="priceb_price"> 60 Р</td>
					</tr>
					<tr class=" hide lvl2" data-secondLvL="valplast">
						<td class="priceb_descr"> <span>33</span></td>
						<td class="priceb_price"> 60 Р</td>
					</tr>
					<tr class="price_row">
						<td class="priceb_descr"><span>Анестезия</span></td>
						<td class="priceb_price">300 Р</td>
					</tr>
				</table>
				<!-- //таблица прайса -->

				<p class="roll"><span>развернуть список</span></p>

 */?>
			</div>

			<!-- меню прайса -->
			<nav class="price_menu">
					<a href="#" data-category="1" class="chose">Имплантация</a>
					<a href="#" data-category="2">Протезирование</a>
					<a href="#" data-category="3">Лечение</a>
					<a href="#" data-category="4">Ортодонтия</a>
					<a href="#" data-category="5">Пародонтология</a>
					<a href="#" data-category="6">Диагностика</a>
					<a href="#" data-category="7">Отбеливание</a>
					<a href="#" data-category="8">Эстетическая стоматология</a>
					<a href="#" data-category="9">Детская стоматология</a>
			</nav>
			<!-- //меню прайса -->
		</div>
	</section>

	<section class="facts">
		<div class="wrap">
			<div class="control">
				<div class="arrow icons-arrow-next"><span></span></div>
				<div class="arrow icons-arrow-prev"><span></span></div>
			</div>
			<div class="swiper-container">


				<div class="swiper-wrapper">
					<!--First Slide-->
					<div class="swiper-slide">
						<p class="facts_text">15 лет работы</p>
					</div>

					<!--Second Slide-->
					<div class="swiper-slide">
						<p class="facts_text">ежедневно более 300 пациентов</p>
					</div>

					<!--Third Slide-->
					<div class="swiper-slide">
						<p class="facts_text">более 50 000 операций по имплантации зубов</p>
					</div>
					<!--Etc..-->
				</div>
			</div>




		</div>
	</section>

	<section class="doctors" id="doctors">
		<div class="wrap">
			<h2>Врачи стоматологии «Интан»</h2>
			<div class="third">
				<h3>Уже больше 25 лет возвращаю улыбкам здоровье</h3>
				<img src="/assets/images/person1.png" alt="">
				<p class="doctor_name">Анисенков Александр Дальевич</p>
				<p class="doctor_description">Стоматолог-ортопед</p>
			</div>
			<div class="third">
				<h3>За время работы установил более 600 имплантатов</h3>
				<img src="/assets/images/person2.png" alt="">
				<p class="doctor_name">Рад Камяр Фредович</p>
				<p class="doctor_description">Кандидат медицинских наук</p>
			</div>
			<div class="third">
				<h3>Осчастливила 303 пациента красивыми улыбками</h3>
				<img src="/assets/images/person3.png" alt="">
				<p class="doctor_name">Костюченко Ирина Валерьевна</p>
				<p class="doctor_description">&nbsp;</p>
			</div>
		</div>
	</section>

	<section class="map" id="map">
		<div class="wrap">
			<h2>Центры «Интан» в Краснодаре</h2>
			<div class="map_box" id="map_canvas">
			</div>

			<div class="map_box" id="map_canvas_2">
			</div>
		</div>
	</section>
	s


	<!-- END CONTENT-->

</section>
<!-- <footer id="footer" class="footer"> -->
<!-- Футер -->
<footer>
	<div class="wrap">
		<div class="footer_top">
			<div class="social-icons">
				<a class="vk" href="#"><i></i></a>
				<a class="fb" href="#"><i></i></a>
				<a class="ok" href="#"><i></i></a>
			</div>
			<a class="gotohome" href="http://intan.ru/" target="_blank">Перейти на основной сайт</a>
		</div>
		<div class="footer_middle">
			<div class="pull_left ib">
				<p>
					ООО «ИНТАН», ИНН 7805105288<br>ОГРН 1027802717800
				</p>
			</div>
			<div class="pull_right ib">
				<a href="http://ralegion.info" class="legion-logo" target="_blank">
					Разработка сайта —
				</a>
			</div>
		</div>

		<p class="copyright"><span>© 1998 - <?= date('Y'); ?> «ИНТАН»</span><span>О возможных противопоказаниях проконсультируйтесь с нашими специалистами.</span></p>
	</div>
</footer>
<!-- Футер -->

<script src="/assets/js/libs/jquery-1.10.2.min.js" type="text/javascript"> </script>

<!--<script src="/assets/js/fancybox-2.1.5/source/jquery.fancybox.js?v=2.1.5" type="text/javascript"  charset="utf-8"> </script> -->


<!-- походу не нужно <script src="/assets/js/jscrollpane/script/jquery.mousewheel.js" type="text/javascript"  charset="utf-8"> </script> -->


<!-- <script src="/assets/js/fancybox-2.1.5/source/helpers/jquery.fancybox-media.js" type="text/javascript"  charset="utf-8"> </script> -->


<!-- походу не нужно <script src="/assets/js/swiper-slider/idangerous.swiper.js" type="text/javascript"  charset="utf-8"> </script>
<script src="/assets/js/swiper-scrollbar/lib/idangerous.swiper.scrollbar.js" type="text/javascript"  charset="utf-8"> </script>
<script src="/assets/js/jscrollpane/script/jquery.jscrollpane.js" type="text/javascript"  charset="utf-8"> </script> -->


<!--<script src="/assets/js/jquery.mask.js" type="text/javascript"  charset="utf-8"> </script>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="/assets/js/js.js" type="text/javascript">  </script> -->

<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="/assets/js/libs/fancybox/jquery.fancybox.pack.js"></script>


<script src="/assets/js/libs/scrollTo.js"></script>

<script src="/assets/js/libs/idangerous.swiper.min.js"></script>
<script src="/assets/js/libs/jquery.maskedinput-1.3.1.js"></script>
<!-- <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="/assets/js/libs/jquery-1.10.2.min.js"></script>
<script src="/assets/js/libs/browser.js"></script>
<script src="/assets/js/libs/jquery.easing.1.3.js"></script>
<script src="/assets/js/libs/jquery.placeholder.js"></script>
<script src="/assets/js/libs/jquery.inputmask.js"></script>
<script src="/assets/js/libs/jquery.scrollTo-1.4.3.1-min.js"></script>
<script src="/assets/js/libs/mousewheel.js"></script>
<script src="/assets/js/libs/fancybox/jquery.fancybox.pack.js"></script>
<script src="/assets/js/libs/jquery.bxslider.min.js"></script>
<script src="/assets/js/libs/jquery.tabslet.min.js"></script>
<script src="/assets/js/libs/jquery.datetimepicker.js"></script>
<script src="/assets/js/libs/ion.rangeSlider.min.js"></script>
<script src="/assets/js/libs/bootstrap-transition.js"></script>
<script src="/assets/js/libs/bootstrap-modal.js"></script> -->
<script src="/assets/js/utils.js"></script>
<!--<script src="/assets/js/helpers.js"></script> -->
<script src="/assets/js/gMaps.js"></script>
<script src="/assets/js/init.js"></script>
</body>
</html>
