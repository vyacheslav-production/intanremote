<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); ?>
<div class="personal_filter_wrap">
    <script type="text/javascript">
        $(function(){
            var insideFilter = false
            $(".filter_buble_wrap").hover(function(){
                insideFilter = true
            }, function(){
                insideFilter = false
            })
            $("body").mouseup(function(e){
                if($(".filter_buble_wrap").is(":visible")){
                    if(e.target.id!="filter_by_center"&&e.target.id!="filter_by_spec"){
                        if(! insideFilter){
                            $(".filter_buble_wrap").hide()
                            $("#filter_by_center").parent().removeClass("active")
                            $("#filter_by_spec").parent().removeClass("active")
                        } 
                    }
                }
            })
            $("#filter_by_spec, #filter_by_center").click(function(){
                var t = $(this);
                function check(t){
                    if(t.next().is(":visible")){
                        t.next().hide()
                        t.parent().removeClass("active")
                    }else{
                        t.next().show()
                        t.parent().addClass("active")
                    }
                }
                switch(t.attr("id")){
                    case "filter_by_spec":
                        $("#filter_by_center").next().hide()
                        $("#filter_by_center").parent().removeClass("active")
                        check(t)
                        break
                    case "filter_by_center":
                        $("#filter_by_spec").next().hide()
                        $("#filter_by_spec").parent().removeClass("active")
                        check(t)
                        break
                }
            })
            //            фильтр по центрам
            $(".per_by_center .filter_buble_inner p span").click(function(){
                var arrCenters = ["35", "34", "36", "33", "37", "3400", "41", "38", "7799", '10055', '10056' , '10057', '68784'];
                document.location.href = "./?arrFilter_pf%5Bbloger_position%5D=&arrFilter_pf%5Bbloger_clinic%5D="+arrCenters[$(this).parent().index()]+"&set_filter=Фильтр&set_filter=Y"
            })
            $(".per_by_spec .filter_buble_inner p span").click(function(){
                document.location.href = "./?arrFilter_pf%5Bbloger_position%5D="+$(this).text()+"&arrFilter_pf%5Bbloger_clinic%5D=&set_filter=Фильтр&set_filter=Y"
            });
            //            if filter is set do some stuff
            (function(){
                var position = "<?= $_GET["arrFilter_pf"]["bloger_position"] ?>"
                var clinic = "<?= $_GET["arrFilter_pf"]["bloger_clinic"] ?>"
                if(position!=""||clinic!=""){
                    $("#personal_filter_reset").show()
                    
                    switch(clinic){
                        case "35":
                            clinic = "Центр на проспекте Стачек"
                            break;
                        case "34":
                            clinic = "Центр на Комендантском проспекте"
                            break;
                        case "36":
                            clinic = "Центр на Лиговском проспекте"
                            break;
                        case "33":
                            clinic = "Центр на проспекте Просвещения"
                            break;
                        case "37":
                            clinic = "Центр на улице Будапештской"
                            break;
                        case "3400":
                            clinic = "Центр на Вознесенском проспекте"
                            break;
                        case "39":
                            clinic = "Центр на Бухарестской улице"
                            break;
                        case "41":
                            clinic = "Центр на Российском проспекте"
                            break;
                        case "38":
                            clinic = "Центр на Большой Пушкарской"
                            break;
						case "7799":
                            clinic = "Центр на проспекте Луначарского"
                            break;
						case "10055":
                            clinic = "Центр на проспекте Славы/Бухарестская"
                            break;
						case "10056":
                            clinic = "Центр на Богатырском проспекте"
                            break;
						case "10057":
                            clinic = "Центр на Комендантском проспекте"
                            break;
                        case "68784":
                            clinic = "Центр на Ленинградской улице"
                            break;
                            
                    }
                    var text = (clinic!=""?clinic:"Специализация "+position)
                    $("#personal_changing_h").text(text)
                }
            })()
        })
    </script>
    <div class="per_by_center">
        <span class="iecor tl"></span>
        <span class="iecor tr"></span>
        <span class="iecor bl"></span>
        <span class="iecor br"></span>
        <span id="filter_by_center">Центр</span>
        <div class="filter_buble_wrap">
            <div class="filter_buble_top"></div>
            <div class="filter_buble_inner">
                <p><span>пр. Стачек, д. 69 &laquo;А&raquo;</span></p>
                <p><span>пр. Комендантский, д. 7, к.1</span></p>
                <p><span>пр. Лиговский, д. 125</span></p>
                <p><span>пр. Просвещения, д. 87, к.2</span></p>
                <p><span>ул. Будапештская, д. 97, к.2</span></p>
                <p><span>пр. Вознесенский, д. 21</span></p>
                <p><span>пр. Российский, д. 8</span></p> 
                <p><span>ул. Б. Пушкарская, д. 41</span></p>
                <p><span>пр. Луначарского, д. 11, к. 3</span></p>
                <p><span>пр. Славы/Бухарестская, д. 96</span></p>
                <p><span>пр. Богатырский, д. 51, к. 1</span></p>
                <p><span>пр. Комендантский, д. 42, к. 1</span></p>
                <p><span>ул. Ленинградская, д. 3</span></p>
            </div>
            <div class="filter_buble_bottom"></div>
        </div>
    </div>
    <div class="per_fil_sep"></div>
    <div class="per_by_spec">
        <span class="iecor tl"></span>
        <span class="iecor tr"></span>
        <span class="iecor bl"></span>
        <span class="iecor br"></span>
        <span id="filter_by_spec">Специализация</span>
        <div class="filter_buble_wrap">
            <div class="filter_buble_top"></div>
            <div class="filter_buble_inner">
                <p><span>Ортопед</span></p>
                <p><span>Терапевт</span></p>
                <p><span>Хирург</span></p>
                <p><span>Имплантолог</span></p>
                <p><span>Пародонтолог</span></p>
                <p><span>Ортодонт</span></p>
            </div>
            <div class="filter_buble_bottom"></div>
        </div>
    </div>
    <a href="/personal/" id="personal_filter_reset">Сбросить фильтр</a>
    <div class="clear"></div>
</div>
<h2 id="personal_changing_h">Все специалисты &laquo;Интан&raquo;</h2>
<div class="personel_list">
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br />
    <? endif; ?>
    <?
    $itemCnt = 0;
    foreach ($arResult["ITEMS"] as $arItem):
        ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <?
        if ($itemCnt > 2) {
            (($itemCnt + 1) % 4 == 0) ? ($itemClass = " last") : ((($itemCnt) % 4 == 0) ? ($itemClass = " clear") : ($itemClass = ""));
        }
        ?>
        <div class="personal_list_item<?= $itemClass ?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><div class="personal_list_img"><img class="preview_picture" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" /></div></a>
                <? else: ?>
                    <div class="personal_list_img"><img class="preview_picture" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" /></div>
                <? endif; ?>
            <? endif ?>
            <? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]): ?>
                <span class="news-date-time"><? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
            <? endif ?>
            <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                    <p class="personal_item_name">
                        <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
                    </p>
                    <p class="personal_item_post" id="post_<?= $arItem['ID'] ?>">
                        <? //foreach ($arItem["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
                        <? //if (is_array($arProperty["DISPLAY_VALUE"])): ?>
                        <? //= implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]); ?>
                        <? // else: ?>
                        <? //= $arProperty["DISPLAY_VALUE"]; ?>
                        <? // endif ?>
                        <? // endforeach; ?>
                        <?= $arItem['PROPERTIES']['bloger_position']['VALUE'] ?>
                    </p>
                    <p id="clinic_<?= $arItem['ID'] ?>" style="display:none;">
                        <?= $arItem['DISPLAY_PROPERTIES']['bloger_clinic']['DISPLAY_VALUE'] ?>
                    </p>
                <? else: ?>
                    <p class="personal_name"><? echo $arItem["NAME"] ?></p>
                <? endif; ?>
            <? endif; ?>		
            <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                <div style="clear:both"></div>
            <? endif ?>
            <? foreach ($arItem["FIELDS"] as $code => $value): ?>
                <small>
                    <?= GetMessage("IBLOCK_FIELD_" . $code) ?>:&nbsp;<?= $value; ?>
                </small><br />
            <? endforeach; ?>

        </div>

        <?
        $itemCnt++;
    endforeach;
    ?>
    <div class="clear"></div>

    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br /><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>
