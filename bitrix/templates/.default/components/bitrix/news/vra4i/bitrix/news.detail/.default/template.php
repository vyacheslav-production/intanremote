<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die(); ?>
<div class="back_to_list">
    <span class="iecor tl"></span>
    <span class="iecor tr"></span>
    <span class="iecor bl"></span>
    <span class="iecor br"></span>
    <span class="left_arrow"></span>
    <a href="/personal/">Вернуться к списку</a>
</div>
<? $APPLICATION->AddChainItem($arResult["NAME"]); ?><div class="personal_silgle">
<? if ($arParams["DISPLAY_NAME"] != "N" && $arResult["NAME"] || true): ?>
        <h2><?= $arResult["NAME"] ?></h2>
    <? endif; ?>
    <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])): ?>
        <div class="personal_silgle_img"><img class="detail_picture" border="0" src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>" height="<?= $arResult["DETAIL_PICTURE"]["HEIGHT"] ?>" alt="<?= $arResult["NAME"] ?>"  title="<?= $arResult["NAME"] ?>" /></div>
    <? endif ?>
    <div class="personal_silgle_text">
        <? if ($arParams["DISPLAY_DATE"] != "N" && $arResult["DISPLAY_ACTIVE_FROM"]): ?>
            <span class="news-date-time"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span>
        <? endif; ?>

            
            
        <p class="personal_silgle_h"><?=$arResult["DISPLAY_PROPERTIES"]["bloger_position"]["NAME"]?></p>
        <div><?=$arResult["DISPLAY_PROPERTIES"]["bloger_position"]["VALUE"]?></div>
        <p class="personal_silgle_h"><?=$arResult["DISPLAY_PROPERTIES"]["biografiya"]["NAME"]?></p>
        <div>
            <?=$arResult["DISPLAY_PROPERTIES"]["biografiya"]["DISPLAY_VALUE"]?>
        </div>
        <p class="personal_silgle_h"><?=$arResult["DISPLAY_PROPERTIES"]["sovet"]["NAME"]?></p>
        <p class="personal_advice"><?=$arResult["DISPLAY_PROPERTIES"]["sovet"]["VALUE"]?></p>

        <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arResult["FIELDS"]["PREVIEW_TEXT"]): ?>
            <p><?= $arResult["FIELDS"]["PREVIEW_TEXT"];
        unset($arResult["FIELDS"]["PREVIEW_TEXT"]); ?></p>
        <? endif; ?>
        <? if ($arResult["NAV_RESULT"]): ?>
                <? if ($arParams["DISPLAY_TOP_PAGER"]): ?><?= $arResult["NAV_STRING"] ?><br /><? endif; ?>
                <? echo $arResult["NAV_TEXT"]; ?>
            <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?><br /><?= $arResult["NAV_STRING"] ?><? endif; ?>
        <? elseif (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
            <p class="personal_silgle_h"><? echo $arResult["DETAIL_TEXT"]; ?></p>
        <? else: ?>
            <? echo $arResult["PREVIEW_TEXT"]; ?>
        <? endif ?>
        <? foreach ($arResult["FIELDS"] as $code => $value): ?>
            <?= GetMessage("IBLOCK_FIELD_" . $code) ?>:&nbsp;<?= $value; ?>
            <br />
        <? endforeach; ?>
        <?
        if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y") {
            ?>
            <div class="news-detail-share">
                <noindex>
                    <?
                    $APPLICATION->IncludeComponent("bitrix:main.share", "", array(
                        "HANDLERS" => $arParams["SHARE_HANDLERS"],
                        "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                        "PAGE_TITLE" => $arResult["~NAME"],
                        "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                        "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                        "HIDE" => $arParams["SHARE_HIDE"],
                            ), $component, array("HIDE_ICONS" => "Y")
                    );
                    ?>
                </noindex>
            </div>
            <?
        }
        ?></div>
    <div class="clear"></div>
</div>
