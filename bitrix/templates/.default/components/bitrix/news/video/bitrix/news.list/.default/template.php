<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)die(); ?>

<?
	CModule::IncludeModule("iblock");

	$elementdb = CIBlockElement::GetList(Array("created" => "DESC"), Array("IBLOCK_ID" => "19"), false, false, Array());
	if ($element = $elementdb->GetNextElement()) {
		$prpts = $element->GetProperties();
		$element = $element->GetFields();
	}
?>

<pre><?// print_r($prpts['video']['VALUE']['TEXT']) ?></pre> 

<? $a = 1; ?>

	<?
	function getVidId($str){
		$ps = strpos($str, "youtube.com/embed/");
		$ps = $ps + 18;
		$vId = "";
		for($i = $ps, $j = strlen($str);$i < $j;$i++){
			if(substr($str, $i, 1)!="?"){
				$vId.= substr($str, $i, 1);
			}else{
				break;
			}
		}
		return $vId;
	}
	?>

<div class="video_left">
	<div class="cat_video_wrap">
		<div class="cat_video">
			<?= $prpts['video']['~VALUE']['TEXT'] ?>
		</div>
	</div>
	<div class="video_list">
		<?$APPLICATION->IncludeComponent("bitrix:news.list", "video", array(
			"IBLOCK_TYPE" => "-",
			"IBLOCK_ID" => "19",
			"NEWS_COUNT" => "20",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_ORDER1" => "DESC",
			"SORT_BY2" => "SORT",
			"SORT_ORDER2" => "ASC",
			"FILTER_NAME" => "",
			"FIELD_CODE" => array(
				0 => "",
				1 => "",
			),
			"PROPERTY_CODE" => array(
				0 => "video",
				1 => "",
			),
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"SET_TITLE" => "Y",
			"SET_STATUS_404" => "N",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"ADD_SECTIONS_CHAIN" => "Y",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"PAGER_TITLE" => "Новости",
			"PAGER_SHOW_ALWAYS" => "Y",
			"PAGER_TEMPLATE" => "legion",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "Y",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"AJAX_OPTION_ADDITIONAL" => ""
			),
			false
		);?> 
	</div>
</div>
<article class="video_right">
	<h1><?= $element['NAME'] ?></h1>
	<p><?= $element['DETAIL_TEXT'] ?> </p>
</article>
<div class="clear"></div>

