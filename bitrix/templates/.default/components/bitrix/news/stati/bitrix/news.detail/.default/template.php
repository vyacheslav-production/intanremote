<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
$APPLICATION->SetTitle($arResult["NAME"]);
$APPLICATION->AddChainItem($APPLICATION->GetTitle());
?>
<article>
    <h1 class="main_h"><?= $arResult["NAME"] ?></h1>
    <p class="posted_date"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></p>
    <div class="single_art">
        <div class="single_art_img">
            <?
            $picKey;
            if (!empty($arResult["DETAIL_PICTURE"])):
                $picKey = "DETAIL_PICTURE";
                ?>				
                <img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"<?php if($arResult["DETAIL_PICTURE"]["DESCRIPTION"]) :?>alt="<?= $arResult["DETAIL_PICTURE"]["DESCRIPTION"] ?>"  title="<?= $arResult["DETAIL_PICTURE"]["DESCRIPTION"] ?>"<?php else:?> alt="<?= $arResult["NAME"] ?>"  title="<?= $arResult["NAME"] ?>"<?php endif; ?> />
            <? else:$picKey = "PREVIEW_PICTURE"; ?>
                <img src="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>"<?php if($arResult["PREVIEW_PICTURE"]["DESCRIPTION"]) :?>alt="<?= $arResult["PREVIEW_PICTURE"]["DESCRIPTION"] ?>"  title="<?= $arResult["PREVIEW_PICTURE"]["DESCRIPTION"] ?>"<?php else:?> alt="<?= $arResult["NAME"] ?>"  title="<?= $arResult["NAME"] ?>"<?php endif; ?> />
            <? endif; ?>
            <? if (($arResult[$picKey]["DESCRIPTION"]) && ($arResult[$picKey]["DESCRIPTION"] != $arResult["NAME"])): ?>
                <p><?php echo $arResult[$picKey]["DESCRIPTION"] ?></p>
<? endif; ?>
        </div>
        <div class="clear"></div>
        <div class="single_art_body">
<? echo $arResult["DETAIL_TEXT"]; ?>
        </div>
    </div>



</article>

