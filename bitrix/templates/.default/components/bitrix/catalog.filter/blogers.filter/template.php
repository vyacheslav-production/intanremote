<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form id="arrFilter_form" name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
	<?foreach($arResult["ITEMS"] as $arItem):
		if(array_key_exists("HIDDEN", $arItem)):
			echo $arItem["INPUT"];
		endif;
	endforeach;?>
	  <div id="filter-all">
	  <table cellpadding="4" cellspacing="0">
	  	<tr><td><p class="font20">Фильтровать по:</p>   </td>
	  	<td><div class="filter-choice"><a id="filter-clinic" href="#1">центрам</a><a id="filter-post" href="#1">специализации</a><a id="filter-blog" href="#1">блогам</a></div></td>
	  	</tr>
	  </table>
		<div id="list_clinic">
		<?foreach($arResult["Clinic"] as $id=>$arClinic):?>
			<p><a href="#1" class="arrFilter" id="<?=$id?>" ><?=$arClinic;?></a></p>
		<?endforeach;?>
		</div>
		<div id="list_post">
			<p><a href="#1" class="arrFilterPost" >Врач-стоматолог-терапевт</a></p>
			<p><a href="#2" class="arrFilterPost" >Врач-стоматолог-ортопед</a></p>
		</div>
		<script type="text/javascript">
		     $('.arrFilter').click(function(){
				$('#arrFilter_clinic').val(this.id);
				$('#arrFilter_form').submit();
			});
			$('.arrFilterPost').click(function(){
				$('#arrFilter_post').val($(this).text());
				$('#arrFilter_form').submit();
			});
			$('#filter-clinic').click(function(){
				$('#list_post').css('display','none');
				$('#list_clinic').css('display','block');
			});
			$('#filter-post').click(function(){
				$('#list_clinic').css('display','none');
				$('#list_post').css('display','block');
			});
			$('#filter-blog').click(function(){
				$('#arrFilter_clinic').val();
				$('#arrFilter_post').val();
				$('#arrFilter_form').submit();
			});
		</script>
		<input type="hidden" id="arrFilter_clinic" name="arrFilter_pf[bloger_clinic]" size="20" value="" />
		<input type="hidden" id="arrFilter_post" name="arrFilter_pf[bloger_position]" size="20" value="" />
		<input type="hidden" name="arrFilter_pf[bloger_blog_code]" size="20" value="" />
		<a name="#1"></a>
       </div>
        <div id="hidden-submit">
				<input type="submit" name="set_filter" value="<?=GetMessage("IBLOCK_SET_FILTER")?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("IBLOCK_DEL_FILTER")?>" /></td>
        </div>
</form>

