<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
?>

<? if (count($arResult["ERRORS"])): ?>
    <?= ShowError(implode("<br />", $arResult["ERRORS"])) ?>
<? endif ?>
<? if (strlen($arResult["MESSAGE"]) > 0): ?>
    <?= ShowNote($arResult["MESSAGE"]) ?>
<? endif ?>
<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data" class="vakansii">

    <?= bitrix_sessid_post() ?>

    <? if ($arParams["MAX_FILE_SIZE"] > 0): ?><input type="hidden" name="MAX_FILE_SIZE" value="<?= $arParams["MAX_FILE_SIZE"] ?>" /><? endif ?>

    <div class="left">
        <p>Имя:</p>
        <p>Email:</p>
        <p>Телефон:</p>
        <p>Специализация:</p>
        <p>Прикрепить резюме:</p>
    </div>
    <div class="fields">
        <p><input type="text" name="PROPERTY[NAME][0]" style="width:330px;height:25px;padding:0 10px;" /></p>
        <p><input type="text" name="PROPERTY[48][0]" style="width:330px;height:25px;padding:0 10px;" /></p>
        <p><input type="text" name="PROPERTY[49][0]" style="width:330px;height:25px;padding:0 10px;" /></p>
        <div class="select_wrap">
            <select name="PROPERTY[50]" style="width:200px;height:20px;">
                <option value="19">Хирург-имплантолог</option>
                <option value="67">Зубной техник (керамист)</option>
                <option value="66">Ассистент стоматолога</option>
                <option value="68">Терапевт-пародонтолог</option>
                <option value="17">Стоматолог</option>
                <option value="18">Парадонтолог</option>
            </select>
        </div>
        <div class="addfile">
            <input type="hidden" name="PROPERTY[51][0]" value="" />
            <input type="file" name="PROPERTY_FILE_51_0" />
        </div>
        <input type="submit" name="iblock_submit" value="Отправить" />
    </div>
</form>