<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '<div class="nstring"><a href="/" title="Стоматология(СПб) Интан">Главная</a> ';

for($index = 1, $itemSize = count($arResult); $index < $itemSize; $index++)
{
	$strReturn .= '<span>/</span> ';

	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($arResult[$index]["LINK"] <> ""&&$index<(count($arResult)-1))
		$strReturn .= '<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a> ';
	else
		$strReturn .= $title;
}

$strReturn .= '</div>';
return $strReturn;
?>
