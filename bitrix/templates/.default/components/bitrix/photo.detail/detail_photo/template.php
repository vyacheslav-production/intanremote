<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="photo-detail">
<table cellspacing="0" cellpadding="0" border="0" width="100%" class="data-table">
<tr>
	<td colspan="5" align="center">
		<?if(is_array($arResult["PICTURE"])):?>
			<img border="0" src="<?=$arResult["PICTURE"]["SRC"]?>" width="<?=$arResult["PICTURE"]["WIDTH"]?>" height="<?=$arResult["PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["PICTURE"]["ALT"]?>" /><br />
		<?endif?>
	</td>
</tr>
<?if(count($arParams["FIELD_CODE"])>0 || count($arResult["DISPLAY_PROPERTIES"])>0):?>
<tr>
	<th colspan="5">
		<?foreach($arParams["FIELD_CODE"] as $code):?>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>&nbsp;:&nbsp;<?=$arResult[$code]?><br />
		<?endforeach?>
		<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<?=$arProperty["NAME"]?>:&nbsp;<?
			if(is_array($arProperty["DISPLAY_VALUE"]))
				echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
			else
				echo $arProperty["DISPLAY_VALUE"];?><br />
		<?endforeach?>
	</th>
</tr>
<?endif?>



</table>
<p>
	<a href="/personal/"><?=GetMessage("PHOTO_BACK")?></a>
</p>
</div>
