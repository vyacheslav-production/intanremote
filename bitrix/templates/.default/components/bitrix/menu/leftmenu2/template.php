<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if (!empty($arResult)){
		$n_tm_items = 0;
		foreach($arResult as $arItem){
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;
		$n_tm_items++;
		if($arItem["SELECTED"]){ ?>
			<div class="left_menu_item_active">
            	<div class="left_menu_active"><?=$arItem["TEXT"]?></div>
            </div>
		<? }else{ ?>
        	<div class="left_menu_item"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
		<? }
		}
	} ?>