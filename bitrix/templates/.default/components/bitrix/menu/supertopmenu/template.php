<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="top_menu">
<? if (!empty($arResult)){
		$n_tm_items = 0;
		foreach($arResult as $arItem){
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;
		$n_tm_items++;
		if($arItem["SELECTED"]){ ?>
    		<a class="top_menu_active" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		<? }else{ ?>
			<a class="top_menu" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		<? }
		}
	} ?>
</div>