<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="undertop1">
	<div class="cmenu">
<? if (!empty($arResult)){
		$n_tm_items = 0;
		foreach($arResult as $arItem){
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;
		$n_tm_items++;
		if($arItem["SELECTED"]){ ?>
    		<div class="cmenu_item cmenu_item_active"><img src="<?=SITE_TEMPLATE_PATH?>/images/cm_menu<?=$n_tm_items ?>.png" border="0" class="cmenu" /><a href="<?=$arItem["LINK"]?>" class="cmenu"><?=$arItem["TEXT"]?></a></div>
		<? }else{ ?>
			<div class="cmenu_item"><img src="<?=SITE_TEMPLATE_PATH?>/images/cm_menu<?=$n_tm_items ?>.png" border="0" class="cmenu" /><a href="<?=$arItem["LINK"]?>" class="cmenu"><?=$arItem["TEXT"]?></a></div>
		<? }
		}
	} ?>
		<div class="br"></div>
    </div>
</div>