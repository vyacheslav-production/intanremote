<section class="kinds_audit">
    <h2 class="st_h">Виды аудита</h2>
    <?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<?if (!empty($arResult)):?>
	<nav class="kinds_audit_list">
	<ul>
	<?
	foreach($arResult as $arItem):
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
			continue;
	?>
	<?if($arItem["SELECTED"]):?>
		<li><a class="active" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
	<?endforeach?>

	</ul></nav>
	<?endif?>
</section>