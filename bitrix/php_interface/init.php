<?

/*
  You can place here your functions and event handlers

  AddEventHandler("module", "EventName", "FunctionName");
  function FunctionName(params)
  {
  //code
  }
 */
 
 
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/log.txt");

AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("RalegionIntanAfterIBlockElementAddClass", "OnAfterIBlockElementAddHandler"));


// Проверка на мобильный
/*
function is_mobile() {
 $response = file_get_contents('http://phd.yandex.net/detect?user-agent='.urlencode($_SERVER['HTTP_USER_AGENT']));
 if (strstr($response, "<yandex-mobile-info>")) {
         return true;
     }
    return false;
}


if (!isset($_COOKIE['mob'])) {
    if (is_mobile()) {
            SetCookie("mob", 'true');
            // редирект
            header("Location: /mobile/");
    } else {
            SetCookie("mob", 'false');
    }
} */

class RalegionIntanAfterIBlockElementAddClass {

    function OnAfterIBlockElementAddHandler(&$arFields) {
        //массив с адресами e-mail
        $arrMail = array(
            '«Интан» на Будапештской 97' => '4535960@mail.ru',
            '«Интан» на Комендантском пр. 7, к.1' => 'intan2reg@mail.ru',
            '«Интан» на Лиговском пр. 125' => '5723318@mail.ru',
            '«Интан» на пр. Просвещения 87, к.2' => 'intan4@yandex.ru',
            '«Интан» на Бухарестской 96' => '3615393@mail.ru',
            '«Интан» на Российском пр. 8' => 'intan-8reg@mail.ru',
            '«Интан» на пр. Стачек 69А' => 'intanreg@mail.ru',
            '«Интан» на Большой Пушкарской 41' => '2336842@mail.ru',
            '«Интан» на Вознесенском 21' => 'Intan-10@mail.ru',
            '«Интан» на Луначарского пр., дом 11' => 'intan11@yandex.ru',
            '«Интан» на Комендантский пр. 42, к. 1' => 'reg13@intan.ru'
        );


        if ($arFields["IBLOCK_ID"] == 16) {
            $arIBlockElement = GetIBlockElement($arFields["ID"]);

            $arSend = array('TEXT' =>
                '<br /><br />Имя: ' . $arFields['NAME'] . '<br />Отчество: ' . $arIBlockElement['PROPERTIES']['otchestvo']['VALUE'] . '<br />Дата рождения: ' . $arIBlockElement['PROPERTIES']['data_rozhdeniya']['VALUE'] . '<br />В какой клинике лечились ранее: ' . $arIBlockElement['PROPERTIES']['klinika_ranee']['VALUE'] . '<br />Контактный телефон: ' . $arIBlockElement['PROPERTIES']['kontakt_phone']['VALUE'] . '<br />Удобное время звонка: ' . $arIBlockElement['PROPERTIES']['vremya_zvonka']['VALUE'] . '<br />E-mail: ' . $arIBlockElement['PROPERTIES']['e_mail']['VALUE'] . '<br />Предпочтительная дата визита: ' . $arIBlockElement['PROPERTIES']['predpocht_data']['VALUE'] . '<br />Услуга: ' . $arIBlockElement['PROPERTIES']['usluga']['VALUE'] . '<br />Удобное время визита: ' . $arIBlockElement['PROPERTIES']['vremya_vizita']['VALUE'] . '<br />Выбранный Вами центр: ' . $arIBlockElement['PROPERTIES']['tsentr']['VALUE'],
                'EMAILSEND' =>$arrMail[$arIBlockElement['PROPERTIES']['tsentr']['VALUE']]);


            CEvent::Send('ZAYAVKA', SITE_ID, $arSend);
            
        } else if ($arFields["IBLOCK_ID"] == 17) {
            $arIBlockElement = GetIBlockElement($arFields["ID"]);

            $arSend = array(
                'NAME' => $arFields['NAME'],
                'EMAIL' => $arIBlockElement['PROPERTIES']['e_mail']['VALUE'],
                'YSLUGI' => $arIBlockElement['PROPERTIES']['uslugi']['VALUE'],
                'DRYGOE' => $arIBlockElement['PROPERTIES']['drugoe']['VALUE']
            );



            CEvent::Send('STOIMOST', SITE_ID, $arSend);
        } else if ($arFields["ID"] > 0 && $arFields["IBLOCK_ID"] == 9) {
            CModule::IncludeModule("iblock");
            /* AddMessage2Log("Запись с кодом ".$arFields["ID"]." добавлена.");
              foreach($arFields as $k=>$v){
              AddMessage2Log($k." __________ ".$v);
              }
              foreach($arFields["PROPERTY_VALUES"] as $k=>$v){
              AddMessage2Log($k." ____---___ ".$v);
              }
              foreach($arFields["PROPERTY_VALUES"][17] as $k=>$v){
              AddMessage2Log($k." ____-17-___ ".$v);
              }
              foreach($arFields["PROPERTY_VALUES"][23] as $k=>$v){
              AddMessage2Log($k." ____-23-___ ".$v);
              }
              foreach($arFields["PROPERTY_VALUES"][24] as $k=>$v){
              AddMessage2Log($k." ____-24-___ ".$v);
              } */

            $klinika = "";
            $klinika_result = CIBlockElement::GetByID($arFields["PROPERTY_VALUES"][23]["VALUE"]);
            if ($klinika_string = $klinika_result->GetNext()) {
                $klinika = $klinika_string["NAME"];
            }
            $uslugi = "";
            if ($uslugi_string = CIBlockPropertyEnum::GetByID($arFields["PROPERTY_VALUES"][22])) {
                $uslugi = $uslugi_string["VALUE"];
            }
            $v_zvonka = "";
            if ($v_zvonka_string = CIBlockPropertyEnum::GetByID($arFields["PROPERTY_VALUES"][20])) {
                $v_zvonka = $v_zvonka_string["VALUE"];
            }
            $v_vizita = "";
            if ($v_vizita_string = CIBlockPropertyEnum::GetByID($arFields["PROPERTY_VALUES"][25])) {
                $v_vizita = $v_vizita_string["VALUE"];
            }
            $arFields1 = array(
                "FAMILIYA" => $arFields["PROPERTY_VALUES"][14],
                "IMYA" => $arFields["PROPERTY_VALUES"][15],
                "OTCHESTVO" => $arFields["PROPERTY_VALUES"][16],
                "DATA_ROJDENIYA" => $arFields["PROPERTY_VALUES"][17]["VALUE"],
                "GDE_LECHILIS" => $arFields["PROPERTY_VALUES"][18],
                "TELEFON" => $arFields["PROPERTY_VALUES"][19],
                "VREMYA_ZVONKA" => $v_zvonka,
                "EMAIL" => $arFields["PROPERTY_VALUES"][21],
                "USLUGI" => $uslugi,
                "KLINIKA" => $klinika,
                "DATA_VIZITA" => $arFields["PROPERTY_VALUES"][24]["VALUE"],
                "VREMYA_VIZITA" => $v_vizita
            );
            CEvent::Send("MEV_ZAYAVKA", array('s1'), $arFields1);
        }
        if ($arFields["ID"] > 0 && $arFields["IBLOCK_ID"] == 10) {
            CModule::IncludeModule("iblock");

            $elementdb = CIBlockElement::GetList(Array("created" => "DESC"), Array("IBLOCK_ID" => "10"), false, false, Array());
            if ($element = $elementdb->GetNextElement()) {
                $prpts = $element->GetProperties();
                $element = $element->GetFields();
            }
            $idotz = $element["ID"];

            $arFields1 = array(
                "FIO" => $arFields["NAME"],
                "EMAIL" => $arFields["PROPERTY_VALUES"][27],
                "ACTIV" => "http://intan.ru/bitrix/admin/iblock_element_edit.php?WF=Y&ID=" . $idotz . "&type=on_line&lang=ru&IBLOCK_ID=10&find_section_section=0",
                "OTZYV" => $arFields["PROPERTY_VALUES"][28]
            );
            CEvent::Send("MEV_OTZYV", array('s1'), $arFields1);
        }
        if ($arFields["ID"] > 0 && $arFields["IBLOCK_ID"] == 4) {
            CModule::IncludeModule("iblock");
            $arIBlockElement = GetIBlockElement($arFields["ID"]);
            $arFields1 = array(
                "DOLGNOST" => $arIBlockElement['PROPERTIES']['dolgnost']['VALUE'],
                "EMAIL" => $arFields["PROPERTY_VALUES"][48],
                "TELEFON" => $arFields["PROPERTY_VALUES"][49],
                "IMYA" => $arFields["NAME"],
                "FILE" => CFile::GetPath($arIBlockElement['PROPERTIES']['rezyme']['VALUE'])
            );
            CEvent::Send("REZYME", array('s1'), $arFields1, 11);
        }

        if ($arFields["ID"] > 0 && $arFields["IBLOCK_ID"] == 27) {
            CModule::IncludeModule("iblock");

            $elementdb = CIBlockElement::GetList(Array("created" => "DESC"), Array("IBLOCK_ID" => "27"), false, false, Array());
            if ($element = $elementdb->GetNextElement()) {
                $prpts = $element->GetProperties();
                $element = $element->GetFields();
            }

            $time = $prpts["time"]["VALUE"];
            $klinika = $prpts["klinika"]["VALUE"];

            $arFields1 = array(
                "NAME" => $arFields['NAME'],
                "PHONE" => $arFields["PROPERTY_VALUES"][99],
                'EMAILSEND' => $arrMail[$arIBlockElement['PROPERTIES']['klinika']['VALUE']],
                "TIME" => $time,
                "KLINIKA" => $klinika
            );
            CEvent::Send("ZVONOK", array('s1'), $arFields1);
        }
        //отправить заявку на кредит
        if ($arFields['ID'] > 0 && $arFields['IBLOCK_ID'] == 28) {
            CModule::IncludeModule("iblock");
            $res = CIBlockElement::GetList(Array("created" => "DESC"), Array("IBLOCK_ID" => '28'), false, false, Array());
            if ($ob = $res->GetNextElement()) {
                $arrC = array(
                    'props' => $ob->GetProperties(),
                    'fields' => $ob->GetFields()
                );
            }
            if ($arrC['props']['107']['VALUE'] == 0) {
                $clinic = 'Любая';
            } else {
                $res = CIBlockElement::GetByID(intval($arrC['props']['107']['VALUE']));
                if ($ar_res = $res->GetNext()) {
                    $clinic = $ar_res['NAME'];
                }
            }
            $arMail = array(
                'NAME' => $arrC['fields']['NAME'],
                'LAST_NAME' => $arrC['props']['102']['VALUE'],
                'MID_NAME' => $arrC['props']['104']['VALUE'],
                'PHONE' => $arrC['props']['105']['VALUE'],
                'SUM' => $arrC['props']['106']['VALUE'],
                'INTAN' => $clinic
            );
            CEvent::Send('CREDIT', array('s1'), $arMail);
        }

        /*
          if ($arFields["IBLOCK_ID"] == 4)

          { $arIBlockElement = GetIBlockElement($arFields["ID"]);

          $arSend = array('TEXT' =>

          '<br /><br />Имя: '.$arFields['NAME'].'<br />E-mail: '.$arIBlockElement['PROPERTIES']['email']['VALUE'].'<br />Телефон: '.$arIBlockElement['PROPERTIES']['telefon']['VALUE'].'<br />Должность: '.$arIBlockElement['PROPERTIES']['dolgnost']['VALUE']);


          CEvent::Send('REZYME',SITE_ID,$arSend);

          }
         */




        /* WEBAUDITPRO */

        if ($arFields["ID"] > 0 && $arFields["IBLOCK_ID"] == 21) {
            CModule::IncludeModule("iblock");
            //$arIBlockElement = GetIBlockElement($arFields["ID"]);
            $element = CIBlockElement::GetList(Array("created" => "DESC"), Array("IBLOCK_ID" => "21"), false, false, Array("PROPERTY_kind", "PROPERTY_service"))->Fetch();
            $arFields1 = array(
                "IMYA" => $arFields["NAME"],
                "EMAIL" => $arFields["PROPERTY_VALUES"][78],
                "TELEFON" => $arFields["PROPERTY_VALUES"][77],
                "COMPANY" => $arFields["PROPERTY_VALUES"][79],
                "YSLUGA" => $element['PROPERTY_SERVICE_VALUE'],
                "TIP" => $element['PROPERTY_KIND_VALUE'],
                "PRIME4ANIYA" => $arFields["PROPERTY_VALUES"][82]
            );
            CEvent::Send("WebAudit Zayavka", array('s2'), $arFields1);
        }
    }

}

?>
