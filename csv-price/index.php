<?
if (isset($_REQUEST['work_start']))
{
	define("NO_AGENT_STATISTIC", true);
	define("NO_KEEP_STATISTIC", true);
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm("Доступ запрещен");

$listCity = getCities();

$listService = [];
$arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listmedservices']];
$arrFilter = getFilArrayByCity($arrFilter);
$res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME', 'DETAIL_PAGE_URL'));
while($ar_res = $res->GetNext()){
	$listService[] = $ar_res;
}
$status = '';
if($_REQUEST['work_start'] && !empty($_FILES["file"]['tmp_name']) && check_bitrix_sessid())
{
	$strNest = [];
	$handle = fopen($_FILES["file"]['tmp_name'], "r+");
	$arr = [];
	while($data = &fgetcsv($handle, 1000, ";") !== FALSE) {
		
		foreach ($data as &$val) {
			$val = iconv("Windows-1251", 'UTF-8', $val);
		}

		//нужная нам строка начинается с цифры
		if(intval($data[0][0])){
			//отрезаем подстроку, содержащую цифры и точки
			preg_match('/([0-9\.]+)/', $data[0], $matches);
			//если отрезанная подстрока содержит точку, то это подзаголовок, иначе элемент
			if(stristr($matches[0], '.')){

				//убираем из строки с названием заголовка нумерацию
				$data[1] = substr($data[0], strlen($matches[0]));

				$strNest = explode('.', $matches[0]);
				$strNest = array_diff($strNest, array(''));

			}

			if(ctype_digit($data[0])){
				$id = $data[0];
				$parent_id = implode('', $strNest);
			}else{
				$id = implode('', $strNest);
				$parent_id = substr(implode('', $strNest), 0, -1) ? substr(implode('', $strNest), 0, -1) : '0';
			}
			$name = $data[1];

			$arr[mt_rand()] = [
				'id' => $id,
				'parent_id' => $parent_id,
				'name' => $name,
				'value' => str_replace(",00", "", $data[4])
			];
		}

	}

	$content = '';
	$elemTree = buildTree($arr);
	$htmlContent = htmlTree($elemTree);
	$htmlContent = sprintf('<ul class="list-price">%s</ul>', $htmlContent);
	$res = CIBlockElement::GetList(array(), array('IBLOCK_ID'=>$arIblockAccord['listprices'], 'PROPERTY_F_BINDCITY'=>(int)$_REQUEST['city_select'], 'PROPERTY_F_BINDSERV'=>(int)$_REQUEST['serv_select']), false, false);
	if($ar_res = $res->fetch()){

		$el = new CIBlockElement;

		$arLoadProductArray = Array(
			"DETAIL_TEXT"    => $htmlContent,
		);

		$PRODUCT_ID = $ar_res['ID'];  // изменяем элемент с кодом (ID) 2
		$res = $el->Update($PRODUCT_ID, $arLoadProductArray);
		if($res)
			$status = 'Элемент успешно обновлен';
	}else{
		$el = new CIBlockElement;

		$PROP = array();
		$PROP['F_BINDCITY'] = (int)$_REQUEST['city_select'];
		$PROP['F_BINDSERV'] = (int)$_REQUEST['serv_select'];

		$arLoadProductArray = Array(
			"IBLOCK_ID"      => $arIblockAccord['listprices'],
			"PROPERTY_VALUES"=> $PROP,
			"NAME"           => current($elemTree)['name'],
			"DETAIL_TEXT"    => $htmlContent,
		);

		if($PRODUCT_ID = $el->Add($arLoadProductArray))
			$status = 'Элемент успешно добавлен';
		else
			$status = 'Ошибка добавления: '.$el->LAST_ERROR;;
	}
}

function buildTree(array &$elements, $parentId = 0) {
	$branch = array();

	foreach ($elements as $element) {
		if ($element['parent_id'] == $parentId) {
			$children = buildTree($elements, $element['id']);
			if ($children) {
				$element['children'] = $children;
			}
			$branch[$element['id']] = $element;
			unset($elements[$element['id']]);
		}
	}
	return $branch;
}

function htmlTree($elements, $level = 0){
	global $content;
	foreach ($elements as $oneElem){
		if(!isset($oneElem['children'])){
			$content .= '<li>
							<span class="name"><span>'.$oneElem['name'].'</span></span>
							<span class="price"><span>'.$oneElem['value'].'</span><i class="rub">p</i></span>
						</li>';
		}else{
			$level = $oneElem['parent_id'] == 0 ? 0 : strlen($oneElem['parent_id']);
			$content .= getWrap($level, $oneElem['name'], 'start');
			htmlTree($oneElem['children']);
			$content .= getWrap($level, $oneElem['name'], 'end');
		}
	}
	return $content;
}

function getWrap($level, $nameElem, $pos = 'start'){
	$result = '';
	if($level == 1 && $pos == 'start'){
		$result .= sprintf('<h3 class="head-3">%s</h3>', $nameElem);
	}elseif($level == 2){
		if($pos == 'start'){
			$result .= sprintf('<li><span class="name"><span class="spoiler">%s</span></span><ul class="spoiler list-price" >', $nameElem);
		}elseif($pos == 'end'){
			$result .= '</ul></li>';
		}
	}elseif($pos == 'start'){
		$result .= sprintf("<h2>%s</h2>", $nameElem);
	}
	return $result;
}

$aTabs = array(array("DIV" => "edit1", "TAB" => "Загрузка"));
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle("Обновление цен");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>


	<form method="post" action="<?echo $APPLICATION->GetCurPage()?>" enctype="multipart/form-data" name="post_form" id="post_form">
		<?
		echo bitrix_sessid_post();

		$tabControl->Begin();
		$tabControl->BeginNextTab();
		?>
		<!--p>
			123
		</p-->
		<?if(!$status):?>
			<tr>
				<td colspan="2">
					<p>
						<input type="file" name="file">
					</p>
					<p>
						<label for="sel_city">Выберите город:</label>
						<select id="sel_city" name="city_select">
							<?foreach($listCity as $oneCity):?>
								<option value="<?=$oneCity['ID']?>"><?=$oneCity['NAME']?></option>
							<?endforeach;?>
						</select>

					</p>
					<p>
						<label for="sel_serv">Выберите услугу:</label>
						<select id="sel_serv" name="serv_select">
							<?foreach($listService as $oneService):?>
								<option value="<?=$oneService['ID']?>"><?=$oneService['NAME']?></option>
							<?endforeach;?>
						</select>

					</p>
					<input type=submit value="Старт" name="work_start" />
				</td>
			</tr>
		<?else:?>
			<p>
				<a href="/csv-price">назад</a>
			</p>
			<p>
				<?=$status?>
			</p>
			<input type=submit value="Скачать" />
		<?endif;?>
		<?
		$tabControl->End();
		?>
	</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>