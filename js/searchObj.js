var SearchObj = function(cat){
    this.strSearch = null;
    this.cat = null;
    this.page = 1;
    var th = this;

    this.init= function(cat){

        $('#category').change(function(){
            th.page = 1;
            th.request($('.filter-docs.search input[type="text"]').val(), $('#category').val());
        });

        $('.filter-docs.search input[type="submit"]').click(function(){
            th.page = 1;
            th.request($('.filter-docs.search input[type="text"]').val(), $('#category').val());
        });

        $(document.body).on('click', '.pagination a', function(){
            th.page = $(this).data('pagenum');
            th.request($('.filter-docs.search input[type="text"]').val(), $('#category').val());
        });

        this.request();
    };

    this.getParams = function(strSearch, cat){

        this.strSearch = strSearch;
        this.cat = cat;

    };

    this.request = function(strSearch, cat){
        th.getParams(strSearch, cat);
        var params = {
            strSearch: this.strSearch,
            cat: this.cat,
            page: this.page
        };
        var url = $('form.reqlist').attr('action');
        $.ajax({
            url:  url,
            type: 'POST',
            data:   params,
            success: function(answer){
                var obj = JSON.parse(answer);

                $('.listelem').html(obj.arElem);
                $('.faq-counter span').html(obj.count);
                console.log(obj);
                $('.review-item').html(obj.review);

            },
            error: function(data){
                console.log(data);
                th.request();
            },
            datastrSearch: 'json'
        });
    };
    return this;
};