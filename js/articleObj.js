var ArticleObj = function(rPage){
    this.strSearch = null;
    this.spec = null;
    this.page = rPage;
    var th = this;

    this.init= function(){

        $('#specs').change(function(){
            th.page = 1;
            th.request($('.filter-docs.search input[type="text"]').val(), $('#specs').val());
        });

        $('.filter-docs.search input[type="submit"]').click(function(){
            th.page = 1;
            th.request($('.filter-docs.search input[type="text"]').val(), $('#specs').val());
        });

        $(document.body).on('click', '.pagination a', function(){
            event.preventDefault();
            th.page = $(this).data('pagenum');
            th.request($('#qsearch').val(), $('#specs').val());
        });

        this.request();
    };

    this.getParams = function(strSearch, spec){

        this.strSearch = strSearch;
        this.spec = spec;

    };

    this.request = function(strSearch, spec){
        th.getParams(strSearch, spec);
        var params = {
            strSearch: this.strSearch,
            spec: this.spec,
            page: this.page
        };
        var url = $('form.reqlist').attr('action');
        $.ajax({
            url:  url,
            type: 'POST',
            data:   params,
            success: function(answer){
                var obj = JSON.parse(answer);

                $('.def-toggle.n').html(obj.arElem);
                $('.def-counter span').html(obj.count);
                console.log(obj);
                $('.review-item').html(obj.review);
                defHider();

                history.pushState(null, null, '?PAGEN_1='+th.page)
            },
            error: function(data){
                console.log(data);
                th.request();
            },
            datastrSearch: 'json'
        });
    };
    return this;
};