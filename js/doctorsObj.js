var DocList = function(){
    this.clinic = null;
    this.spec = null;
    var th = this;

    this.init= function(){

        $('#centers, #specs').change(function(){
            th.request($('#centers').val(), $('#specs').val());
        });

        this.request(th.getParameterByName('clinic'));
    };

    this.getParams = function(clinic, spec){

        this.clinic = clinic;
        this.spec = spec;

    };

    this.getParameterByName = function(name){
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    this.request = function(clinic, spec){
        th.getParams(clinic, spec);
        var params = {clinic: this.clinic, spec: this.spec};
        var url = $('form.reqlist').attr('action');
        $.ajax({
            url:  url,
            type: 'POST',
            data:   params,
            success: function(html){
                var obj = $(html);
                $('.listdoc').html(obj);
                if (window.isMobile) showMore($('.listdoc'), 'Загрузить еще', 5); 
            },
            error: function(data){
                console.log(data);
                th.request();
            },
            dataType: 'html'
        });
    };
    return this;
};
