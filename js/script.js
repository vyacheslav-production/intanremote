$(function(){

  
    if(!$.cookie('selCity')){
        if(!window.isMobile){
            $(".change-city-drop-select").hide();
            $('.change-city-drop-is_saintp').toggle();
        }else{
            $(".change-city-drop-select").toggle();
            $('.change-city-drop-is_saintp').hide();
        }
    }

if(window.isMobile && $('.enroll-2-doctor').length)
{
    var $formSource = $('.enroll-2-doctor').attr('id','fromLoad'),
        button = $('.appointment'),
        txt = $('#fromLoad .h2').text() || 'Записаться на прием';

    $formSource.hide();
    button
        .attr('href', '#fromLoad')
            .find('span')
                .text(txt);
}
if(window.isMobile && $('#ymaps-map-container').length)
{
    $('.head-doc').appendTo('section .w65.ib');
    $('.how-2-get').appendTo('section .w65.ib');
    $('#ymaps-map-container').appendTo('section .w65.ib');
    $('section .head-doc .i .prof').appendTo('section .head-doc .i');
}
    //Adaptive scripts 14.09.2015
  // var menuContent = $('header .head .head-nav').eq(0).html();
  // $('.head_small .menu-adaptive').eq(0).html(menuContent)
  //   .find('a')
  //     .each(function(){
  //       var $th = $(this);
  //       var link = $th.text();
  //       $th.html('<span>'+link+'</span>');
  //   });

function docsCarusel(){ // вертим докторов
  if($(document).width() <= 930){
    if(!$('.slick-slider').length){
      $('.doctors .table .row').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true
      });
    }
  } else {
    if($('.slick-slider').length){
      $('.slick-slider').slick('unslick');
    }
  }
};
docsCarusel();
$(window).resize(function(){
  setInterval(docsCarusel, 500);
});

  $('.toggle-head_small, .head_small').click(function(e){
      $('.toggle-head_small').toggleClass('active')
      $('html,body').toggleClass('unactive');
      $('.head_small').toggleClass('active');
  });

  $('.menu-adaptive li a[href="#"]').click(function(e){
    e.stopPropagation();
    e.preventDefault();
    $(this).next('.under').slideToggle();
  });


  $(document).on('click', '.pagination a', function(){
      var $parent = $(this).parent().parent(),
          toTop = $parent.position().top;
      $('html:not(animate),body:not(animate)').animate({scrollTop: toTop}, "fast");
  });

  if(window.isMobile)
  {
      $('.docinfoblock-about-btn a').click(function(){
          $('html,body').animate({scrollTop: "0px"}, "fast");
      });
  }

});// end


function showMore($obj, str, count){
  var ch = $obj.children();
  var all = ch.size();
  var step = count
    if(ch.length > count)
    {
      ch.each(function(e){
          if(e > count)
            $(this).css({'display':'none'});
      });
      $obj.append('<a href="#" class="show-more"><span>'+str+'</span></a>');
      $('.show-more').on('click', function(e){
        e.preventDefault();
        count = (count+step <= all) ? count+step : all;
        ch.each(function(e){
          if(e < count)
          {
            $(this).show();
          }
        });
        if(count == all) $(this).hide();
        // ch.slideToggle();
        // $(this).remove();
      });
    }
}
