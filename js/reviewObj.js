var ReviewList = function(rPage){
    this.clinic = null;
    this.spec = null;
    this.page = rPage;
    var th = this;

    this.init= function(){

        $('#centers, #specs').change(function(){
            th.page = 1;
            th.request($('#centers').val(), $('#specs').val());
        });

        $('.filter-docs input[type="reset"]').click(function(){
            th.page = 1;
            th.request(0, 0);
        });

        $(document.body).on('click', '.pagination a', function(){
            event.preventDefault();
            th.page = $(this).data('pagenum');
            th.request($('#centers').val(), $('#specs').val());
        });

        this.request($('#centers').val(), $('#specs').val());
    };

    this.getParams = function(clinic, spec){

        this.clinic = clinic;
        this.spec = spec;

    };

    this.request = function(clinic, spec){
        th.getParams(clinic, spec);
        var params = {
            clinic: this.clinic,
            spec: this.spec,
            page: this.page
        };
        var url = $('form.reqlist').attr('action');
        $.ajax({
            url:  url,
            type: 'POST',
            data:   params,
            success: function(html){
                var obj = $(html);
                $('.listrev').html(obj);

                history.pushState(null, null, '?PAGEN_1='+th.page)
            },
            error: function(data){
                console.log(data);
                th.request();
            },
            dataType: 'html'
        });
    };
    return this;
};