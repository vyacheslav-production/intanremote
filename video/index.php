<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интан собрал для вас подборку интересных видеозаписей");
$APPLICATION->SetTitle("Интересные видео");

$arOldtoNew = array(
    1060=>79212,
    1058=>79213,
    1057=>79214,
    1056=>79215,
    1025=>79216,
    1023=>79217,
    1022=>79218,
    1021=>79219,
    1020=>79220,
    1019=>79221,
    1018=>79222,
    1017=>79223,
    1016=>79224,
    1012=>79225,
    1011=>79226,
    1004=>79227,
    1001=>79228,
    1003=>79229,
    1002=>79230,
    1059=>79234
);

function getVidId($str){
    $ps = strpos($str, "youtube.com/embed/");
    $ps = $ps + 18;
    $vId = "";
    for($i = $ps, $j = strlen($str);$i < $j;$i++){
        if(substr($str, $i, 1)!="?"){
            $vId.= substr($str, $i, 1);
        }else{
            break;
        }
    }
    return $vId;
}

$arNavParams = array(
    'nPageSize' => 7
);

//список видео
$listVideo = array();
$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listvideo']), false, $arNavParams, array('ID', 'CODE', 'NAME', 'DETAIL_PAGE_URL', 'PROPERTY_video'));
while($ar_res = $res->GetNext()){
    $listVideo[] = $ar_res;
}
?>

    <section>
        <?if(!empty($_REQUEST['ELEMENT_ID'])){
            $vID = array_key_exists($_REQUEST['ELEMENT_ID'], $arOldtoNew) ? $arOldtoNew[$_REQUEST['ELEMENT_ID']] : $_REQUEST['ELEMENT_ID'];
            $oneElem = array();
            $res = CIBlockElement::GetList(array(), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listvideo'], 'ID'=>$vID), false, false, array('IBLOCK_ID', 'ID', '*', 'PROPERTY_video'));
            if($ar_res = $res->GetNext()){
                $oneElem = $ar_res;
            }else{
                LocalRedirect("/404.php", "404 Not Found");
            }
        }else{
            reset($listVideo);
            $firstElem = current($listVideo);
            if(!empty($listVideo)){
                LocalRedirect(SITE_DIR.'video/?ELEMENT_ID='.$firstElem['ID']);
            }
        }?>
        <div class="wrap">
            <h1>Интересные видео</h1>
        </div>

        <div class="wrap">
            <div class="w65  ib">
                <div class="main-video-block video-block">
                    <?=$oneElem['~PROPERTY_VIDEO_VALUE']['TEXT']?>
                </div>
                <h2><?=$oneElem['NAME']?></h2>
                <p>
                    <?=$oneElem['~DETAIL_TEXT']?>
                </p>
            </div>
            <div class="w35 ib">
                <div class="video-view slider-container">
                    <div class="swiper-wrapper">
                        <?foreach($listVideo as $keyVideo=>$oneVideo){?>
                            <?
                            /*
                            $apiURL = 'http://gdata.youtube.com/feeds/api/videos/'.getVidId($oneVideo['PROPERTY_VIDEO_VALUE']['TEXT']);
                            $youtube = simplexml_load_file($apiURL);
                            $media = $youtube->children('http://search.yahoo.com/mrss/');
                            $yt = $media->children('http://gdata.youtube.com/schemas/2007');
                            $attrs = $yt->duration->attributes();
                            $length = (int) $attrs['seconds'];*/
                            ?>
                            <a href="/video/?ELEMENT_ID=<?=$oneVideo['ID']?>" class="video-item swiper-slide" data-time="<?//=date('i:s',$length);?>" data-text="<?=$oneVideo['NAME']?>">
                                <span class="img" style="background-image: url(http://i2.ytimg.com/vi/<?=getVidId($oneVideo['PROPERTY_VIDEO_VALUE']['TEXT'])?>/default.jpg)"></span>
                            </a>
                        <?}?>
                    </div>
                    <!-- <div class="more">
                        <a href="#" class="more-link">Загрузить еще</a>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>