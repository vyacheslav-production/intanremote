<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интан собрал для вас подборку интересных видеозаписей");
$APPLICATION->SetTitle("Интересные видео");
?>

<aside class="left_sidebar">
    <?
    $APPLICATION->IncludeComponent("bitrix:menu", "tree1", Array(
        "ROOT_MENU_TYPE" => "top", // Тип меню для первого уровня
        "MENU_CACHE_TYPE" => "N", // Тип кеширования
        "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
        "MAX_LEVEL" => "2", // Уровень вложенности меню
        "CHILD_MENU_TYPE" => "podmenu", // Тип меню для остальных уровней
        "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N", // Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
            ), false
    );
    ?>
<?
    $APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/inc_vert_list.php",
	"EDIT_TEMPLATE" => ""
	),
	false
);
    ?>
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:main.include", "", Array(
        "AREA_FILE_SHOW" => "sect",
        "AREA_FILE_SUFFIX" => "inc_banners",
        "AREA_FILE_RECURSIVE" => "Y",
        "EDIT_TEMPLATE" => ""
            ), false
    );
    ?>
</aside>
<div class="content_part">
    <?
    $APPLICATION->IncludeComponent("bitrix:breadcrumb", "template1", Array(
            ), false
    );
    ?>
    <section>
        <h1 class="main_h"><?$APPLICATION->ShowTitle(false)?></h1>

        <div class="video_wrap">
			
			<?$APPLICATION->IncludeComponent("bitrix:news", "video", array(
	"IBLOCK_TYPE" => "News",
	"IBLOCK_ID" => "19",
	"NEWS_COUNT" => "20",
	"USE_SEARCH" => "N",
	"USE_RSS" => "N",
	"USE_RATING" => "N",
	"USE_CATEGORIES" => "N",
	"USE_FILTER" => "N",
	"SORT_BY1" => "ACTIVE_FROM",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"CHECK_DATES" => "Y",
	"SEF_MODE" => "N",
	"SEF_FOLDER" => "/voprosy/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "Y",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "Y",
	"USE_PERMISSIONS" => "N",
	"PREVIEW_TRUNCATE_LEN" => "",
	"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
	"LIST_FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"LIST_PROPERTY_CODE" => array(
		0 => "video",
		1 => "",
	),
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"DISPLAY_NAME" => "N",
	"META_KEYWORDS" => "kw",
	"META_DESCRIPTION" => "ds",
	"BROWSER_TITLE" => "NAME",
	"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
	"DETAIL_FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"DETAIL_PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"DETAIL_DISPLAY_TOP_PAGER" => "N",
	"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
	"DETAIL_PAGER_TITLE" => "Страница",
	"DETAIL_PAGER_TEMPLATE" => "",
	"DETAIL_PAGER_SHOW_ALL" => "Y",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Вопросы",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "legion",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "Y",
	"DISPLAY_DATE" => "N",
	"DISPLAY_PICTURE" => "N",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"USE_SHARE" => "N",
	"AJAX_OPTION_ADDITIONAL" => "",
	"VARIABLE_ALIASES" => array(
		"SECTION_ID" => "SECTION_ID",
		"ELEMENT_ID" => "ELEMENT_ID",
	)
	),
	false
);?> 
			
			 <?
			$uri = $APPLICATION->GetCurUri();
			if (strpos($uri, "?ELEMENT_ID") !== true) {
				$ps = strpos($uri, "PAGEN_1=");
				$ps = $ps + 8;
				$i = 1;
				$curpage = substr($uri, $ps, $i);
				$i++;
				if (is_numeric(substr($uri, $ps, $i)))
					$curpage = substr($uri, $ps, $i);
				$i++;
				if (is_numeric(substr($uri, $ps, $i)))
					$curpage = substr($uri, $ps, $i);
			//echo $curpage;
				$title = "Интересные видео";
				if ($curpage != 0 && is_numeric($curpage)) {
					$title.=" стр. " . $curpage;
				}
				$APPLICATION->SetTitle($title);
				unset($i, $curpage);
			}
			?> 
			            
        </div>
    </section>
</div>
<div class="clear"></div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>