var gulp = require('gulp'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer');

var cssSource = [
  'css/stylesheets/screen.css',
  'css/fonts/stylesheet.css',
  'css/stylesheets/calc.css',
  'js/select2-4.0.0-beta.3/dist/css/select2.css',
  'js/swiper-slider/idangerous.swiper.css',
  'js/swiper-scrollbar/lib/idangerous.swiper.scrollbar.css',
  'js/jscrollpane/style/jquery.jscrollpane.css',
  'js/fancybox-2.1.5/source/jquery.fancybox.css',
  'js/slick/slick.css',
  'js/slick/slick-theme.css'
];

var sassSource = [
  'css/sass/adaptive.scss',
  'css/sass/srg_style_file.sass'
];

var jsSource = [
  'js/jquery-ui-1.10.4.custom/js/jquery-1.10.2.js',
  'js/fancybox-2.1.5/source/jquery.fancybox.js?v=2.1.5',
  'js/jscrollpane/script/jquery.mousewheel.js',
  'js/fancybox-2.1.5/source/*.js',
  'js/fancybox-2.1.5/source/helpers/*.js',
  'js/swiper-slider/idangerous.swiper.js',
  'js/swiper-scrollbar/lib/idangerous.swiper.scrollbar.js',
  'js/jscrollpane/script/jquery.jscrollpane.js',
  'js/jquery.mask.js',
  'js/doctorsObj.js',
  'js/reviewObj.js',
  'js/articleObj.js',
  'js/faqObj.js',
  'js/searchObj.js',
  'http://maps.google.com/maps/api/js?sensor=false',
  'js/js.js',
  'js/select2-4.0.0-beta.3/dist/js/select2.js',
  'js/script.js',
  'js/jquery.cookie.js',
  'js/slick/slick.min.js'
];

gulp.task('css', function(){
  gulp.src(cssSource)
    .pipe(sourcemaps.init())
      .pipe(concat('styles.css'))
        .pipe(sourcemaps.write())
          .pipe(gulp.dest('css/stylesheets'))
});

gulp.task('sass', function(){
  gulp.src(sassSource)
    .pipe(sass())
        .pipe(concat('adaptive.css'))
          .pipe(gulp.dest('css/stylesheets'))
});

gulp.task('js', function(){
  gulp.src(jsSource)
    .pipe(sourcemaps.init())
      //.pipe(uglify())
        .pipe(concat('global.js'))
          .pipe(sourcemaps.write())
            .pipe(gulp.dest('js'))
});

gulp.task("watch", function(){
  gulp.watch(cssSource, ['css']);
  gulp.watch(sassSource, ['sass']);
  gulp.watch(jsSource, ['js']);
});

gulp.task('default', function() {
    gulp.start('watch', 'css', 'sass', 'js');
});
