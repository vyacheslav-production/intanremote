<div class="aside_news">
        <?/*<h2>Новости</h2>
        <div class="asn_item">
            <h3><a href="">Бесплатное лечение — каковы последствия?</a></h3>
            <p class="mp_news_date">22.07.12</p>
            <div class="mp_news_text">
                Кариес — очень распространённое заболевание. Оно занимает первое место среди хронических заболеваний...
            </div>
        </div>
        <div class="asn_item">
            <h3><a href="">Новая технология в изготовлении брекет-систем</a></h3>
            <p class="mp_news_date">20.07.12</p>
            <div class="mp_news_text">
                Сложные ортодонтические устройства, для коррекции положения зубов при нарушениях прикуса. 
            </div>
        </div>
        <div class="asn_item">
            <h3><a href="">Проблемы современной cтоматологии</a></h3>
            <p class="mp_news_date">17.07.12</p>
            <div class="mp_news_text">
                Частота, с которой зуб подвергается кариесогенному воздействию кислот, влияет на вероятность кариеса...
            </div>
        </div>*/?>
        <?
        $APPLICATION->IncludeComponent("bitrix:news.list", "vert_list", array(
	"IBLOCK_TYPE" => "intanNews",
	"IBLOCK_ID" => "29",
	"NEWS_COUNT" => "3",
	"SORT_BY1" => "ACTIVE_FROM",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "ACTIVE_FROM",
	"SORT_ORDER2" => "DESC",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "/novosti/?ID=#ID#",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);
        ?>
    </div>