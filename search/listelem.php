<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

if(!empty($strSearch)){

    $arFilter = [];
    $arFilter['ACTIVE'] = 'Y';

    $arFilter['IBLOCK_ID'] = array_key_exists($catSearch, $arIblockAccord) ? $arIblockAccord[$catSearch] : array_values($arIblockAccord);

    $arFilter['SEARCHABLE_CONTENT'] = '%'.$strSearch.'%';

    $arNavParams = array(
        'nPageSize' => 5
    );

    $arNavParams['iNumPage'] = $nPage ? $nPage : 1;

    $listElement = array();
    $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter, false, $arNavParams);
    while($ar_res = $res->GetNext()){
        $listElement[] = $ar_res;
    }

    //подсчитаем общее кол-во элементов, удовлетворяющих запросу
    $cntElem = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter, array(), $arNavParams);

    $strElem = '';
    foreach ($listElement as $oneElem) {
        $strElem .= '<div class="def-item">';
        if($oneElem['DETAIL_PAGE_URL']){
            $strElem .=    '<a href="'.$oneElem['DETAIL_PAGE_URL'].'" class="def-item-item">'.preg_replace('/'.$strSearch.'/is', '<b>'.strtoupper($strSearch).'</b>', $oneElem['NAME']).'</a>';
        }else{
            $strElem .=    '<a class="def-item-item">'.$oneElem['NAME'].'</a>';
        }
        $strElem .=    '<p><span>';
        $strElem .=        preg_replace('/'.$strSearch.'/is', '<b>'.strtoupper($strSearch).'</b>', strip_tags($oneElem['DETAIL_TEXT']));
        $strElem .=    '</span></p></div>';
    }

    $res->nPageWindow = 3;
    $NAV_STRING = $res->GetPageNavStringEx($navComponentObject, '/search/', 'pagenavicust_search', '');
    if($NAV_STRING){
        $strElem .= $NAV_STRING;
    }
}

//echo json_encode(array('arElem'=>$strElem, 'count'=>$cntElem));?>
<div class="faq-counter">
    Кол-во результатов: <span><?=$cntElem?></span>
</div>
<div class="listelem">
    <?=$strElem?>
</div>
