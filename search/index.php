<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");

$strSearch = $_GET['q'] ? $_GET['q'] : '';
$catSearch = $_GET['cat'] ? $_GET['cat'] : '';
$nPage = $_REQUEST['PAGEN_1'] ? (int)$_REQUEST['PAGEN_1'] : 1;
?>

    <script>
        function selectSubmit(obj){
            var strSearch = $('.reqlist input[name="q"]').val();
            window.location='/search/?q='+strSearch+'&cat='+obj.value;
        }
    </script>

    <section>

        <div class="wrap">
            <h1>Поиск по сайту</h1>
            <div class="w65 faq">

                <div class="filter-docs search">
                    <form action="/search/" class="reqlist" method="GET">
                        <input name="q" type="text" placeholder="Введите часть слова или слово целиком" value="<?=$strSearch ? $strSearch : ''?>">
                        <input type="submit" value="Найти">
                        <select name="cat" id="category" onchange="selectSubmit(this)">
                            <option value="" <?=!$catSearch ? 'selected' : ''?>>Все категории</option>
                            <option value="listmedservices" <?=$catSearch == 'listmedservices' ? 'selected' : ''?>>Услуги</option>
                            <option value="blogers" <?=$catSearch == 'blogers' ? 'selected' : ''?>>Врачи</option>
                            <option value="listfaq" <?=$catSearch == 'listfaq' ? 'selected' : ''?>>Вопрос-ответ</option>
                            <option value="listarticles" <?=$catSearch == 'listarticles' ? 'selected' : ''?>>Статьи</option>
                            <option value="listencyc" <?=$catSearch == 'listencyc' ? 'selected' : ''?>>Энциклопедия</option>
                            <option value="otzyiv" <?=$catSearch == 'otzyiv' ? 'selected' : ''?>>Отзывы</option>
                        </select>
                    </form>
                </div>
                <?include_once($_SERVER['DOCUMENT_ROOT'].'/search/listelem.php')?>

            </div>

            <div class="w35">
                <div class="reviews">
                    <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
                    if($ar_res = $res->GetNext()){?>
                        <div class="h2"><i></i><span>Отзывы о нас</span></div>
                        <div class="review-item">
                            <div class="review-user-name" data-date="<?//=date('d.m.Y', strtotime($ar_res['DATE_CREATE']))?>"><?=$ar_res['NAME']?></div>
                            <div class="def-toggle">
                            <p>
                                <?=$ar_res['DETAIL_TEXT']?>
                            </p>
                                </div>
                        </div>
                    <?}?>
                    <a href="/stomatologiya-ceny/" class="right-link price-price"><span>Цены на наши услуги</span></a>
                </div>
            </div>

        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>