<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<div class="w35 ib">

    <div class="newrightpane enroll-2-doctor ">
        <div class="newrightpane-head">Отдел кадров</div>
        <div class="newrightpane-text">
            <?
            $arrFilter = ['IBLOCK_ID'=>$arIblockAccord['listcities'], 'ACTIVE'=>'Y', 'CODE'=>getCodeCity()];
            $res = CIBlockElement::GetList(array('NAME'=>'ASC'), $arrFilter, false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_F_CONTACTHR'));
            if($ar_res = $res->fetch()){?>
                <?=$ar_res['PROPERTY_F_CONTACTHR_VALUE']['TEXT']?>
            <?}?>
            <br/>
            <a href="#" class="sender"> Отправить резюме</a>
        </div>
        <span class="resume-bg"></span> 
        <div class="resume">
            <a href="#" class="close-resume"><img src="/images/close-vacancy.png" alt=""></a>
            <div class="h2">Ваше резюме</div>
            <form action="" method="post" enctype="multipart/form-data">
                <input name="form_subm" value="<?=$arIblockAccord['reqforjob']?>" type="hidden" />
                <input name="name" required type="text" placeholder="Имя">
                <input name="email" required type="Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="example@mail.ru" placeholder="Email">
                <input name="telefon" required type="text"  placeholder="Телефон" title="Введите телефон вформате +7 (999) 999-99-99" placeholder="+7 (9__)-___-__-__" class="phone">
                <select name="dolgnost" id="" class="time">
                    <option value="">Специализация</option>
                    <?$res = CIBlockElement::GetList(array('NAME'=>'ASC'), array('IBLOCK_ID'=>$arIblockAccord['listvacan'], 'ACTIVE'=>'Y'));
                    while($ar_res = $res->GetNext()){?>
                        <option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                    <?}?>
                </select>
                <div class="file">
                                    <span>
                                        <a class="orange-link">Загрузить резюме</a>
                                    </span>
                    <ins></ins>
                    <input name="rezyme" type="file">
                </div>
                <textarea name="F_COMMENT" placeholder="Комментарий"></textarea>
                <input type="submit" value="Отправить">
            </form>
        </div>
    </div>

    <div class="history-right-block">
        
    </div>

</div>
