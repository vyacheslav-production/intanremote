<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetPageProperty("keywords", "стоматология cанкт петербург петербург, стоматология СПб, стоматологические клиники, интан центр имплантация санкт петербург");
$APPLICATION->SetPageProperty("description", "Интан - стоматологии в Петербурге. Стоматология: низкие цены и квалифицированные специалисты. Стоматология - весь спектр услуг: лечение, имплантация и протезирование, детская стоматология.");
$APPLICATION->SetPageProperty("title", "Стоматология Спб: клиника семейной стоматологии Санкт-Петербург. Стоматологическая клиника Интан.");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Стоматологическая клиника Интан в Санкт-Петербурге");
?>


<section>

    <?
    $GLOBALS['arrFilter'] = ['PROPERTY_F_BINDCITY.CODE'=>getCodeCity()];
    $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "main_sales",
        array(
            "IBLOCK_TYPE" => "spb",
            "IBLOCK_ID" => "33",
            "NEWS_COUNT" => "20",
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "ACTIVE_FROM",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilter",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "",
                1 => "F_PHOTOSLIDER",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "N",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_ADDITIONAL" => ""
        ),
        false
    );?>

    <div class="wrap">

        <div class="doctors">
            <div class="title">
                <a href="/<?=getCodeCity()?>/personal/"><span class="PFDin">Врачи стоматологии ИНТАН</span></a>
            </div>
            <div class="table">
                <div class="row">
                    <?$listElement = array();
                    $arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['blogers'], 'PROPERTY_F_ONMAIN'=>true];
                    $arrFilter = getFilArrayByCity($arrFilter);
                    $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('IBLOCK_ID', 'ID', 'NAME', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'PROPERTY_bloger_position', 'PROPERTY_F_MAINTEXT'));
                    while($ar_res = $res->GetNext()){
                        $listElement[] = $ar_res;
                    }
                    foreach($listElement as $oneElem){?>
                        <div class="cell">
                            <p class="PFDin"><?=$oneElem['PROPERTY_F_MAINTEXT_VALUE']?></p>
                            <?$img = CFile::ResizeImageGet($oneElem["PREVIEW_PICTURE"], array('width'=>285, 'height'=>285), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
                            <a href="<?=$oneElem['DETAIL_PAGE_URL']?>" class="doctor-photo" style="background-image: url(<?=$img['src']?>)">
                            </a>
                            <div class="doctor-name">
                                <a href="<?=$oneElem['DETAIL_PAGE_URL']?>"><?=$oneElem['NAME']?></a>
                                <p><?=$oneElem['PROPERTY_BLOGER_POSITION_VALUE']?></p>
                            </div>
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
    </div>
    <div class="operation-to-implant">
        <section class="facts">
            <div class="wrap">
                <div class="control">
                    <div class="arrow icons-arrow-next"><span></span></div>
                    <div class="arrow icons-arrow-prev"><span></span></div>
                </div>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?$listElement = array();
                        $arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listthes']];
                        $arrFilter = getFilArrayByCity($arrFilter);
                        $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME'));
                        while($ar_res = $res->GetNext()){
                            $listElement[] = $ar_res;
                        }
                        foreach($listElement as $oneElem){?>
                            <div class="swiper-slide">
                                <div class="table">
                                    <div class="row">
                                        <div class="cell"><p class="facts_text"><?=$oneElem['NAME']?></p></div>
                                    </div>
                                </div>
                            </div>
                        <?}?>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="wrap">
        <h3>Преимущества стоматологии «Интан»</h3>
        <div id="desktop" class="tab-toggler">
            <?$listElement = array();
            $arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listbenef']];
            $arrFilter = getFilArrayByCity($arrFilter);
            $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME', 'PREVIEW_TEXT'));
            while($ar_res = $res->GetNext()){
                $listElement[] = $ar_res;
            }?>
            <div class="tab-toggler-head">
                <span class="tab-toggler-toggler"></span>
                <?foreach($listElement as $keyElem=>$oneElem){?>
                    <a class="<?=$keyElem == 0 ? 'active' : ''?>" href="javascript:;"><span><?=$oneElem['NAME']?></span></a>
                <?}?>
            </div>
            <div class="tab-toggler-body">
                <?foreach($listElement as $keyElem=>$oneElem){?>
                    <div class="tab-toggler-item <?=$keyElem == 0 ? 'active' : ''?>">
                        <p>
                            <?=$oneElem['~PREVIEW_TEXT']?>
                        </p>
                    </div>
                <?}?>
            </div>
        </div>
        <div id="desktopMobile" style="display: none;" class="tab-toggler">
            <?$listElement = array();
            $arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listbenef']];
            $arrFilter = getFilArrayByCity($arrFilter);
            $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME', 'PREVIEW_TEXT'));
            while($ar_res = $res->GetNext()){
                $listElement[] = $ar_res;
            }?>
            <span class="tab-toggler-toggler"></span>
            <?php foreach($listElement as $keyElem=>$oneElem) :?>
                <div class="tab-toggler-head">
                    <a  class="<?=$keyElem == 0 ? 'active' : ''?>" href="javascript:;"><span class="toggler <?=$keyElem == 0 ? 'rotate' : ''?>"><?=$oneElem['NAME']?></span></a>
                </div>
                <div class="tab-toggler-item <?=$keyElem == 0 ? 'active' : ''?>">
                    <p>
                        <?=$oneElem['~PREVIEW_TEXT']?>
                    </p>

                </div>
            <? endforeach;?>
        </div>
    </div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
