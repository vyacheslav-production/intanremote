<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Врачи центров стоматологии «Интан»");
$APPLICATION->SetPageProperty("keywords", "врачи,стоматологическая клиника, интан, персонал, стоматологи");
$APPLICATION->SetPageProperty("description", "Интан объединил самых лучших и квалифицированных стоматологов в Петербурге");
$APPLICATION->SetTitle("Врачи центров стоматологии «Интан»");
CModule::IncludeModule("iblock");

if(!$_GET['selCity']){
    LocalRedirect("/404.php", "404 Not Found");
}
?>

    <script>
        var docElem;
        $(function(){
            docElem = new DocList();
            docElem.init();
        });
    </script>

    <section>
        <div class="wrap">
            <h1 class="center">Врачи стоматологии ИНТАН</h1>
            <div class="filter-docs">
                <form action="<?=SITE_DIR?>personal/listdoctors.php" class="reqlist" method="POST">
                    <div class="title-docs">
                        <div class="address">
                            <span class="txt">Центр: </span>
                            <select name="" id="centers">
                                <option value="0">Все центры</option>
                                <?
                                $arrFilter = ['IBLOCK_ID'=>$arIblockAccord['dental_centers'], 'ACTIVE'=>'Y'];
                                $arrFilter = getFilArrayByCity($arrFilter);
                                $res = CIBlockElement::GetList(array('NAME'=>'ASC'), $arrFilter);
                                while($ar_res = $res->GetNext()){?>
                                    <option value="<?=$ar_res['ID']?>" <?=$ar_res['ID'] == $_GET['clinic'] ? 'selected' : ''?>><?=$ar_res['NAME']?></option>
                                <?}?>
                            </select>
                        </div>
                        <div class="spec">
                            <span  class="txt">Специализация:</span>
                            <select name="" id="specs">
                                <option value="">Все специализации</option>
                                <option value="стоматолог">стоматолог</option>
                                <option value="хирург">хирург</option>
                                <option value="имплантолог">имплантолог</option>
                                <option value="пародонтолог">пародонтолог</option>
                                <option value="терапевт">терапевт</option>
                                <option value="ортопед">ортопед</option>
                                <option value="детский стоматолог">детский стоматолог</option>
                                <option value="гигиенист">гигиенист</option>
                                <option value="ортодонт">ортодонт</option>
                                <option value="гигиенист">гигиенист</option>
                                <option value="рентгенолог">рентгенолог</option>
                                <option value="детский терапевт">детский терапевт </option>
                            </select>
                        </div>
                        <input type="reset" value="Сбросить фильтр">
                    </div>
                </form>
            </div>

            <div class="docs-list">
                <!--div class="title-docs">
                    <div class="address">Центр на проспекте Славы/Бухарестская</div>
                    <div class="spec">Специализация: Пародонтология</div>
                </div-->
                <div class="listdoc">

                </div>
            </div>
        </div>
    </section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
