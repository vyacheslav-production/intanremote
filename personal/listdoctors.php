<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arFilter = array(
    'ACTIVE'=>'Y',
    'IBLOCK_ID'=>$arIblockAccord['blogers']
);

//������ ������ �������� �������
$listClinics = [];
$arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['dental_centers']];
$arrFilter = getFilArrayByCity($arrFilter);
$res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('IBLOCK_ID', 'ID', '*', 'PROPERTY_bloger_position', 'PROPERTY_F_CENTRE'));
while($ar_res = $res->GetNext()){
    $listClinics[] = $ar_res['ID'];
}

if(!empty($_POST['clinic'])){
    $arFilter['PROPERTY_F_CENTRE'] = (int)$_POST['clinic'];
}else{
    $arFilter['PROPERTY_F_CENTRE'] = $listClinics;
    $arFilter['PROPERTY_F_CENTRE'][] = false;
}

if(!empty($_POST['spec'])){
    $arFilter['PROPERTY_bloger_position'] = '%'.$_POST['spec'].'%';
}

$listElement = [];
$arFilter = getFilArrayByCity($arFilter);
$res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter, false, false, array('IBLOCK_ID', 'ID', '*', 'PROPERTY_bloger_position', 'PROPERTY_F_CENTRE'));
while($ar_res = $res->GetNext()){
    $listElement[] = $ar_res;
}

foreach ($listElement as $oneElem) {?>
    <div class="docs-item">
        <a href="<?=$oneElem['DETAIL_PAGE_URL']?>">
            <?$img = CFile::ResizeImageGet($oneElem["PREVIEW_PICTURE"], array('width'=>180, 'height'=>180), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
            <span class="img" style="background-image: url(<?=$img['src']?>)"></span>
            <div class="doc-name"><?=$oneElem['NAME']?></div>
            <p><?=$oneElem['PROPERTY_BLOGER_POSITION_VALUE']?></p>
        </a>
    </div>
<?}?>