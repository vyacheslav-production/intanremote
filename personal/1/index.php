<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Врачи центров стоматологии &laquo;Интан&raquo;");
?>

<aside class="left_sidebar">
    <?
    $APPLICATION->IncludeComponent("bitrix:menu", "tree1", Array(
        "ROOT_MENU_TYPE" => "top", // Тип меню для первого уровня
        "MENU_CACHE_TYPE" => "N", // Тип кеширования
        "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
        "MAX_LEVEL" => "2", // Уровень вложенности меню
        "CHILD_MENU_TYPE" => "podmenu", // Тип меню для остальных уровней
        "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N", // Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
            ), false
    );
    ?>
    <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "inc_banners",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
false
);?>
</aside>
<div class="content_part">
    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "template1", Array(
	
	),
	false
    );?>
    <article>
        <h1 class="main_h">Врачи центров стоматологии &laquo;Интан&raquo;</h1>
        <div class="back_to_list">
            <span class="iecor tl"></span>
            <span class="iecor tr"></span>
            <span class="iecor bl"></span>
            <span class="iecor br"></span>
            <span class="left_arrow"></span>
            <a href="/personal/">Вернуться к списку</a>
        </div>
        <div class="personal_silgle">
            <h2>Минаков Алексей Васильевич</h2>
            <div class="personal_silgle_img"><img src="/images/minakov.jpg" alt="" /></div>
            <div class="personal_silgle_text">
                <p class="personal_silgle_h">Должность</p>
                <div>Врач-ортопед-хирург-имплантолог</div>
                <p class="personal_silgle_h">Краткая биография</p>
                <div>
                    <p>1999 Кубанская медицинская академия им. Красной Армии</p>
                    <p>Стаж работы - с 1999 года</p>
                    <p>Что больше всего ценю в своей работе - качество</p>
                </div>
                <p class="personal_silgle_h">Совет на здоровье</p>
                <p class="personal_advice">Поступайте как я - всегда улыбайтесь!</p>
            </div>
            <div class="clear"></div>
        </div>
    </article>
</div>
<div class="clear"></div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>