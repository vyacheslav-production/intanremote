<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Услуги");
?>

    <section>
        <?if(!empty($_GET['ELEMENT_CODE'])){
            $oneElem = array();
            $arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listmedservices'], 'CODE'=>$_GET['ELEMENT_CODE']];
            $arrFilter = getFilArrayByCity($arrFilter);
            $res = CIBlockElement::GetList(array(), $arrFilter, false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));//'PROPERTY_F_PRICE', 'PROPERTY_F_DOP', 'PROPERTY_F_PHOTO',
            if($ar_res = $res->GetNextElement()){
                $oneElem = $ar_res->GetFields();
                $oneElem['prop'] = $ar_res->GetProperties();

                //SEO вкладка в админке
                $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($oneElem["IBLOCK_ID"], $oneElem["ID"]);
                $seoProp = $ipropValues->getValues();
                $h1Text = $seoProp['ELEMENT_PAGE_TITLE'];
            }else{
                LocalRedirect("/404.php", "404 Not Found");
            }
        }else{
            reset($listServ);
            $firstElem = current($listServ);
            LocalRedirect('/stomatologiya-ceny/'.$firstElem['CODE'].'/');
        }

        $divText = '';
        //если передан тип услуги/фото/цены
        if(isset($_GET['type']) && !empty($_GET['type'])){

            if($_GET['type'] == 'tseny'){

                if(!$_GET['selCity']){
                    LocalRedirect("/404.php", "404 Not Found");
                }

                $arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listprices']];
                $arrFilter = getFilArrayByCity($arrFilter);
                $arrFilter[] = ['LOGIC' => 'OR',
                    array('PROPERTY_F_BINDSERV.CODE'=>$_GET['ELEMENT_CODE']),
                    array('PROPERTY_F_BINDSERV'=>false)
                ];
                $res = CIBlockElement::GetList(array(), $arrFilter, false, false, array('IBLOCK_ID', 'ID', 'DETAIL_TEXT'));
                if($ar_res = $res->fetch()){
                    $divText = $ar_res['DETAIL_TEXT'];
                }

            }elseif($_GET['type'] == 'photo_do_i_posle'){
                if(is_array($oneElem['prop']['F_PHOTO']['VALUE'])){
                    $arPhoto = array();
                    foreach ($oneElem['prop']['F_PHOTO']['VALUE'] as $keyPhoto=>$onePhoto) {
                        $img = CFile::ResizeImageGet($onePhoto, array('width'=>200, 'height'=>200), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                        $divText .= '<img src="'.$img['src'].'">';
                    }
                }
            }else{//все остальное, кроме цен, инфо и фото "до" и "после"
                $res = CIBlockElement::GetList(array(),
                    array(
                        'ACTIVE'=>'Y',
                        'IBLOCK_ID'=>$arIblockAccord['doplistmedserv'],
                        'SECTION_ID'=>$oneElem['prop']['F_DOP']['VALUE'],
                        'CODE'=>$_GET['type']),
                    false,
                    false,
                    array('DETAIL_TEXT', 'IBLOCK_ID', 'ID'));
                if($ar_res = $res->GetNext()){
                    $divText = $ar_res['DETAIL_TEXT'];

                    //SEO вкладка в админке
                    $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($ar_res["IBLOCK_ID"], $ar_res["ID"]);
                    $seoProp = $ipropValues->getValues();
                    $h1Text = $seoProp['ELEMENT_PAGE_TITLE'];
                }else{
                    LocalRedirect("/404.php", "404 Not Found");
                }
            }
        }else{
            $divText = $oneElem['~DETAIL_TEXT'];
        }?>
        <div class="wrap">
            <h1><?=$h1Text ? $h1Text : $oneElem['NAME']?></h1>
            <div class="mobile-inner-menu">
                <div class="menu-blok ">
                    <ul>
                        <li><a href="<?=$oneElem['DETAIL_PAGE_URL']?>" class="<?=empty($_GET['type']) ? 'active' : ''?>"><span>Подробнее об услуге</span></a></li>
                        <li><a href="<?='/'.getCodeCity().$oneElem['DETAIL_PAGE_URL'].'tseny/'?>" class="<?=$_GET['type'] == 'tseny' ? 'active' : ''?>"><span>Цены</span></a></li>
                        <?$res = CIBlockElement::GetList(array('SORT'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['doplistmedserv'], 'SECTION_ID'=>$oneElem['prop']['F_DOP']['VALUE']), false, false, array('ID', 'CODE', 'NAME'));
                        while($ar_res = $res->GetNext()){?>
                            <li><a href="<?=$oneElem['DETAIL_PAGE_URL'].$ar_res['CODE'].'/'?>" class="<?=$_GET['type'] == $ar_res['CODE'] ? 'active' : ''?>"><span><?=$ar_res['NAME']?></span></a></li>
                        <?}?>
                        <!--li><a href="<?=$oneElem['DETAIL_PAGE_URL'].'photo_do_i_posle/'?>" class="<?=$_GET['type'] == 'photo_do_i_posle' ? 'active' : ''?>"><span>Фотографии до и после</span></a></li-->
                    </ul>
                </div>

                <div class="reviews">
                    <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv'], 'PROPERTY_F_BIND'=>$oneElem['ID']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
                    if($ar_res = $res->GetNext()){?>
                        <a href="/otzyv/?specs=<?=$oneElem['ID']?>" class="title">
                            <div class="h2"><i></i><span>Отзывы об услуге</span></div>
                        </a>
                        <div class="review-item">
                            <div class="review-user-name" data-date="<?//=date('d.m.Y', strtotime($ar_res['DATE_CREATE']))?>"><?=$ar_res['NAME']?></div>
                            <div class="def-toggle">
                            <p>
                                <?=$ar_res['DETAIL_TEXT']?>
                            </p>
                                </div>
                        </div>
                    <?}?>
                </div>
            </div>
            <div class="w65 ib">
                <?
                $listSales = array();
                $arrFilter = ['ACTIVE'=>'Y', 'ACTIVE_DATE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listsale'], 'PROPERTY_F_BINDCITY.CODE' => getCodeCity(),
                                array('LOGIC' => 'OR',
                                    array('PROPERTY_F_SERVICE'=>$oneElem['ID']),
                                    array('PROPERTY_F_SERVICE'=>false)
                            )];
                $res = CIBlockElement::GetList(array('RAND'=>'ASC'), $arrFilter);
                while($ar_res = $res->GetNext()){
                    $listSales[] = $ar_res;
                }
                if(!empty($listSales)){?>
                    <h2>Акции по услуге</h2>
                    <div class="b-slider mini">
                        <div class="controls">
                            <span  class="prev"></span>
                            <span class="next"></span>
                        </div>
                        <div class="swiper-container" >
                            <div class="swiper-wrapper">
                                <?foreach($listSales as $oneSale){?>
                                    <?$img = CFile::ResizeImageGet($oneSale["PREVIEW_PICTURE"], array('width'=>427, 'height'=>240), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
                                    <a href="<?=$oneSale['DETAIL_PAGE_URL']?>" class="img swiper-slide" style="background-image: url(<?=$img['src']?>)"></a>
                                <?}?>
                            </div>
                        </div>
                        <div class="swiper-scrollbar"></div>
                    </div>
                <?}?>

                <div class="text-item">
					<div class="wrap-text">
                        <?=$divText?>
                        <?
                        if(empty($_GET['type']) || ($_GET['type'] != 'tseny')){

                            $strBindCode = !empty($_GET['type']) ? $_GET['type'] : $_GET['ELEMENT_CODE'];

                            $arrFilter = ['ACTIVE' => 'Y', 'IBLOCK_ID' => $arIblockAccord['pricesfrom']];
                            $arrFilter = getFilArrayByCity($arrFilter);
                            $arrFilter[] = ['LOGIC' => 'OR',
                                array('PROPERTY_F_BINDSERV.CODE' => $strBindCode),
                                array('PROPERTY_F_BINDSERV' => false)
                            ];
                            $res = CIBlockElement::GetList(array(), $arrFilter, false, false, array('IBLOCK_ID', 'ID', 'NAME', 'PROPERTY_F_SUMM'));
                            if ($ar_res = $res->fetch()){?>
                                <!-- блок ценником -->
                                <div class="priceblock ">
                                    <div class="priceblock-text">
                                        <div class="priceblock-table">
                                            <div class="row">
                                                <div class="cell priceblock-text-text">
                                                    <?= $ar_res['NAME'] ?>
                                                </div>
                                                <div class="cell priceblock-text-price">
                                                    от <span><?= $ar_res['PROPERTY_F_SUMM_VALUE'] ?></span><i
                                                        class="rub">p</i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="priceblock-btn">
                                        <a class="default-btn"
                                           href="<?= '/' . getCodeCity() . $oneElem['DETAIL_PAGE_URL'] . 'tseny/' ?>">Подробнее</a>
                                    </div>
                                </div>
                            <?}
                        }?>
                        <br>
					</div>
                </div>
            </div>
            <div class="w35 ib fixed-scrolling">
                <div class="menu-blok ">
                    <ul>
                        <li><a href="<?=$oneElem['DETAIL_PAGE_URL']?>" class="<?=empty($_GET['type']) ? 'active' : ''?>"><span>Подробнее об услуге</span></a></li>
                        <li><a href="<?='/'.getCodeCity().$oneElem['DETAIL_PAGE_URL'].'tseny/'?>" class="<?=$_GET['type'] == 'tseny' ? 'active' : ''?>"><span>Цены</span></a></li>
                        <?$res = CIBlockElement::GetList(array('SORT'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['doplistmedserv'], 'SECTION_ID'=>$oneElem['prop']['F_DOP']['VALUE']), false, false, array('ID', 'CODE', 'NAME'));
                        while($ar_res = $res->GetNext()){?>
                            <li><a href="<?=$oneElem['DETAIL_PAGE_URL'].$ar_res['CODE'].'/'?>" class="<?=$_GET['type'] == $ar_res['CODE'] ? 'active' : ''?>"><span><?=$ar_res['NAME']?></span></a></li>
                        <?}?>
                        <!--li><a href="<?=$oneElem['DETAIL_PAGE_URL'].'photo_do_i_posle/'?>" class="<?=$_GET['type'] == 'photo_do_i_posle' ? 'active' : ''?>"><span>Фотографии до и после</span></a></li-->
                    </ul>
                </div>

                <div class="reviews">
                    <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv'], 'PROPERTY_F_BIND'=>$oneElem['ID']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
                    if($ar_res = $res->GetNext()){?>
                        <a href="/otzyv/?specs=<?=$oneElem['ID']?>" class="title">
                            <div class="h2"><i></i><span>Отзывы об услуге</span></div>
                        </a>
                        <div class="review-item">
                            <div class="review-user-name" data-date="<?//=date('d.m.Y', strtotime($ar_res['DATE_CREATE']))?>"><?=$ar_res['NAME']?></div>
                            <div class="def-toggle">
                            <p>
                                <?=$ar_res['DETAIL_TEXT']?>
                            </p>
                                </div>
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
    </section>
<?
if($seoProp['ELEMENT_META_TITLE']){
    $APPLICATION->SetPageProperty("title", $seoProp['ELEMENT_META_TITLE']);
}
if($seoProp['ELEMENT_META_KEYWORDS']){
    $APPLICATION->SetPageProperty("keywords", $seoProp['ELEMENT_META_KEYWORDS']);
}
if($seoProp['ELEMENT_META_DESCRIPTION']){
    $APPLICATION->SetPageProperty("description", $seoProp['ELEMENT_META_DESCRIPTION']);
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
