<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Карьера");
?>

  <section>
        <div class="wrap">

            <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_page_vac", Array(

            ),
                false
            );?>
            <div class="w65 text ib">
                <div class="text-item vacancy-item ">
                    <div class="vacancy-toptext cariera" style="background: url(/images/banner-cariera1.png) right top no-repeat;">
                        <table>
                            <tbody><tr>
                                <td>  В Центрах Имплантации и Стоматологии «ИНТАН» осуществляется индивидуальный подход к каждому сотруднику</td>
                            </tr>
                        </tbody></table>
                    </div>
                </div>
            </div>
            <?include_once($_SERVER['DOCUMENT_ROOT'].'/include/sidebar_right_vac.php');?>
            <?$listClinic = [];
            $arrFilter = ['ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['dental_centers']];
            $arrFilter = getFilArrayByCity($arrFilter);
            $res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arrFilter, false, false, array('ID', 'NAME', 'DETAIL_PAGE_URL', 'PROPERTY_F_SALE', 'PROPERTY_F_SALE.DETAIL_PAGE_URL'));
            while($ar_res = $res->GetNext()){
                $listClinic[] = $ar_res;
            }
            $more = array(2,3,4);
            if (count($listClinic) == 1) {
                $clinic = 'КЛИНИКА';
            }elseif (in_array(count($listClinic), $more)) {
                $clinic = 'КЛИНИКИ';
            }else{
                $clinic = 'КЛИНИК';
            }
            ?>
            <div class="cariera">

                <div class="cariera-item">
                    <?if ($_REQUEST['selCity'] == 'krd') {?>                    
                        <div class="cariera-item-title">
                            <span>ЦЕНТРЫ В РАЗНЫХ РАЙОНАХ ГОРОДА: ВЫБЕРИТЕ УДОБНЫЙ</span>
                        </div>
                        <div class="w50 w560">
                            <div class="inner cariera-item-first-inner">
                                <!-- <h4 class="bf">
                                    Больше не нужно тратить много времени на дорогу:
                                </h4>-->
                                <p>
                                    Мы постарались выбрать такое расположение для наших Центров, чтобы вам было удобно добираться на работу, вне зависимости от того, в каком районе вы живете.
                                </p>
                            </div>
                            <div class="w50 hide640">
                                <img src="/images/cariera-4.png" alt="">
                            </div>
                            <div class="w50 cariera-item-first-desc">
                                <p>
                                    В наших клиниках мы стремимся создать все условия для комфортной работы: просторные и уютные кабинеты, новое оборудование, современные системы диагностики.
                                </p>
                            </div>
                        </div>
                    <?}elseif ($_REQUEST['selCity'] == 'nvr') {?>
                        <div class="cariera-item-title">
                            <span>ЦЕНТР С ВИДОМ НА МОРЕ: ВСЕГО 5 МИНУТ ОТ БУХТЫ</span>
                        </div>
                        <div class="w50 w560">
                            <div class="inner cariera-item-first-inner">
                                <!-- <h4 class="bf">
                                    Больше не нужно тратить много времени на дорогу:
                                </h4> -->
                                <p>
                                    Наш Центр располагается в непосредственной близости от Цемесской бухты и бульвара Черняховского. Открывающийся из окна пейзаж украсит ваш рабочий день.
                                </p>
                            </div>
                            <div class="w50 hide640">
                                <img src="/images/cariera-4.png" alt="">
                            </div>
                            <div class="w50 cariera-item-first-desc">
                                <p>
                                    В наших клиниках мы стремимся создать все условия для комфортной работы: просторные и уютные кабинеты, новое оборудование, современные системы диагностики.
                                </p>
                            </div>
                        </div>
                    <?}else{?>
                        <div class="cariera-item-title">
                            <span><?=count($listClinic)?> <?=$clinic?> ПО ВСЕМУ ГОРОДУ: ВЫБЕРИТЕ УДОБНУЮ</span>
                        </div>
                        <div class="w50 w560">
                            <div class="inner cariera-item-first-inner">
                                <h4 class="bf">
                                    Больше не нужно тратить много времени на дорогу:
                                </h4>
                                <p>
                                    Сотрудникам достаточно выбрать ближайший из <?=count($listClinic)?> центров ИНТАН,расположенных в разных районах города. Необязательно ограничиваться одним: можно совмещать работу в нескольких центрах.
                                </p>
                            </div>
                            <div class="w50 hide640">
                                <img src="/images/cariera-4.png" alt="">
                            </div>
                            <div class="w50 cariera-item-first-desc">
                                <p>
                                    В наших клиниках мы стремимся создать все условия для комфортной работы: просторные и уютные кабинеты, новое оборудование, современные системы диагностики.
                                </p>
                            </div>
                        </div>
                    <?}?>
                    <?
                    global $arIblockAccord;
                    $arMapCenter = array(
                        's1' => '59.933286, 30.333057',
                        's4' => '45.06148367, 38.96222000'
                    );

                    $resElem = [];
                    $arrFilter = ['IBLOCK_ID'=>$arIblockAccord['listcities'], 'ACTIVE'=>'Y', 'CODE'=>$_REQUEST['selCity']];
                    $res = CIBlockElement::GetList(array('NAME'=>'ASC'), $arrFilter, false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_F_COORD', 'PROPERTY_F_METRO'));
                    if($ar_res = $res->fetch()){
                        $resElem = $ar_res;
                    }
                    ?>
                    <?
                    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*","DETAIL_PAGE_URL");
                    $arFilter = Array("IBLOCK_ID"=>37, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
                    while($ob = $res->GetNextElement()){ 
                        $arFields = $ob->GetFields();                          
                        $arProps = $ob->GetProperties();
                        $arResult['ITEMS'][] = array('NAME'=>$arFields['NAME'], "F_PHONE"=>$arProps['F_PHONE']['VALUE'], 'F_COORD'=>$arProps['F_COORD']['VALUE'], 'DETAIL_PAGE_URL'=>$arFields['DETAIL_PAGE_URL']);
                    }
                    ?>
                    <div class="w50 w670">
                        <div id="map">
                            <div id="ymaps-map-container" style="width: 100%; height: 561px; margin-top:0;"></div>
                            <script type="text/javascript" src="http://api-maps.yandex.ru/2.0/?coordorder=latlong&load=package.full&wizard=constructor&lang=ru-RU"></script>

                            <script type="text/javascript">
                                $(function(){

                                    $('#map').click(function(e){
                                        
                                        var $cont = $("#ymaps-map-container");

                                        if ($cont.attr("init") != "true") {
                                            ymaps.ready(init);
                                            $cont.attr("init", "true");
                                        } else {
                                            // e.preventDefault();
                                        }
                                    });

                                    $('#map').click();
                                

                                });

                                function init () {
                                    var map = new ymaps.Map("ymaps-map-container", {
                                        center: [<?=$resElem['PROPERTY_F_COORD_VALUE']?>],
                                        zoom: 10,
                                        type: "yandex#map"
                                    });
                                    map.controls
                                        .add("smallZoomControl")
                                        .add(new ymaps.control.TypeSelector(["yandex#map", "yandex#satellite", "yandex#hybrid", "yandex#publicMap"]));
                                    map.events.add('click', function (e) {
                                        map.balloon.close();
                                    });

                                    myCollection = new ymaps.GeoObjectCollection();

                                    <?$i = 1;
                                    foreach($arResult["ITEMS"] as $arItem){
                                        $strPhone = '';
                                        if(count($arItem['F_PHONE'] > 0)){
                                            foreach($arItem['F_PHONE'] as $onePhone){
                                                $strPhone.= '<a href="tel: '.$onePhone.'">'.$onePhone.'</a>';
                                            }
                                        }?>
                                    myPlcmrk<?=$i?> = new ymaps.Placemark([<?=$arItem['F_COORD']?>], {
                                        balloonContent: '<a id="bxid_723423" href="<?=$arItem["DETAIL_PAGE_URL"]?>" ><?=$arItem["NAME"]?></a> <div class="phones"><?=$strPhone?></div>'
                                    }, {
                                        iconImageHref: '/images/map_mark.png', // картинка иконки
                                        iconImageSize: [43, 36], // размеры картинки
                                        iconImageOffset: [-13, -32] // смещение картинки
                                    });
                                    myCollection.add(myPlcmrk<?=$i?>);
                                    <?$i++;
                                }?>

                                    // Добавление коллекции на карту
                                    map.geoObjects.add(myCollection);

                                    //удаление карты
                                    $('#w_by_subway, #w_as_list').click(function(){
                                        map.destroy();
                                    })
                                };
                            </script>
                        </div>
                        <!-- <img style="max-width:100%" src="/images/map.png" alt=""> -->
                    </div>
                </div>


                <div class="cariera-item">
                    <div class="cariera-item-title">
                        <?if ($_REQUEST['selCity'] == 'nvr' || $_REQUEST['selCity'] == 'krd') { ?>
                            <span>СОБСТВЕННЫЙ УЧЕБНЫЙ ЦЕНТР В САНКТ-ПЕТЕРБУРГЕ: СОВЕРШЕНСТВУЙТЕСЬ С НАМИ</span>
                        <?}else{?>
                            <span>СОБСТВЕННЫЙ УЧЕБНЫЙ ЦЕНТР: СОВЕРШЕНСТВУЙТЕСЬ С НАМИ</span>
                        <?}?>
                        
                    </div>
                    <div class="w65">
                        <div class="w50 cariera-item-second-pic hide640">
                            <img style="max-width:100%" src="/images/cariera-55.png" alt="">
                        </div>
                        <div class="w50 cariera-item-second-desc">
                            <div class="hidendeskt"><strong>ОБУЧЕНИЕ:</strong></div>
                            Профессия врача требует постоянного обучения. Для тех, кто хочет развиваться в профессии, мы открыли учебный центр ИНТАН. Здесь можно повысить квалификацию или пройти переподготовку с получением сертификата государственного образца. 
                        </div>
                    </div>
                    <div class="w35 cariera-item-second-pic2 hide640">
                        <img style="max-width:100%" src="/images/cariera-6.png" alt="">
                    </div>
                    <br>
                    <br>
                    <div class="w65 hide1024">
                        <img style="max-width:100%" src="/images/cariera-77.png" alt="">
                    </div>
                    <div class="w35">
                        <div class="inner cariera-item-second-inner">
                            <p class="bf">
                                В программе лекционные и практические курсы по различным направлениям стоматологии, мастер-классы, показательные операции. К проведению занятий в Учебном центре привлекаются не только внутренние преподаватели, но и ведущие российские и зарубежные эксперты.
                            </p>
                        </div>
                        <img style="max-width:100%" src="/images/cariera-88.png" alt="" class="hide1024">
                    </div>
                </div>

                <div class="cariera-item">
                    <div class="cariera-item-title">
                        <span>18 ЛЕТ РАЗВИТИЯ: СТАНЬТЕ ЧАСТЬЮ КОМАНДЫ</span>
                    </div>
                    <div class="w50">
                        <div class="inner cariera-item-tree-inner">
                            <h4 class="bf">
                                Мы уверены в нашей команде:
                            </h4>
                            <p>
                                Это отличные специалисты и чуткие, внимательные коллеги.
                            </p>
                            <h4>Работа в атмосфере взаимной поддержки и обмена опытом</h4>
                            <p>
                                помогает развиваться в своем направлении и получать удовлетворение от работы.
                            </p>
                            <a href="#" class="sender green-btn">
                                <span>Отправить резюме</span>
                            </a>
                        </div>
                    </div>
                    <div class="w50 cariera-item-tree-pic">
                        <img style="max-width:100%" src="/images/cariera-99.png" alt="">
                    </div>
                </div>

            </div>
        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>