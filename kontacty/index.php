<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Стоматология");

if(!$_GET['selCity']){
	//LocalRedirect("/404.php", "404 Not Found");
}
?>

	<section>

		<?$GLOBALS['arrFilter'] = getFilArrayByCity();
		$APPLICATION->IncludeComponent(
			"bitrix:news",
			"centers",
			array(
				"IBLOCK_TYPE" => "spb",
				"IBLOCK_ID" => "37",
				"NEWS_COUNT" => "20",
				"USE_SEARCH" => "N",
				"USE_RSS" => "N",
				"USE_RATING" => "N",
				"USE_CATEGORIES" => "N",
				"USE_FILTER" => "Y",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"CHECK_DATES" => "Y",
				"SEF_MODE" => "Y",
				"SEF_FOLDER" => "/kontacty/",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"SET_STATUS_404" => "Y",
				"SET_TITLE" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
				"ADD_SECTIONS_CHAIN" => "Y",
				"ADD_ELEMENT_CHAIN" => "N",
				"USE_PERMISSIONS" => "N",
				"DISPLAY_DATE" => "N",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "N",
				"USE_SHARE" => "N",
				"PREVIEW_TRUNCATE_LEN" => "",
				"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"LIST_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"LIST_PROPERTY_CODE" => array(
					0 => "F_SALE",
					1 => "F_COORD",
					2 => "F_NEW",
					3 => "F_METRO",
					4 => "F_PHONE",
					5 => "F_CLASS",
					6 => "",
				),
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"DISPLAY_NAME" => "Y",
				"META_KEYWORDS" => "-",
				"META_DESCRIPTION" => "-",
				"BROWSER_TITLE" => "-",
				"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"DETAIL_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"DETAIL_PROPERTY_CODE" => array(
					0 => "F_ADDR",
					1 => "F_TOUR",
					2 => "F_CHDOC",
					3 => "F_HOWTOGET",
					4 => "F_COORD",
					5 => "F_MODE",
					6 => "F_METRO",
					7 => "F_PHONE",
					8 => "F_CLASS",
					9 => "",
				),
				"DETAIL_DISPLAY_TOP_PAGER" => "N",
				"DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
				"DETAIL_PAGER_TITLE" => "Страница",
				"DETAIL_PAGER_TEMPLATE" => "",
				"DETAIL_PAGER_SHOW_ALL" => "N",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"FILTER_NAME" => "arrFilter",
				"FILTER_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"FILTER_PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"SEF_URL_TEMPLATES" => array(
					"news" => "",
					"section" => "",
					"detail" => "#ELEMENT_CODE#/",
				)
			),
			false
		);?>

	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>