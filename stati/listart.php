<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$arFilter = array(
    'ACTIVE'=>'Y',
    'IBLOCK_ID'=>$arIblockAccord['listarticles']
);

if(!empty($_POST['strSearch'])){
    $arFilter[] = array(
        'LOGIC' => 'OR',
        array('NAME' => '%'.$_POST['strSearch'].'%'),
        array('DETAIL_TEXT' => '%'.$_POST['strSearch'].'%')
    );
}

if(!empty($_POST['spec'])){
    $arFilter['PROPERTY_F_CATEGORY'] = (int)$_POST['spec'];
}

$arNavParams = array(
    'nPageSize' => 5
);

$arNavParams['iNumPage'] = $_POST['page'] ? $_POST['page'] : 1;

$listElement = array();
$res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter, false, $arNavParams, array('IBLOCK_ID', 'ID', '*', 'PROPERTY_bloger_position', 'PROPERTY_F_CENTRE'));
while($ar_res = $res->GetNext()){
    $listElement[] = $ar_res;
}

//подсчитаем общее кол-во элементов, удовлетворяющих запросу
$cntElem = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter, array(), $arNavParams, array('IBLOCK_ID', 'ID', '*', 'PROPERTY_bloger_position', 'PROPERTY_F_CENTRE'));

$strElem = '';
foreach ($listElement as $oneElem) {
    $strElem .= '<div class="def-item">';
    $strElem .=    '<a href="'.$oneElem['DETAIL_PAGE_URL'].'" class="def-item-item">'.$oneElem['NAME'].'</a>';
    $strElem .=    '<p><span>';
    $strElem .=        $oneElem['PREVIEW_TEXT'];
    $strElem .=    '</span></p></div>';
}

$res->nPageWindow = 3;
$NAV_STRING = $res->GetPageNavStringEx($navComponentObject, '', 'pagenavicust2', '');
if($NAV_STRING){
    $strElem .= $NAV_STRING;
}

// отзыв по услуге, если выбрана
$strRev = '';
if(!empty($_POST['spec'])){
    //забираем данные по услуге
    $arMedServ = array();
    $res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['listmedservices'], 'ID'=>$_POST['spec']), false, false, array('ID', 'DETAIL_PAGE_URL', 'PROPERTY_F_NPREPOSIT', 'PROPERTY_F_NACCUSAT'));
    if($ar_res = $res->GetNext()){
        $arMedServ = $ar_res;
    }

    //на случай, если не указаны значения
    $arMedServ['PAGE_URL'] = $arMedServ['DETAIL_PAGE_URL'] ? $arMedServ['DETAIL_PAGE_URL'].'tseny/' : '/stomatologiya-ceny/';
    $arMedServ['PROPERTY_F_NACCUSAT_VALUE'] = $arMedServ['PROPERTY_F_NACCUSAT_VALUE'] ? $arMedServ['PROPERTY_F_NACCUSAT_VALUE'] : 'наши услуги';
    $arMedServ['PROPERTY_F_NPREPOSIT_VALUE'] = $arMedServ['PROPERTY_F_NPREPOSIT_VALUE'] ? $arMedServ['PROPERTY_F_NPREPOSIT_VALUE'] : 'нашиx услугах';

    $res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv'], 'PROPERTY_F_BIND'=>$_POST['spec']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
    if($ar_res = $res->GetNext()){
        $strRev .= '<a href="/otzyv/?specs='.$arMedServ['ID'].'" class="title"><div class="h2"><i></i><span>Отзывы</span></div><div class="review-item"></a>';
        //$strRev .= '<div class="review-user-name" data-date="'.date('d.m.Y', strtotime($ar_res['DATE_CREATE'])).'">'.$ar_res['NAME'].'</div>';
        $strRev .= '<div class="review-user-name">'.$ar_res['NAME'].'</div>';
        $strRev .= '<div class="def-toggle"><p>'.$ar_res['DETAIL_TEXT'].'</p></div>';
    }
    if($arMedServ['DETAIL_PAGE_URL']){
        $strRev .= '<a href="'.$arMedServ['DETAIL_PAGE_URL'].'" class="right-link info-lech"><span>Информация о '.$arMedServ['PROPERTY_F_NPREPOSIT_VALUE'].'</span></a>';
        $strRev .= '<a href="'.$arMedServ['PAGE_URL'].'" class="right-link price-price"><span>Цены на '.$arMedServ['PROPERTY_F_NACCUSAT_VALUE'].'</span></a>';
    }
}else{
    $res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv'], 'PROPERTY_F_BIND'=>$_POST['spec']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
    if($ar_res = $res->GetNext()){
        $strRev .= '<a href="/otzyv/" class="title"><div class="h2"><i></i><span>Отзывы</span></div><div class="review-item"></a>';
        //$strRev .= '<div class="review-user-name" data-date="'.date('d.m.Y', strtotime($ar_res['DATE_CREATE'])).'">'.$ar_res['NAME'].'</div>';
        $strRev .= '<div class="review-user-name">'.$ar_res['NAME'].'</div>';
        $strRev .= '<div class="def-toggle"><p>'.$ar_res['DETAIL_TEXT'].'</p></div>';
    }
    $strRev .= '<a href="/stomatologiya-ceny/" class="right-link price-price"><span>Цены на наши услуги</span></a>';
}

echo json_encode(array('arElem'=>$strElem, 'count'=>$cntElem, 'review'=>$strRev));