<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Статьи");
CModule::IncludeModule("iblock");

$nPage = $_REQUEST['PAGEN_1'] ? (int)$_REQUEST['PAGEN_1'] : 1;
?>

    <script>
        var artiElem;
        $(function(){
            artiElem = new ArticleObj(<?=$nPage?>);
            artiElem.init();
        });
    </script>

    <section>

        <div class="wrap">
            <h1>Статьи</h1>
            <div class="w65 faq">

                <div class="filter-docs search">
                    <form action="<?=SITE_DIR?>stati/listart.php" class="reqlist" method="POST" onsubmit="return false;">
                        <input id="qsearch" type="text" placeholder="Введите часть слова или слово целиком">
                        <input type="submit" value="Найти"><select name="" id="specs">
                            <option value="">Все услуги</option>
                            <?$res = CIBlockElement::GetList(array('SORT'=>'ASC'), array('IBLOCK_ID'=>$arIblockAccord['listmedservices'], 'ACTIVE'=>'Y'));
                            while($ar_res = $res->GetNext()){?>
                                <option value="<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></option>
                            <?}?>
                        </select>

                    </form>
                </div>
                <div class="def-counter">
                    Количество статей: <span></span>
                </div>
                <div class="def-toggle n">
                </div>

            </div>

            <div class="w35">
                <div class="reviews">
                    <div class="review-item">
                    </div>
                </div>
            </div>

        </div>
    </section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>