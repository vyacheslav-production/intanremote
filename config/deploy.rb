# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'newintan'

set :repo_url, '.'
# set :repo_url, 'git@dev.legion.info/intan.git'

set :branch, 'master'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/tmp'

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

set :scm, :gitcopy

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
#set :linked_files, fetch(:linked_files, []).push('config/database.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('bitrix', 'upload', 'tour', 'cgi-bin')

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do

    desc 'Create public_html symlink'
    task :public_html do
        on roles(:all) do
            execute "rm -R -f #{deploy_to}/public_html"
            execute "ln -s #{current_path} #{deploy_to}/public_html"
        end
    end

end

namespace :bitrix do

    desc 'Download bitrixsetup and change current symlink'
    task :setup do
        on roles(:all) do
            execute "wget -P #{shared_path} http://www.1c-bitrix.ru/download/scripts/bitrixsetup.php"
            execute "rm -f #{current_path}"
            execute "ln -s #{shared_path} #{current_path}"
        end
    end

    desc 'Copy bitrix folder'
    task :copy do
        on roles(:all) do
            system "tar -czf bitrix.tar.gz bitrix"
            upload!('bitrix.tar.gz', "#{shared_path}", {:recursive => true, :via => :scp})
            system "rm -f bitrix.tar.gz"
            execute "tar xvzf #{shared_path}/bitrix.tar.gz -C #{shared_path}"
            execute "rm -f #{shared_path}/bitrix.tar.gz"
        end
    end

    desc 'Copy upload folder'
    task :upload do
        on roles(:all) do
            system "tar -czf upload.tar.gz upload"
            upload!('upload.tar.gz', "#{shared_path}", {:recursive => true, :via => :scp})
            system "rm -f upload.tar.gz"
            execute "tar xvzf #{shared_path}/upload.tar.gz -C #{shared_path}"
            execute "rm -f #{shared_path}/upload.tar.gz"
        end
    end

    desc 'Copy tour folder'
        task :tour do
            on roles(:all) do
                system "tar -czf tour.tar.gz tour"
                upload!('tour.tar.gz', "#{shared_path}", {:recursive => true, :via => :scp})
                system "rm -f tour.tar.gz"
                execute "tar xvzf #{shared_path}/tour.tar.gz -C #{shared_path}"
                execute "rm -f #{shared_path}/tour.tar.gz"
            end
        end

end
