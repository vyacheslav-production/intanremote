role :web, %w{user@dev.legion.info}

server 'dev.legion.info', user: 'user', roles: %w{web}

set :deploy_to, '/var/www/intan'

;