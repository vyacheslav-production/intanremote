role :web, %w{tflegi8s@quasar.beget.ru}

server 'quasar.beget.ru', user: 'tflegi8s', roles: %w{web}

set :deploy_to, '/home/t/tflegi8s/intan.ru'