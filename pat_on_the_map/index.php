<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<!DOCTYPE html>
<html>
  <head>
    <title>Адреса пациентов на карте</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    
    <!-- Сделаем броузеры ES5 friendly -->
    <!--script src="/es5-shim/es5-shim.js" type="text/javascript"></script-->
    <script src="//yandex.st/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU&coordorder=longlat" type="text/javascript"></script>
    <!-- Класс множественного геокодирования -->
    <style type="text/css">
        html, body, #map {
            width: 90%;
            height: 90%;
            margin: 0;
            padding: 0;
        }
        .undefined{
            position: relative;
            top: 20px;
        }
    </style>
</head>
    <body>
        <?
        if(CModule::IncludeModule("iblock")){
            //если не передан id клиники, то выводим список всех клиник
            if(!isset($_GET['idClinic']) || empty($_GET['idClinic'])){?>
                <p>
                    <ul>
                        <?$res = CIBlockElement::GetList(array(), array('IBLOCK_CODE'=>'clinics', 'ACTIVE'=>'Y'));
                        while($ar_res = $res->GetNext()){?>
                            <li><a href="?idClinic=<?=$ar_res['ID']?>"><?=$ar_res['NAME']?></a></li>
                        <?}?>
                    </ul>
                </p>
            <?}else{
                require_once('map.php');
            }
        }
        ?>
    </body>
</html>