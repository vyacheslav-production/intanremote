<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$api_key = 'AH_U_lMBAAAAociKXAIAlbbrpvfJ-PW0rm4isPxstCtjOaQAAAAAAAAAAACuv13yIEqzUm9QGYDWNlQ8kExfKg==';//yandex
set_time_limit(0);
if(CModule::IncludeModule("iblock")){
    if(isset($_GET['idClinic']) && !empty($_GET['idClinic'])){
        $res = CIBlockElement::GetList(array(), array('IBLOCK_CODE'=>'clinics', 'ID'=>(int)$_GET['idClinic']), false, false, array('ID', 'PROPERTY_coord', 'NAME'));
        //существует ли клиника
        if($ar_res = $res->GetNext()){
            $oneClinic = $ar_res;
            $res = CIBlockElement::GetList(array(), array('IBLOCK_CODE'=>'listpatmap', 'PROPERTY_clinic'=>(int)$_GET['idClinic']), false, false, array('ID', 'PROPERTY_addr', 'PROPERTY_coord'));
            //$res = CIBlockElement::GetList(array(), array('IBLOCK_CODE'=>'listpatmap', 'PROPERTY_clinic'=>(int)$_GET['idClinic'], '!PROPERTY_coord'=>false), false, false, array('ID', 'PROPERTY_addr', 'PROPERTY_coord'));
                        
            $info = array();
            $points = array();
            //$listPatAddr = array();
            $listUndefinedCoord = array();
            while($ar_res = $res->GetNext()){
                if($ar_res['PROPERTY_COORD_VALUE']){
                    //$listPatAddr[$ar_res['ID']] = $ar_res['PROPERTY_ADDR_VALUE'];
                    $info[] = $ar_res['PROPERTY_ADDR_VALUE'];
                    $points[] = '['.$ar_res['PROPERTY_COORD_VALUE'].']';
                }else{
                    $listUndefinedCoord[] = $ar_res;
                }  
            }
            $points = '[ ' . implode(', ', $points) . ' ]';
            ?>
            <script type="text/javascript">
                ymaps.ready(function () {
                    
                    info = <?=json_encode($info)?>
                    
                    var myMap = new ymaps.Map('map', {
                            center: [<?=$oneClinic['PROPERTY_COORD_VALUE']?>],
                            zoom: 11,
                            behaviors: ['default', 'scrollZoom']
                        }),
                        
                        clusterer = new ymaps.Clusterer({
                            preset: 'islands#invertedVioletClusterIcons',
                            groupByCoordinates: false,
                            clusterDisableClickZoom: true,
                            clusterHideIconOnBalloonOpen: false,
                            geoObjectHideIconOnBalloonOpen: false
                        }),
                        
                        getPointData = function (index) {
                            return {
                                balloonContentBody: info[index],
                                clusterCaption: info[index]
                            };
                        },
                        
                        getPointOptions = function () {
                            return {
                                preset: 'islands#violetIcon'
                            };
                        },
                        points = <?=$points?>,
                        geoObjects = [];
                
                    for(var i = 0, len = points.length; i < len; i++) {
                        geoObjects[i] = new ymaps.Placemark(points[i], getPointData(i), getPointOptions());
                    }

                    /**
                     * Можно менять опции кластеризатора после создания.
                     */
                
                    /**
                     * В кластеризатор можно добавить javascript-массив меток (не геоколлекцию) или одну метку.
                     * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Clusterer.xml#add
                     */
                    clusterer.add(geoObjects);
                    myMap.geoObjects.add(clusterer);
                
                    /**
                     * Спозиционируем карту так, чтобы на ней были видны все объекты.
                     */
                
                    /*
                    myMap.setBounds(clusterer.getBounds(), {
                        checkZoomRange: true
                    });*/
                    
                    //откроем балун с клиникой
                    var myPlacemark = new ymaps.Placemark([<?=$oneClinic['PROPERTY_COORD_VALUE']?>], {
                        balloonContentBody: 'Клиника <?=$oneClinic['NAME']?>'
                    }, {
                        preset: 'islands#dotCircleIcon'
                    });
                
                    myMap.geoObjects.add(myPlacemark);
                    myPlacemark.balloon.open();
                });
        
            </script>
             
            <div id="map"></div>
            <p><a href="/pat_on_the_map/">Вернуться к списку клиник</a></p>
            <?if($listUndefinedCoord){?>
                <div class="undefined">
                    <table border="1" cellpadding="5" style="border-collapse: collapse; border: 1px solid black;">
                        <caption>Список адресов без координат</caption>
                        <tr style="background-color: silver">
                            <th>ID записи</th>
                            <th>Адрес</th>
                        </tr>
                        <?foreach($listUndefinedCoord as $oneUndefined){?>
                            <tr>
                                <td><a href="/bitrix/admin/iblock_element_edit.php?WF=Y&ID=<?=$oneUndefined['ID']?>&type=pat_on_the_map&lang=ru&IBLOCK_ID=32"><?=$oneUndefined['ID']?></a></td>
                                <td><?=$oneUndefined['PROPERTY_ADDR_VALUE']?></td>
                            </tr>
                        <?}?>
                    </table>
                </div>
            <?}?>
        <?}else{?>
            <p>
                Клиника не найдена!
            </p>  
        <?}
    }
}