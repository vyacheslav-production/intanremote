<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");

if(!$_GET['selCity']){
    LocalRedirect("/404.php", "404 Not Found");
}
?>

    <section>
        <div class="wrap">

            <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_page_vac", Array(

            ),
                false
            );?>
            <div class="w65 text ib">
                <div class="text-item vacancy-item ">

                    <?
                    $arrFilter = ['IBLOCK_ID'=>$arIblockAccord['listcities'], 'ACTIVE'=>'Y', 'CODE'=>getCodeCity()];
                    $res = CIBlockElement::GetList(array('NAME'=>'ASC'), $arrFilter, false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_F_PHOTOHR', 'PROPERTY_F_NAMEHR', 'PROPERTY_F_DESCRHR'));
                    if($ar_res = $res->fetch()){?>
                        <?=$ar_res['PROPERTY_F_CONTACTHR_VALUE']['TEXT']?>
                        <div class="vacancy-toptext">
                            <div class="table">
                                <div class="row">
                                    <div class="cell vacancy-toptext-images">
                                        <div class="ib">
                                            <?$img = CFile::ResizeImageGet($ar_res["PROPERTY_F_PHOTOHR_VALUE"], array('width'=>180, 'height'=>180), BX_RESIZE_IMAGE_EXACT, true);?>
                                            <img src="/images/fotovak2.png" alt="" />
                                       </div>
                                    </div>
                                    <div class="cell vacancy-toptext-text">
                                        <?=$ar_res['PROPERTY_F_DESCRHR_VALUE']['TEXT']?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?}?>
                </div>
            </div>
            <?include_once($_SERVER['DOCUMENT_ROOT'].'/include/sidebar_right_vac.php');?>
            <?if ($_REQUEST['selCity'] == 'spb') {
                $id = 119337;
            }elseif ($_REQUEST['selCity'] == 'nvr') {
                $id = 119338;
            }elseif ($_REQUEST['selCity'] == 'krd') {
                $id = 119343;
            };
            GLOBAL $arrFilter;
            $arrFilter = array('PROPERTY_F_BINDCITY' => $id)?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "vakansii",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "spb",
                    "IBLOCK_ID" => "35",
                    "NEWS_COUNT" => "20",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "ACTIVE_FROM",
                    "SORT_ORDER2" => "DESC",
                    "FILTER_NAME" => "arrFilter",
                    "FIELD_CODE" => array("NAME"),
                    "PROPERTY_CODE" => array(
                        0 => "REQUIRE",
                        1 => "WORK_PLACE",
                        2 => "DUTIES",
                        3 => "CONDITIONS",
                        4 => "F_BINDCITY",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "Y",
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                ),
            false
            );?>
            
        </div>
    </section>
<script type="text/javascript">
    if ( $(window).width() < 931 ) {
        setTimeout(function(){
            $('.newrightpane').prependTo('.w65').eq(0).slideDown();
        }, 500);
    }
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
