<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интан - энциклопедия стоматологии");
$APPLICATION->SetTitle("Стоматологическая энциклопедия");
?><script type="text/javascript">
    $(function(){
        LETTER = "А";
        LANG = "RU";
        function writeCookie(){
            LETTER==""?LETTER="А":LETTER
            LANG==""?LANG="RU":LANG
            $.cookie('enc_letter', LETTER);
            $.cookie('enc_lang', LANG);
        }
        function getLET(){
            return $.cookie('enc_letter');
        }
        function getLANG(){
            return $.cookie('enc_lang');
        }
        //        load state
        ;(function(){
            var _let = "<?= $_REQUEST["enc"] ?>"
            LETTER = getLET()
            if(_let!=""){LETTER = _let}/*else{LETTER = "А"}*/
            $("#enc_"+LETTER).addClass("active")
            switch(getLANG()){
                case "RU":
                    m_show_ru()
                    LANG = "RU"
                    writeCookie()
                    break
                case "ENG":
                    m_show_eng()
                    LANG = "ENG"
                    writeCookie()
                    break
            }
        })()
        $(".e_lang_change div p").click(function(){
            var t = $(this).attr("id")
            switch(t){
                case "sh_e_rus_nav":
                    m_show_ru()
                    LANG = "RU"
                    LETTER = $("#ru_let p a.active").text()
                    writeCookie()
                    break
                case "sh_e_eng_nav":
                    m_show_eng()
                    LANG = "ENG"
                    LETTER = $("#en_let p a.active").text()
                    writeCookie()
                    break
            }
        })
        //        macro functions
        function m_show_ru(){
            $("#sh_e_rus_nav").parent().hide()
            $("#sh_e_eng_nav").parent().show()
            $("#en_let").hide()
            $("#ru_let").show()
        }
        function m_show_eng(){
            $("#sh_e_eng_nav").parent().hide()
            $("#sh_e_rus_nav").parent().show()
            $("#ru_let").hide()
            $("#en_let").show()
        }
    })
</script>
<aside class="left_sidebar">
    <?
    $APPLICATION->IncludeComponent("bitrix:menu", "tree1", Array(
        "ROOT_MENU_TYPE" => "top", // Тип меню для первого уровня
        "MENU_CACHE_TYPE" => "N", // Тип кеширования
        "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
        "MAX_LEVEL" => "2", // Уровень вложенности меню
        "CHILD_MENU_TYPE" => "podmenu", // Тип меню для остальных уровней
        "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N", // Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
            ), false
    );
    ?>
<?
    $APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/inc_vert_list.php",
	"EDIT_TEMPLATE" => ""
	),
	false
);
    ?>
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:main.include", "", Array(
        "AREA_FILE_SHOW" => "sect",
        "AREA_FILE_SUFFIX" => "inc_banners",
        "AREA_FILE_RECURSIVE" => "Y",
        "EDIT_TEMPLATE" => ""
            ), false
    );
    ?>
</aside>
<div class="content_part">
    <?
    $APPLICATION->IncludeComponent("bitrix:breadcrumb", "template1", Array(
            ), false
    );
    ?>
    <section>
        <div class="e_lang_change">
            <div class="e_lang_rus">
                <p id="sh_e_rus_nav" title="русский алфавит"></p>
            </div>
            <div class="e_lang_eng">
                <P id="sh_e_eng_nav" title="английский алфавит"></P>
            </div>
        </div>
        <h1 class="main_h">Энциклопедия</h1>
        <?
        CModule::IncludeModule("iblock");
        $ruLet = Array("А", "Б", "В", "Г", "Д", "Е", "Ж", "3", "И", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Э", "Ю", "Я");
        $enLet = Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
        ?>
        <nav id="ru_let" class="enc_letters">
            <p class="first">
                <?
                for ($i = 0, $j = 13; $i < $j; $i++) {
                    if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $ruLet[$i] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                        ?>
                        <a href="./?enc=<?= $ruLet[$i] ?>" class="enc_letter" id="enc_<?= $ruLet[$i] ?>"><?= $ruLet[$i] ?></a>  
                    <? } else { ?>
                        <span class="enc_letter enc_empty"><?= $ruLet[$i] ?></span>  
                        <?
                    }
                }
                if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $ruLet[13] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                    ?>
                    <a href="./?enc=<?= $ruLet[13] ?>" class="enc_letter last" id="enc_<?= $ruLet[13] ?>"><?= $ruLet[13] ?></a>  
                <? } else { ?>
                    <span class="enc_letter enc_empty last"><?= $ruLet[13] ?></span>  
                <? } ?>
            </p>
            <p class="last">
                <?
                for ($i = 14, $j = 27; $i < $j; $i++) {
                    if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $ruLet[$i] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                        ?>
                        <a href="./?enc=<?= $ruLet[$i] ?>" class="enc_letter" id="enc_<?= $ruLet[$i] ?>"><?= $ruLet[$i] ?></a>  
                    <? } else { ?>
                        <span class="enc_letter enc_empty"><?= $ruLet[$i] ?></span>  
                        <?
                    }
                }
                if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $ruLet[27] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                    ?>
                    <a href="./?enc=<?= $ruLet[27] ?>" class="enc_letter last" id="enc_<?= $ruLet[27] ?>"><?= $ruLet[27] ?></a>  
                <? } else { ?>
                    <span class="enc_letter enc_empty last"><?= $ruLet[27] ?></span>  
                <? } ?>
            </p>
        </nav>
        <nav id="en_let" class="enc_letters" style="display:none;">
            <p class="first">
                <?
                for ($i = 0, $j = 13; $i < $j; $i++) {
                    if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $enLet[$i] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                        ?>
                        <a href="./?enc=<?= $enLet[$i] ?>" class="enc_letter" id="enc_<?= $enLet[$i] ?>"><?= $enLet[$i] ?></a>  
                    <? } else { ?>
                        <span class="enc_letter enc_empty"><?= $enLet[$i] ?></span>  
                        <?
                    }
                }
                if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $enLet[13] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                    ?>
                    <a href="./?enc=<?= $enLet[13] ?>" class="enc_letter last" id="enc_<?= $enLet[13] ?>"><?= $enLet[13] ?></a>  
                <? } else { ?>
                    <span class="enc_letter enc_empty last"><?= $enLet[13] ?></span>  
                <? } ?>
            </p>
            <p class="last">
                <?
                for ($i = 14, $j = 25; $i < $j; $i++) {
                    if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $enLet[$i] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                        ?>
                        <a href="./?enc=<?= $enLet[$i] ?>" class="enc_letter" id="enc_<?= $enLet[$i] ?>"><?= $enLet[$i] ?></a>  
                    <? } else { ?>
                        <span class="enc_letter enc_empty"><?= $enLet[$i] ?></span>  
                        <?
                    }
                }
                if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $enLet[25] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                    ?>
                    <a href="./?enc=<?= $enLet[25] ?>" class="enc_letter last" id="enc_<?= $enLet[25] ?>"><?= $enLet[25] ?></a>  
                <? } else { ?>
                    <span class="enc_letter enc_empty last"><?= $enLet[25] ?></span>  
                <? } ?>
            </p>
        </nav>
        <div class="enc_wrap">
            <?
            $arrFilter["NAME"] = "А%";
            if($_COOKIE["enc_letter"]){
                $arrFilter["NAME"] = $_COOKIE["enc_letter"] . "%";
            }
            if ($_REQUEST["enc"]) {
                $arrFilter["NAME"] = $_REQUEST["enc"] . "%";
            }
            ?>
            <?
            $APPLICATION->IncludeComponent("bitrix:catalog.section", "entsiklopediya_list_template", array(
	"IBLOCK_TYPE" => "News",
	"IBLOCK_ID" => "22",
	"SECTION_ID" => $_REQUEST["SECTION_ID"],
	"SECTION_CODE" => "",
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => "name",
	"ELEMENT_SORT_ORDER" => "asc",
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "N",
	"PAGE_ELEMENT_COUNT" => "50",
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "N",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
            ?>
        </div>
    </section>
</div>
<div class="clear"></div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>