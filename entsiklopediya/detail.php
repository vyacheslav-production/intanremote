<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Энциклопедия");
?>
<script type="text/javascript">
    $(function(){
        LETTER = "А";
        LANG = "RU";
        function writeCookie(){
            LETTER==""?LETTER="А":LETTER
            LANG==""?LANG="RU":LANG
            $.cookie('enc_letter', LETTER);
            $.cookie('enc_lang', LANG);
        }
        function getLET(){
            return $.cookie('enc_letter');
        }
        function getLANG(){
            return $.cookie('enc_lang');
        }
        //        load state
        ;(function(){
            var _let = "<?= $_REQUEST["enc"] ?>"
            LETTER = getLET()
            if(_let!=""){LETTER = _let}
            $("#enc_"+LETTER).addClass("active")
            switch(getLANG()){
                case "RU":
                    m_show_ru()
                    LANG = "RU"
                    writeCookie()
                    break
                case "ENG":
                    m_show_eng()
                    LANG = "ENG"
                    writeCookie()
                    break
            }
        })()
        $(".e_lang_change div p").click(function(){
            var t = $(this).attr("id")
            switch(t){
                case "sh_e_rus_nav":
                    m_show_ru()
                    LANG = "RU"
                    LETTER = $("#ru_let p a.active").text()
                    writeCookie()
                    break
                case "sh_e_eng_nav":
                    m_show_eng()
                    LANG = "ENG"
                    LETTER = $("#en_let p a.active").text()
                    writeCookie()
                    break
            }
        })
        //        macro functions
        function m_show_ru(){
            $("#sh_e_rus_nav").parent().hide()
            $("#sh_e_eng_nav").parent().show()
            $("#en_let").hide()
            $("#ru_let").show()
        }
        function m_show_eng(){
            $("#sh_e_eng_nav").parent().hide()
            $("#sh_e_rus_nav").parent().show()
            $("#ru_let").hide()
            $("#en_let").show()
        }
    })
</script>
<aside class="left_sidebar">
    <?
    $APPLICATION->IncludeComponent("bitrix:menu", "tree1", Array(
        "ROOT_MENU_TYPE" => "top", // Тип меню для первого уровня
        "MENU_CACHE_TYPE" => "N", // Тип кеширования
        "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
        "MAX_LEVEL" => "2", // Уровень вложенности меню
        "CHILD_MENU_TYPE" => "podmenu", // Тип меню для остальных уровней
        "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N", // Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
            ), false
    );
    ?>
    <?
    $APPLICATION->IncludeComponent(
            "bitrix:main.include", "", Array(
        "AREA_FILE_SHOW" => "sect",
        "AREA_FILE_SUFFIX" => "inc_banners",
        "AREA_FILE_RECURSIVE" => "Y",
        "EDIT_TEMPLATE" => ""
            ), false
    );
    ?>
</aside>
<div class="content_part">
    <?
    $APPLICATION->IncludeComponent("bitrix:breadcrumb", "template1", Array(
            ), false
    );
    ?>
    <section>
        <div class="e_lang_change">
            <div class="e_lang_rus">
                <p id="sh_e_rus_nav" title="русский алфавит"></p>
            </div>
            <div class="e_lang_eng">
                <P id="sh_e_eng_nav" title="английский алфавит"></P>
            </div>
        </div>
        <h1 class="main_h">Энциклопедия</h1>
        <?
        CModule::IncludeModule("iblock");
        $ruLet = Array("А", "Б", "В", "Г", "Д", "Е", "Ж", "3", "И", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Э", "Ю", "Я");
        $enLet = Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
        ?>
        <nav id="ru_let" class="enc_letters">
            <p class="first">
                <?
                for ($i = 0, $j = 13; $i < $j; $i++) {
                    if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $ruLet[$i] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                        ?>
                        <a href="./?enc=<?= $ruLet[$i] ?>" class="enc_letter" id="enc_<?= $ruLet[$i] ?>"><?= $ruLet[$i] ?></a>  
                    <? } else { ?>
                        <span class="enc_letter enc_empty"><?= $ruLet[$i] ?></span>  
                        <?
                    }
                }
                if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $ruLet[13] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                    ?>
                    <a href="./?enc=<?= $ruLet[13] ?>" class="enc_letter last" id="enc_<?= $ruLet[13] ?>"><?= $ruLet[13] ?></a>  
                <? } else { ?>
                    <span class="enc_letter enc_empty last"><?= $ruLet[13] ?></span>  
                <? } ?>
            </p>
            <p class="last">
                <?
                for ($i = 14, $j = 27; $i < $j; $i++) {
                    if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $ruLet[$i] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                        ?>
                        <a href="./?enc=<?= $ruLet[$i] ?>" class="enc_letter" id="enc_<?= $ruLet[$i] ?>"><?= $ruLet[$i] ?></a>  
                    <? } else { ?>
                        <span class="enc_letter enc_empty"><?= $ruLet[$i] ?></span>  
                        <?
                    }
                }
                if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $ruLet[27] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                    ?>
                    <a href="./?enc=<?= $ruLet[27] ?>" class="enc_letter last" id="enc_<?= $ruLet[27] ?>"><?= $ruLet[27] ?></a>  
                <? } else { ?>
                    <span class="enc_letter enc_empty last"><?= $ruLet[27] ?></span>  
                <? } ?>
            </p>
        </nav>
        <nav id="en_let" class="enc_letters" style="display:none;">
            <p class="first">
                <?
                for ($i = 0, $j = 13; $i < $j; $i++) {
                    if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $enLet[$i] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                        ?>
                        <a href="./?enc=<?= $enLet[$i] ?>" class="enc_letter" id="enc_<?= $enLet[$i] ?>"><?= $enLet[$i] ?></a>  
                    <? } else { ?>
                        <span class="enc_letter enc_empty"><?= $enLet[$i] ?></span>  
                        <?
                    }
                }
                if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $enLet[13] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                    ?>
                    <a href="./?enc=<?= $enLet[13] ?>" class="enc_letter last" id="enc_<?= $enLet[13] ?>"><?= $enLet[13] ?></a>  
                <? } else { ?>
                    <span class="enc_letter enc_empty last"><?= $enLet[13] ?></span>  
                <? } ?>
            </p>
            <p class="last">
                <?
                for ($i = 14, $j = 25; $i < $j; $i++) {
                    if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $enLet[$i] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                        ?>
                        <a href="./?enc=<?= $enLet[$i] ?>" class="enc_letter" id="enc_<?= $enLet[$i] ?>"><?= $enLet[$i] ?></a>  
                    <? } else { ?>
                        <span class="enc_letter enc_empty"><?= $enLet[$i] ?></span>  
                        <?
                    }
                }
                if (CIBlockElement::GetList(Array("name" => "ASC"), Array("IBLOCK_ID" => "22", "NAME" => $enLet[25] . "%"), false, Array("nPageSize" => 1, "bShowAll" => false), Array("ID"))->Fetch()) {
                    ?>
                    <a href="./?enc=<?= $enLet[25] ?>" class="enc_letter last" id="enc_<?= $enLet[25] ?>"><?= $enLet[25] ?></a>  
                <? } else { ?>
                    <span class="enc_letter enc_empty last"><?= $enLet[25] ?></span>  
                <? } ?>
            </p>
        </nav>
        <div class="enc_wrap">
            <div class="back_to_list">
                <span class="iecor tl"></span>
                <span class="iecor tr"></span>
                <span class="iecor bl"></span>
                <span class="iecor br"></span>
                <span class="left_arrow"></span>
                <a href="/entsiklopediya/">Вернуться к списку</a>
            </div>
            <?
            $APPLICATION->IncludeComponent("bitrix:catalog.element", "entsiklopediya_detail_template", Array(
	"IBLOCK_TYPE" => "News",	// Тип инфо-блока
	"IBLOCK_ID" => "22",	// Инфо-блок
	"ELEMENT_ID" => $_REQUEST["ID"],	// ID элемента
	"ELEMENT_CODE" => "",	// Код элемента
	"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
	"SECTION_CODE" => "",	// Код раздела
	"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
	"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
	"BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
	"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
	"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
	"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
	"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
	"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
	"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
	"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
	"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
	"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
	"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
	"PROPERTY_CODE" => "",	// Свойства
	"PRICE_CODE" => "",	// Тип цены
	"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
	"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
	"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
	"PRICE_VAT_SHOW_VALUE" => "N",	// Отображать значение НДС
	"PRODUCT_PROPERTIES" => "",	// Характеристики товара
	"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
	"LINK_IBLOCK_TYPE" => "",	// Тип инфо-блока, элементы которого связаны с текущим элементом
	"LINK_IBLOCK_ID" => "",	// ID инфо-блока, элементы которого связаны с текущим элементом
	"LINK_PROPERTY_SID" => "",	// Свойство в котором хранится связь
	"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",	// URL на страницу где будет показан список связанных элементов
	"CACHE_TYPE" => "A",	// Тип кеширования
	"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
	"CACHE_NOTES" => "",
	"CACHE_GROUPS" => "Y",	// Учитывать права доступа
	),
	false
);
            ?>
        </div>
    </section>
</div>
<div class="clear"></div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>