<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Энциклопедия");
?>

<aside class="left_sidebar">
    <?
    $APPLICATION->IncludeComponent("bitrix:menu", "tree1", Array(
        "ROOT_MENU_TYPE" => "top", // Тип меню для первого уровня
        "MENU_CACHE_TYPE" => "N", // Тип кеширования
        "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
        "MAX_LEVEL" => "2", // Уровень вложенности меню
        "CHILD_MENU_TYPE" => "podmenu", // Тип меню для остальных уровней
        "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N", // Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
            ), false
    );
    ?>
    <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "inc_banners",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
false
);?>
</aside>
<div class="content_part">
    <?
    $APPLICATION->IncludeComponent("bitrix:breadcrumb", "template1", Array(
            ), false
    );
    ?>
    <section>
        <h1 class="main_h">Энциклопедия</h1>
        <nav class="enc_letters">
            <p class="first">
                <a href="" class="active">А</a>
                <a href="">Б</a>
                <a href="">В</a>
                <a href="">Г</a>
                <a href="">Д</a>
                <a href="">Е</a>
                <a href="">Ж</a>
                <a href="">З</a>
                <a href="">И</a>
                <a href="">К</a>
                <a href="">Л</a>
                <a href="">М</a>
                <a href="">Н</a>
                <a href="" class="last">О</a>
            </p>
            <p class="last">
                <a href="">П</a>
                <a href="">Р</a>
                <a href="">С</a>
                <a href="">Т</a>
                <a href="">У</a>
                <a href="">Ф</a>
                <a href="">Х</a>
                <a href="">Ц</a>
                <a href="">Ч</a>
                <a href="">Ш</a>
                <a href="">Щ</a>
                <a href="" style="margin-right:11px;">Э</a>
                <a href="">Ю</a>
                <a href="" class="last">Я</a>
            </p>
        </nav>
        <div class="enc_wrap">
            <div class="back_to_list">
                <span class="iecor tl"></span>
                <span class="iecor tr"></span>
                <span class="iecor bl"></span>
                <span class="iecor br"></span>
                <span class="left_arrow"></span>
                <a href="/entsiklopediya/">Вернуться к списку</a>
            </div>
            <article class="rly_art">
                <h1 class="secondary_h">Аномалии зубные</h1>
                <div class="rly_art_body">
                    <div class="rly_art_img"><img src="http://intan.ru/upload/iblock/848/848b7cdf9e45e3f8dd30cfdf7e3e29b2.jpeg" alt="" /></div>
                    <div class="rly_art_text">
                        <p>Всевозможные изменения зубов (размер, форма, число, положение и так далее).</p>
                        <p><b>Аномалии величины:</b></p>
                        <ul>
                            <li>Гигантские зубы – зубы с коронками большого размера.</li>
                            <li>Мелкие зубы – зубы с коронками маленького размера.</li>
                        </ul>
                        <p><b>Аномалии формы:</b></p> 
                        <ul>
                            <li>Шиповидные зубы – зубы с коронками, конусовидной формы.</li>
                            <li>Уродливые зубы – зубы с неправильной формой коронки.</li>
                        </ul>
                        <p><b>Аномалии положения:</b></p>
                        <ul>
                            <li>Диастема – щель, находящаяся между двумя центральными резцами</li>
                            <li>Тремы – все промежутки между зубами, кроме центральных резцов.</li>
                            <li>Тортопозиция зубов – разворот зубов по оси.</li>
                            <li>Скученность – расположение, при котором зубы налегают друг на друга, стоят повёрнутыми по оси.</li>
                            <li>Дистопозиция боковых зубов – дистальное направление, наклон зубов.</li>
                            <li>Мезиопозиция зубов – симметричное или ассиметричное смещение зубов по зубной дуге.</li>
                            <li>Инфра- и супрапозиция зубов – смещение зубов по отношению к вертикальной поверхности.</li>
                            <li>Транспозиция зубов – положение зубов, при котором происходит их взаимозамещение.</li>
                            <li>Экзопозиция передних и боковых зубов – отклонение или смещение верхних или нижних зубов.</li>
                            <li>Пропозиция – экзопозиция верхних зубов</li>
                            <li>Эндопозиция передних и боковых зубов – оральное положение зубов с наклоном внутрь от зубного ряда.</li>
                            <li>Ретропозиция – эндопозиция резцов.</li>
                        </ul>
                        <p><b>Аномалии числа зубов:</b></p>
                        <ul>
                            <li>Адентия – врождённое отсутствие зубов. Выделяют два вида адентии: частичную и полную.</li>
                            <li>Гипердентия – наличие лишних зубов.</li>
                        </ul>
                        <p><b>Аномалии сроков прорезывания зубов:</b></p>
                        <ul>
                            <li>Досрочное прорезывание – преждевременное прорезывание.</li>
                            <li>Позднее прорезывание – запоздалое прорезывание.</li>
                            <li>Ретенция зубов – отсутствие прорезывания сформировавшегося зуба.</li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
            </article>
        </div>
    </section>
</div>
<div class="clear"></div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>