<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интан - энциклопедия стоматологии");
$APPLICATION->SetTitle("Стоматологическая энциклопедия");
CModule::IncludeModule("iblock");
?>

<?
/*
$sQuery = '';
$sQuery .= 'SELECT DISTINCT ';
$sQuery .= 'UPPER(LEFT(LTRIM(NAME), 30)) AS LETTER ';
$sQuery .= 'FROM b_iblock_element AS BE ';
$sQuery .= 'WHERE BE.IBLOCK_ID = 22 AND BE.ACTIVE = "Y" AND BE.WF_STATUS_ID = 1 AND BE.WF_PARENT_ELEMENT_ID IS NULL';
$sQuery .= 'ORDER BY LETTER ASC ';
$rsItems = $GLOBALS['DB']->Query($sQuery, false, __LINE__);
while($arItem = $rsItems->GetNext(false, false)) {
    $arResult['ITEMS'][$arItem['LETTER']] = $arItem;
}*/
?>

<?
if(!$_GET['let']){
    LocalRedirect("/entsiklopediya/?let=А", "404 Not Found");
}

$ruLet = Array("А", "Б", "В", "Г", "Д", "Е", "Ж", "3", "И", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Э", "Ю", "Я");
$enLet = Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
$arGeneralLet = array_merge($ruLet, $enLet);

foreach($arGeneralLet as &$oneLet){
    $oneLet = $oneLet.'%';
}

$arFilter = array(
    'ACTIVE'=>'Y',
    'IBLOCK_ID'=>$arIblockAccord['listencyc']
);

$arFilter['NAME'] = $arGeneralLet;

$arFirstLetFromResult = array();
$res = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter, false, false, array('ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_TEXT'));
while($ar_res = $res->GetNext()){
    $arFirstLetFromResult[substr($ar_res['NAME'], 0, 1)][] = $ar_res;
}

$arLetFilter = array();
?>

    <section>

        <div class="wrap">
            <h1>Энциклопедия</h1>
            <div class="w65 enz">
                <div class="booker">
                    <div>
                        <?foreach($ruLet as $oneLet){?>
                            <a class="<?=array_key_exists($oneLet, $arFirstLetFromResult) ? '' : 'none'?> <?=$oneLet == $_GET['let'] ? 'active' : ''?>" href="?let=<?=$oneLet?>"><span><?=$oneLet?></span></a>
                        <?}?>
                    </div>
                    <div>
                        <?foreach($enLet as $oneLet){?>
                            <a class="<?=array_key_exists($oneLet, $arFirstLetFromResult) ? '' : 'none'?>" href="?let=<?=$oneLet?>"><span><?=$oneLet?></span></a>
                        <?}?>
                    </div>
                </div>

                <div class="def-toggle">
                    <?if(!empty($arFirstLetFromResult[$_GET['let']])){
                        foreach($arFirstLetFromResult[$_GET['let']] as $oneElem){?>
                            <div class="def-item">
                                <div class="def-item-item">
                                    <?=$oneElem['NAME']?>
                                    <?if($oneElem['PREVIEW_PICTURE']):?>
                                        <span class="view"><i></i><span><img src="<?=CFile::GetPath($oneElem['PREVIEW_PICTURE'])?>" alt=""></span></span>
                                    <?endif;?>
                                </div>
                                <p>
								<span>
									<?=$oneElem['DETAIL_TEXT']?>
								</span>

                                </p>
                            </div>
                        <?}
                    }else{?>
                        В энциклопедии отсутствуют записи
                    <?}?>
                </div>

            </div>

            <div class="w35">
                <div class="reviews">
                    <?$res = CIBlockElement::GetList(array('RAND'=>'ASC'), array('ACTIVE'=>'Y', 'IBLOCK_ID'=>$arIblockAccord['otzyiv']), false, false, array('IBLOCK_ID', 'ID', '*', 'DETAIL_TEXT'));
                    if($ar_res = $res->GetNext()){?>
                        <div class="h2"><i></i><span>Отзывы о нас</span></div>
                        <div class="review-item">
                            <div class="review-user-name" data-date="<?//=date('d.m.Y', strtotime($ar_res['DATE_CREATE']))?>"><?=$ar_res['NAME']?></div>
                            <div class="def-toggle">
                                <p>
                                    <?=$ar_res['DETAIL_TEXT']?>
                                </p>
                            </div>
                        </div>
                    <?}?>
                    <a href="/stomatologiya-ceny/" class="right-link price-price"><span>Цены на наши услуги</span></a>
                </div>
            </div>

        </div>
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>